<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>a New Site Coming Soon - powered by BigDomain.my</title>
	<base href="{{base_url}}" />
			<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="Welcome to the Temporary New Site , automatically built using Bigdomain.my Big Website Builder" />
	<meta name="keywords" content="Free Website Builder,Bigdomain,Cheap Domain" />
	<!-- Facebook Open Graph -->
	<meta name="og:title" content="a New Site Coming Soon - powered by BigDomain.my" />
	<meta name="og:description" content="Welcome to the Temporary New Site , automatically built using Bigdomain.my Big Website Builder" />
	<meta name="og:image" content="" />
	<meta name="og:type" content="article" />
	<meta name="og:url" content="{{curr_url}}" />
	<!-- Facebook Open Graph end -->
		
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/main.js?v=20180222160538" type="text/javascript"></script>

	<link href="css/font-awesome/font-awesome.min.css?v=4.7.0" rel="stylesheet" type="text/css" />
	<link href="css/site.css?v=20180215144631" rel="stylesheet" type="text/css" />
	<link href="css/common.css?ts=1519719212" rel="stylesheet" type="text/css" />
	<link href="css/1.css?ts=1519719212" rel="stylesheet" type="text/css" />
	{{ga_code}}<meta name="google-site-verification" content="" />
	<script type="text/javascript">var currLang = '';</script>	
	<!--[if lt IE 9]>
	<script src="js/html5shiv.min.js"></script>
	<![endif]-->
</head>


<body><div class="root"><div id="wb_bgs_cont"><div id="wb_page_1_bg"></div></div><div id="wb_header_bg"></div><div id="wb_sbg_placeholder"></div><div class="vbox wb_container wb_header_fixed" id="wb_header">
	
<div id="wb_cs_row_1_wrp" class="wb_cont_inner_rel"><div id="wb_cs_row_1" class="wb-cs-row"><div id="wb_cs_col_2" class="wb-cs-col"><div id="wb_element_instance1" class="wb_element wb_element_shape wb-cs-elem"><div class="wb_shp"></div></div></div><div id="wb_cs_col_3" class="wb-cs-col"><div id="wb_element_instance2" class="wb_element wb-cs-elem" style=" line-height: normal;"><h4 class="wb-stl-pagetitle">a BigDomain Hosted Site</h4>
</div></div><div id="wb_cs_col_4" class="wb-cs-col wb-cs-right"><div id="wb_element_instance0" class="wb_element wb-menu wb-menu-mobile wb-cs-elem"><a class="btn btn-default btn-collapser"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><ul class="hmenu menu-landing"><li class="active"><a href="#home" target="_self">Home</a></li></ul><div class="clearfix"></div></div></div><div class="wb-cs-clear"></div></div><div class="wb-cs-clear"></div></div></div>
<div class="vbox wb_container" id="wb_main">
	
<div class="wb_page" id="page_1"><a name="home" class="wb_page_anchor wb_anchor" id="page_1_anchor"></a><div id="wb_cs_row_8" class="wb-cs-row"><div id="wb_cs_col_9" class="wb-cs-col"><div id="wb_element_instance8" class="wb_element wb-elm-orient-horizontal wb-cs-elem"><div class="wb-elm-line"></div></div></div><div class="wb-cs-clear"></div></div><div id="wb_cs_row_10" class="wb-cs-row"><div id="wb_cs_col_11" class="wb-cs-col"><div id="wb_element_instance7" class="wb_element wb_element_picture wb-cs-elem"><img alt="gallery/comp" src="gallery_gen/2ee1158f6d0d756e66396814aa6828ec_780x760.png"></div></div><div id="wb_cs_col_12" class="wb-cs-col"><div id="wb_cs_row_13" class="wb-cs-row"><div id="wb_cs_col_14" class="wb-cs-col"><div id="wb_element_instance6" class="wb_element wb-cs-elem" style=" line-height: normal;"><h1 class="wb-stl-heading1"><strong><span class="wb_tr_ok">New Website Coming Soon</span></strong></h1>
</div></div><div class="wb-cs-clear"></div></div><div id="wb_cs_row_15" class="wb-cs-row"><div id="wb_cs_col_16" class="wb-cs-col"><div id="wb_element_instance5" class="wb_element wb-cs-elem" style=" line-height: normal;"><p class="wb-stl-normal"><strong>Start Building your website with our 200 FREE Design Template here right inside your Cpanel. </strong></p>

<ol><li class="wb-stl-normal">Login to your Cpanel</li>
	<li class="wb-stl-normal">Locate BigWeb Website Builder</li>
	<li class="wb-stl-normal">Select a Lovely Template  like this one</li>
	<li class="wb-stl-normal">Start Building you website and you only need 5 mins </li>
</ol><p class="wb-stl-normal">Review our <a data-name="http://www.bigdomain.my/bigwebdemo" href="http://www.bigdomain.my/bigwebdemo">200+ Websie Template Here</a></p>
</div></div><div class="wb-cs-clear"></div></div></div><div class="wb-cs-clear"></div></div></div><div id="wb_element_instance9" class="wb_element" style="width: 100%;">
			<?php
				global $show_comments;
				if (isset($show_comments) && $show_comments) {
					renderComments(1);
			?>
			<script type="text/javascript">
				$(function() {
					var block = $("#wb_element_instance9");
					var comments = block.children(".wb_comments").eq(0);
					var contentBlock = $("#wb_main");
					contentBlock.height(contentBlock.height() + comments.height());
				});
			</script>
			<?php
				} else {
			?>
			<script type="text/javascript">
				$(function() {
					$("#wb_element_instance9").hide();
				});
			</script>
			<?php
				}
			?>
			</div></div>
<div class="vbox wb_container" id="wb_footer" style="height: 129px;">
	
<div id="wb_cs_row_5_wrp" class="wb_cont_inner_rel"><div id="wb_cs_row_5" class="wb-cs-row"><div id="wb_cs_col_6" class="wb-cs-col"><div id="wb_element_instance4" class="wb_element wb-cs-elem" style=" line-height: normal;"><p class="wb-stl-footer">© Powered By <a data-name="https://bigdomain.my" href="https://bigdomain.my">BigDomain.my</a>    Malaysia Coolest Hosting Company</p>
</div></div><div id="wb_cs_col_7" class="wb-cs-col wb-cs-right"><div id="wb_element_instance3" class="wb_element wb_element_picture wb-cs-elem"><a href="https://bigdomain.my" target="1"><img alt="gallery/2f91c22b9f6f5f382b7aff1e577e5a89.lock" src="gallery_gen/07637f46ea8aa75cd4974f1c3eff253c_224x78.png"></a></div></div><div class="wb-cs-clear"></div></div><div class="wb-cs-clear"></div></div><div id="wb_element_instance10" class="wb_element" style="text-align: center; width: 100%;"><div class="wb_footer"></div><script type="text/javascript">
			$(function() {
				var footer = $(".wb_footer");
				var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
				if (!html) {
					footer.parent().remove();
					footer = $("#wb_footer, #wb_footer .wb_cont_inner");
					footer.css({height: ""});
				}
			});
			</script></div></div><div class="wb_sbg"></div></div>{{hr_out}}</body>
</html>
