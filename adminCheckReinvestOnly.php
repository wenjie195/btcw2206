<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{ 
     if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
          $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
     {
          header('Location:index.php');
          exit();
     }
     else
     {
          $uid = $_SESSION['uid'];
          $conn = connDB();
          $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
     }
}

$investmentRows = array();
$userDetails = array();

if($_SERVER['REQUEST_METHOD'] == 'POST') {
     if(isset($_POST['field_1'])) {
         $dateInvested = rewrite($_POST['field_1']);

         $thRows = getTransactionHistory($conn," WHERE date_created >= ? AND btc_type_id = 10 ",array("date_created"),array($dateInvested),"s");

         if($thRows){
            foreach ($thRows as $th){
                $thisUid = $th->getUid();
                $thisUserData = null;
                if(!array_key_exists($thisUid, $userDetails) || is_null($userDetails[$thisUid])){
                    $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($thisUid),"s");
                    if($userRows){
                        $thisUserData = $userRows[0];
                    }
                }else{
                    $thisUserData = $userDetails[$thisUid];
                }

                if($thisUserData){
                    $tempInvestInner = array();
                    $tempInvestInner['th'] = $th;
                    $tempInvestInner['user'] = $thisUserData;
                    array_push($investmentRows,$tempInvestInner);
                }
            }
         }
     }
}

?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <title>Check Reinvest Only</title>
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();

  ?>
  <div id="firefly" class="firefly-class min-height">  
     <div class="width100 same-padding more-separate-margin-top edit-div">
          <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a> 
          <div class="clear"></div>
         <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Check Reinvest Data</b></h4>
            <form class="register-form"  method="POST" >
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Reinvest Start Date until today (example: 2019-05-20)</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb" required name="field_1" id="field_1" value="<?php if(isset($_POST['field_1'])){echo $_POST['field_1'];} ?>"></h4></td>
                  </tr>
                    <tr>
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">More than or equal to</b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="more_field" id="more_field" value="<?php if(isset($_POST['more_field'])){echo $_POST['more_field'];} ?>"></h4></td>
                    </tr>

                    <tr>
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Less than or equal to</b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="less_field" id="less_field" value="<?php if(isset($_POST['less_field'])){echo $_POST['less_field'];} ?>"></h4></td>
                    </tr>

                  <!-- <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">邮箱</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" placeholder="电邮" ></h4></td>
                  </tr> -->

                </table>
                <div class="clear"></div>
               <button class="register-button2 clean orange-hover inputb-button" name="insertValue" id="insertValue" >Check Reinvestment Data</button>

          </form>
          </div> 
          <div class="clear"></div>
          <div class="row">
               <div class="col-md-1"></div>
               <div class="col-md-10">
                         <h4 class="btcw-h4 separate-title white-text mt-5"><b class="weight-700">Total Rows : <?php if($_SERVER['REQUEST_METHOD'] == 'POST')
{ echo count($investmentRows);}?></b></h4>
                    <h4 class="btcw-h4 separate-title white-text"><b class="weight-700">Reinvestment Data：</b> </h4>
                    <table cellspacing="0" cellpadding="0" class="dark-table recommend-table2 white-text-table mt-5">

                <?php
                if($_SERVER['REQUEST_METHOD'] == 'POST')
                {
                    if($investmentRows && count($investmentRows) > 0) {
                         echo '
                             <tr>
                                 <th class="table2-1">No.</th>
                                 <th class="table2-2">UID</th>
                                 <th class="table2-3">用户名</th>
                                 <th class="table2-4">Email</th>
                                 <th class="table2-5">Amount</th>
                                 <th class="table2-6">Plan</th>
                                 <th class="table2-7">Date</th>
                             </tr>
                         ';

                         $currentNo = 0;
                         foreach ($investmentRows as $row) {
                             $currentNo++;

                             $user = $row['user'];
                             $th = $row['th'];
                             $plan = "null";
                             if($th->getPlanId() === 1){
                                 $plan = "A";
                             }else if($th->getPlanId() === 2){
                                 $plan = "B";
                             }
                             $amount = removeUselessZero($th->getReturnedBtcAmountOut());

                             echo '
                                         <tr class="tr2">
                                             <td class="table2-1">'.$currentNo.'.</td>
                                             <td class="table2-2">'.$user->getUid().'</td>
                                             <td class="table2-3">'.$user->getUsername().'</td>
                                             <td class="table2-4">'.$user->getEmail().'</td>';
                             if((isset($_POST['more_field']) && strlen($_POST['more_field']) > 0) || isset($_POST['less_field']) && strlen($_POST['less_field']) > 0 ){
                                 if(strlen($_POST['more_field']) > 0 && strlen($_POST['less_field']) > 0){
                                     if($amount >= $_POST['more_field'] && $amount <= $_POST['less_field']){
                                         echo '<td style="background-color: yellow; color: black;">'.$amount.' (HERE)</td>';
                                     }else{
                                         echo '<td>'.$amount.'</td>';
                                     }
                                 }
                                 else if(strlen($_POST['more_field']) > 0){
                                     if($amount >= $_POST['more_field']){
                                         echo '<td style="background-color: yellow; color: black;">'.$amount.' (HERE)</td>';
                                     }else{
                                         echo '<td>'.$amount.'</td>';
                                     }
                                 }else if(strlen($_POST['less_field']) > 0){
                                     if($amount <= $_POST['less_field']){
                                         echo '<td style="background-color: yellow; color: black;">'.$amount.' (HERE)</td>';
                                     }else{
                                         echo '<td>'.$amount.'</td>';
                                     }
                                 }
                             }else{
                                 echo   '<td>'.$amount.'</td>';
                             }

                             echo       '<td>'.$plan.'</td>';
                             echo       '<td>'.$th->getDateCreated().'</td>';
                             echo '</tr>';
                         }
 
                         echo "</table>";
                     }else{
                         echo "<p class='white-text'>No Record.</p>";
                     }
 
                     $conn->close();
                }
                ?>
            
            </table>
               </div>
               <div class="col-md-1"></div>
          </div>
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <?php require_once 'mainFooter.php';?>
  </body>
</html>