<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
<meta name="author" content="BTCW">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://btcworg.com/img/banner.jpg" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137243110-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137243110-1');
</script>

<link rel="icon" href="img/sherry/favicon.png"  type="image/x-icon"   />
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700,900" rel="stylesheet">

<!-- Main Style -->
<link rel="stylesheet" type="text/css" href="css/main.css?version=1.0.17"/>
<link rel="stylesheet" type="text/css" href="css/mainQuery.css?version=1.0.17"/>
<link rel="stylesheet" type="text/css" href="css/sherry.css?version=1.0.39" />

<!--newly added jquery and confirm-->
<link rel="stylesheet" type="text/css" href="css/jquery-confirm.css?version=1.0.17"/>
