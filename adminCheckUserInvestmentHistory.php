

<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{ 
     if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
          $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
     {
          header('Location:index.php');
          exit();
     }
     else
     {
          $uid = $_SESSION['uid'];
          $conn = connDB();
          $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
        //   
        }
}

$totalTransactionHistoryRows = array();
$applicationHistoryRows = array();
$userDetail = null;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     if(isset($_POST['field_1']))
     {
          $emailORUsername = $_POST['field_1'];
          $error = FALSE;

         $userDetailsRow = getUser($conn,
                         " WHERE email = ? OR username = ? OR uid = ? ",
                         array("email","username","uid"),
                         array($emailORUsername,$emailORUsername,$emailORUsername),
                         "sss");

          if($userDetailsRow){
              $userDetail = $userDetailsRow[0];
              $totalTransactionHistoryRows = getAllInvestment($conn,$userDetail->getUid());
              $applicationHistoryRows = getAllApplicationHistory($conn,$userDetail->getUid());
          }
     }
}

$conn->close();

function getAllInvestment($conn,$uid){
    $thRows = getTransactionHistory($conn," WHERE uid = ? AND btc_type_id = 2 ",array("uid"),array($uid),"s");

    if($thRows){
        return $thRows;
    }else{
        return array();
    }
}

function getAllApplicationHistory($conn,$uid){
    $ahRows = getApplicationHistory($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

    if($ahRows){
        return $ahRows;
    }else{
        return array();
    }
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <title>ZCXC</title>
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();

  ?>
  <div id="firefly" class="firefly-class min-height">  
     <div class="width100 same-padding more-separate-margin-top edit-div">
          <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a>
          <div class="clear"></div>
         <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Check total investments</b></h4>
            <form class="register-form"  method="POST" >
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Email/Username/User ID</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_1" id="field_1" value="<?php if(isset($_POST['field_1'])){ echo $_POST['field_1']; } ?>"></h4></td>
                  </tr>

                  <!-- <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">邮箱</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" placeholder="电邮" ></h4></td>
                  </tr> -->

                </table>
                <div class="clear"></div>
               <button class="register-button2 clean orange-hover inputb-button" name="search_form" id="search_form" >Search</button>

          </form>
          </div>

<!--            transaction history section-->
          <div class="clear"></div>
          <div class="row mt-5">
               <div class="col-md-1"></div>
               <div class="col-md-10">

                        <?php

                        if($userDetail){
                            echo '<h4 class="white-text">UID: '.$userDetail->getUid().'</h4>';
                            echo '<h4 class="white-text">Username: '.$userDetail->getUsername().'</h4>';
                            echo '<h4 class="white-text">Email: '.$userDetail->getEmail().'</h4>';
                            echo '<h4 class="white-text">Phone Number: '.$userDetail->getPhoneNo().'</h4>';
                            echo "</br>";
                        }else{
                            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                                echo '<h1 class="white-text">No such user found!</h1>';
                            }
                        }

                        if(count($totalTransactionHistoryRows) > 0)
                        {
                            echo '
                            <h2 class="white-text">Investment History</h2>
                            
                            <table class="table table-dark">
                                <tr>
                                    <td>No</td>
                                    <td>Amount (In BTC)</td>
                                    <td>Date Invested</td>
                                    <td>Plan</td>
                                    <td>Is Withdraw</td>
                                </tr>
                            ';

                            for($index = 0; $index < count($totalTransactionHistoryRows); $index++){
                                $th = $totalTransactionHistoryRows[$index];

                                $plan = "???";
                                if($th->getPlanId() === 1){
                                    $plan = "A";
                                }else if($th->getPlanId() === 2){
                                    $plan = "B";
                                }

                                $isWithdraw = "no";
                                if($th->getWithdrawStatus() === 1 || $th->getWithdrawStatus() === 2){
                                    $isWithdraw = "YES";
                                }

                                echo '
                                    <tr>
                                        <td>'.($index + 1).'</td>
                                        <td>'.$th->getCapitalBtcAmountIn().'</td>
                                        <td>'.$th->getDateCreated().'</td>
                                        <td>'.$plan.'</td>
                                        <td>'.$isWithdraw.'</td>
                                    </tr>
                                ';
                            }

                            echo '</table>';
                        }else{
                            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                                echo '<h1 class="white-text">No investment history!</h1>';
                            }
                        }
                        ?>
               </div>
               <div class="col-md-1"></div>
          </div>

<!--            application history section-->
          <div class="clear"></div>
          <div class="row mt-5">
              <div class="col-md-1"></div>
              <div class="col-md-10">

                  <?php
                  if(count($applicationHistoryRows) > 0)
                  {
                      echo '
                                <h2 class="white-text">Application History</h2>
                                
                                <table class="table table-dark">
                                    <tr>
                                        <td>No</td>
                                        <td>Date Invested</td>
                                        <td>Date Approved</td>
                                        <td>Plan</td>
                                        <td>Receiving Address</td>
                                        <td>Address ID</td>
                                        <td>Has Transaction?</td>
                                    </tr>
                                ';

                      for($i = 0; $i < count($applicationHistoryRows); $i++){
                          $ah = $applicationHistoryRows[$i];

                          $dateApproved = $ah->getDateUpdated();
                          if($dateApproved === $ah->getDateCreated()){
                              $dateApproved = "-";
                          }

                          $plan = "???";
                          if($ah->getPlanId() === 1){
                              $plan = "A";
                          }else if($ah->getPlanId() === 2){
                              $plan = "B";
                          }

                          $hasTransaction = "no";
                          if($ah->getCoinbaseAccountId() && strlen($ah->getCoinbaseAccountId()) > 0){
                              $hasTransaction = "YES";
                          }

                          echo '
                                        <tr>
                                            <td>'.($i + 1).'</td>
                                            <td>'.$ah->getDateCreated().'</td>
                                            <td>'.$dateApproved.'</td>
                                            <td>'.$plan.'</td>
                                            <td>'.$ah->getCoinbaseReceiveAddress().'</td>
                                            <td>'.$ah->getCoinbaseAddressId().'</td>
                                            <td>'.$hasTransaction.'</td>
                                        </tr>
                                    ';
                      }

                      echo '</table>';
                  }else{
                      if($_SERVER['REQUEST_METHOD'] == 'POST'){
                          echo '<h1 class="white-text">No application history!</h1>';
                      }
                  }
                  ?>
              </div>
              <div class="col-md-1"></div>
          </div>
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <?php require_once 'mainFooter.php';?>
  </body>
</html>