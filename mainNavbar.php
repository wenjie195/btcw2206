<div class="header-div width100 same-padding">
    <div class="logo-div">
        <?php
        if(!empty($_SESSION['uid']))
        {
        ?>
            <a href="bitcoin.php" class="a-hover"><img src="img/sherry/logo-white.png" class="logo-img opacity-hover" alt="bitcoin" title="bitcoin"></a>
        <?php
        }
        else
        {
        ?>
            <a href="bitcoin.php" class="a-hover"><img src="img/sherry/logo-white.png" class="logo-img opacity-hover" alt="bitcoin" title="bitcoin"></a>
        <?php
        }
        ?>
        
    </div>
    <div class="menu-right-div">
        <?php
        if(empty($_SESSION['uid']))
        {
        ?>
        <!--<a class="menu-a login-a a-hover  white-text-a yellow-hover" href="download-btcw-app.php">下载</a>-->
        <?php
            if(isset($_SESSION['lang']) && $_SESSION['lang'] === "en"){
                echo '<a class="menu-a login-a a-hover  white-text-a yellow-hover" onclick="changeLang(\'ch\');">转换成中文</a>';
            }else{
                echo '<a class="menu-a login-a a-hover  white-text-a yellow-hover" onclick="changeLang(\'en\');">Switch to English</a>';
            }
        ?>
        <a class="menu-a login-a a-hover  white-text-a yellow-hover" href="indexLogin.php"><?= _footer_login ?></a>
        <a class="menu-a register-a a-hover  white-text-a yellow-hover" href="indexRegister.php"><?= _footer_register ?></a>
        <?php
        }
        else
        {
        ?>
        <!--<a class="menu-a login-a a-hover  white-text-a yellow-hover menu-w" href="download-btcw-app.php">下载</a>-->
        <a class="menu-a login-a a-hover  white-text-a yellow-hover menu-w" href="profile.php"><?= _footer_profile ?></a>
        <!-- Fizo Here Unhide the below specialProfile link and only specialProfile can access it-->
        <?php 
        if(isset($uid) && ($uid == "CUSTOM-ID-01dc43a42713ebad5780176ba6d62e8407950605"
                || $uid == "VXtbpgh5sdSoEXGqhKK54UOZDd92"
                || $uid == "CUSTOM-ID-691768c4318f014372931f1a8225b6386315a442"
                || $uid == "CUSTOM-ID-2d777f2fca4f62a0d4d98f0b98efa6e829878335"))
        {
         ?>
         <a class="menu-a login-a a-hover  white-text-a yellow-hover menu-w" href="specialProfile.php"><?= _footer_refer_bonus ?></a>
         <?php
        }
        ?>
        <a class="menu-a login-a a-hover  white-text-a yellow-hover menu-w" href="market.php"><?= _footer_market ?></a>
        <!---<a class="menu-a login-a a-hover  white-text-a yellow-hover menu-w" href="specialProfile.php">推荐收益</a>-->
        <a class="menu-a register-a a-hover  white-text-a yellow-hover menu-w" href="recommend.php"><?= _footer_refer ?></a>
       
        <a class="menu-a register-a a-hover  white-text-a yellow-hover menu-w" href="notice.php"><?= _footer_notice ?></a>                
        <a class="menu-a login-a a-hover  white-text-a yellow-hover menu-w display-none" href="#"><?= _footer_contact ?></a>
        <?php
        if(isset($_SESSION['lang']) && $_SESSION['lang'] === "en"){
            echo '<a class="menu-a register-a a-hover  white-text-a yellow-hover menu-w" onclick="changeLang(\'ch\');">转换成中文</a>';
        }else{
            echo '<a class="menu-a register-a a-hover  white-text-a yellow-hover menu-w" onclick="changeLang(\'en\');">Switch to English</a>';
        }
        ?>
        <a class="menu-a register-a a-hover  white-text-a yellow-hover menu-w" href="indexLogOut.php"><?= _footer_logout ?></a>                 
        <span class="pointer menu-icon m-menu open-menu">&#9776;</span>          
                                

        <?php
        }
        ?>
       </div> 
    </div>
<div class="clear"></div> 