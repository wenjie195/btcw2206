<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        
        <meta property="og:url" content="https://btcworg.com/adminTransferHistory.php" />
        <meta property="og:title" content="BTCW - Transfer Bitcoin  History" />
        <meta name="description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
        
        <title>BTCW - Transfer Bitcoin History</title>
        <link rel="canonical" href="https://btcworg.com/adminTransferHistory.php" />
  </head>
  <body>
  		<?php require 'adminNavbar.php';?>

  		<div class="width100 same-padding padding-top-bottom">
            <div class="width100 edit-spacing">
                <div class="float-left left-title">
                    <h4 class="btcw-h4 weight-700 record-h4">比特币转移申请记录 <a class="orange2-text plan-a" href="adminTransferRequest.php"> （申请列表）</a></h4>
                </div>
                <div class="right-filter float-right">
                    <select class="inputa clean2 filter-select"  name="">
                                    <option selected>最新审批申请开始</option>
                                    <option>最旧审批申请开始</option>
                                    <option>最大转移总额开始</option>  
                                    <option>最小转移总额开始</option>                                                                                         
                   </select>
                </div>
            </div>		
         <div class="clear"></div>
         <div class="scroll-table-div">
             <table class="table100 long-table">
                <thead class="orange-thead">
                    <tr class="orange-tr">
                        <th class="white-text auto-width">序</th>
                        <th class="white-text">审批日期</th>
                        <th class="white-text">管理员</th>
                        <th class="white-text">申请日期</th>
                        <th class="white-text">用户名</th>
                        <th class="white-text">转移总额</th>
                        <th class="white-text">备注</th>
                        <th class="white-text auto-width">文件</th>                    
                        <th class="white-text auto-width">决策</th>
                        <th class="white-text">拒绝原因</th>                     
                    </tr>
                </thead> 
                <tr class="transparent-tr">
                    <td>1.</td>
                    <td>11/2/2019</td>
                    <td>Admin 1</td>
                    <td>11/2/2019</td>
                    <td><a href="profile.php" target="_blank" class="orange1-text">小明</a></td>                                
                    <td>500</td>
                    <td>我想加入这平台</td>
                    <td><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                    <td><img src="img/sherry/approve.png" class="opacity-hover action-btn-img"></td>
                    <td>-</td>                
                </tr>   
                <tr class="transparent-tr">
                    <td>2.</td>
                    <td>11/2/2019</td>
                    <td>Admin 2</td>
                    <td>11/2/2019</td>
                    <td><a href="profile.php" target="_blank" class="orange1-text">小芳</a></td>                                
                    <td>100</td>
                    <td>我想加入这平台</td>
                    <td><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                    <td><img src="img/sherry/approve.png" class="opacity-hover action-btn-img"></td>
                    <td>-</td>                
                </tr> 
                <tr class="transparent-tr">
                    <td>3.</td>
                    <td>11/2/2019</td>
                    <td>Admin 2</td>
                    <td>11/2/2019</td>
                    <td><a href="profile.php" target="_blank" class="orange1-text">小李</a></td>                                
                    <td>300</td>
                    <td>我想加入这平台</td>
                    <td><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                    <td><img src="img/sherry/reject.png" class="opacity-hover action-btn-img"></td>
                    <td>资料有误</td>                
                </tr>            
                <tr class="transparent-tr">
                    <td>4.</td>
                    <td>11/2/2019</td>
                    <td>Admin 2</td>
                    <td>11/2/2019</td>
                    <td><a href="profile.php" target="_blank" class="orange1-text">小紫</a></td>                                
                    <td>400</td>
                    <td>我想加入这平台</td>
                    <td><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                    <td><img src="img/sherry/approve.png" class="opacity-hover action-btn-img"></td>
                    <td>-</td>                
                </tr> 
                <tr class="transparent-tr">
                    <td>5.</td>
                    <td>11/2/2019</td>
                    <td>Admin 2</td>
                    <td>11/2/2019</td>
                    <td><a href="profile.php" target="_blank" class="orange1-text">小芯</a></td>                                
                    <td>500</td>
                    <td>我想加入这平台</td>
                    <td><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                    <td><img src="img/sherry/reject.png" class="opacity-hover action-btn-img"></td>
                    <td>资料有误</td>                
                </tr>           
                     
            </table>    
          </div>                                                         
        </div> 
        <div class="width100 element-div extra-padding-bottom profile-divider">
                <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>        

        <!--- Modal --->
        <div id="myModal12" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width">
            <span class="close12 ow-close2 close-style">&times;</span>
            
            <div class="width100 small-padding">
            	<h4 class="btcw-h4 form-h4"><b class="weight-700">上载资料</b></h4>
            	<img src="img/sherry/document2.png" class="width100" alt="uploaded-document" title="uploaded-document">
            </div>
          </div>
        </div>
	

         
 		<?php require 'adminFooter.php';?>
          <script>



			// Get the modal
			var modal12 = document.getElementById('myModal12');
			
			// Get the button that opens the modal
			var btn12 = document.getElementsByClassName("document-btn")[0];
			
			// Get the button that opens the modal
			var btn12a = document.getElementsByClassName("document-btn")[1];			
			
			// Get the button that opens the modal
			var btn12b = document.getElementsByClassName("document-btn")[2];	
			
			
			// Get the button that opens the modal
			var btn12c = document.getElementsByClassName("document-btn")[3];			

			
			// Get the button that opens the modal
			var btn12d = document.getElementsByClassName("document-btn")[4];	
			

						
			// Get the <span> element that closes the modal
			var span12 = document.getElementsByClassName("close12")[0];

		









			
			// When the user clicks the button, open the modal
			btn12.onclick = function() {
			  modal12.style.display = "block";
			};
			
			// When the user clicks the button, open the modal
			btn12a.onclick = function() {
			  modal12.style.display = "block";
			};
			
			// When the user clicks the button, open the modal
			btn12b.onclick = function() {
			  modal12.style.display = "block";
			};			
			
			// When the user clicks the button, open the modal
			btn12c.onclick = function() {
			  modal12.style.display = "block";
			};
			
			// When the user clicks the button, open the modal
			btn12d.onclick = function() {
			  modal12.style.display = "block";
			};	



						
	
			// When the user clicks on <span> (x), close the modal
			span12.onclick = function() {
			  modal12.style.display = "none";
			};


			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			
			  if (event.target == modal12) {
				modal12.style.display = "none";
			  }

			}
		</script>

  </body>
</html>