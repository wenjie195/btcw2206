<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    
    header('Location:index.php');
    exit();
}
else
{ 
    $uid = $_SESSION['uid'];

    if($uid == "CUSTOM-ID-01dc43a42713ebad5780176ba6d62e8407950605"
    || $uid == "VXtbpgh5sdSoEXGqhKK54UOZDd92"
    || $uid == "CUSTOM-ID-691768c4318f014372931f1a8225b6386315a442" 
    || $uid == "CUSTOM-ID-2d777f2fca4f62a0d4d98f0b98efa6e829878335")
    {
     
    }
    else
    {
      header('Location:index.php');
    }
}

require_once 'generalFunction.php';
require_once 'utilities/calculationFunction.php';
require_once 'utilities/databaseFunction.php';

$conn = connDB();

$errorMsg = null;
$successMsg = null;

$link_url = $_SERVER['REQUEST_URI'];
if(strpos($link_url, 'msgType') !== false) 
{
   $msgTypeArr = explode("msgType=",$link_url);

   $msgType = $msgTypeArr[1];

   if ($msgType == 1 && $_SESSION['spValid'] == 0) 
   {
       $errorMsg = _profile_error_too_low;
       $_SESSION['spValid'] = 1;
   }
   else if($msgType == 2 && $_SESSION['spValid'] == 0)
   {
       $errorMsg = _profile_error_overload;
       $_SESSION['spValid'] = 1;
   }
   else if($msgType == 3 && $_SESSION['spValid'] == 0)
   {
       $errorMsg = _profile_error_try_again;
       $_SESSION['spValid'] = 1;
   }
   else if ($msgType == 4 && $_SESSION['spValid'] == 0)
   {
     $successMsg = _profile_in_progress;
     $_SESSION['spValid'] = 1;
   }
}
else
{
  $_SESSION['spValid'] = 0;
}

// if(isset($errorMsg) || isset($successMsg)){
//             promptAlertMsg($errorMsg,$successMsg);
// }
    

$userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid),"s");

   //get Comission Things
   $planACommissionTotal = getPlanComission($uid,1);
   $planBCommissionTotal = getPlanComission($uid,2);

   $groupSalesPlanA = getTotalAmount(1,$uid);
   $groupSalesPlanB = getTotalAmount(2,$uid);
   
   $newComissionAdditionA = 0;
   $newComissionAdditionB = 0;





//******************************************EXAMPLE START******************************************

//echo $userRows[0]->getDateCreated();
//echo $userRows[0]->getUsername();

//$newTableId = insertDynamicData($conn,"temp_invest_table",array("uid","amount"),array("whahaha id",123.2222),"sd");
//if($newTableId){
//    echo "newId: ".$newTableId;
//}else{
//    echo "error";
//}

////update based on id
//if(updateDynamicData($conn,"temp_invest_table"," WHERE id = ? ",array("uid","amount"),array("uid changed",555.22,2),"sdi")){
//    echo "updated";
//}else{
//    echo "update error";
//}

////update all
//if(updateDynamicData($conn,"temp_invest_table",null,array("uid","amount"),array("uid changed",555.22),"sd")){
//    echo "updated";
//}else{
//    echo "update error";
//}

////update with or
//if(updateDynamicData($conn,"temp_invest_table"," WHERE uid = ? OR uid = ? OR amount = ?",array("uid","amount"),array("yay",888.8888888,"333","555",1.0),"sdssd")){
//    echo "updated";
//}else{
//    echo "update error";
//}

//******************************************EXAMPLE END********************************************

////todo INVEST BTC EXAMPLE THIS 1 IMPORTANT, DONT DELETE START
//$errorMsg = null;
//$successMsg = null;
//$amount = 0.025;
//$planId = 1;
//$errorMsg = investCapitalToPlan($conn,$amount,$planId,null);
//if(!isset($errorMsg)){
//    $amountInvestedString = removeUselessZero($amount) . " BTC!";
//    $successMsg = _profile_donate_success."$amountInvestedString";
//}
////todo INVEST BTC EXAMPLE THIS 1 IMPORTANT, DONT DELETE END

    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if(isset($_POST['opt1_withdrawal_submit']))
        {
            $amountToWithdraw = $_POST['opt1_withdrawal_amount'];
            $BTCaddress = $_POST['opt1_withdrawal_BTCaddress'];
            $comission = $_POST['currentTotalMaxComission1'];


            $_SESSION['spValid'] = 0;

            if($amountToWithdraw > 0 && $comission > 0)
            {
              if($amountToWithdraw <= $comission)
              {
                $amountAfter = $comission - $amountToWithdraw;

                //need to add the bitcoin address and banks info
                $confirmedWithdrawal = insertDynamicData($conn,"temp_specialacc",
                  array("uid","planAComission","planBComission",
                  "totalSalesPlanA","totalSalesPlanB","amountBeforeWithdrawal",
                  "comissionWithdrawalAmount","amountAfterWithdrawal","btcAddress"),
                  array($uid,$planACommissionTotal,$planBCommissionTotal,
                  $groupSalesPlanA,$groupSalesPlanB,$comission,
                  $amountToWithdraw,$amountAfter,$BTCaddress),
                  "sddddddds");

                  if(!$confirmedWithdrawal)
                  {
                    
                    header('Location:specialProfile.php?msgType=3');
                      // echo "error cannot insert";
                  }
                  else
                  {
                    // $errorMsg = "asd";
                    header('Location:specialProfile.php?msgType=4');
                    // echo "successfully created";
                  }
              }
              else
              {
                header('Location:specialProfile.php?msgType=2');
                // echo "cannot be less than or equal to max amount";
              }
            }
            else
            {
              header('Location:specialProfile.php?msgType=1');
              // echo "cannot be zero";
            }
        }
        else if(isset($_POST['opt2_withdrawal_submit']))
        {
            
          $amountToWithdraw = $_POST['opt2_withdrawal_amount'];

          $fullName = $_POST['opt2_withdrawal_fullName'];
          $country = $_POST['opt2_withdrawal_country'];
          $bankName = $_POST['opt2_withdrawal_bankName'];
          $bankAcc = $_POST['opt2_withdrawal_bankAccountNo'];

          $comission = $_POST['currentTotalMaxComission1'];

          $_SESSION['spValid'] = 0;

          if($amountToWithdraw > 0 && $comission > 0)
          {
            if($amountToWithdraw <= $comission)
            {
              $amountAfter = $comission - $amountToWithdraw;

              //need to add the bitcoin address and banks info
              $confirmedWithdrawal = insertDynamicData($conn,"temp_specialacc",
                array("uid","planAComission","planBComission",
                "totalSalesPlanA","totalSalesPlanB","amountBeforeWithdrawal",
                "comissionWithdrawalAmount","amountAfterWithdrawal","fullname",
                "country","bankName","bankAcc"),
                array($uid,$planACommissionTotal,$planBCommissionTotal,
                $groupSalesPlanA,$groupSalesPlanB,$comission,
                $amountToWithdraw,$amountAfter,$fullName,
                $country,$bankName,$bankAcc),
                "sdddddddssss");

                if(!$confirmedWithdrawal)
                {
                  header('Location:specialProfile.php?msgType=3');
                    // echo "error cannot insert";
                }
                else
                {
                  header('Location:specialProfile.php?msgType=4');
                  // echo "successfully created";
                }
            }
            else
            {
              header('Location:specialProfile.php?msgType=2');
              // echo "cannot be less than or equal to max amount";
            }
          }
          else
          {
            header('Location:specialProfile.php?msgType=1');
            // echo "cannot be zero";
          }
        }
    }


$sql = "SELECT id,planAComission,planBComission,totalSalesPlanA,totalSalesPlanB,amountBeforeWithdrawal,comissionWithdrawalAmount,amountAfterWithdrawal,date_created FROM temp_specialacc WHERE uid = ? ORDER BY date_created DESC LIMIT 1";
if ($stmt = $conn->prepare($sql)) 
{
    $stmt->bind_param('s',$uid);

    /* execute query */
    $stmt->execute();

    /* Store the result (to get properties) */
    $stmt->store_result();

    /* Get the number of rows */
    $num_of_rows_specACC = $stmt->num_rows;

    /* Bind the result to variables */
    $stmt->bind_result($idDB,$planAComissionDB,$planBComissionDB,$totalSalesPlanADB,$totalSalesPlanBDB,$amountBeforeWithdrawalDB,$comissionWithdrawalAmountDB,$amountAfterWithdrawalDB,$date_createdDB);


    echo $amountAfterWithdrawalDB;
    while($stmt->fetch())
    {
      
    }

    /* free results */
    $stmt->free_result();

    /* close statement */
    $stmt->close();

    // if ($num_of_rows > 0) 
    // {
    //     return $totalBTCTheyInvest;
    // } 
    // else
    // {
    //     return null;
    // }
  }
  else
  {
    //echo "Prepare Error: ($conn->errno) $conn->error";
    echo'<script>
      console.log("('.$conn->errno.') '.$conn->error.'");
      </script>';
  }

$totalReferredCount = 0;
if($userRows && count($userRows) > 0){
    $user = $userRows[0];
    $username = $user->getUsername();
    $phoneNo = getPhoneNo($conn,$user);
    $email = $user->getEmail();
    $userPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,1),"si");
    $totalReturnedBtc = 0;
    $totalCapitalBtc = 0;
    $totalFreeBtc = 0;
    $allCombinedTotalBtc = 0;
    $currentUtcTimeInMs =  getCurrentUtcTimeInMs();
    if($userPlanRows)
    {
        $totalReturnedBtc = $userPlanRows[0]->getReturnedBtc();
        $totalCapitalBtc = $userPlanRows[0]->getCapitalBtc();
        $totalFreeBtc = $userPlanRows[0]->getFreeBtc();
        $allCombinedTotalBtc = $totalReturnedBtc + $totalCapitalBtc + $totalFreeBtc;
    }
    $totalReferredCount = getCount($conn,"referral_history","referrer_id"," WHERE referrer_id = ? ", array("referrer_id"), array($user->getUid()),"s");

    $planAResult = displayTransactionHistory($conn,$uid,1,5);

    if($num_of_rows_specACC <= 0)
    {
      $currentTotalMaxComission = getComissionAmount($uid);
    }
    else
    {
      $currentTotalMaxComission = $amountAfterWithdrawalDB;

      // if($groupSalesPlanA > $totalSalesPlanADB)
      // {
      //   $newComissionAdditionA = $planACommissionTotal - $planAComissionDB;
      //   $currentTotalMaxComission += $newComissionAdditionA;
      // }
      // if($groupSalesPlanB > $totalSalesPlanBDB)
      // {
      //   $newComissionAdditionB = $planBCommissionTotal - $planBComissionDB;
      //   $currentTotalMaxComission += $newComissionAdditionB;
      // }
    }
    
    $sumOfWithdrawals = getSum($conn,"temp_specialacc","comissionWithdrawalAmount"," WHERE uid = ? ", array("uid"), array($uid),"s");
    if(!$sumOfWithdrawals)
    {
      $sumOfWithdrawals = 0;
    }
    
    $conn->close();
}
else
{
    $conn->close();
    header('Location:index.php');
    exit();
}
function getTotalComissionFromDB()
{
  return null;
}
function getTotalAmount($plan,$uid)
{
  $conn = connDB();
  
  $referralHistoryRows = array();
  $thisUserFirstTimeReferralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ORDER BY id ASC LIMIT 1 ",array("referrer_id"),array($uid),"s");
  if($thisUserFirstTimeReferralHistoryRows){
    $topReferrerId = $thisUserFirstTimeReferralHistoryRows[0]->getTopReferrerId();
    if($topReferrerId === $uid){
        $thisUserFirstLevel = 1;
        $thisUserLastLevel = 10;
    }else{
        $thisUserFirstLevel = $thisUserFirstTimeReferralHistoryRows[0]->getCurrentLevel();
        $thisUserLastLevel = $thisUserFirstLevel + 9;
    }

    $downlineReferralIdArray = array();
    $downlineReferralIdArray[$uid]  = true ;
    for($loopCurrentLevel = $thisUserFirstLevel; $loopCurrentLevel <= $thisUserLastLevel; $loopCurrentLevel++){
        $thisLevelReferralHistoryRows = getReferralHistory($conn," WHERE top_referrer_id = ? AND current_level = $loopCurrentLevel ORDER BY id ",array("top_referrer_id"),array($topReferrerId),"s");
        if($thisLevelReferralHistoryRows){
            $newDownlineReferralIdArray = array();
            foreach ($thisLevelReferralHistoryRows as $tempReferral) {
                $canAdd = false;
                foreach ($downlineReferralIdArray as $key => $value) {
                    if($tempReferral->getReferrerId() === $key){
                        $newDownlineReferralIdArray[$tempReferral->getReferralId()]  = true ;
                        $canAdd = true;
                        break;
                    }
                }
                if($canAdd){
                    array_push($referralHistoryRows,$tempReferral);
                }
            }
            unset($downlineReferralIdArray);
            $downlineReferralIdArray = array();
            foreach ($newDownlineReferralIdArray as $newDownlineReferralId => $value) {
                $downlineReferralIdArray[$newDownlineReferralId]  = true ;
            }
            unset($newDownlineReferralIdArray);
        }
    }
  }


  if($referralHistoryRows && count($referralHistoryRows) > 0) 
  {
    $currentLevel = $thisUserFirstLevel - 1;
    $currentNo = 0;

    $totalAInvested = 0;
    $totalBInvested = 0;
    // $arraYNo = array();

    foreach ($referralHistoryRows as $thisReferral) 
    {
      // echo  $thisReferral->getId()."<br>";
      $currentNo++;
      if($currentLevel < $thisReferral->getCurrentLevel())
      {
          $currentLevel++;
      }

      $referralUid = $thisReferral->getReferralId();
      // $tempUserRow = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($referralUid),"s");

      $totalInvestedAndWithdrawn = getSum($conn,"transaction_history","capital_btc_amount_in"," WHERE uid = ? AND btc_type_id = 2 AND plan_id = $plan AND (withdraw_status = 0 OR withdraw_status = 3)", array("uid"), array($referralUid),"s");
      if($totalInvestedAndWithdrawn && $totalInvestedAndWithdrawn != 0)
      {
        $totalAInvested += $totalInvestedAndWithdrawn;
        // array_push($arraYNo,$totalInvestedAndWithdrawn);
      }
    }
    // echo $totalAInvested;
    return $totalAInvested;
  }
  else
  {
    echo "-";
  }
}
function getPlanComission($uidUser,$planType)
{
  $planATotalSales = getTotalAmount(1,$uidUser);
  $planBTotalSales = getTotalAmount(2,$uidUser);

  $totalAccumulatedAmount = $planATotalSales + $planBTotalSales;
  
  //Comission rate for each plan
  $planAComissionRate = null;
  $planBComissionRate = null;

  if($uidUser == "CUSTOM-ID-01dc43a42713ebad5780176ba6d62e8407950605")//1668368941@qq.com
  {
    // if(idate("m") <= 6 && idate("d") <= 20 && $totalAccumulatedAmount >= 100)// Have To change later after 20th June
    if(idate("m") <= 6 && $totalAccumulatedAmount >= 100)
    {
      $planAComissionRate = 0.15;     //15%
      $planBComissionRate = 0.3;      //30%
    }
    else
    {
      $planAComissionRate = 0.15;     //15%
      $planBComissionRate = 0.2;      //20%
    }
  }
  else if($uidUser == "CUSTOM-ID-2d777f2fca4f62a0d4d98f0b98efa6e829878335")//634223478@qq.com
  {
    // if(idate("m") <= 6 && idate("d") <= 20 && $totalAccumulatedAmount >= 20) // Have To change later after 20th June
    if(idate("m") <= 6 && $totalAccumulatedAmount >= 20)
    {
      $planAComissionRate = 0.15;      //15%
      $planBComissionRate = 0.15;      //15%
    }
    else
    {
      $planAComissionRate = 0.1;      //10%
      $planBComissionRate = 0.1;      //10%
    }
  }
  else if($uidUser == "CUSTOM-ID-691768c4318f014372931f1a8225b6386315a442")//44615518@qq.com
  {
    // if(idate("m") <= 6 && idate("d") <= 20)
    if(idate("m") <= 6 ) // Have To change later after 20th June
    {
      $planAComissionRate = 0.1;      //10%
      $planBComissionRate = 0.1;      //10%
    }
    else
    {
      //have to ask michael
      //1. Total of both plan A or Plan B must be 100 or one of them must be 100
      //2. What happen to this user after 20th June
      $planAComissionRate = 0;      //0%
      $planBComissionRate = 0;      //0%
    }
  }

  if($planAComissionRate && $planBComissionRate)
  {
    if($planType == 1)
    {
      $planA_AfterComissionRate = $planATotalSales * $planAComissionRate;
      return $planA_AfterComissionRate;
    }
    else if($planType == 2)
    {
      $planB_AfterComissionRate = $planBTotalSales * $planBComissionRate;
      return $planB_AfterComissionRate;
    }
  }
  else
  {
    return null;
  }
}
function getComissionAmount($uidUser)
{

    $getPlanComission_planA = getPlanComission($uidUser,1);
    $getPlanComission_planB = getPlanComission($uidUser,2);

    if(($getPlanComission_planA != null || $getPlanComission_planA == 0)  &&  ($getPlanComission_planB != null || $getPlanComission_planB == 0))
    {
      $totalAccumulatedAmountAfterComission = $getPlanComission_planA + $getPlanComission_planB;
      return $totalAccumulatedAmountAfterComission;
    }
    else
    {
      return null;
    }
}

function getPhoneNo($conn,$user){
    $countryRows = getCountries($conn," WHERE id = ? ",array("id"),array($user->getCountryId()),"i");
    $countryCode = "";
    if($countryRows && count($countryRows) > 0){
        $countryCode = "+".$countryRows[0]->getPhonecode()." ";
    }
    return $countryCode . $user->getPhoneNo();
}

?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/specialProfile.php" />
        <meta property="og:title" content="<?= _special_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

        <title><?= _special_title ?></title>
        <link rel="canonical" href="https://btcworg.com/specialProfile.php" />
  </head>
  <body>
  <?php require 'mainNavbar.php';
    generateInputModalForInvest("invest");
    generateInputModalForInvest("withdraw");
  ?>

    <div id="firefly" class="firefly-class min-height">
    	<div class="width100 same-padding menu-content-distance">
        	<div class="first-profile-div">
            	<p class="white-text profile-data-p hover-container">
                	<b class="weight-700"><?= _profile_ni_hao ?> <?php echo $username; ?></b> 
                	<a href="editProfile.php" ><img src="img/sherry/edit-white.png" class="edit-icon3 hover-1" alt="<?= _editPassword_edit_details ?>" title="<?= _editPassword_edit_details ?>"></a>
                	<a href="editProfile.php" ><img src="img/sherry/edit-yellow.png" class="edit-icon3 hover-2" alt="<?= _editPassword_edit_details ?>" title="<?= _editPassword_edit_details ?>"></a>
                </p>
                <p class="white-text profile-data-p"><b class="weight-700"><?= _profile_phone_number ?> </b> <?php echo $phoneNo; ?></p>
                <p class="white-text profile-data-p"><b class="weight-700"><?= _profile_email ?> </b> <?php echo $email; ?></p>
                <!--<p class="white-text profile-data-p"><b class="weight-700">社区等级：</b> 
                	<img src="img/sherry/star1.png" class="star-img" alt="社区等级" title="社区等级">
                    <img src="img/sherry/star0.png" class="star-img" alt="社区等级" title="社区等级">
                    <img src="img/sherry/star0.png" class="star-img" alt="社区等级" title="社区等级">
                    <img src="img/sherry/star0.png" class="star-img" alt="社区等级" title="社区等级">
                    <img src="img/sherry/star0.png" class="star-img" alt="社区等级" title="社区等级">
                </p>-->
            </div>	
            <div class="second-big-div">
                <div class="second-profile-div">
                    <p class="white-text profile-data-p text-center time-p-text weight-700"><?= _profile_newyork ?></p>
                    <div class="clock neon-text">
                      <div class="hours">
                        <div class="first">
                          <div class="number">0</div>
                        </div>
                        <div class="second">
                          <div class="number">0</div>
                        </div>
                      </div>
                      <div class="tick">:</div>
                      <div class="minutes">
                        <div class="first">
                          <div class="number">0</div>
                        </div>
                        <div class="second">
                          <div class="number">0</div>
                        </div>
                      </div>
<!--                      <div class="tick">:</div>-->
                      <div class="seconds">
                        <div class="first">
                          <div class="number">0</div>
                        </div>
                        <div class="second infinite">
                          <div class="number">0</div>
                        </div>
                      </div>
                </div>
                </div>  
                <div class="third-profile-div">
                    <div class="green-dot">&nbsp;</div><div class="next-to-green"><p class="white-text online-number profile-data-p"><span id="user-online-count-span"></span><span class="grey-span">/108000+</span> <?= _profile_online ?></p></div>
                </div> 
           </div>                  
        </div> 
        <div class="clear"></div> 
        <?php //echo getTotalAmount(1,$uid); ?>
        <!--<div class="width100 same-padding button-div-distance">
        	<div class="fill-space3"></div>
            <div class="button-space-div"><button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now invest-button1">捐赠</button></div>
            <div class="fill-space3"></div>
        </div>
        <div class="clear"></div>-->
        <div class="width100 same-padding two-item-distance">
            <div class="plan-a-box text-center white-text">
             
                <div class="dark-box graph-bg special-mheight">
                	<div class="min-height-container">
                        <img src="img/sherry/bitcoin-white.png" class="box-img" alt="<?= _profile_bitcoin ?>" title="<?= _profile_bitcoin ?>">
                        <p class="box-title spcial-box-title"><?= _special_withdraw_point ?> (btc)</p>
                        <p class="box-big-font"><?php  
                        if($currentTotalMaxComission)
                        {
                          echo bcdiv($currentTotalMaxComission, 1, 5)."<br>";
                          // echo removeUselessZero($currentTotalMaxComission + 0 )."<br>";
                          // echo floatval(sprintf("%.5f", $currentTotalMaxComission + 0));
                        }
                        else
                        {
                          echo "0";
                        }
                        ?></p>
                    </div>
                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn"><?= _profile_withdrawal1 ?></button>
                </div>
                <div class="dark-box second-dark empty-dark star-bg special-mheight">
                	<div class="min-height-container">
                        <img src="img/sherry/triple-white.png" class="box-img" alt="<?= _special_ald_withdraw_point ?>" title="<?= _special_ald_withdraw_point ?>">
                        <p class="box-title spcial-box-title"><?= _special_ald_withdraw_point ?> (btc）</p>
                        <!-- Fizo here change into already withdraw point -->
                        <p class="box-big-font"><?php echo removeUselessZero($sumOfWithdrawals); ?></p>
                       
                    </div>

                  
                </div>

            </div>         
            <div class="plan-a-box plan-b-box text-center white-text">
               
                <div class="dark-box graph-bg special-mheight">
                    <img src="img/sherry/low-risk-white.png" class="box-img" alt="<?= _special_plan_a_refer ?>" title="<?= _special_plan_a_refer ?>">
                    <p class="box-title spcial-box-title"><?= _special_plan_a_community ?> (btc)</p>
					<p class="box-big-font"><?php echo floatval(sprintf("%.5f", $groupSalesPlanA + 0)); ?></p>

                </div>
                <div class="dark-box second-dark empty-dark star-bg special-mheight">
                    <img src="img/sherry/influence-white.png" class="box-img" alt="<?= _special_plan_b_refer ?>" title="<?= _special_plan_b_refer ?>">
                    <p class="box-title spcial-box-title"><?= _special_plan_b_community ?> (btc)</p>
					<p class="box-big-font"><?php echo floatval(sprintf("%.5f", $groupSalesPlanB + 0));?></p>
                  
                </div>

            </div>
            
           
        </div>
	</div>

  <?php require 'mainFooter.php';?>
     
  <!--- Before Edit-->
  
<!--  old version please refer to profile2.php-->

     
        <!--- Withdrawal Modal --->
        <div id="myModal3" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
            <span class="close3 ow-close2 close-style">&times;</span>
      <!--  <form class="register-form width100 same-padding" method="POST" id="withdrawForm" onsubmit="return withdrawalValidation();"> -->
            <form class="register-form width100 same-padding" method="POST" id="withdrawForm" >
              <h4 class="btcw-h4 form-h4 ow-form-h4 withdraw-h4"><b class="weight-700" id="withdrawal_title"><?= _profile_withdrawal1 ?></b></h4>
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table withdraw-table mb-2">
                    <tr>
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_choose ?></b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top">
                            <h4 class="btcw-h4 edit2-h4">
                                <select class="inputa clean2 disabled-input no-padding-left"  name="paymentOption" id="paymentOption" required onchange="selectPaymentOption(this.value);"><!--remove class disabled input if it is not disabled-->
                                    <option disabled selected ><?= _profile_choose1 ?></option>
                                    <option value="1"><?= _profile_bitcoin ?></option>
                                    <option value="2"><?= _profile_c2c ?></option>
                                </select>
                            </h4>
                        </td>
                    </tr>  
                </table>
                <div id="paymentOption_1" style="display:none;">
                  <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table withdraw-table mb-2">
                      <tr>
                          <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_total_amount ?> (btc)</b></h4></td>
                          <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                          <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4" ><?php
                          if($currentTotalMaxComission)
                          {
                            echo bcdiv($currentTotalMaxComission, 1, 5);
                            // echo  floatval(sprintf("%.5f", $currentTotalMaxComission + 0));
                          }
                          else
                          {
                            echo "0";
                          }?></h4></td>
                      </tr>
                      <tr>
                          <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">输入提取总额</b></h4></td> -->
                          <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_keyin_withdraw_amount ?> (btc)</b></h4></td>
                          <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                          <td class="third-td2" width="58%" valign="top">
                              <h4 class="btcw-h4 edit2-h4">
                                  <input type="number" class="inputa clean2 inputb"  name="opt1_withdrawal_amount" id="opt1_withdrawal_amount" placeholder="<?= _profile_keyin_withdraw_amount ?>" step="0.0000000001" >
                              </h4>
                          </td>
                      </tr>
                      <tr>
                          <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_btc_address ?></b></h4></td>
                          <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                          <td class="third-td2" width="58%" valign="top">
                              <h4 class="btcw-h4 edit2-h4">
                                  <input type="text" class="inputa clean2 inputb"  name="opt1_withdrawal_BTCaddress" id="opt1_withdrawal_BTCaddress" placeholder="<?= _profile_keyin_btc_address ?>" >
                              </h4>
                              <p class="btcw-h4 edit2-h4"><?= _profile_keyin_btc_address_note ?></p>
                          </td>
                      </tr> 
                  </table>
                  <div class="clear"></div>                 
                   <div class="checkbox-div">
                        <div class="checkbox1">
                            <input class="form-check-input position-relative-important" type="checkbox" value="" name="confirmOpt1" id="confirmOpt1">
                            <label class="form-check-label profile-checkbox" for="defaultCheck1">
                                *<?= _profile_keyin_btc_address_note ?>
                            </label>
                         </div>
                    </div>            
                    
                    <div class="width100 overflow">
                        <div class="fill-up-space1 modal-fill-space-1"></div>
                          <input type="hidden" name="currentTotalMaxComission1" id="currentTotalMaxComission1" value="<?php echo $currentTotalMaxComission;?>">
                            <button class="register-button2 clean orange-hover invest-btn text-center" name="opt1_withdrawal_submit" id="opt1_withdrawal_submit"><?= _profile_confirm_withdraw ?></button>
                            <!-- <button class="register-button2 clean orange-hover invest-btn" name="opt1_withdrawal_submit" id="opt1_withdrawal_submit" >Confirm Withdrawal</button> -->
                        <div class="fill-up-space1 modal-fill-space-1"></div>
                    </div>
                </div>
                <div id="paymentOption_2" style="display:none;">
                  <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table withdraw-table mb-2">
                    <tr>
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_can_withdraw_amount ?> (btc)</b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4" ><?php 
                        if($currentTotalMaxComission)
                        {
                          echo bcdiv($currentTotalMaxComission, 1, 5);
                          // echo  floatval(sprintf("%.5f", $currentTotalMaxComission + 0));
                        }
                        else
                        {
                          echo "0";
                        }
                        ?></h4></td>
                    </tr>
                    <tr>
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_reg_bank_name ?></b></h4></td>
                        <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Full Name</b></h4></td> -->
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top">
                            <h4 class="btcw-h4 edit2-h4">
                                <input type="text" class="inputa clean2 inputb"  name="opt2_withdrawal_fullName" id="opt2_withdrawal_fullName" placeholder="<?= _profile_keyin_name ?>" >
                            </h4>
                        </td>
                    </tr> 
                    <tr>
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _general_country ?></b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top">
                            <h4 class="btcw-h4 edit2-h4">
                                <input type="text" class="inputa clean2 inputb"  name="opt2_withdrawal_country" id="opt2_withdrawal_country" placeholder="<?= _profile_keyin_country ?>" >
                            </h4>
                        </td>
                    </tr>
                    <tr>
                        <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">银行名字</b></h4></td> -->
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_bank_name ?></b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top">
                            <h4 class="btcw-h4 edit2-h4">
                                <input type="text" class="inputa clean2 inputb"  name="opt2_withdrawal_bankName" id="opt2_withdrawal_bankName" placeholder="<?= _profile_keyin_bank_name ?>" >
                            </h4>
                        </td>
                    </tr>
                    <tr>
                        <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">银行帐户号码</b></h4></td> -->
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_bank_acc_no ?></b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top">
                            <h4 class="btcw-h4 edit2-h4">
                                <input type="text" class="inputa clean2 inputb"  name="opt2_withdrawal_bankAccountNo" id="opt2_withdrawal_bankAccountNo" placeholder="<?= _profile_keyin_bank_acc_no ?>" >
                            </h4>
                        </td>
                    </tr> 
                    <tr id="hideForCapitalPlanA_5" style="display:table-row;">
                        <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">输入提取总额</b></h4></td> -->
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_withdrawal_amount2 ?> (btc)</b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top">
                            <h4 class="btcw-h4 edit2-h4">
                                <input type="number" class="inputa clean2 inputb"  name="opt2_withdrawal_amount" id="opt2_withdrawal_amount" placeholder="<?= _profile_keyin_amount2 ?>" step="0.0000000001">
                            </h4>
                            <p class="btcw-h4 edit2-h4"><?= _profile_withdrawal_amount2_note ?></p>
                        </td>
                    </tr>  
                  </table>
                  <div class="clear"></div>                 
                   <div class="checkbox-div">
                        <div class="checkbox1">
                            <input class="form-check-input position-relative-important" type="checkbox" value=""  name="confirmOpt2" id="confirmOpt2">
                            <label class="form-check-label profile-checkbox" for="confirmOpt2">
                                *<?= _profile_correct_info_note ?>
                            </label>
                         </div>
                    </div> 
                    <div class="width100 overflow">
                        <div class="fill-up-space1 modal-fill-space-1"></div>
                            <input type="hidden" name="currentTotalMaxComission2" id="currentTotalMaxComission2" value="<?php echo $currentTotalMaxComission;?>">
                            <button class="register-button2 clean orange-hover invest-btn text-center" name="opt2_withdrawal_submit" id="opt2_withdrawal_submit" ><?= _profile_confirm_withdraw ?></button>
                            <!-- <div class="register-button2 clean orange-hover invest-btn" name="opt2_withdrawal_submit" id="opt2_withdrawal_submit" >Confirm Withdrawal</div> -->
                        <div class="fill-up-space1 modal-fill-space-1"></div>
                    </div>
                </div>
            </form>  
          </div>
        </div>
	
        <!--- Reconfirm Withdraw Modal --->
        <div id="myModal18" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
            <span class="close18 ow-close2 close-style">&times;</span>
            <h4 class="btcw-h4 form-h4 ow-form-h4 same-padding sepcial-h4"><b class="weight-700 special-h4"><?= _profile_withdrawal1 ?></b></h4>  
            <h4 class="reconfirm-h4"><b class="weight-700">提取总额 : </b>echo here total withdraw amount</h4>
            <h4 class="reconfirm-h4"><b class="weight-700">比特币转账链接 : </b>echo here</h4>
            
            <p class="text-center"><button class="register-button2 clean orange-hover complete-btn button-y-width close18" >确认提取</button></p>
            <p class="text-center"><button class="register-button2 clean orange-hover complete-btn button-y-width copy-yellow-line3 withdrawal-btn" >返回</button></p>                
          </div>
        </div>
 <!---  End of Sherry coded part-->
        
  <!-- ****************************************************MODALS***********************************************************-->
<!--  <div class="modal fade" id="withdrawModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--      <div class="modal-dialog" role="document">-->
<!--          <div class="modal-content">-->
<!--              <div class="modal-header">-->
<!--                  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>-->
<!--                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--                      <span aria-hidden="true">&times;</span>-->
<!--                  </button>-->
<!--              </div>-->
<!--              <div class="modal-body">-->
<!--                  ...-->
<!--              </div>-->
<!--              <div class="modal-footer">-->
<!--                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
<!--                  <button type="button" class="btn btn-primary">Save changes</button>-->
<!--              </div>-->
<!--          </div>-->
<!--      </div>-->
<!--  </div>-->
   <div id="myModal10" class="modal2 ow-modal2 ow-modal10">
 
          <div class="modal-content2 modal-content-white ow-modal-width ow-modal10a">
            <h4 class="btcw-h4 text-center ow10-h4"><?= _profile_link_copied ?></h4>
          </div>
   </div>

<!--	<div class="width100 element-div extra-padding-bottom profile-divider">-->
<!--        	<img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">-->
<!--    </div>    -->


  <!-- ****************************************************MODALS***********************************************************-->
  <script>
      function randomFunc(type) {
          // Get the modal
          var modal = document.getElementById(type + '-modal');

          // Get the <span> element that closes the modal
          var span = document.getElementsByClassName('closeNoticeModal')[0];
          var span1 = document.getElementsByClassName('closeNoticeModal')[1];

          modal.style.display = 'block';

          // When the user clicks on <span> (x), close the modal
          span.onclick = function() {
              modal.style.display = 'none';
          };
          span1.onclick = function() {
              modal.style.display = 'none';
          };

          // When the user clicks anywhere outside of the modal, close it
          window.onclick = function(event) {
              if (event.target == modal) {
                  modal.style.display = 'none';
              }
          }
      }

  

      $("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#invest-now-referral-link').attr("href");
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
      });
  </script>
          <script>



			// Get the modal
			var modal3 = document.getElementById('myModal3');
			
			// Get the button that opens the modal
			var btn3 = document.getElementsByClassName("withdrawal-btn")[0];
			
			// Get the button that opens the modal
			var btn3a = document.getElementsByClassName("withdrawal-btn")[1];	
			// Get the button that opens the modal
			var btn3b = document.getElementsByClassName("withdrawal-btn")[2];			
			// Get the button that opens the modal
			var btn18 = document.getElementsByClassName("reconfirm-btn")[0];	
		
					
			// Get the <span> element that closes the modal
			var span3 = document.getElementsByClassName("close3")[0];


			// Get the modal
			var modal16 = document.getElementById('myModal16');
			
			
			// Get the <span> element that closes the modal
			var span16 = document.getElementsByClassName("close16")[0];
			
			
			// Get the <span> element that closes the modal
			var span16a = document.getElementsByClassName("close16")[1];	
			
			
			


			// Get the modal
			var modal18 = document.getElementById('myModal18');
			
			
			// Get the <span> element that closes the modal
			var span18 = document.getElementsByClassName("close18")[0];
			
			
			// Get the <span> element that closes the modal
			var span18a = document.getElementsByClassName("close18")[1];             
			// When the user clicks the button, open the modal
         
      <?php
				  if(isset($errorMsg) || isset($successMsg)){
                      promptAlertMsg($errorMsg,$successMsg);
				  }
              ?>

			// When the user clicks the button, open the modal
			if(btn3){
                btn3.onclick = function() {
                    modal3.style.display = "block";
                };
            }

            if(btn3a){
                btn3a.onclick = function() {
                    modal3.style.display = "block";
                };
            }

            if(btn3b){
                btn3b.onclick = function() {
                    modal3.style.display = "block";
					 modal18.style.display = "none";
                };
            }
            if(btn18){
                btn18.onclick = function() {
                    modal3.style.display = "none";
					 modal18.style.display = "block";
                };
            }
			
			// When the user clicks on <span> (x), close the modal
			if(span3){
                span3.onclick = function() {
                    modal3.style.display = "none";
                    document.getElementById("withdrawForm").reset();
                };
            }

		
			// When the user clicks on <span> (x), close the modal
			if(span16){
                span16.onclick = function() {
                    modal16.style.display = "none";
                };
            }
			// When the user clicks on <span> (x), close the modal
			if(span16a){
                span16a.onclick = function() {
                    modal16.style.display = "none";
                };
            }	
			// When the user clicks on <span> (x), close the modal
			if(span18){
                span18.onclick = function() {
                    modal18.style.display = "none";
                };
            }		
			// When the user clicks on <span> (x), close the modal
			if(span18a){
                span18a.onclick = function() {
                    modal18.style.display = "none";
                };
            }					
			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {

			  if (event.target == modal3) {
				modal3.style.display = "none";
        document.getElementById("withdrawForm").reset();
			  }		 
			}
		</script>
  <!--      
         <script>
			// Get the modal
			var modal10 = document.getElementById('myModal10');
			
			// Get the button that opens the modal
			var btn10 = document.getElementById("copy-referral-link");
            
			// When the user clicks the button, open the modal
			btn10.onclick = function() {
			  modal10.style.display = "block";
                setTimeout(closeCopiedModalWithDelay, 1600);
			};


			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal10) {
				modal10.style.display = "none";
			  }
			};

            function closeCopiedModalWithDelay() {
                modal10.style.display = "none";
            }
		</script>  -->
  
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
            }
    
            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
            
            /*close when you click outside of the sideNav*/
            $('body').on('click touchstart', function(){
              if( parseInt( $('#mySidenav').css('width') ) > 0 ){
                closeNav();
              }
            });
           /*$('body').on('click', function(){
           if( parseInt( $('#mySidenav').css('width') ) > 0 ){
           closeNav();
           }
           });*/
	</script>
    
    <script>
var hoursContainer = document.querySelector('.hours');
var minutesContainer = document.querySelector('.minutes');
var secondsContainer = document.querySelector('.seconds');
var tickElements = Array.from(document.querySelectorAll('.tick'));

var last = new Date(0);
last.setUTCHours(-1);

var currentTimeInMs = <?php echo $currentUtcTimeInMs; ?>;
function updateTime () {
  currentTimeInMs += 1000;
  var now = new Date(currentTimeInMs).toLocaleString("en-US", {timeZone: "America/New_York"});

  if(!now || now === null || now.length <= 0 || now === "Na" || now === "NaN" || now.length <= 10){
      var d = new Date();
      var offset = d.getTimezoneOffset() / 60;
      offset = 60 * 60 * offset * 1000;
      now = new Date(currentTimeInMs - (60 * 60 * 4 * 1000) + offset);
  }else{
      now = new Date(now);
  }

  var lastHours = last.getHours().toString();
  var nowHours = now.getHours().toString();
  if (lastHours !== nowHours) {
      if(now.getHours() > 12){
          nowHours = (now.getHours() - 12).toString();
      }else if(now.getHours() === 0){
          nowHours = "12";
      }
    updateContainer(hoursContainer, nowHours)
  }
  
  var lastMinutes = last.getMinutes().toString();
  var nowMinutes = now.getMinutes().toString();
  if (lastMinutes !== nowMinutes) {
    updateContainer(minutesContainer, nowMinutes)
  }
  
  // var lastSeconds = last.getSeconds().toString();
  var lastSeconds = last.getHours() < 12? 'am' : 'pm';
  // var nowSeconds = now.getSeconds().toString();
  var nowSeconds = now.getHours() < 12? 'am' : 'pm';
  // if (lastSeconds !== nowSeconds) {
  //   //tick()
  //   updateContainer(secondsContainer, nowSeconds);
  // }
  updateContainer(secondsContainer, nowSeconds);

  last = now
}

function tick () {
  tickElements.forEach(t => t.classList.toggle('tick-hidden'));
}

function updateContainer (container, newTime) {
  var time = newTime.split('');
  
  if (time.length === 1) {
    time.unshift('0')
  }
  
  
  var first = container.firstElementChild;
  if (first.lastElementChild.textContent !== time[0]) {
    updateNumber(first, time[0])
  }
  
  var last = container.lastElementChild;
  if (last.lastElementChild.textContent !== time[1]) {
    updateNumber(last, time[1])
  }
}

function updateNumber (element, number) {
  //element.lastElementChild.textContent = number
  var second = element.lastElementChild.cloneNode(true);
  second.textContent = number;
  
  element.appendChild(second);
  element.classList.add('move');

  setTimeout(function () {
    element.classList.remove('move')
  }, 990)
  setTimeout(function () {
    element.removeChild(element.firstElementChild)
  }, 990)
}

setInterval(updateTime, 1000);

//random number generator
var max = 4598;
var min = 2168;
$("#user-online-count-span").html(Math.floor(Math.random()*(max-min+1)+min));

</script>
<script>

// function withdrawalValidation()
// {
//     let errorArray = [];
//     let canValidateLevel_2 = true;
//     let combinationArray =[errorArray,canValidateLevel_2];

//     let paymentOp = $('#paymentOption').val();
//     if(paymentOp != null)
//     {
//       if(paymentOp == 1)
//       {
//         let opt1_withdrawal_amount = $('#opt1_withdrawal_amount').val();
//         let opt1_withdrawal_BTCaddress = $('#opt1_withdrawal_BTCaddress').val();
//         let confirmOpt1 = $('#confirmOpt1').val();

//         combinationArray = checkNullOrEmpty(combinationArray,opt1_withdrawal_amount,"Please enter your withdrawal amount");
//         combinationArray = checkNullOrEmpty(combinationArray,opt1_withdrawal_BTCaddress,"Please enter the btc address");

//         if(!combinationArray[1])
//         {
//             let errorText = "请输入以下详细信息 ：<br>";
//             for(let counter = 0;counter < combinationArray[0].length ;counter++)
//             {
//                 errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
//             }

//             putNoticeJavascript("注意 ！",errorText);
//             event.preventDefault();
//         }
//         else
//         {
//             if($('#confirmOpt1').is(":checked"))
//             {
                
//             }
//             else
//             {
//                 putNoticeJavascript("注意 ！","请打勾以确认资料填写正确。");
//                 event.preventDefault();
//             }
//         }
//       }
//       else if(paymentOp == 2)
//       {
//         let confirmOpt2 = $('#confirmOpt2').val();

//         let opt2_withdrawal_fullName = $('#opt2_withdrawal_fullName').val();
//         let opt2_withdrawal_country = $('#opt2_withdrawal_country').val();
//         let opt2_withdrawal_bankName = $('#opt2_withdrawal_bankName').val();
//         let opt2_withdrawal_bankAccountNo = $('#opt2_withdrawal_bankAccountNo').val();
//         let opt2_withdrawal_amount = $('#opt2_withdrawal_amount').val();

//         combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_fullName,"请输入真实姓名");
//         combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_country,"请输入国家");
//         combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_bankName,"请输入银行账户名称");
//         combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_bankAccountNo,"请输入银行账户号码");
//         combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_amount,"请输入您的提款数额");

//         if(!combinationArray[1])
//         {
//             let errorText = "请输入以下详细信息 ：<br>";
//             for(let counter = 0;counter < combinationArray[0].length ;counter++)
//             {
//                 errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
//             }

//             putNoticeJavascript("注意 ！",errorText);
//             event.preventDefault();
//         }
//         else
//         {
//             if($('#confirmOpt2').is(":checked"))
//             {
                
//             }
//             else
//             {
//                 putNoticeJavascript("注意 ！","请打勾以确认资料填写正确。");
//                 event.preventDefault();
//             }
//         }
//       }
//     }
// }
function planSelected(value)
{
    if(value == "A")
    {
        document.getElementById("field_2").max = 0.2;
    }
    if(value == "B")
    {
        document.getElementById("field_2").max = 50;
    }
}
function selectPaymentOption(paymentOption)
{
    if(paymentOption == 1)
    {
        document.getElementById('paymentOption_1').style.display = 'block';
        document.getElementById('paymentOption_2').style.display = 'none';

        document.getElementById("opt1_withdrawal_amount").required = true;
        document.getElementById("opt1_withdrawal_BTCaddress").required = true;
        document.getElementById("confirmOpt1").required = true;
        // document.getElementById("withdrawForm").reset();
    }
    else if(paymentOption == 2)
    {
        document.getElementById('paymentOption_1').style.display = 'none';
        document.getElementById('paymentOption_2').style.display = 'block';

        document.getElementById("opt2_withdrawal_fullName").required = true;
        document.getElementById("opt2_withdrawal_country").required = true;
        document.getElementById("opt2_withdrawal_bankName").required = true;
        document.getElementById("opt2_withdrawal_bankAccountNo").required = true;
        document.getElementById("opt2_withdrawal_amount").required = true;
        document.getElementById("confirmOpt2").required = true;
        // document.getElementById("withdrawForm").reset();
    }
    // onvalid
}
function validatePlans()
{
    let errorArray = [];
    let canValidateLevel_2 = true;
    let combinationArray =[errorArray,canValidateLevel_2];

    let plans = $('#field_1').val();
    let investedAmount = $('#field_2').val();
    let hashkey = $('#field_3').val();

    combinationArray = checkNullOrEmpty(combinationArray,plans,"Please select a plan");
    combinationArray = checkNullOrEmpty(combinationArray,investedAmount,"Please enter Your invested amount");
    combinationArray = checkNullOrEmpty(combinationArray,hashkey,"Please enter A hashkey");

    if(!combinationArray[1])
    {
        let errorText = "<?= _profile_keyin_below_details ?> ：<br>";
        for(let counter = 0;counter < combinationArray[0].length ;counter++)
        {
            errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
        }

        putNoticeJavascript("<?= _general_notice ?>",errorText);
        event.preventDefault();
    }
}
</script>
  </body>
</html>