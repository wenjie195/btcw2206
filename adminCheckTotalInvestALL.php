<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{
    if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
        $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
    {
        header('Location:index.php');
        exit();
    }
    else
    {
        $uid = $_SESSION['uid'];
        $conn = connDB();
        $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
    }
}

$userData = array();

$startDate = "2018-05-20";
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['field_1'])){
    $startDate = rewrite($_POST['field_1']);
}

$userInvestedData = array();
$userInvestedData = getUsersInvestedData($conn,$userInvestedData,$startDate);
$conn->close();

function getUsersInvestedData($conn,$userInvestedData,$startDate){
    $sql = "SELECT uid FROM transaction_history  WHERE btc_type_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) AND date_created >= '$startDate' GROUP BY uid ";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $totalInvestedPlanA = getSum($conn,"transaction_history","capital_btc_amount_in","  WHERE uid = ? AND date_created >= ? AND plan_id = 1 AND btc_type_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ",array("uid","date_created"),array($row["uid"],$startDate),"ss");
            $totalInvestedPlanB = getSum($conn,"transaction_history","capital_btc_amount_in","  WHERE uid = ? AND date_created >= ? AND plan_id = 2 AND btc_type_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ",array("uid","date_created"),array($row["uid"],$startDate),"ss");
            $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($row["uid"]),"s");
            if(($totalInvestedPlanA > 0 || $totalInvestedPlanB > 0) > 0 && $userRows){
                $thisUser = $userRows[0];
                if(array_key_exists($thisUser->getUid(), $userInvestedData)){
                    $userInvestedData[$thisUser->getUid()]['planA'] = removeUselessZero(bcadd($userInvestedData[$thisUser->getUid()]['planA'],$totalInvestedPlanA,25));
                    $userInvestedData[$thisUser->getUid()]['planB'] = removeUselessZero(bcadd($userInvestedData[$thisUser->getUid()]['planB'],$totalInvestedPlanB,25));
                }else{
                    $thisUserData = array();
                    $thisUserData['planA'] = removeUselessZero($totalInvestedPlanA);
                    $thisUserData['planB'] = removeUselessZero($totalInvestedPlanB);
                    $thisUserData['user'] = $thisUser;
                    $userInvestedData[$thisUser->getUid()] = $thisUserData;
                }
            }
        }
    } else {
        echo "0 results";
    }
    return $userInvestedData;
}
?>
<!doctype html>
<html lang="en">
<head>
    <?php require_once 'mainHeader.php';?>
    <title>Level 1 Invest</title>
</head>
<body>
<?php
require_once 'mainNavbar.php';
generateSimpleModal();

?>
<div id="firefly" class="firefly-class min-height">
    <div class="width100 same-padding more-separate-margin-top edit-div">
        <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a>
        <div class="clear"></div>

        <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>

        <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Query:</b></h4>
        <form class="register-form"  method="POST" >
            <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Investment Start Date until today (example: 2019-05-20)</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb" required name="field_1" id="field_1" value="<?php if(isset($_POST['field_1'])){echo $_POST['field_1'];} ?>"></h4></td>
                </tr>
                <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">More than or equal to</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="more_field" id="more_field" value="<?php if(isset($_POST['more_field'])){echo $_POST['more_field'];} ?>"></h4></td>
                </tr>

                <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Less than or equal to</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="less_field" id="less_field" value="<?php if(isset($_POST['less_field'])){echo $_POST['less_field'];} ?>"></h4></td>
                </tr>

                <!-- <tr>
                  <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">邮箱</b></h4></td>
                  <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                  <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" placeholder="电邮" ></h4></td>
                </tr> -->

            </table>
            <div class="clear"></div>
            <button class="register-button2 clean orange-hover inputb-button" name="insertValue" id="insertValue" >Check Insertion Value</button>

        </form>
    </div>
    <div class="clear"></div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h4 class="btcw-h4 separate-title white-text mt-5"><b class="weight-700">Total User : <?php echo count($userInvestedData);?></b></h4>
            <h4 class="btcw-h4 separate-title white-text"><b class="weight-700">All user's total investment data：</b> </h4>
            <table cellspacing="0" cellpadding="0" class="dark-table recommend-table2 white-text-table mt-5">

                <?php
                if($userInvestedData && count($userInvestedData) > 0) {
                    echo '
                             <tr>
                                 <th class="table2-1">序</th>
                                 <th class="table2-2">UID</th>
                                 <th class="table2-3">用户名</th>
                                 <th class="table2-4">Email</th>
                                 <th>Plan A</th>
                                 <th>Plan B</th>
                             </tr>
                         ';

                    $currentNo = 0;
                    foreach ($userInvestedData as $data) {
                        $currentNo++;

                        echo '
                                         <tr class="tr2">
                                             <td class="table2-1">'.$currentNo.'.</td>
                                             <td class="table2-2">'.$data['user']->getUid().'</td>
                                             <td class="table2-3">'.$data['user']->getUsername().'</td>
                                             <td class="table2-4">'.$data['user']->getEmail().'</td>';
                        if((isset($_POST['more_field']) && strlen($_POST['more_field']) > 0) || isset($_POST['less_field']) && strlen($_POST['less_field']) > 0 ){
                            if(strlen($_POST['more_field']) > 0 && strlen($_POST['less_field']) > 0){
                                if($data['planA'] >= $_POST['more_field'] && $data['planA'] <= $_POST['less_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planA'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planA'].'</td>';
                                }
                                if($data['planB'] >= $_POST['more_field'] && $data['planB'] <= $_POST['less_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planB'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planB'].'</td>';
                                }
                            }
                            else if(strlen($_POST['more_field']) > 0){
                                if($data['planA'] >= $_POST['more_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planA'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planA'].'</td>';
                                }
                                if($data['planB'] >= $_POST['more_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planB'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planB'].'</td>';
                                }
                            }else if(strlen($_POST['less_field']) > 0){
                                if($data['planA'] <= $_POST['less_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planA'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planA'].'</td>';
                                }
                                if($data['planB'] <= $_POST['less_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planB'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planB'].'</td>';
                                }
                            }
                        }else{
                            echo   '<td>'.$data['planA'].'</td>';
                            echo   '<td>'.$data['planB'].'</td>';
                        }

                        echo       '</tr>';
                    }

                    echo "</table>";
                }else{
                    echo "<p class='white-text'>还没推荐任何一个人。</p>";
                }

                ?>

            </table>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
        <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
    </div>
</div>
<?php require_once 'mainFooter.php';?>
</body>
</html>