function changePhoneNo(value)
{
    let field_4_CountryCode = document.getElementById('field_4_CountryCode');
    let countryCode = "";
    countryCode += "<?php getCountryCode("+value+");?>";

    field_4_CountryCode.value = countryCode;

    console.log(countryCode);
    $.ajax({
            url: "generalFunction.php",
            type: 'POST',
            async: false,
            data:
            {
                value: value,
                fromPage:"login"
            }
        })
        .done(function(message) 
        {
            field_4_CountryCode.value= message;
            console.log(message);
        })
        .fail(function() 
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
}
function changePhoneNoEditProfile(value)
{
    let field_4_CountryCode = document.getElementById('field_6_CountryCode');
    let countryCode = "";
    countryCode += "<?php editProfileGetPhoneCode("+value+");?>";

    field_4_CountryCode.value = countryCode;

    $.ajax({
        url: "generalFunction.php",
        type: 'POST',
        async: false,
        data:
            {
                value: value,
                fromPage:"login"
            }
    })
        .done(function(message)
        {
            field_4_CountryCode.value= message;
            console.log(message);
        })
        .fail(function()
        {
            alert( "Error : There is a server problem OR there is no internet connection" );
        });
}
function putNoticeJavascript(header,text)
{
    document.getElementById('noticeHeader').innerHTML = header;
    document.getElementById('noticeText').innerHTML = text;

    // Get the modal
    var modal = document.getElementById('noticeModal');

    // Get the <span> element that closes the modal
    var span = document.getElementById('closeNoticeModal');

    modal.style.display = 'block';


    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = 'none';
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = 'none';
        }
    }
}
function putNoticeJavascriptWithLocation(header,text,location)
{
    document.getElementById('noticeHeader').innerHTML = header;
    document.getElementById('noticeText').innerHTML = text;

    // Get the modal
    var modal = document.getElementById('noticeModal');

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName('closeNoticeModal')[0];

    modal.style.display = 'block';


    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = 'none';
        window.location.replace(location);
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = 'none';
            window.location.replace(location);
        }
    }
}
function checkNullOrEmpty(combinationArray,field,errTXT)
{
    if(field == null || field == "" || field.length == 0)
    {
        combinationArray[0].push(errTXT);
        combinationArray[1] = false;
    }
    return combinationArray;
}
function validateEditProfileForm(msgArray)
{
    let errorArray = [];
    let canValidateLevel_1 = true;
    let combinationArray =[errorArray,canValidateLevel_1];

    let username = $('#field_1').val();
    let email = $('#field_2').val();
    let ic_no = $('#field_3').val();
    let country = $('#country').val();
    console.log(country);
    // let phone = $('#field_6').val();

    combinationArray = checkNullOrEmpty(combinationArray,username,msgArray["_MAINJS_ENTER_USERNAME"]);
    combinationArray = checkNullOrEmpty(combinationArray,email,msgArray["_MAINJS_ENTER_EMAIL"]);
    combinationArray = checkNullOrEmpty(combinationArray,ic_no,msgArray["_MAINJS_ENTER_ICNO"]);
    // combinationArray = checkNullOrEmpty(combinationArray,country,"请选择你的国家");
    // combinationArray = checkNullOrEmpty(combinationArray,phone,"请输入您的电话号码");

    if(!combinationArray[1])
    {
        let errorText = msgArray["_MAINJS_ENTER_BELOW_INFO"] + " ：<br>";
        for(let counter = 0;counter < combinationArray[0].length ;counter++)
        {
            errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
        }

        putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",errorText);
        event.preventDefault();
    }
    else
    {console.log("1");
        if(country == 44 || country == "44") //China
        {
            if(!validid.cnid(ic_no))
            {
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
        else if(country == 214 || country == "214") //Taiwan
        {
            console.log("2");
            console.log(validid.twid(ic_no));
            console.log(validid.twrc(ic_no));
            if(!validid.twid(ic_no) && !validid.twrc(ic_no))
            {
                console.log("3");
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
        else if(country == 98 || country == "98") //Hong Kong
        {
            if(!validid.hkid(ic_no))
            {
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
        else if(country == 196 || country == "196") //Singapore ->LINK : https://stackoverflow.com/questions/29743154/regular-expression-for-nric-fin-in-singapore
        {
            if(!ic_no.match(/^[STFG]\d{7}[A-Z]$/))
            {
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
        else if(country == 132 || country == "132") //Malaysia
        {
            if(!ic_no.match(/^([0-9][0-9])((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))([0-9][0-9])([0-9][0-9][0-9][0-9])$/))
            {
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
    }
}
function validateMyForm(msgArray)
{
    let errorArray = [];
    let canValidateLevel_2 = true;
    let combinationArray =[errorArray,canValidateLevel_2];

    let username = $('#field_1').val();
    let email = $('#field_2').val();
    let country = $('#field_3').val();
    let phone = $('#field_4').val();
    let password = $('#field_5').val();
    let confirmPassword = $('#field_6').val();
    let ic_no = $('#field_7').val();

    combinationArray = checkNullOrEmpty(combinationArray,username,msgArray["_MAINJS_ENTER_USERNAME"]);
    combinationArray = checkNullOrEmpty(combinationArray,email,msgArray["_MAINJS_ENTER_EMAIL"]);
    combinationArray = checkNullOrEmpty(combinationArray,country,msgArray["_MAINJS_SELECT_COUNTRY"]);
    combinationArray = checkNullOrEmpty(combinationArray,phone,msgArray["_MAINJS_ENTER_PHONENO"]);
    combinationArray = checkNullOrEmpty(combinationArray,password,msgArray["_MAINJS_ENTER_PASSWORD"]);
    combinationArray = checkNullOrEmpty(combinationArray,confirmPassword,msgArray["_MAINJS_ENTER_CONFIRM_PASSWORD"]);
    combinationArray = checkNullOrEmpty(combinationArray,ic_no,msgArray["_MAINJS_ENTER_ICNO"]);

    if($('#defaultCheck1').is(':checked') == false)
    {
        combinationArray[0].push(msgArray["_MAINJS_ACCEPT_TERMS"]);
        combinationArray[1] = false;
    }
    if(!combinationArray[1])
    {
        let errorText = msgArray["_MAINJS_ENTER_BELOW_INFO"] + " ：<br>";
        for(let counter = 0;counter < combinationArray[0].length ;counter++)
        {
            errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
        }

        putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",errorText);
        event.preventDefault();
    }
    else
    {
        if(country == 44 || country == "44") //China
        {
            if(!validid.cnid(ic_no))
            {
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
        else if(country == 214 || country == "214") //Taiwan
        {
            if(!validid.twid(ic_no) && !validid.twrc(ic_no))
            {
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
        else if(country == 98 || country == "98") //Hong Kong
        {
            if(!validid.hkid(ic_no))
            {
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
        else if(country == 196 || country == "196") //Singapore ->LINK : https://stackoverflow.com/questions/29743154/regular-expression-for-nric-fin-in-singapore
        {
            if(!ic_no.match(/^[STFG]\d{7}[A-Z]$/))
            {
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
        else if(country == 132 || country == "132") //Malaysia
        {
            if(!ic_no.match(/^([0-9][0-9])((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))([0-9][0-9])([0-9][0-9][0-9][0-9])$/))
            {
                putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",msgArray["_MAINJS_ICNO_WRONG_"]);
                event.preventDefault();
            }
        }
    }
}
function validateEditPasswordForm(msgArray)
{
    let errorArray = [];
    let canValidateLevel_1 = true;
    let combinationArray =[errorArray,canValidateLevel_1];

    let currentPassword = $('#field_1').val();
    let newPassword = $('#field_2').val();
    let confirmNewPaswword = $('#field_3').val();

    combinationArray = checkNullOrEmpty(combinationArray,currentPassword,msgArray["_MAINJS_ENTER_CURRENT_PASSWORD"]);
    combinationArray = checkNullOrEmpty(combinationArray,newPassword,msgArray["_MAINJS_ENTER_NEW_PASSWORD"]);
    combinationArray = checkNullOrEmpty(combinationArray,confirmNewPaswword,msgArray["_MAINJS_REENTER_NEW_PASSWORD"]);

    if(!combinationArray[1])
    {
        let errorText = msgArray["_MAINJS_ENTER_BELOW_INFO"] + " ：<br>";
        for(let counter = 0;counter < combinationArray[0].length ;counter++)
        {
            errorText += "("+(counter+1)+") " + combinationArray[0][counter]+"<br>";
        }

        putNoticeJavascript(msgArray["_MAINJS_ATTENTION"] + " ！",errorText);
        event.preventDefault();
    }
}



