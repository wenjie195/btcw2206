<?php

//EXECUTE THIS ONCE ONLY AND IS AFTER UPDATING THE REGISTER FUNCTION TO NEWEST WORKING 1 ONLY CAN EXECUTE THIS
//THE REGISTER FUNCTION MUST BE ABLE TO GIVE USER FREE ENTRY BTC AND GIVE INTRO BTC TO REFERRER IF REQUIREMENT MET (5 PPL AND IF ALREADY GIVEN LAST TIME WONT GIVE AGAIN)

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();
$userRows = getUser($conn);

echo "length: " . count($userRows) . ":: ";

$minIntroPpl = 0;
$introBtc = 0;
$introBtcTypeRows = getBtcType($conn," WHERE id = ? ORDER BY date_updated DESC LIMIT 1",array("id"),array(5),"i");
if($introBtcTypeRows) {
    $minIntroPpl = $introBtcTypeRows[0]->getMinPplIntro();
    $introBtc = $introBtcTypeRows[0]->getAmount();
}

foreach($userRows as $user) {
    $hasTakenEntryReward = getFreeBtc($conn," WHERE uid = ? AND btc_type_id = ? ",array("uid","btc_type_id"),array($user->getUid(),4),"si"); //MAKE IT ONE TIME ONLY (ACTUALLY IS NOT ONE TIME ONLY AND NOT EVERY 5 PERSON LOL)
    if(!$hasTakenEntryReward){
        $totalIntroBtcEarned = 0.05;

        $descriptionTH = "注册收益";
        $freeBtcEntryInTHId = insertIntoTransactionHistory($conn, $introBtc, 0, 0, 0,
            0,0,0,$introBtc,
            0, 0, $user->getUid(), 0, null,
            0, null, 4, $descriptionTH, 1);

        if (!$freeBtcEntryInTHId) {
//                echo "free btc in error";
            echo "对不起,未知错误发生了！！";
        } else {
            //now insert the free btc into free_btc table for everyday 0.2% drop. the above insert is just for transaction_history
            $newEntryFreeBtcTableId = insertDynamicData($conn, "free_btc", array("total_amount", "uid", "btc_type_id", "transaction_id"),
                array($introBtc, $user->getUid(), 4, $freeBtcEntryInTHId), "dsii");
            if (!$newEntryFreeBtcTableId) {
//                echo "insert free btc table error";
                echo "对不起,未知错误发生了！！";
            }else{
                if(!updateTransactionHistoryWithNewFreeBtcId($conn,$newEntryFreeBtcTableId,$freeBtcEntryInTHId)){
                    echo "对不起,未知错误发生了！！";
                }
            }

            //update user's current total free btc and capital btc into user_plan table
            if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = ? ",array("capital_btc","free_btc"),
                array(0,$totalIntroBtcEarned,
                    $user->getUid(),1),
                "ddsi")){
//                echo "update free and capital btc error";
                echo "对不起,未知错误发生了！！";
            }
        }
    }
}

foreach($userRows as $user) {
    $hasTakenIntroReward = getFreeBtc($conn," WHERE uid = ? AND btc_type_id = ? ",array("uid","btc_type_id"),array($user->getUid(),5),"si"); //MAKE IT ONE TIME ONLY (ACTUALLY IS NOT ONE TIME ONLY AND NOT EVERY 5 PERSON LOL)
    if(!$hasTakenIntroReward){
        $totalIntroBtcEarned = 0.0;

        $referralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($user->getUid()),"s");
        if($referralHistoryRows){
            $needLoopHowManyTimes = floor(count($referralHistoryRows) / $minIntroPpl);
            $userPlanRows = getUserPlan($conn," WHERE uid = ? && plan_id = ? ORDER BY date_created DESC LIMIT 1",array("uid","plan_id"),array($user->getUid(),1),"si");
            echo $user->getUid() . ": " . count($referralHistoryRows) . " need loop: " . $needLoopHowManyTimes ."</br>";
            if($needLoopHowManyTimes > 1){//MAKE IT ONE TIME ONLY (ACTUALLY IS NOT ONE TIME ONLY AND NOT EVERY 5 PERSON LOL)
                $needLoopHowManyTimes = 1;
            }
            for($i = 0; $i < $needLoopHowManyTimes; $i++){
                $totalIntroBtcEarned += $introBtc;

                $descriptionTH = "推荐5个会员收益";
                $freeBtcInTHId = insertIntoTransactionHistory($conn,$introBtc,0,0,0,
                    0,0,$userPlanRows[0]->getReturnedBtc(),$userPlanRows[0]->getFreeBtc() + $totalIntroBtcEarned,
                    $userPlanRows[0]->getCapitalBtc(),0,$user->getUid(),0,null,
                    0,null,5,$descriptionTH,1);

                if(!$freeBtcInTHId){
//                echo "free btc in error";
                    echo "对不起,未知错误发生了！！";
                }else{
                    //now insert the free btc into free_btc table for everyday 0.2% drop. the above insert is just for transaction_history
                    $newFreeBtcTableId = insertDynamicData($conn,"free_btc",array("total_amount","uid","btc_type_id","transaction_id"),
                        array($introBtc,$user->getUid(),5,$freeBtcInTHId),"dsii");
                    if(!$newFreeBtcTableId){
//                echo "insert free btc table error";
                        echo "对不起,未知错误发生了！！";
                    }else{
                        if(!updateTransactionHistoryWithNewFreeBtcId($conn,$newFreeBtcTableId,$freeBtcInTHId)){
                            echo "对不起,未知错误发生了！！";
                        }
                    }
                }
            }
        }

        if($totalIntroBtcEarned > 0){
            //update user's current total free btc and capital btc into user_plan table
            if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = ? ",array("capital_btc","free_btc"),
                array($userPlanRows[0]->getCapitalBtc(),$userPlanRows[0]->getFreeBtc() + $totalIntroBtcEarned,
                    $user->getUid(),1),
                "ddsi")){
//                echo "update free and capital btc error";
                echo "对不起,未知错误发生了！！";
            }
        }
    }
}

//foreach($userRows as $user) {
//    $referralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($user->getUid()),"s");
//    if($referralHistoryRows){
//        $needLoopHowManyTimes = floor(count($referralHistoryRows) / $minIntroPpl);
//        echo $user->getUid() . ": " . count($referralHistoryRows) . " need loop: " . $needLoopHowManyTimes ."</br>";
//    }
//}


$conn->close();