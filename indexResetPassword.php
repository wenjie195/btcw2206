<?php
require_once 'utilities/checkSessionFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require 'generalFunction.php';
    $modalDesc = resetPasswordInDB();
?>

<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/indexResetPassword.php/" />
        <meta property="og:title" content="<?= _reset_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Join us now!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Join us now!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, register, member, 会员注册, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
        
        <title><?= _reset_title ?></title>
        <link rel="canonical" href="https://btcworg.com/indexResetPassword.php/" />
  </head>
  <body>
   <?php 
        generateSimpleModal(); // Must be first in order to put modal boxes
       if($modalDesc)
       {
            if($modalDesc != _reset_success)
            {
               putNotice(_general_notice,$modalDesc);
            }
            else
            {
                putNoticeFromThisPageToThatPage(_general_notice,$modalDesc,"indexLogin.php");
            }
       }
       ?>
    <!--<div class="header-div width100 same-padding absolute">
    	<div class="logo-div">
        	<a href="bitcoin.php" class="a-hover"><img src="img/sherry/logo-white.png" class="logo-img opacity-hover" alt="bitcoin" title="bitcoin"></a>
        </div>
        <div class="menu-right-div">
        	<a class="menu-a login-a a-hover white-text-a yellow-hover" href="indexLogin.php">登录</a>
            <a class="menu-a register-a a-hover white-text-a yellow-hover" href="indexRegister.php">注册</a>
        </div>
    </div> -->
    <div id="firefly" class="firefly-class min-height">
    	<?php require 'mainNavbar.php';?>  
    	<div class="width100 same-padding">
    		<div class="login-width text-center white-text">
                <h1 class="register-title white-text"><?= _reset_reset ?></h1>
				<form class="register-form"  method="POST">  
                    	<div class="input-container1">
                        	<!--Fizo here-->
                            <img src="img/sherry/eye.png" class="input-icon button-icon-eye" id="seePassword1">
                			<input type="password" class="inputa clean2"  name="field_1" id="field_1" placeholder="<?= _reset_code ?>" >
							<img src="img/sherry/password.png" class="input-icon">
                        </div>
                        
                        <div class="input-container2">
                            <!--Fizo here-->
                            <img src="img/sherry/eye.png" class="input-icon button-icon-eye" id="seePassword2">                      
                        	<input type="password" class="inputa clean2 password-input" name="field_2"  id="field_2" placeholder="<?= _reset_keyin_new_pw ?>">
                        	<img src="img/sherry/password.png" class="input-icon">
						</div>
                        
                        <div class="input-container3">
                            <!--Fizo here-->
                            <img src="img/sherry/eye.png" class="input-icon button-icon-eye" id="seePassword3">                     
                        	<input type="password" class="inputa clean2 password-input"  name="field_3"  id="field_3" placeholder="<?= _reset_retype_new_pw ?>">
                        	<img src="img/sherry/password.png" class="input-icon">                        	
						</div>
                         <button class="register-button2 clean orange-hover button-width" name="" ><?= _reset_confirm ?></button>  
                        
          		</form>
             
            </div>
        </div>
    </div>    
    
   
       
    <?php require 'mainFooter.php';?>  
    <script>
        var img1 = document.getElementById('seePassword1');
        var img2 = document.getElementById('seePassword2');
        var img3 = document.getElementById('seePassword3');

        var inputPassword1 = document.getElementById('field_1');
        var inputPassword2 = document.getElementById('field_2');
        var inputPassword3 = document.getElementById('field_3');

        img1.onclick = function () 
        {
            if(inputPassword1.type == "password")
            {
                inputPassword1.type = "text";
            }
            else if(inputPassword1.type == "text")
            {
                inputPassword1.type = "password";
            }
        }
        img2.onclick = function () 
        {
            if(inputPassword2.type == "password")
            {
                inputPassword2.type = "text";
            }
            else if(inputPassword2.type == "text")
            {
                inputPassword2.type = "password";
            }
        }
        img3.onclick = function () 
        {
            if(inputPassword3.type == "password")
            {
                inputPassword3.type = "text";
            }
            else if(inputPassword3.type == "text")
            {
                inputPassword3.type = "password";
            }
        }
</script>
  </body>
</html>