<?php
require_once 'utilities/checkSessionFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require 'generalFunction.php';
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/forgotPassword.php/" />
        <meta property="og:title" content="<?= _forgotPassword_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency wallet platform for Bitcoin in the world. Join us now!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency wallet platform for Bitcoin in the world. Join us now!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency wallet platform, e-wallet, investment, invest, register, member, 会员登入,登录, 比特币, 比特钱包, 预约, 创富平台, 捐赠, 利益, 收益, etc">
       
        <title><?= _forgotPassword_title ?></title>
        <link rel="canonical" href="https://btcworg.com/forgotPassword.php/" />
  </head>
  <body>
     <?php 
        generateSimpleModal();
        forgotPassword();
       ?>
    <div id="firefly" class="firefly-class min-height">
        <?php require 'mainNavbar.php';?>  
    	<div class="width100 same-padding">
    		<div class="login-width text-center white-text">
                <h1 class="register-title white-text"><?= _forgotPassword_forgot2 ?></h1>
                <form class="register-form"  method="POST">  
                		<p class="numbering"><?= _forgotPassword_desc ?></p> 
                    	<div class="input-container1 ow-input-container1">
                			<input type="text" class="inputa clean2"  name="field_1" placeholder="<?= _forgotPassword_keyin_email ?>">
							<img src="img/sherry/email.png" class="input-icon ow-human-icon">
                        </div>
                        
                       <button class="register-button2 clean orange-hover button-width" name="loginButton" id="loginButton" ><?= _forgotPassword_confirm ?></button>  
             		   <p class="loginRegister"><?= _forgotPassword_not_yet ?><a class="loginRegisterLink orange-text-hover" href="indexRegister.php"><?= _forgotPassword_register_here ?></a></p>
            		   <p class="loginRegister edit-mtop"><a class="loginRegisterLink orange-text-hover" href="indexLogin.php"><?= _forgotPassword_login_again ?></a></p>                                  	                      
                </form>          
            </div>
        </div>
    </div>    
     
 
       
    <!-- <div class="loginDisplayBackground">
        <div class="loginWidth">
            <img src="img/loginBackground.png" class="loginBackground">
            <a class="removeLinksDecorations loginLogo" href="index.php">  
                BTCW
            </a>
        </div>
        <form class="loginWidth loginRightSide" method="POST">
            <h1 class="loginHeader">
                会员登录
            </h1>

            <div class="loginInput loginInputmargin">
                <input type="text" class="loginInputStyle1" name="field_1" placeholder="用户名">
            </div>
            <div class="loginInput">
                <input type="password" class="loginInputStyle2" name="field_2" placeholder="密码">
            </div>

            <div class="loginCheckbox">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        记下登录资料
                    </label>
                </div>
            </div>

                <button class="loginButton">
                    登录
                </button>
            
            <p class="loginRegister">还未有账号？<a class="loginRegisterLink" href="#####">点击这里注册</a></p>
        </form>
    </div> -->
    <?php require 'mainFooter.php';?>
  </body>
</html>