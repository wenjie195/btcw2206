<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{ 
     if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
          $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
     {
          header('Location:index.php');
          exit();
     }
     else
     {
          $uid = $_SESSION['uid'];
          $conn = connDB();
          $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
     }
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $emailORUsername = $_POST['field_1'];
     $error = FALSE;

     $userDetails = getUser($conn,
                    " WHERE email = ? OR username = ? OR uid = ? OR phone_no = ? ",
                    array("email","username","uid","phone_no"),
                    array($emailORUsername,$emailORUsername,$emailORUsername,$emailORUsername),
                    "ssss");
     if($userDetails != null) 
     {
          $uidSearch = $userDetails[0]->getUid();
          $sql_plan_A = "SELECT planType,btcSetToThisUser,dateCreated,isPromotion,promotionMultiplier FROM temporary_plan_b WHERE uid = ? ";
          if ($stmt = $conn->prepare($sql_plan_A)) 
          {
             $stmt->bind_param('s',$uidSearch);
     
             $stmt->execute();
     
             $stmt->store_result();
     
             $num_of_rows = $stmt->num_rows;
     
             $arrPlanType = array();
             $arrBtcSetToThisUser = array();
             $arrDateCreated = array();
             $arrIsPromo = array();
             $arrPromoMulti = array();

             $stmt->bind_result($planType,$btcSetToThisUser,$dateCreated,$isPromo,$promoMulti);
               
             $totalValueA = 0;
             $totalValueB = 0;
             
             while($stmt->fetch())
             {
                  array_push($arrPlanType,$planType);
                  array_push($arrBtcSetToThisUser,$btcSetToThisUser);
                  array_push($arrDateCreated,$dateCreated);
                  array_push($arrIsPromo,$isPromo);
                  array_push($arrPromoMulti,$promoMulti);

                   if($planType == 1)
                   {
                         $totalValueA += $btcSetToThisUser;
                   }
                   else if($planType == 2)
                   {
                         $totalValueB += $btcSetToThisUser;
                   }
             }
     
             /* free results */
             $stmt->free_result();
     
          }
          else
          {
               $error = TRUE;
          }
     }
}

function getMultiplier($sessionUID,$planType)
{
    $conn = connDB();
    $totalBTCTheyInvest = 0;
    $sql = "SELECT btcSetToThisUser,promotionMultiplier FROM temporary_plan_b WHERE uid = ? AND planType = ? ";
    if ($stmt = $conn->prepare($sql)) 
    {
        $stmt->bind_param('si',$sessionUID,$planType);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $numrows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($btcAssigned,$promoMulti);

        while($stmt->fetch())
        {
            // echo $btcAssigned." ".$promoMulti."<br>";
            // echo "<br>";

            $totalBTCTheyInvest += ($btcAssigned * $promoMulti);
            // $totalBTCTheyInvest += ($btcAssigned * $promoMulti);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if ($numrows > 0) 
        {
            return $totalBTCTheyInvest;
        } 
        else
        {
            return null;
        }
    }
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <title>ZCXC</title>
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();

  ?>
  <div id="firefly" class="firefly-class min-height">  
     <div class="width100 same-padding more-separate-margin-top edit-div">
          <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a> 
          <div class="clear"></div>
            <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Check Value Here</b></h4>
            <form class="register-form"  method="POST" >
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Email/Username/User ID</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_1" id="field_1" ></h4></td>
                  </tr>

                  <!-- <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">邮箱</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" placeholder="电邮" ></h4></td>
                  </tr> -->

                </table>
                <div class="clear"></div>
               <button class="register-button2 clean orange-hover inputb-button" name="insertValue" id="insertValue" >Check Insertion Value</button>

          </form>
     </div> 
      <div class="clear"></div>
      <?php 
          if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST')
          {
               if($userDetails != null)
               {
                    if($error == FALSE)
                    {
                    ?>
                     <div class="row">
                         <div class="col-md-2"></div>
                         <div class="col-md-8">
                              <h6 class="white-text mt-5"><?php echo "Username = ".$userDetails[0]->getUsername()."";?></h6>
                              <h6 class="white-text"><?php echo "Email = ".$userDetails[0]->getEmail()."";?></h6>
                              <h6 class="white-text"><?php echo "User ID = ".$userDetails[0]->getUid()."";?></h6>
                              <table class="table table-bordered text-center">
                                   <tr>
                                        <td class="text-light" colspan="2">Plan A</td>
                                        <td class="text-light" colspan="2">Plan B</td>
                                   </tr>
                                   <tr>
                                        <td class="text-light" >Original Plan A</td>
                                        <td class="text-light" >Plan A Multiplier</td>
                                        <td class="text-light" >Original Plan B</td>
                                        <td class="text-light" >Plan B Multiplier</td>
                                   </tr>
                                   <tr>
                                        <td class="text-light" >
                                             <?php
                                             $rounded = 0.001 * (int)($totalValueA*1000);
                                             if($rounded != 0)
                                             {echo $rounded;}
                                             else
                                             {echo "-";}
                                             ?>
                                        </td>
                                        <td class="text-light" >
                                             <?php
                                              $multiplyTotalValueA = $totalValueA * 3;
                                              $multiplyTotalValueRounded_A = 0.001 * (int)($multiplyTotalValueA*1000);
                                              if($multiplyTotalValueRounded_A != 0)
                                              {echo $multiplyTotalValueRounded_A;}
                                              else
                                              {echo "-";}
                                             ?>
                                        </td>
                                        <td class="text-light" >
                                             <?php
                                              $roundedB = 0.001 * (int)($totalValueB*1000);
                                              if($roundedB != 0)
                                              {echo $roundedB;}
                                              else
                                              {echo "-";}
                                             ?>
                                        </td>
                                        <td class="text-light" >
                                        <?php
                                              $planB = getMultiplier($userDetails[0]->getUid(),2);
                                              if($planB != 0)
                                              {echo $planB;}
                                              else
                                              {echo "-";}
                                             ?>
                                        </td>
                                   </tr>
                              </table>
                         </div>
                         <div class="col-md-2"></div>
                    </div>
                    <div class="row mt-5">
                         <div class="col-md-2"></div>
                         <div class="col-md-8">
                         <h6 class="white-text"><?php echo "User Investment History";?></h6>
                              <table class="table table-bordered text-center">
                                   <tr>
                                        <td class="text-light" >No</td>
                                        <td class="text-light" >Plan Type</td>
                                        <td class="text-light" >Date Inserted</td>
                                        <td class="text-light" >Amount BTC Entered</td>
                                        <td class="text-light" >Promotion</td>
                                        <td class="text-light" >Promotion Multiplier </td>
                                   </tr>
                                   <?php for($cnt = 0;$cnt < $num_of_rows;$cnt++)
                                        {
                                   ?>
                                    <tr>
                                        <td class="text-light" ><?php echo $cnt+1;?></td>
                                        <td class="text-light" >
                                             <?php 
                                             if($arrPlanType[$cnt] == 1)
                                             {
                                                  echo "A";
                                             }
                                             else
                                             {
                                                  echo "B";
                                             }
                                             ?>
                                        </td>
                                        <td class="text-light" ><?php echo date("g:i:s a",strtotime($arrDateCreated[$cnt]))." on ".date("d M y",strtotime($arrDateCreated[$cnt]))?></td>
                                        <td class="text-light" ><?php echo removeUselessZero($arrBtcSetToThisUser[$cnt]);?></td>
                                        <td class="text-light" >
                                         <?php 
                                             if($arrIsPromo[$cnt] == 1)
                                             {
                                                  echo "Yes";
                                             }
                                             else
                                             {
                                                  echo "No";
                                             }
                                             ?>
                                        </td>
                                        <td class="text-light" >
                                             <?php echo removeUselessZero($arrBtcSetToThisUser[$cnt])." x ".$arrPromoMulti[$cnt]." = ".$arrBtcSetToThisUser[$cnt] * $arrPromoMulti[$cnt];?>
                                        </td>
                                   </tr>
                                   <?php 
                                        }
                                   ?>
                              </table>
                         </div>
                         <div class="col-md-2"></div>
                    </div>
                    <?php
                    }
                    else
                    {
                         echo "
                              <div class='row mt-5'>
                                   <div class='col-md-2'></div>
                                   <div class='col-md-8'>
                                        <h3 class='text-light'>This user is not inserted</h3>
                                   </div>
                                   <div class='col-md-2'></div>
                              </div>
                              ";
                    }
               }
               else
               {
                    echo "
                    <div class='row mt-5'>
                         <div class='col-md-2'></div>
                         <div class='col-md-8'>
                              <h3 class='text-light'>No User With This Email/Username</h3>
                         </div>
                         <div class='col-md-2'></div>
                    </div>
                    ";
               }
          }

      ?>
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <?php require_once 'mainFooter.php';?>
  </body>
</html>