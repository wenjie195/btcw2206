

<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{ 
     if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
          $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
     {
          header('Location:index.php');
          exit();
     }
     else
     {
          $uid = $_SESSION['uid'];
          $conn = connDB();
          $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
        //   
        }
}

$additionalInfo = array();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     if(isset($_POST['field_1']))
     {
          $emailORUsername = $_POST['field_1'];
          $error = FALSE;

          $userDetails = getUser($conn,
                         " WHERE email = ? OR username = ? OR uid = ? ",
                         array("email","username","uid"),
                         array($emailORUsername,$emailORUsername,$emailORUsername),
                         "sss");

            if($userDetails) 
            {
                $getTop10 = getTop10ReferrerOfUser($conn,$userDetails[0]->getUid());
                if ($getTop10)
                {
                    $arrayEachTop10 = array();
                    for($nt = 0; $nt < count($getTop10) ;$nt++)
                    {
                        $eachTop10 = getUser($conn,
                        " WHERE uid = ? ",
                        array("uid"),
                        array($getTop10[$nt]),
                        "s");

                        array_push($arrayEachTop10,$eachTop10);
                    }

                    $additionalInfo = getTotalInvestedAmountOfBothPlans($conn,$arrayEachTop10);
                }
            }
     }
}

function getTotalInvestedAmountOfBothPlans($conn, $arrayEachTop10){
    $additionalInfo = array();
    for($i = 0; $i < count($arrayEachTop10); $i++){
        $tempUid = $arrayEachTop10[$i][0]->getUid();

        $totalInvestedPlanA = getSum($conn,"transaction_history","capital_btc_amount_in"," WHERE uid = ? AND plan_id = 1 AND btc_type_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ",array("uid"),array($tempUid),"s");
        $totalInvestedPlanB = getSum($conn,"transaction_history","capital_btc_amount_in"," WHERE uid = ? AND plan_id = 2 AND btc_type_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ",array("uid"),array($tempUid),"s");

        $tempInfo = array();
        $tempInfo['planA'] = removeUselessZero($totalInvestedPlanA);
        $tempInfo['planB'] = removeUselessZero($totalInvestedPlanB);

        array_push($additionalInfo,$tempInfo);
    }
    return $additionalInfo;
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <title>ZCXC</title>
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();

  ?>
  <div id="firefly" class="firefly-class min-height">  
     <div class="width100 same-padding more-separate-margin-top edit-div">
          <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a> 
          <div class="clear"></div>
         <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Check Referral (Upline)</b></h4>
            <form class="register-form"  method="POST" >
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Email/Username/User ID</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_1" id="field_1" ></h4></td>
                  </tr>

                  <!-- <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">邮箱</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" placeholder="电邮" ></h4></td>
                  </tr> -->

                </table>
                <div class="clear"></div>
               <button class="register-button2 clean orange-hover inputb-button" name="insertValue" id="insertValue" >Check Upline</button>

          </form>
          </div> 
          <div class="clear"></div>
          <div class="row mt-5">
               <div class="col-md-1"></div>
               <div class="col-md-10">
                   
                       

                        <?php
                        if(isset($arrayEachTop10))
                        {
                            ?>
                            <table class="table table-dark">
                                <tr>
                                    <td>No</td>
                                    <td>UID</td>
                                    <td>Username</td>
                                    <td>Email</td>
                                    <td>Plan A</td>
                                    <td>Plan B</td>
                                </tr>
                            <?php
                            for($counter = 0;$counter < count($arrayEachTop10);$counter++)
                            {
                                $getEachUser = $arrayEachTop10[$counter] ;
                                $getEachUserDetails = $getEachUser[0];

                            ?>
                                <tr>
                                    <td><?php echo $counter+1;?></td>
                                    <td><?php echo $getEachUserDetails->getUid();?></td>
                                    <td><?php echo $getEachUserDetails->getUsername();?></td>
                                    <td><?php echo $getEachUserDetails->getEmail();?></td>
                                    <td><?php echo $additionalInfo[$counter]['planA'];?></td>
                                    <td><?php echo $additionalInfo[$counter]['planB'];?></td>
                                </tr>
                            <?php  
                            }
                            ?>
                            </table>
                        <?php
                        }
                        ?>
               </div>
               <div class="col-md-1"></div>
          </div>
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <?php require_once 'mainFooter.php';?>
  </body>
</html>