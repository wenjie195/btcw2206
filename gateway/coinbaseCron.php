<?php
ini_set('max_execution_time', 0);
require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/coinbaseCredential.php';

$conn = connDB();

checkCoinbaseTransactionStatus($conn,$coinbaseClient,$coinbaseBtcAccount);

$conn->close();

function checkCoinbaseTransactionStatus($conn,$coinbaseClient,$coinbaseBtcAccount){
    // "3D3B8a2MRZgTVPgm5jqKp8d6TchkHh7cnh" receive address example
    // "c7dc26f1-8ecc-52ef-9188-174bad8407a7" address id example
    $applicationRows = getApplicationHistory($conn," WHERE coinbase_account_id IS NOT NULL AND status = 1 ");

    if($applicationRows){
        foreach ($applicationRows as $application){
            try{
                $coinbaseAddress = $coinbaseClient->getAccountAddress($coinbaseBtcAccount, $application->getCoinbaseAddressId());
                $transactions = $coinbaseClient->getAddressTransactions($coinbaseAddress);

                if(count($transactions) > 0){
                    $transaction = $transactions[0];//only get the latest 1 and no for loop because i only want one transaction(latest)
                    if($transaction->getStatus() === "completed"){
                        $planId = $application->getPlanId();

                        $amountInvested = 0;
                        if($application->getAmount() > 0){
                            $amountInvested = $application->getAmount();
                        }else{
                            $amountInvested = $transaction->getAmount()->getAmount();
                        }
                        $errorMsg = null;

                        if($planId === 1){
                            $errorMsg = investCapitalToPlan($conn,$amountInvested,$planId,$application->getId(),$application->getUid());
                        }else if($planId === 2){
                            $errorMsg = investCapitalToPlanB($conn,$amountInvested,$application->getId(),$application->getUid());
                        }

                        if(!isset($errorMsg)){
                            //success
                            if(!updateDynamicData($conn,"application_history","WHERE id = ? ",array("status"),array(2,$application->getId()),"ii")){
                                logError("ERROR updating application history status to complete. plan: $planId amount invested: $amountInvested btc  application ID: " . $application->getId() . " address ID: " . $coinbaseAddress->getId());
                            }
                        }else{
                            //failed
                            logError("ERROR investing btc. plan: $planId amount invested: $amountInvested btc  application ID: " . $application->getId() . " address ID: " . $coinbaseAddress->getId() . " ERROR MSG: $errorMsg");
                        }
                    }
                }
            }
            catch (Exception $ex) {
                logError("");
                logError("--------------------------------COINBASE API ERROR (PROBABLY NO SUCH ADDRESS ID FOUND START)----------------------------------------");
                logError("ERROR investing btc. plan: ".$application->getPlanId()." application ID: " . $application->getId() . " address ID: " . $application->getCoinbaseAddressId() . " ERROR MSG: $ex");
                logError("--------------------------------COINBASE API ERROR (PROBABLY NO SUCH ADDRESS ID FOUND END)----------------------------------------");
                logError("");
            }
        }
    }
}

function logError($errorMsg){
    $datetimeInGMT = new DateTime("now", new DateTimeZone("UTC"));
    $errorMsg = $datetimeInGMT->format('l, Y/m/d H:i:s') . " (UTC) ::: " . $errorMsg;
    error_log("$errorMsg\n", 3, "cron-errors.log");
}



