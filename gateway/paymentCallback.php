<?php
//todo 8) (WITHDRAW ON HOLD FIRST, BUT I ALREADY CREATED A SIMPLE FUNCTION TO WITHDRAW MONEY OUT IN gateway/111.php)ask michael for withdraw, need to do the same thing or just remain old method (because i think the client would want to control the withdrawal themselves and this is another different feature which will take some time too)
//Your callback url should also use the https protocol for greater security. This allows us to ensure we are sending callbacks to the correct server and prevents any parameters from being read as they travel over the internet. Additionally, the developer should reject all callbacks that do not originate from Coinbase’s network: 54.175.255.192/27

//from here https://github.com/coinbase/coinbase-php
//from here https://developers.coinbase.com/api/v2?javascript#notification-resource
//from here https://developers.coinbase.com/docs/wallet/notifications

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/coinbaseCredential.php';

logError("");
logError("---------------------------------------------------------------------------------------");
logError("request::: " . http_build_query($_REQUEST,'',', '));
logError("post::: " . http_build_query($_POST,'',', '));
logError("get::: " . http_build_query($_GET,'',', '));

$jsonData = file_get_contents('php://input');

//$jsonData = '{
//  "id": "6bf0ca21-0b2f-5e8a-b95e-7bd7eaccc338",
//  "type": "wallet:buys:completed",
//  "data": {
//    "id": "67e0eaec-07d7-54c4-a72c-2e92826897df",
//    "status": "completed",
//    "payment_method": {
//      "id": "83562370-3e5c-51db-87da-752af5ab9559",
//      "resource": "payment_method",
//      "resource_path": "/v2/payment-methods/83562370-3e5c-51db-87da-752af5ab9559"
//    },
//    "transaction": {
//      "id": "441b9494-b3f0-5b98-b9b0-4d82c21c252a",
//      "resource": "transaction",
//      "resource_path": "/v2/accounts/2bbf394c-193b-5b2a-9155-3b4732659ede/transactions/441b9494-b3f0-5b98-b9b0-4d82c21c252a"
//    },
//    "amount": {
//      "amount": "1.00000000",
//      "currency": "BTC"
//    },
//    "total": {
//      "amount": "10.25",
//      "currency": "USD"
//    },
//    "subtotal": {
//      "amount": "10.10",
//      "currency": "USD"
//    },
//    "created_at": "2015-01-31T20:49:02Z",
//    "updated_at": "2015-02-11T16:54:02-08:00",
//    "resource": "buy",
//    "resource_path": "/v2/accounts/2bbf394c-193b-5b2a-9155-3b4732659ede/buys/67e0eaec-07d7-54c4-a72c-2e92826897df",
//    "committed": true,
//    "instant": false,
//    "fee": {
//      "amount": "0.15",
//      "currency": "USD"
//    },
//    "payout_at": "2015-02-18T16:54:00-08:00"
//  },
//  "additional_data": {},
//  "user": {
//    "id": "f01c821e-bb35-555f-a4da-548672963119",
//    "resource": "user",
//    "resource_path": "/v2/users/f01c821e-bb35-555f-a4da-548672963119"
//  },
//  "account": {
//    "id": "8d5f086c-d7d5-58ee-890e-c09b3d8d4434",
//    "resource": "account",
//    "resource_path": "/v2/accounts/8d5f086c-d7d5-58ee-890e-c09b3d8d4434"
//  },
//  "delivery_attempts": 0,
//  "created_at": "2015-11-10T19:15:06Z",
//  "resource": "notification",
//  "resource_path": "/v2/notifications/6bf0ca21-0b2f-5e8a-b95e-7bd7eaccc338"
//}';
//
//$jsonData = '{"id":"b86a15d0-ce3a-589a-8c88-b94345698209","type":"wallet:addresses:new-payment","data":{"id":"c7dc26f1-8ecc-52ef-9188-174bad8407a7","address":"3D3B8a2MRZgTVPgm5jqKp8d6TchkHh7cnh","address_info":{"address":"3D3B8a2MRZgTVPgm5jqKp8d6TchkHh7cnh"},"name":"New Address 1112223334","created_at":"2019-06-04T03:51:37Z","updated_at":"2019-06-04T03:51:37Z","network":"bitcoin","uri_scheme":"bitcoin","resource":"address","resource_path":"/v2/accounts/c1b58596-7d84-5424-ae4f-ad579ae1df4f/addresses/c7dc26f1-8ecc-52ef-9188-174bad8407a7","warnings":[{"title":"Only send Bitcoin (BTC) to this address","details":"Sending any other digital asset, including Bitcoin Cash (BCH), will result in permanent loss.","image_url":"https://dynamic-assets.coinbase.com/e785e0181f1a23a30d9476038d9be91e9f6c63959b538eabbc51a1abc8898940383291eede695c3b8dfaa1829a9b57f5a2d0a16b0523580346c6b8fab67af14b/asset_icons/b57ac673f06a4b0338a596817eb0a50ce16e2059f327dc117744449a47915cb2.png"}],"warning_title":"Only send Bitcoin (BTC) to this address","warning_details":"Sending any other digital asset, including Bitcoin Cash (BCH), will result in permanent loss.","deposit_uri":"bitcoin:3D3B8a2MRZgTVPgm5jqKp8d6TchkHh7cnh"},"user":{"id":"f15a56c4-2af7-5aae-a191-74e3f09dcc5c","resource":"user","resource_path":"/v2/users/f15a56c4-2af7-5aae-a191-74e3f09dcc5c"},"account":{"id":"c1b58596-7d84-5424-ae4f-ad579ae1df4f","resource":"account","resource_path":"/v2/accounts/c1b58596-7d84-5424-ae4f-ad579ae1df4f"},"delivery_attempts":0,"created_at":"2019-06-04T07:53:07Z","resource":"notification","resource_path":"/v2/notifications/b86a15d0-ce3a-589a-8c88-b94345698209","additional_data":{"hash":"c42e1de8f7da52f7a2ee24a0e21dcdc7c42568d14d38a3ac16a65129712dd6ce","amount":{"amount":"1.00002000","currency":"BTC"},"transaction":{"id":"3378b87d-d224-511b-bf02-ef73f3974be1","resource":"transaction","resource_path":"/v2/accounts/c1b58596-7d84-5424-ae4f-ad579ae1df4f/transactions/3378b87d-d224-511b-bf02-ef73f3974be1"}}}';
//new pending payment
//$jsonData = '{"id":"92d67767-3540-5470-b094-761f00101fb0","type":"wallet:addresses:new-payment","data":{"id":"318b6115-3a9f-5abe-8bf0-25cf08c25e91","address":"34gmN2AJiNRqPgTx4JJTTjTWTzSvq6bq51","address_info":{"address":"34gmN2AJiNRqPgTx4JJTTjTWTzSvq6bq51"},"name":"CUSTOM-ID-6e7520ca71b735d399685cf9c8acf19e14da34d4","created_at":"2019-06-24T05:55:14Z","updated_at":"2019-06-24T05:55:14Z","network":"bitcoin","uri_scheme":"bitcoin","resource":"address","resource_path":"/v2/accounts/0f277603-9c03-55b3-bd2a-9abf4f75744f/addresses/318b6115-3a9f-5abe-8bf0-25cf08c25e91","warnings":[{"title":"Only send Bitcoin (BTC) to this address","details":"Sending any other digital asset, including Bitcoin Cash (BCH), will result in permanent loss.","image_url":"https://dynamic-assets.coinbase.com/e785e0181f1a23a30d9476038d9be91e9f6c63959b538eabbc51a1abc8898940383291eede695c3b8dfaa1829a9b57f5a2d0a16b0523580346c6b8fab67af14b/asset_icons/b57ac673f06a4b0338a596817eb0a50ce16e2059f327dc117744449a47915cb2.png"}],"warning_title":"Only send Bitcoin (BTC) to this address","warning_details":"Sending any other digital asset, including Bitcoin Cash (BCH), will result in permanent loss.","deposit_uri":"bitcoin:34gmN2AJiNRqPgTx4JJTTjTWTzSvq6bq51"},"user":{"id":"9dd9ce16-7036-5c2b-98ab-72fa5746146c","resource":"user","resource_path":"/v2/users/9dd9ce16-7036-5c2b-98ab-72fa5746146c"},"account":{"id":"0f277603-9c03-55b3-bd2a-9abf4f75744f","resource":"account","resource_path":"/v2/accounts/0f277603-9c03-55b3-bd2a-9abf4f75744f"},"delivery_attempts":0,"created_at":"2019-06-24T05:59:20Z","resource":"notification","resource_path":"/v2/notifications/92d67767-3540-5470-b094-761f00101fb0","additional_data":{"hash":"05eafae26ee81699af651efec15e9a53a3b6fd239740de15259cb29a205ce627","amount":{"amount":"0.02500000","currency":"BTC"},"transaction":{"id":"e574f10d-d58b-5bc4-8e68-a0e9a17f77df","resource":"transaction","resource_path":"/v2/accounts/0f277603-9c03-55b3-bd2a-9abf4f75744f/transactions/e574f10d-d58b-5bc4-8e68-a0e9a17f77df"}}}';

$postDatacoinbase = json_decode($jsonData, true);

//decode with from here http://freeonlinetools24.com/json-decode
logError ("file_get_contents-whole-json::: " . json_encode($jsonData));

//logError ("file_get_contents::: " . $postDatacoinbase);

if($postDatacoinbase && $postDatacoinbase['data'] && $postDatacoinbase['data']['id']){
    $conn = connDB();
    $addressId = $postDatacoinbase['data']['id'];

    $amount = 0;
    if($postDatacoinbase['additional_data'] && $postDatacoinbase['additional_data']['amount'] && $postDatacoinbase['additional_data']['amount']['amount']){
        $amount = $postDatacoinbase['additional_data']['amount']['amount'];
    }

    getAddressDetails($conn,$coinbaseClient,$coinbaseBtcAccount,$addressId,$amount);
    $conn->close();
}

logError("---------------------------------------------------------------------------------------");
logError("");

//can try this from here https://stackoverflow.com/questions/16114839/coinbase-address-callbacks-not-working
function getAddressDetails($conn,$coinbaseClient,$coinbaseBtcAccount,$addressId,$amount){
    $addressDetails = $coinbaseClient->getAccountAddress($coinbaseBtcAccount, $addressId);
    $accountId = $addressDetails->getAccountId();
    $addressId = $addressDetails->getId();
    $address = $addressDetails->getAddress();

    if(!updateDynamicData($conn,"application_history"," WHERE coinbase_address_id = ? AND coinbase_receive_address = ? ",
        array("coinbase_account_id","amount"),array($accountId,$amount,$addressId,$address),"sdss")){
        logError("Failed to update application history with account id of: $accountId addressId: $addressId address: $address");
    }
}

function logError($errorMsg){
    $datetimeInGMT = new DateTime("now", new DateTimeZone("UTC"));
    $errorMsg = $datetimeInGMT->format('l, Y/m/d H:i:s') . " (UTC) ::: " . $errorMsg;
    error_log("$errorMsg\n", 3, "cron-errors.log");
}