<?php
//from here https://github.com/coinbase/coinbase-php
//from here https://developers.coinbase.com/api/v2?javascript#notification-resource
//from here https://developers.coinbase.com/docs/wallet/notifications

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/coinbaseCredential.php';

use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Resource\Transaction;
use Coinbase\Wallet\Value\Money;

$conn = connDB();

$amountInBtc = 0.00000000001;
//$amountInBtc = 0.000275;
$address = '1F9e38dgY3UqWzuPawTZyC984e3nJTY7dZ';
$desc = 'Withdrawal from https://btcworg.com';
$errorMsg = withdrawBtcFromCoinbase($conn,$coinbaseClient,$coinbaseBtcAccount,$amountInBtc,$address,$desc);
if(isset($errorMsg)){
    echo "Error: $errorMsg";
}

$conn->close();

function withdrawBtcFromCoinbase($conn,$coinbaseClient,$coinbaseBtcAccount,$amountInBtc,$address,$desc){
//    Amount in btc cannot be below 0.0001 BTC (minimum is 0.0001 BTC)
    try{
        $exchangeRates = $coinbaseClient->getExchangeRates("BTC");
        $usdRate = $exchangeRates['rates']['USD'];

        $amountInUsd = removeUselessZero(bcmul($amountInBtc,$usdRate,25));
        $amountInUsd = sprintf("%0.2f",$amountInUsd);

//        if($amountInUsd < 20){
//            return "withdrawal amount must be more than 20 USD";
//        }

        $transaction = Transaction::send([
            'toBitcoinAddress' => $address,
            'amount'           => new Money($amountInUsd, CurrencyCode::USD),
            'description'      => $desc
        ]);

        //$transaction = Transaction::send();
        //$transaction->setToBitcoinAddress('here btc address to transfer');
        //$transaction->setAmount(new Money(0.0010, CurrencyCode::BTC));
        //$transaction->setDescription('this is optional');

        try {
            $coinbaseClient->createAccountTransaction($coinbaseBtcAccount, $transaction);

            //do database withdraw function
            return null;
        }
        catch(Exception $e) {
            //  error example: https://developers.coinbase.com/docs/wallet/error-codes
            //  Invalid amount
            //  The transaction fee has expired.
            //  You must enter a valid amount
            //  Please enter a valid email or Bitcoin address
            // Amount is below the minimum (0.0001 BTC) required to send on-blockchain.
            return $e->getMessage();
        }
    }catch (Exception $e){
        return $e->getMessage();
    }
}

