<?php
//from here https://github.com/coinbase/coinbase-php
//from here https://developers.coinbase.com/api/v2?javascript#notification-resource
//from here https://developers.coinbase.com/docs/wallet/notifications

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/coinbaseCredential.php';

$exchangeRates = $coinbaseClient->getExchangeRates("BTC");
$usdRate = $exchangeRates['rates']['USD'];

$conn = connDB();
if(!updateDynamicData($conn,"btc_usd_rate"," WHERE id = 1 ",array("usd","rate"),array(rewrite($usdRate),rewrite($usdRate)),"dd")){
    logError("update usd rate failed");
}
$conn->close();

function logError($errorMsg){
    $datetimeInGMT = new DateTime("now", new DateTimeZone("UTC"));
    $errorMsg = $datetimeInGMT->format('l, Y/m/d H:i:s') . " (UTC) ::: " . $errorMsg;
    error_log("$errorMsg\n", 3, "cron-errors.log");
}

