<?php
session_start();
//from here https://github.com/coinbase/coinbase-php
//from here https://developers.coinbase.com/api/v2?javascript#notification-resource
//from here https://developers.coinbase.com/docs/wallet/notifications

$result = array();
$result['isSuccess'] = "1";

if(empty($_SESSION['uid'])){
    $result['isSuccess'] = "0";
    $result['error'] = "no uid provided";
    echo json_encode($result);
    exit();
}

require_once dirname(__FILE__) . '/coinbaseCredential.php';
use Coinbase\Wallet\Resource\Address;

//foreach ($accounts as $a){
//    echo "id: " . $a->getId();
//    echo "</br>";
//    echo "name ". $a->getName();
//}

$address = new Address([
    'name' => $_SESSION['uid']
]);
$coinbaseClient->createAccountAddress($coinbaseBtcAccount, $address);

//$addresses = $client->getAccountAddresses($accounts[0],array("8adb1093-885a-5b85-8f12-1478547e3c03"));
//
//$count = 0;
//foreach ($addresses as $address){
//    $count ++;
//
//    echo "</br>";
//    echo "No.: $count";
//    echo "</br>";
//    echo "id " . $address->getId();
//    echo "</br>";
//    echo "address name " . $address->getName();
//    echo "</br>";
//    echo "address " . $address->getAddress();
//    echo "</br>";
//}
//$address1 = $client->getAccountAddress($accounts[0], "519bbd81-3203-587c-b7a2-b40b5889c883");

$result['address'] = $address->getAddress();
$result['addressId'] = $address->getId();
echo json_encode($result);

