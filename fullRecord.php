<?php
//Start the session
session_start();

//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
}else{
    $uid = $_SESSION['uid'];
}

require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'generalFunction.php';
require_once 'utilities/calculationFunction.php';
require_once 'utilities/databaseFunction.php';

$conn = connDB();
$userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
//echo $userRows[0]->getDateCreated();
//echo $userRows[0]->getUsername();

//$newTableId = insertDynamicData($conn,"temp_invest_table",array("uid","amount"),array("whahaha id",123.2222),"sd");
//if($newTableId){
//    echo "newId: ".$newTableId;
//}else{
//    echo "error";
//}

////update based on id
//if(updateDynamicData($conn,"temp_invest_table"," WHERE id = ? ",array("uid","amount"),array("uid changed",555.22,2),"sdi")){
//    echo "updated";
//}else{
//    echo "update error";
//}

////update all
//if(updateDynamicData($conn,"temp_invest_table",null,array("uid","amount"),array("uid changed",555.22),"sd")){
//    echo "updated";
//}else{
//    echo "update error";
//}

////update with or
//if(updateDynamicData($conn,"temp_invest_table"," WHERE uid = ? OR uid = ? OR amount = ?",array("uid","amount"),array("yay",888.8888888,"333","555",1.0),"sdssd")){
//    echo "updated";
//}else{
//    echo "update error";
//}

$errorMsg = null;
$successMsg = null;
$amount = 0.025;
$planId = 1;
$errorMsg = investCapitalToPlan($conn,$amount,$planId,null);
if(!isset($errorMsg)){
    $amountInvestedString = removeUselessZero($amount) . " BTC!";
    $successMsg = _profile_donate_success."$amountInvestedString";
}

$conn->close();

//if($_SERVER["REQUEST_METHOD"] == "POST"){
//    $conn = connDB();
//
//    if(isset($_POST['invest-form'])){
//        $type = "invest";
//        $hasError = false;
//
//        if(!isPostVariableEmpty("$type-btc-amount")){
//            $uid = $_SESSION["uid"];
//            $amount = $_POST["$type-btc-amount"];
//
//            $sql = "INSERT INTO temp_invest_table (uid, amount) VALUES (?, ?)";
//
//            if ($stmt = $conn->prepare($sql)) {
//
//                $stmt->bind_param("sd",$_SESSION["uid"],$_POST["$type-btc-amount"]);
//
//                if(!$stmt->execute()){
////                    $errorMsg = "Error: ".$stmt->error;
//                    $errorMsg = "对不起,未知错误发生了！！";
//                }
//
//                $tempInvestTableId = $stmt->insert_id;
//
//                $stmt->close();
//            }else{
////                $errorMsg = "Error: ".$conn->error;
//                $errorMsg = "对不起,未知错误发生了！！";
//            }
//
//            if(!isset($errorMsg)){
//                $successMsg = _profile_donate_success;
//            }
//        }else{
//            $hasError = true;
//        }
//
////
////        if(!isPostVariableEmpty("$type-btc-amount")){
////            $amount = $_POST["$type-btc-amount"];
////
////            $userPlanRows = getUserPlan($conn," WHERE id = ? ORDER BY date_created DESC LIMIT 1",array("id"),array("1"),"i");
//////            $userPlan = getUserPlan($conn);
////            if($userPlanRows !== null){
////                $planRows = getPlan($conn," WHERE id = ? OR id = ? ORDER BY date_created DESC LIMIT 2",array("id","id"),array($userPlanRows[0]->getPlanId(),"2"),"ii");
////                if($planRows !== null){
//////                    if($amount + $userPlan->getCapitalBtc() > $plan->getMaxBtc())//todo get userTopupcriteria
////                    $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
////                    echo "should have result: " . $userPlanRows[0]->getDateCreated();
////                    echo "should have result: " . $planRows[0]->getInvestmentReturn();
////                    echo "should have result: " . $planRows[1]->getInvestmentReturn();
////                    echo "should have result: " . $userRows[0]->getDateCreated();
////                }else{
////                    $hasError = true;
////                }
////            }else{
////                $errorMsg = "对不起,未知错误发生了！！";
////            }
////
////        }else{
////            $hasError = true;
////        }
////
//
//        if($hasError){
//            $errorMsg = "请写下捐赠数量";
//        }
//    }else if(isset($_POST['withdraw-form'])){
//
//    }
//
//    $conn->close();
//}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/fullRecord.php" />
        <meta property="og:title" content="BTCW - History" />
        <meta name="description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

        <title>BTCW - History</title>
        <link rel="canonical" href="https://btcworg.com/fullRecord.php" />
  </head>
  <body>
  <?php require 'mainNavbar.php';
    generateInputModalForInvest("invest");
    generateInputModalForInvest("withdraw");
  ?>
  
 
    <div class="width100 same-padding table-width-div profile-divider4">
	<h4 class="btcw-h4 weight-700 plan-a-h4">总收益完整记录</h4>    
   	 <table class="table100">
    	<thead class="orange-thead">
        	<tr class="orange-tr">
            	<th class="th1 th-1 white-text">日期</th>
                <th class="th2 white-text">记录</th>
                <th class="th3 white-text">增加(BTC)</th>
                <th class="th4 white-text">减少(BTC)</th>
                <th class="th5 white-text">免费BTC总数</th>
                <th class="th6 white-text">非免费BTC总数</th>
                <th class="th7 th-7 white-text">所有BTC总数</th>
            </tr>
        </thead>            
        	<tr class="transparent-tr">
            	<td class="th1">11/2/2019</td>
                <td class="th2">社区-小芳的收益</td>
                <td class="th3 color-downline">0.05</td>
                <td class="th4">-</td>
                <td class="th5">0.1</td>
                <td class="th6">299.95</td>
                <td class="th7">300.00</td>
            </tr>            
        	<tr class="transparent-tr">
            	<td class="th1">11/1/2019</td>
                <td class="th2">推荐5个会员收益</td>
                <td class="th3 color-freebtc">0.05</td>
                <td class="th4">-</th>
                <td class="th5">0.1</th>
                <td class="th6">299.90</th>
                <td class="th7">299.95</th>
            </tr>            
        	<tr class="transparent-tr">
            	<td class="th1">5/1/2019</td>
                <td class="th2">计划A收益</td>
                <td class="th3 color-reward">199.90</td>
                <td class="th4">-</td>
                <td class="th5">0.05</td>
                <td class="th6">299.85</td>
                <td class="th7">299.90</td>
            </tr>	
        	<tr class="transparent-tr">
            	<td class="th1">5/1/2019</td>
                <td class="th2">捐赠计划A</td>
                <td class="th3 color-invest">99.95</td>
                <td class="th4">-</td>
                <td class="th5">0.05</td>
                <td class="th6">99.95</td>
                <td class="th7">100.0</td>
            </tr> 
        	<tr class="transparent-tr">
            	<td class="th1">5/1/2019</td>
                <td class="th2">注册收益</td>
                <td class="th3 color-freebtc">0.05</td>
                <td class="th4">-</td>
                <td class="th5">0.05</td>
                <td class="th6">0</td>
                <td class="th7">0.05</td>
            </tr>            
               
    	</table>
        <div class="color-div width100">
            <div class="circle-span color-invest-div"></div> <span class="color-span">捐赠</span>
            <div class="circle-span color-free-btc-div"></div> <span class="color-span">免费BTC</span>
            <div class="circle-span color-reward-div"></div> <span class="color-span">捐赠收益</span>
            <div class="circle-span color-recommend-div"></div> <span class="color-span">推荐会员收益</span>        
            <div class="circle-span color-downline-div"></div> <span class="color-span">社区比例收益</span>
            <div class="circle-span color-other-div"></div> <span class="color-span">其他收益</span>             
        </div> 
     </div>            
 
     <div class="width100 same-padding profile-divider">
        	<h4 class="btcw-h4 text-center weight-700 width60">用户只允许在30天内的第10天，第20天或第30天提取金额和需要缴付0%手续费。用户也可以选择投入本金随时提取收益但需缴付18%手续费。</h4>
        </div>  
  	 <div class="clear"></div>



    <div class="width100 same-padding profile-divider3 overflow-hidden">
            <div class="fill-small-space"></div>
            <div class="yellow-box yellow-box-width yellow-box-hover white-text text-center invest-now">捐赠</div>
            <div class="fill-small-space fill-small-space-m"></div>
            <div class="fill-small-space fill-small-space-m"></div>
            <a class="a-hover"><div class="yellow-line-box yellow-box-width yellow-line-box-hover white-text text-center withdrawal-btn">提取收益</div></a>
            <div class="fill-small-space"></div>
    </div>  
    <div class="clear"></div> 
	<div class="width100 element-div extra-padding-bottom profile-divider">
        	<img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
    </div>     
    

	
        <!--- Modal --->
        <div id="myModal2" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width">
            <span class="close2 ow-close2">&times;</span>
            <p class="qr-img"><img src="img/sherry/invest.png" class="qr" alt="捐赠" title="捐赠"></p>
            
                <form class="register-form width100 same-padding"  method="POST">
                	    <h4 class="btcw-h4 form-h4"><b class="weight-700">捐赠</b></h4>  
                    	<div class="input-container1">
                            <select class="inputa clean2"  name="field_1">
                    			<option disabled selected>选择要捐赠的计划</option>
                    			<option>计划A</option>
                    			<option>计划B</option>                                
                			</select>
							<img src="img/sherry/plan.png" class="input-icon">
                        </div>
                        
                        <div class="input-container2">
                        	<input type="number" class="inputa clean2"  name="field_2" placeholder="输入捐赠金额">
                        	<img src="img/sherry/invest-amount.png" class="input-icon">
						</div>
                        
                        <div class="input-container3">	
                            <select class="inputa clean2"   name="field_3">
                    			<option disabled selected>捐赠备注</option>
                    			<option>想增加收益</option>                                
                    			<option>想加入此计划</option>
                    			<option>想提取收益</option>                                 
                    			<option>其他</option>
                			</select>
                        	<img src="img/sherry/remark.png" class="input-icon">
						</div>
                        <textarea class="textarea-reason" name="" placeholder="如果捐赠备注选其他，需填写"></textarea>
						<div class="width100 overflow">
                        	<div class="fill-up-space1 modal-fill-space-1"></div>
                        	<button class="register-button2 clean orange-hover invest-btn" name="loginButton" id="loginButton" >确认捐赠</button>
                            <div class="fill-up-space1 modal-fill-space-1"></div>
                         </div>
                         <div class="clear"></div>
 						<div class="width100 overflow">
                        	<div class="fill-up-space1 modal-fill-space-1"></div>                        
                        	<button class="register-button2 clean orange-line-hover withdrawal-btn withdraw-btn" name="loginButton" id="loginButton" >确认捐赠并提取收益</button>
                            <div class="fill-up-space1 modal-fill-space-1"></div>
                        </div>                            
               </form>            
          </div>
        </div>
     
        <!--- Withdrawal Modal --->
        <div id="myModal3" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width">
            <span class="close3 ow-close2 close-style">&times;</span>
           
            
                <form class="register-form width100 same-padding"  method="POST">
                	    <h4 class="btcw-h4 form-h4 ow-form-h4"><b class="weight-700">提取收益</b></h4>  
            <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table withdraw-table">
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">BTC总额</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">300</h4></td>
              </tr>
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">非免费BTC总额</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">299.90</h4></td>
              </tr>
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">免费BTC总额</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">0.1</h4></td>
              </tr>
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">捐赠收益</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">199.90</h4></td>
              </tr>
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">非免费BTC提取总额</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">
                	<input type="" class="inputa clean2 inputb"  name="field_1" placeholder="输入提取总额">
                    </h4>
                </td>
              </tr> 
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">免费BTC提取总额</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">
                	<input type="" class="inputa clean2 inputb"  name="field_2" placeholder="输入提取总额">
                    </h4>
                </td>
              </tr>              
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">手续费</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"></h4>
                </td>
              </tr>
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">最终可提取BTC总额</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"> </h4>
                </td>
              </tr>              
                                         
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">提取收益原因</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top">
                	<h4 class="btcw-h4 edit2-h4">
                            <select class="inputa clean2 padding-0"   name="field_3">
                    			<option disabled selected>提取收益原因</option>
                    			<option>想领取定时收益</option>                                
                    			<option>想退出此计划</option>
                    			<option>急需用钱</option>                                 
                    			<option>其他</option>
                			</select>
                   </h4>
                </td>
              </tr>                 
        
            </table>
            <textarea class="textarea-reason textarea-reason2" name="" placeholder="如果捐赠备注选其他，需填写"></textarea>
						<div class="width100 overflow">
                        	<div class="fill-up-space1 modal-fill-space-1"></div>
                        	<button class="register-button2 clean orange-hover invest-btn" name="loginButton" id="loginButton" >确认提取</button>
                            <div class="fill-up-space1 modal-fill-space-1"></div>
                         </div>                           
           </form>            
          </div>
        </div>


    
  <?php require 'mainFooter.php';?>

  <!-- ****************************************************MODALS***********************************************************-->
  <script>
      function randomFunc(type) {
          // Get the modal
          var modal = document.getElementById(type + '-modal');

          // Get the <span> element that closes the modal
          var span = document.getElementsByClassName('closeNoticeModal')[0];
          var span1 = document.getElementsByClassName('closeNoticeModal')[1];

          modal.style.display = 'block';

          // When the user clicks on <span> (x), close the modal
          span.onclick = function() {
              modal.style.display = 'none';
          };
          span1.onclick = function() {
              modal.style.display = 'none';
          };

          // When the user clicks anywhere outside of the modal, close it
          window.onclick = function(event) {
              if (event.target == modal) {
                  modal.style.display = 'none';
              }
          }
      }

      <?php
          if(isset($errorMsg) || isset($successMsg)){
              promptAlertMsg($errorMsg,$successMsg);
          }
      ?>
  </script>
  
         <script>
			// Get the modal
			var modal2 = document.getElementById('myModal2');
			
			// Get the button that opens the modal
			var btn2 = document.getElementsByClassName("invest-now")[0];
	
			
				
			// Get the <span> element that closes the modal
			var span2 = document.getElementsByClassName("close2")[0];


			// Get the modal
			var modal3 = document.getElementById('myModal3');
			
			// Get the button that opens the modal
			var btn3 = document.getElementsByClassName("withdrawal-btn")[0];
			
			// Get the button that opens the modal
			var btn3a = document.getElementsByClassName("withdrawal-btn")[1];			
			// Get the <span> element that closes the modal
			var span3 = document.getElementsByClassName("close3")[0];





			
			// When the user clicks the button, open the modal 
			btn2.onclick = function() {
			  modal2.style.display = "block";
			}

						
			// When the user clicks on <span> (x), close the modal
			span2.onclick = function() {
			  modal2.style.display = "none";
			}



			
			// When the user clicks the button, open the modal 
			btn3.onclick = function() {
			  modal3.style.display = "block";
			}
			btn3a.onclick = function() {
			  modal2.style.display = "none";				
			  modal3.style.display = "block";
			}			
			// When the user clicks on <span> (x), close the modal
			span3.onclick = function() {
			  modal3.style.display = "none";
			}


			
			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal2) {
				modal2.style.display = "none";
			  }
			  if (event.target == modal3) {
				modal3.style.display = "none";
			  }		  
			  		  
			}
		</script>  
    
  
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
            }
    
            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
            
            /*close when you click outside of the sideNav*/
            $('body').on('click touchstart', function(){
              if( parseInt( $('#mySidenav').css('width') ) > 0 ){
                closeNav();
              }
            });
           /*$('body').on('click', function(){
           if( parseInt( $('#mySidenav').css('width') ) > 0 ){
           closeNav();
           }
           });*/
	</script>
  </body>
</html> 