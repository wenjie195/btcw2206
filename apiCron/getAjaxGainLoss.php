<?php

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

$allResultArray = array();
$allResultArray['huobi'] = getGainLossOfPlatform($conn,1);
$allResultArray['binance'] = getGainLossOfPlatform($conn,2);
$allResultArray['okex'] = getGainLossOfPlatform($conn,3);
//$allResultArray['bitstamp'] = getGainLossOfPlatform($conn,4);
//$allResultArray['poloniex'] = getGainLossOfPlatform($conn,5);
$allResultArray['bitfinex'] = getGainLossOfPlatform($conn,6);
$allResultArray['coinbene'] = getGainLossOfPlatform($conn,7);
$allResultArray['bitforex'] = getGainLossOfPlatform($conn,8);

$conn->close();

echo json_encode($allResultArray);

//**********************************************FUNCTIONS****************************************************/
function getGainLossOfPlatform($conn,$apiType){
    $gainLossRows = getBtcTradesGainLoss($conn," WHERE api_type = ? ORDER BY id DESC LIMIT 1 ",array("api_type"),array($apiType),"i");

    if($gainLossRows){
        //same format as bitstamp and others
        return $gainLossRows;
    }else{
        return array();
    }
}