<?php
require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

//do around every 10seconds like that
for($i = 0; $i < 13; $i++){
    if($i === 0){
        $delaySeconds = 0;
    }else{
        $delaySeconds = 4;//sleep function doesnt care about decimal points
    }
    delayCallFunction($conn,$delaySeconds);
}

$conn->close();

function delayCallFunction($conn,$delaySeconds){
    if($delaySeconds != 0){
        sleep($delaySeconds);
    }

    //Huobi
    getPlatformLatestSell($conn,1);

    //Binance
    getPlatformLatestSell($conn,2);

    //OKCoin
    getPlatformLatestSell($conn,3);

//    //Bitstamp
//    getPlatformLatestSell($conn,4);
//
//    //Poloniex
//    getPlatformLatestSell($conn,5);

    //Bitfinex
    getPlatformLatestSell($conn,6);

    //Coinbene
    getPlatformLatestSell($conn,7);

    //Bitforex
    getPlatformLatestSell($conn,8);
}

function getPlatformLatestSell($conn,$apiType){
    $tradesRow = getBtcTrades($conn, " WHERE api_type = ? AND direction = 2 ORDER BY timestamp DESC, api_id DESC LIMIT 1 ", array("api_type"),array($apiType),"i");

    if($tradesRow){
        $latestSell = $tradesRow[0]->getPrice();
        createGainLossData($conn,$apiType,$latestSell);
    }
}

function createGainLossData($conn,$apiType,$latestSell){

    $isGain = getIsGainOrLoss();
    $percent = getPercentage($isGain);
    $latestBuy = $latestSell;

    if($isGain){
        //if profit/isgain means buy is lower than sell, so minus
        $latestBuy = bcsub($latestSell,bcdiv(bcmul($latestSell,$percent,25),100,25),25);
    }else{
        $latestBuy = bcadd($latestSell,bcdiv(bcmul($latestSell,$percent,25),100,25),25);
    }

    insertIntoGainLossTable($conn,$apiType,$latestBuy,$latestSell,$percent,$isGain);
}

function getPercentage($isGain){
    if($isGain){
        $minGain = 25;//cant use decimal, later need divide by 100
        $maxGain = 350;
        return (mt_rand($minGain,$maxGain) / 100);
    }else{
        $minLoss = 75;
        $maxLoss = 125;
        return (mt_rand($minLoss,$maxLoss) / 100);
    }
}

function getIsGainOrLoss(){
    //from here https://www.geeksforgeeks.org/php-mt_rand-function/
    //inclusive

    //gainrate is 70 - 80%
    //loss rate is 20 - 30%
    $gainRate = 70 + mt_rand(0, 10);//min,max

    $currentRate = mt_rand(0, 100);

    if($currentRate >= 0 && $currentRate <= $gainRate){
        return true;
    }else{
        return false;
    }
}

function insertIntoGainLossTable($conn,$apiType,$latestBuy,$latestSell,$percent,$isGain){
    insertDynamicData($conn,"btc_trades_gain_loss",array("api_type","latest_buy","latest_sell","percent","is_gain"),array($apiType,$latestBuy,$latestSell,$percent,$isGain),"idddi");
}