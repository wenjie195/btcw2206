<?php

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

$allResultArray = array();
$allResultArray['live_trades'] = getTrades($conn);

$conn->close();

echo json_encode($allResultArray);

//**********************************************FUNCTIONS****************************************************/
function getTrades($conn){
    $tradesRow = getBtcTrades($conn," ORDER BY timestamp DESC, api_id DESC LIMIT 10 ");

    if($tradesRow){
        //same format as bitstamp and others
        return $tradesRow;
    }else{
        return array();
    }
}