<?php
require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../vendor/autoload.php';
//request api feature from here https://guzzle.readthedocs.io/en/stable/
//from here https://stackoverflow.com/questions/9802788/call-a-rest-api-in-php

//$apiType List
// 1 = Huobi,
// 2 = Binance,
// 3 = OKCoin/Okex,
// 4 = Bitstamp,
// 5 = Poloniex,
// 6 = Bitfinex
// 7 = Coinbene,
// 8 = Bitforex //has 10second timeout (it wont allow us to call too frequently)

//can use order by date_created and then follow by api_id (since their id i checked (1 to 6) are all incremental)
//*poloniex trade is not a lot sometimes
//*bitforex can only be called every 10 seconds
$size = 2;

$conn = connDB();

//hacky way to run cron job to fetch 6 platforms' bitcoin trade data every 11-15 seconds
//dont know server will explode or not

//cant really do it 12 times for 5 sec zhun zhun because scare will affect the next minute's job
for($i = 0; $i < 8; $i++){
    if($i === 0){
        $delaySeconds = 0;
    }else{
        $delaySeconds = 5;//sleep function doesnt care about decimal points
    }
    delayCallFunction($conn,$size,$delaySeconds);
}

$conn->close();

function delayCallFunction($conn,$size,$delaySeconds){
    if($delaySeconds != 0){
        sleep($delaySeconds);
    }

    getHuobiLiveTrade($conn,$size);// 1
    getBinanceLiveTrade($conn,$size);// 2
    getOkexLiveTrade($conn,$size);// 3
//    getBitstampLiveTrade($conn);// 4
//    getPoloniexLiveTrade($conn);// 5
    getBitfinexLiveTrade($conn,$size);// 6
    getCoinbeneLiveTrade($conn,$size);// 7
    getBitforexLiveTrade($conn,$size);// 8
}

function getHuobiLiveTrade($conn,$size){
//    https://huobi.readme.io/reference
    $apiType = 1;

    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', 'https://api.huobi.com/market/history/trade?symbol=btcusdt&size='.$size);

    if($res->getStatusCode() === 200 && $res->getBody()){
        $recentTradesRows = json_decode($res->getBody(), true);

        if($recentTradesRows['status'] === "error"){
            logError("Huobi api got error ::: err code: " . $recentTradesRows['err-code'] . " ::: Error msg: " . $recentTradesRows['err-msg']);
            return;
        }

        foreach ($recentTradesRows['data'] as $allTradeData){
            $thisTrade = $allTradeData['data'][0];
            $apiId = $thisTrade['id'];
            $amount = $thisTrade['amount'];
            $price = $thisTrade['price'];
            $direction = 0;
            if($thisTrade['direction'] === "buy"){
                $direction = 1;
            }else if($thisTrade['direction'] === "sell"){
                $direction = 2;
            }
            $timestamp = date('Y/m/d H:i:s',getTimeInMalaysiaTimeMs($thisTrade['ts']/1000));
//            echo "<p>".$timestamp."</p>";
            storeBtcTradeApiDataToDb($conn,$apiType,$apiId,$amount,$price,$direction,$timestamp);
        }
    }else{
        logError("header returns error (not 200) in huobi api or body is null");
    }
}

function getBinanceLiveTrade($conn,$size){
//    https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md
    $apiType = 2;

    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', 'https://api.binance.com/api/v1/trades?symbol=BTCUSDT&limit='.$size);

    if($res->getStatusCode() === 200 && $res->getBody()){
        $recentTradesRows = json_decode($res->getBody(), true);

        foreach ($recentTradesRows as $thisTrade){
            $apiId = $thisTrade['id'];
            $amount = $thisTrade['qty'];
            $price = $thisTrade['price'];

            //explanation for isBuyerMaker from here https://money.stackexchange.com/questions/90686/what-does-buyer-is-maker-mean
            //if isBuyerMaker === false, then it BUY(green) SELL(red)
            $direction = 0;
            if($thisTrade['isBuyerMaker'] === false){//buy
                $direction = 1;//buy
            }else if($thisTrade['isBuyerMaker'] === true){//sell
                $direction = 2;//sell
            }
            $timestamp = date('Y/m/d H:i:s',getTimeInMalaysiaTimeMs($thisTrade['time']/1000));
//            echo "<p>".$timestamp."</p>";
            storeBtcTradeApiDataToDb($conn,$apiType,$apiId,$amount,$price,$direction,$timestamp);
        }
    }else{
        logError("header returns error (not 200) in Binance api or body is null");
    }
}

function getOkexLiveTrade($conn,$size){
//    https://www.okex.com/docs/en/#spot-deal_information
    $apiType = 3;

    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', "https://www.okex.com/api/spot/v3/instruments/BTC-USDT/trades?limit=".$size);

    if($res->getStatusCode() === 200 && $res->getBody()){
        $recentTradesRows = json_decode($res->getBody(), true);

        foreach ($recentTradesRows as $thisTrade){
            $apiId = $thisTrade['trade_id'];
            $amount = $thisTrade['size'];
            $price = $thisTrade['price'];
            $direction = 0;
            if($thisTrade['side'] === "buy"){
                $direction = 1;
            }else if($thisTrade['side'] === "sell"){
                $direction = 2;
            }
            $timestamp = date('Y/m/d H:i:s',getTimeInMalaysiaTimeMs(strtotime($thisTrade['timestamp'])));
//            echo "<p>".$timestamp."</p>";
            storeBtcTradeApiDataToDb($conn,$apiType,$apiId,$amount,$price,$direction,$timestamp);
        }
    }else{
        logError("header returns error (not 200) in okex api or body is null");
    }
}

function getBitstampLiveTrade($conn){
    //https://www.bitstamp.net/api/
    $apiType = 4;

    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', 'https://www.bitstamp.net/api/v2/transactions/btcusd/?time=minute');//1min

    if($res->getStatusCode() === 200 && $res->getBody()){
        $recentTradesRows = json_decode($res->getBody(), true);

        foreach ($recentTradesRows as $thisTrade){
            $apiId = $thisTrade['tid'];
            $amount = $thisTrade['amount'];
            $price = $thisTrade['price'];
            $direction = 0;
            if($thisTrade['type'] === "0"){
                $direction = 1;
            }else if($thisTrade['type'] === "1"){
                $direction = 2;
            }
            $timestamp = date('Y/m/d H:i:s',getTimeInMalaysiaTimeMs($thisTrade['date']));
//            echo "<p>".$timestamp."</p>";
            storeBtcTradeApiDataToDb($conn,$apiType,$apiId,$amount,$price,$direction,$timestamp);
        }
    }else{
        logError("header returns error (not 200) in bitstamp api or body is null");
    }
}

function getPoloniexLiveTrade($conn){
    //https://docs.poloniex.com/#returntradehistory-public
    $apiType = 5;

    $end = idate("U");
    $start = $end - (15);//15sec

    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', "https://poloniex.com/public?command=returnTradeHistory&currencyPair=USDT_BTC&start=$start&end=$end");

    if($res->getStatusCode() === 200 && $res->getBody()){
        $recentTradesRows = json_decode($res->getBody(), true);

        foreach ($recentTradesRows as $thisTrade){
            $apiId = $thisTrade['globalTradeID'];
            $amount = $thisTrade['amount'];
            $price = $thisTrade['rate'];
            $direction = 0;
            if($thisTrade['type'] === "buy"){
                $direction = 1;
            }else if($thisTrade['type'] === "sell"){
                $direction = 2;
            }
            $timestamp = date('Y/m/d H:i:s',getTimeInMalaysiaTimeMs(strtotime($thisTrade['date'])));//my localhost will less 2 hours(+6 only) but in server will correct because +8
//            echo "<p>".$timestamp."</p>";
            storeBtcTradeApiDataToDb($conn,$apiType,$apiId,$amount,$price,$direction,$timestamp);
        }
    }else{
        logError("header returns error (not 200) in poloniex api or body is null");
    }
}

function getBitfinexLiveTrade($conn,$size){
    //https://docs.bitfinex.com/reference#rest-public-trades
    $apiType = 6;

    $client = new GuzzleHttp\Client();
//    $res = $client->request('GET', "https://api.bitfinex.com/v1/trades/btcusd?timestamp=$start");
    $res = $client->request('GET', "https://api.bitfinex.com/v1/trades/btcusd?limit_trades=".$size);

    if($res->getStatusCode() === 200 && $res->getBody()){
        $recentTradesRows = json_decode($res->getBody(), true);

        foreach ($recentTradesRows as $thisTrade){
            $apiId = $thisTrade['tid'];
            $amount = $thisTrade['amount'];
            $price = $thisTrade['price'];
            $direction = 0;
            if($thisTrade['type'] === "buy"){
                $direction = 1;
            }else if($thisTrade['type'] === "sell"){
                $direction = 2;
            }
            $timestamp = date('Y/m/d H:i:s',getTimeInMalaysiaTimeMs($thisTrade['timestamp']));
//            echo "<p>".$timestamp."</p>";
            storeBtcTradeApiDataToDb($conn,$apiType,$apiId,$amount,$price,$direction,$timestamp);
        }
    }else{
        logError("header returns error (not 200) in bitfinex api or body is null");
    }
}

function getCoinbeneLiveTrade($conn,$size){
    //https://github.com/Coinbene/API-Documents/wiki/1.1.2-Get-Trades-%5BMarket%5D
    $apiType = 7;

    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', "http://api.coinbene.com/v1/market/trades?symbol=btcusdt&size=".$size);

    if($res->getStatusCode() === 200 && $res->getBody()){
        $recentTradesRows = json_decode($res->getBody(), true);

        foreach ($recentTradesRows['trades'] as $thisTrade){
            $apiId = $thisTrade['tradeId'];
            $amount = $thisTrade['quantity'];
            $price = $thisTrade['price'];
            $direction = 0;
            if($thisTrade['take'] === "buy"){
                $direction = 1;
            }else if($thisTrade['take'] === "sell"){
                $direction = 2;
            }
            $timestamp = date('Y/m/d H:i:s',getTimeInMalaysiaTimeMs($thisTrade['time']/1000));
//            echo "<p>".$timestamp."</p>";
            storeBtcTradeApiDataToDb($conn,$apiType,$apiId,$amount,$price,$direction,$timestamp);
        }
    }else{
        logError("header returns error (not 200) in coinbene api or body is null");
    }
}

function getBitforexLiveTrade($conn,$size){
    //https://github.com/bitforexapi/API_Doc_en/wiki/Trading-record-information
    $apiType = 8;

    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', "https://api.bitforex.com/api/v1/market/trades?symbol=coin-usdt-btc&size=".$size);

    if($res->getStatusCode() === 200 && $res->getBody()){
        $recentTradesRows = json_decode($res->getBody(), true);

        if($recentTradesRows['success'] === 1 || $recentTradesRows['success'] === "1" || $recentTradesRows['success'] === true || $recentTradesRows['success'] === "true"){
            foreach ($recentTradesRows['data'] as $thisTrade){
                $apiId = $thisTrade['time'];//dont know why he returns tid as 0 always, so i use timestamp instead
                $amount = $thisTrade['amount'];
                $price = $thisTrade['price'];

                //Direction of deal, 1 buyer meal list 2 seller meal list, same as me lo
                $direction = $thisTrade['direction'];
                $timestamp = date('Y/m/d H:i:s',getTimeInMalaysiaTimeMs($thisTrade['time']/1000));
//            echo "<p>".$timestamp."</p>";
                storeBtcTradeApiDataToDb($conn,$apiType,$apiId,$amount,$price,$direction,$timestamp);
            }
        }
    }else{
        logError("header returns error (not 200) in bitforex api or body is null");
    }
}

function storeBtcTradeApiDataToDb($conn,$apiType,$apiId,$amount,$price,$direction,$timestamp){
    $getBtcTradeRows = getBtcTrades($conn," WHERE api_id = ? AND api_type = ? ",array("api_id","api_type"),array($apiId,$apiType),"si");
    if(!$getBtcTradeRows){
        insertDynamicData($conn,"btc_trades",array("amount","price","api_type","timestamp","direction","api_id"),array($amount,$price,$apiType,$timestamp,$direction,$apiId),"ddisis");
    }
}

function logError($errorMsg){
    $datetimeInGMT = new DateTime("now", new DateTimeZone("UTC"));
    $errorMsg = $datetimeInGMT->format('l, Y/m/d H:i:s') . " (UTC) ::: " . $errorMsg;
    error_log("$errorMsg\n", 3, "cron-errors.log");
}

function getTimeInMalaysiaTimeMs($timestamp){
    $currentTime = idate("U",$timestamp);//unix
    $timezoneOffsetInSecond = idate("Z");//get this server's timezone offset
    $ourTimeZoneMalaysiaOffset = 60 * 60 * 8;//our malaysia timezone +8 GMT
    $finalTimeInMalaysiaTimeZone = $currentTime - $timezoneOffsetInSecond + $ourTimeZoneMalaysiaOffset;
    return $finalTimeInMalaysiaTimeZone;
}

//CAN TRY
//
//https://poloniex.com/public?command=returnChartData&currencyPair=USDT_BTC&start=1558682040&end=1558682100&period=300
//
//2019-05-23T00:00:00.123Z
//
//https://rest.coinapi.io/v1/ohlcv/OKCOIN_USD_SPOT_BTC_USD/latest?period_id=1MIN&apikey=00AC125C-3359-4CBF-8272-BE45046ED68B
//https://rest.coinapi.io/v1/ohlcv/HUOBIPRO_SPOT_BTC_USDT/latest?period_id=1MIN&apikey=00AC125C-3359-4CBF-8272-BE45046ED68B
//
//https://rest.coinapi.io/v1/symbols?apikey=00AC125C-3359-4CBF-8272-BE45046ED68B
//
//https://api.huobi.com/market/history/kline?period=1min&size=50&symbol=btcusdt
//
//USD 250 per month for 10k calls