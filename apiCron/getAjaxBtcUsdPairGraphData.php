<?php

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

echo json_encode(getGraphData($conn));

$conn->close();

function getGraphData($conn){
    $graphDataRows = getBtcUsdPairGraph($conn," ORDER BY id DESC LIMIT 1 ");


    $graphData = $graphDataRows[0];

    $thisGraphDate = date( 'i:s', strtotime($graphData->getDateCreated()) );

    $resultArray = array();
    $resultArray['dateString'] = $thisGraphDate;
    $resultArray['btcString'] = $graphData->getBtcTransaction();
    $resultArray['usdString'] = $graphData->getUsdTransaction();

    return $resultArray;
}