<?php
require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

delete1HourTrades($conn);
delete1HourTradesGainLoss($conn);
delete1HourGraphData($conn);

$conn->close();

function delete1HourTrades($conn){
    $oneHourInMs = 60 * 60;
    $deleteDate = date("Y-m-d H:i:s",getCurrentTimeInMalaysiaTimeMs($oneHourInMs));

    $sql = "DELETE FROM btc_trades WHERE date_created <= '$deleteDate'";

    if ($conn->query($sql) === TRUE) {

    } else {
        logError("Something wrong in deleting trades");
    }
}

function delete1HourTradesGainLoss($conn){
    $oneHourInMs = 60 * 60;
    $deleteDate = date("Y-m-d H:i:s",getCurrentTimeInMalaysiaTimeMs($oneHourInMs));

    $sql = "DELETE FROM btc_trades_gain_loss WHERE date_created <= '$deleteDate'";

    if ($conn->query($sql) === TRUE) {

    } else {
        logError("Something wrong in deleting trades gain loss");
    }
}

function delete1HourGraphData($conn){
    $oneHourInMs = 60 * 60;
    $deleteDate = date("Y-m-d H:i:s",getCurrentTimeInMalaysiaTimeMs($oneHourInMs));

    $sql = "DELETE FROM btc_usd_pair_graph WHERE date_created <= '$deleteDate'";
    $conn->query($sql);

    if ($conn->query($sql) === TRUE) {

    } else {
        logError("Something wrong in deleting graph data");
    }
}

function getCurrentTimeInMalaysiaTimeMs($minusMs){
    $currentTime = idate("U");//unix
    $timezoneOffsetInSecond = idate("Z");//get this server's timezone offset
    $ourTimeZoneMalaysiaOffset = 60 * 60 * 8;//our malaysia timezone +8 GMT
    $finalTimeInMalaysiaTimeZone = $currentTime - $timezoneOffsetInSecond + $ourTimeZoneMalaysiaOffset;
    return ($finalTimeInMalaysiaTimeZone - $minusMs);
}

function logError($errorMsg){
    $datetimeInGMT = new DateTime("now", new DateTimeZone("UTC"));
    $errorMsg = $datetimeInGMT->format('l, Y/m/d H:i:s') . " (UTC) ::: " . $errorMsg;
    error_log("$errorMsg\n", 3, "cron-errors.log");
}