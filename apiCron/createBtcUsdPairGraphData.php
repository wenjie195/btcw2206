<?php
require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

//cant really do it 12 times for 5 sec zhun zhun because scare will affect the next minute's job
for($i = 0; $i < 59; $i++){
    if($i === 0){
        $delaySeconds = 0;
    }else{
        $delaySeconds = 1;//sleep function doesnt care about decimal points
    }
    delayCallFunction($conn,$delaySeconds);
}

$conn->close();

function delayCallFunction($conn,$delaySeconds){
    if($delaySeconds != 0){
        sleep($delaySeconds);
    }

    createGraphData($conn);
}

function createGraphData($conn){
    $minBtc = 323;
    $maxBtc = 500;
    $minUsd = 150;
    $maxUsd = 412;

    //from here https://www.geeksforgeeks.org/php-mt_rand-function/
    //inclusive
    $todayBtc = mt_rand($minBtc, $maxBtc);
    $todayUsd = mt_rand($minUsd, $maxUsd);

    insertDynamicData($conn,"btc_usd_pair_graph",array("btc_transaction","usd_transaction"),array($todayBtc,$todayUsd),"ii");
}