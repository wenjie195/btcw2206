<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
require_once 'utilities/calculationFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{ 
     if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
          $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
     {
          header('Location:index.php');
          exit();
     }
     else
     {
          $uid = $_SESSION['uid'];
          $conn = connDB();
          $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
     }
}

$errorMsg = null;
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     if (!isPostVariableEmpty("field_1") && 
     !isPostVariableEmpty("field_2") &&
     !isPostVariableEmpty("field_3"))
     {
          $email = $_POST["field_1"];
          $planSelected = $_POST["field_2"];
          $amountBTC = $_POST["field_3"];
          
          if($planSelected && $amountBTC != 0)
          {
               $userRows = getUser($conn,
               " WHERE email = ? ORDER BY date_created DESC LIMIT 1",
               array("email"),
               array($email),
               "s");

               if($userRows != null)
               {
                    if($planSelected == "A")
                    {
                         $planType = 1;
                         $multiplyValue = 3;
                         $isPromo = 0;
                    }
                    else
                    {
                         $planType = 2;
                         $multiplyValue = 8;
                         $isPromo = 0;
                    }

                    $confirmedInvesment = insertDynamicData($conn,
                    "temporary_plan_b",
                    array("uid","planType","btcSetToThisUser","isPromotion","promotionMultiplier"),
                    array($userRows[0]->getUid(),$planType,$amountBTC,$isPromo,$multiplyValue),
                    "sdsii");

                    if($confirmedInvesment)
                    {
                        $errorMsg = "Successfully Entered This ".$amountBTC." BTC to ".$userRows[0]->getUsername()." for plan ".$planSelected;
                    
                    }
                    else
                    {
                        $errorMsg = "Error Creating Fields";
                    }
               }
               else
               {
                    $errorMsg = "There is no user with this email";
               }
          }
          else
          {
               $errorMsg = "Please Select A Plan";
          }
     }
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <title>ZCXC</title>
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();
     if($errorMsg != null)
     {
          putNotice("Notice",$errorMsg);
     }
  ?>
  <div id="firefly" class="firefly-class min-height">  
     <div class="width100 same-padding more-separate-margin-top edit-div">
          <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a> 
          <div class="clear"></div>
            <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Insert Value Here (For Special ID)</b></h4>
            <p class="white-text"><b class="weight-700">This is for the confirmed transaction in which they only gives email and amount</b></p>
            <form class="register-form"  method="POST" >
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Email</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_1" id="field_1" required></h4></td>
                  </tr>
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Plan Type</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top">
                         <h4 class="btcw-h4 edit2-h4">
                              <select class="inputa clean2"  name="field_2" id="field_2" required >
                    			<option disabled selected>Please Choose</option>
                    			<option value="A">Plan A</option>
                    			<option value="B">Plan B</option>                                
                			</select>
                         </h4>
                    </td>
                  </tr>
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">BTC Value</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top">
                         <h4 class="btcw-h4 edit2-h4">
                              <input type="number" class="inputa clean2 inputb"  name="field_3" id="field_3" required min="0.001" step="0.001" max="50">
                         </h4>
                    </td>
                  </tr>

                </table>
                <div class="clear"></div>
               <button class="register-button2 clean orange-hover inputb-button" name="insertValue" id="insertValue" >Insert Value</button>

          </form>
     </div> 
      <div class="clear"></div>
      <?php 
          if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST')
          {
              
          }
      ?>
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <?php require_once 'mainFooter.php';?>
  </body>
</html>