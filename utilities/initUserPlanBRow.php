<?php
require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();
$userRows = getUser($conn);

foreach ($userRows as $user){
    $uid = $user->getUid();
    $planId = 2;
    $userPlanRows = getUserPlan($conn," WHERE uid = ? && plan_id = ? ORDER BY date_created DESC LIMIT 1",array("uid","plan_id"),array($uid,$planId),"si");

    if(!$userPlanRows){
        if(!insertDynamicData($conn,"user_plan",array("uid","plan_id"),array($uid,$planId),"si")){
            //return "failed to insert new user_plan for plan B";
            return "对不起,未知错误发生了！！";
        }else{
            $userPlanRows = getUserPlan($conn," WHERE uid = ? && plan_id = ? ORDER BY date_created DESC LIMIT 1",array("uid","plan_id"),array($uid,$planId),"si");
            if(!$userPlanRows){
                //return "failed to insert new user_plan for plan B";
                return "对不起,未知错误发生了！！";
            }
        }
    }
}

$conn->close();