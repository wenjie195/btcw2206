<?php

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

moveTemporaryPlanBTableToRealPlace($conn,1);
//investBtcPlanA($conn);
//sqlMaker($conn);

$conn->close();

function moveTemporaryPlanBTableToRealPlace($conn,$planId){
    $tempPlanBRows = getTemporaryPlanB($conn," WHERE planType = ? AND isMovedToUserPlan = 0 ORDER BY dateCreated ASC ", array("planType"), array($planId),"i");

    if($tempPlanBRows){
        foreach ($tempPlanBRows as $tempInvest){
            if(investBtcPlanA($conn,$tempInvest->getUid(),$tempInvest->getBtcSetToThisUser(),$planId)){
                if(!updateDynamicData($conn,"temporary_plan_b"," WHERE id = ? ",array("isMovedToUserPlan"),array(1,$tempInvest->getId()),"ii")){
                    echo "failed to update temporary_plan_b's isMovedToUserPlan. ID: " . $tempInvest->getId();
                }
            }
        }
    }else{
        echo "No record found for Plan $planId";
    }
}

function investBtcPlanA($conn,$uid,$amount,$planId){
    $errorMsg = null;
    $successMsg = null;
//    $amount = 0.15;
//    $planId = 1;
//    $uid = "CUSTOM-ID-15a35f53f0b3c7059320a57a83bac03118ff35b5";
    $errorMsg = investCapitalToPlan($conn,$amount,$planId,null,$uid);

    if(!isset($errorMsg)){
        $amountInvestedString = removeUselessZero($amount) . " BTC!";
        $successMsg = _profile_donate_success."$amountInvestedString";
        echo $successMsg;
        return true;
    }else{
        echo "THIS USER ERROR: $uid</br>";
        echo $errorMsg;
        echo "</br>";
        echo "</br>";
        return false;
    }

//    SELECT *  FROM `user_plan` WHERE `uid` LIKE 'CUSTOM-ID-15a35f53f0b3c7059320a57a83bac03118ff35b5'
//    OR `uid` LIKE 'CUSTOM-ID-5178d02c6f71afd0f2e804b05ac863cd32295137'
//    OR `uid` LIKE 'CUSTOM-ID-5c4d1166a4fdac06decf27d7837567501c96a7eb'
//    OR `uid` LIKE 'CUSTOM-ID-bd72937c6d8603ea314989e5ee568a7286bcbd29'
//    OR `uid` LIKE 'CUSTOM-ID-01dc43a42713ebad5780176ba6d62e8407950605'
//    OR `uid` LIKE 'lZGYMyfT5HU3WAcQ59wyKzuHuDv1'
//    OR `uid` LIKE 'vlZAxQtxguXvOWZe1glStjPPLC42'
}

function sqlMaker($conn){
    $uid = "vlZAxQtxguXvOWZe1glStjPPLC42";

    $referralHistoryRows = array();
    $thisUserFirstTimeReferralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ORDER BY id ASC LIMIT 1 ",array("referrer_id"),array($uid),"s");
    if($thisUserFirstTimeReferralHistoryRows){
        $topReferrerId = $thisUserFirstTimeReferralHistoryRows[0]->getTopReferrerId();
        if($topReferrerId === $uid){
            $thisUserFirstLevel = 1;
            $thisUserLastLevel = 10;
        }else{
            $thisUserFirstLevel = $thisUserFirstTimeReferralHistoryRows[0]->getCurrentLevel();
            $thisUserLastLevel = $thisUserFirstLevel + 9;
        }

        $downlineReferralIdArray = array();
        $downlineReferralIdArray[$uid]  = true ;
        for($loopCurrentLevel = $thisUserFirstLevel; $loopCurrentLevel <= $thisUserLastLevel; $loopCurrentLevel++){
            $thisLevelReferralHistoryRows = getReferralHistory($conn," WHERE top_referrer_id = ? AND current_level = $loopCurrentLevel ORDER BY id ",array("top_referrer_id"),array($topReferrerId),"s");
            if($thisLevelReferralHistoryRows){
                $newDownlineReferralIdArray = array();
                foreach ($thisLevelReferralHistoryRows as $tempReferral) {
                    $canAdd = false;
                    foreach ($downlineReferralIdArray as $key => $value) {
                        if($tempReferral->getReferrerId() === $key){
                            $newDownlineReferralIdArray[$tempReferral->getReferralId()]  = true ;
                            $canAdd = true;
                            break;
                        }
                    }
                    if($canAdd){
                        array_push($referralHistoryRows,$tempReferral);
                    }
                }
                unset($downlineReferralIdArray);
                $downlineReferralIdArray = array();
                foreach ($newDownlineReferralIdArray as $newDownlineReferralId => $value) {
                    $downlineReferralIdArray[$newDownlineReferralId]  = true ;
                }
                unset($newDownlineReferralIdArray);
            }
        }
    }

//$sql = "SELECT * FROM free_btc WHERE  ";
    $sql = "SELECT SUM(total_amount) AS totalFreeBtc FROM free_btc WHERE  ";

    $levelArrayInChinese = array("第一代","第二代","第三代","第四代","第五代","第六代","第七代","第八代","第九代","第十代");
    if($referralHistoryRows && count($referralHistoryRows) > 0) {
        $currentLevel = $thisUserFirstLevel - 1;
        $currentNo = 0;

        foreach ($referralHistoryRows as $thisReferral) {
            $currentNo++;
            if($currentLevel < $thisReferral->getCurrentLevel()){
                $currentLevel++;
                echo '<h4 class="btcw-h4 separate-title white-text"><b class="weight-700">'.$levelArrayInChinese[$currentLevel - $thisUserFirstLevel].'：</b> </h4>
                                    <table cellspacing="0" cellpadding="0" class="transparent-table recommend-table white-text-table">';
            }

            $referralUid = $thisReferral->getReferralId();
            $tempUserRow = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($referralUid),"s");
            if($tempUserRow && count($tempUserRow) > 0){
                $referralUser = $tempUserRow[0];
                $totalReferredCount = getCount($conn,"referral_history","referrer_id"," WHERE referrer_id = ? ", array("referrer_id"), array($referralUser->getUid()),"s");

                if($currentLevel == 2){//start from 1 not 0
                    $sql .= " OR uid LIKE '" . $referralUser->getUid() ."' ";
                }

                echo '<tr>
                                    <td class="c-td1" valign="top"><p class="weight-700">'.$currentNo.'.</p></td>
                                    <td class="c-td2" valign="top"><p class="weight-700">用户名</p></td>
                                    <td class="c-td3" valign="top"><p>:</p></td>
                                    <td class="c-td4" valign="top"><p>'.$referralUser->getUsername().'</p></td>
                                  </tr>
                                  <tr>
                                    <td class="c-td1" valign="top"><p class="weight-700">&nbsp;</p></td>
                                    <td class="c-td2" valign="top"><p class="weight-700">加入日期</p></td>
                                    <td class="c-td3" valign="top"><p>:</p></td>
                                    <td class="c-td4" valign="top"><p>'.$referralUser->getUid() .'</p></td>
                                  </tr>
                                  <tr>
                                    <td class="c-td1" valign="top"><p class="weight-700">&nbsp;</p></td>
                                    <td class="c-td2" valign="top"><p class="weight-700">已推荐</p></td>
                                    <td class="c-td3" valign="top"><p>:</p></td>
                                    <td class="c-td4" valign="top"><p>'.$totalReferredCount.'个用户</p></td>
                                  </tr>';
            }

            if($currentNo < count($referralHistoryRows) && $referralHistoryRows[$currentNo] && $referralHistoryRows[$currentNo]->getCurrentLevel() == $currentLevel + 1){
                echo "</table>";
            }
        }

        echo "</table>";
    }else{
        echo "<p class='white-text'>还没推荐任何一个人。</p>";
    }

    echo $sql;

    echo "</br>";
    echo "</br>";
    echo "</br>";

    $uid = "CUSTOM-ID-15a35f53f0b3c7059320a57a83bac03118ff35b5";//username: summer, level 6 of the vlZAxQtxguXvOWZe1glStjPPLC42 guy

    $allTop10ReferrerId = getTop10ReferrerOfUser($conn,$uid);
    $commissionLevel = 0;
    $level = 1;
    foreach ($allTop10ReferrerId as $thisReferrerId){
        echo "</br>";
        echo "LEvel $level : ".$thisReferrerId;
        echo "</br>";
        $level++;
    }
}

function getTop5ReferrerOfUser($conn,$uid){
    $all10ReferrerId = array();

    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");

    if(!$referralHistoryRows){
        //meaning this user is not referred by anyone
    }else{
        $currentLevel = $referralHistoryRows[0]->getCurrentLevel();
        $maxLevel = $currentLevel - 5;//minus ten because i want update the people above me not the 1 below me and update all the people above me

        $currentReferralId = $uid;
        while($currentLevel > 0 && $maxLevel < $currentLevel){
            $currentReferralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($currentReferralId),"s");

            if($currentReferralHistoryRows){
                $currentReferralId = $currentReferralHistoryRows[0]->getReferrerId();
                array_push($all10ReferrerId,$currentReferralId);
            }else{
                break;
            }
            $currentLevel--;
        }
    }
    return $all10ReferrerId;
}