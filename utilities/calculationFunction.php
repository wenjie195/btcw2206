<?php

if(isset($_SESSION['lang'])){
    require_once dirname(__FILE__) . "/../lang/".$_SESSION['lang'].".php";
}else{
    require_once dirname(__FILE__) . "/../lang/ch.php";
}

function generateInputModalForInvest($type)
{
//    regex for bitcoin decimal from here https://stackoverflow.com/questions/33142190/html5-pattern-match-float-and-or-percentage
    //or from here https://stackoverflow.com/questions/34057595/allow-2-decimal-places-in-input-type-number/34057860
    $buttonLabel = "";
    if($type === "withdraw"){
        $buttonLabel = "提取收益";
    }else if($type === "invest"){
        $buttonLabel = "捐赠";
    }
    $label = $buttonLabel . "数量(以btc為計算單位):";

    echo "
        <div id='$type-modal' class='noticeModal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <span class='closeNoticeModal'>&times;</span>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-10'>
                       
                        <form action='' method='post'>
                          <div class=\"form-group\">
                            <label for='$type-btc-amount'>$label</label>
                            <input type='text' pattern='[0-9]+(\.[0-9]{0,5})?' class='form-control' name='$type-btc-amount' required>
                          </div>
                          <button type='submit' name='$type-form' class='indexBtnSize indexBtnStart'>$buttonLabel</button>
                        </form>
                        
                    </div>
                    <div class='col'></div>
                </div>
               
            </div>
        </div>
    ";
}

function getUserTimeZoneDateTime($userPlan){
    //todo need to change this because the database is now in UTC timestamp
    echo $userPlan->getDateCreated();
    $datetimeInGMT = new DateTime($userPlan->getDateCreated(), new DateTimeZone("Asia/Kuala_Lumpur"));
    $datetimeInGMT->setTimezone(new DateTimeZone("JST"));
    echo $datetimeInGMT->format('d/m/Y H:i:sP');
}

function getCurrentUtcTimeInMs(){
    $datetimeInGMT = new DateTime("now", new DateTimeZone("UTC"));
//    $datetimeInGMT->setTimezone(new DateTimeZone("America/New_York"));
//    return $datetimeInGMT->format('d/m/Y H:i:sP');
    return strtotime($datetimeInGMT->format('Y/m/d H:i:sP')) * 1000;
}

function promptAlertMsg($errorMsg,$successMsg){
    if($errorMsg !== null && isset($errorMsg)){
//        $errorMsg = htmlentities($errorMsg);
        echo "$.alert({
                              title: '"._profile_error_modal_title."',
                              content: '$errorMsg'
                          });";
    }else if($successMsg !== null && isset($successMsg)){
//        $successMsg = htmlentities($successMsg);
        echo "$.alert({
                                  title: '"._profile_success_modal_title."',
                                  content: '$successMsg'
                              });";
    }
}

function isPostVariableEmpty($name){
    if(isset($_POST[$name]) && isNotEmpty($_POST[$name])){
        return false;
    }else{
        return true;
    }
}

function isNotEmpty($input)
{
    $strTemp = $input;
    $strTemp = trim($strTemp);

    if($strTemp !== '')
    {
        return true;
    }

    return false;
}

//function getDataFromDatabase($conn,$sql){
//    $query1 = 'SELECT id, first_name, last_name, username FROM table WHERE id = ?';
//    $id = 5;
//
//    if($stmt = $conn->prepare($query)){
//        /*
//             Binds variables to prepared statement
//
//             i    corresponding variable has type integer
//             d    corresponding variable has type double
//             s    corresponding variable has type string
//             b    corresponding variable is a blob and will be sent in packets
//        */
//        $stmt->bind_param('i',$id);
//
//        /* execute query */
//        $stmt->execute();
//
//        /* Store the result (to get properties) */
//        $stmt->store_result();
//
//        /* Get the number of rows */
//        $num_of_rows = $stmt->num_rows;
//
//        /* Bind the result to variables */
//        $stmt->bind_result($id, $first_name, $last_name, $username);
//
//        while ($stmt->fetch()) {
//            echo 'ID: '.$id.'<br>';
//            echo 'First Name: '.$first_name.'<br>';
//            echo 'Last Name: '.$last_name.'<br>';
//            echo 'Username: '.$username.'<br><br>';
//        }
//
//        /* free results */
//        $stmt->free_result();
//
//        /* close statement */
//        $stmt->close();
//    }
//
//    /* close connection */
//    $conn->close();
//}