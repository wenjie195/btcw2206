<?php

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

//LEFT IS REFERRER/UPLINE RIGHT IS REFERRAL/DOWNLINE
//$emailArray = array(
//    array("15045757161@163.com","13845475533@163.com"),
//    array("2012178166@qq.com","149783114@qq.com"),
//    array("339291785@qq.com","239459535@qq.com"),
//    array("346776781@qq.com","1248950091@qq.com"),
//    array("346776781@qq.com","1344345717@qq.com"),
//    array("346776781@qq.com","1048710529@qq.com"),
//    array("346776781@qq.com","550270590@qq.com"),
//    array("346776781@qq.com","1508542555@qq.com"),
//    array("346776781@qq.com","370616589@qq.com"),
//    array("339291785@qq.com","24831327@qq.com"),
//    array("1620156088@qq.com","15580652721@qq.com"),
//    array("15580652721@qq.com","203557312@qq.com"),
//    array("339291785@qq.com","239459535@qq.com"),
//    array("573827152@qq.com","2051698238@qq.com"),
//    array("15580652721@qq.com","1290752079@qq.com"),
//    array("2447699880@qq.com","383901504@qq.com"),
//    array("1620156088@qq.com","2447699880@qq.com"),
//    array("15580652721@qq.com","214360535@qq.com"),
//    array("15580652721@qq.com","724162359@qq.com"),
//    array("15580652721@qq.com","1324304311@qq.com"),
//    array("2274217914@qq.com","1204479145@qq.com"),
//    array("2444199248@qq.com","2088115216@qq.com"),
//    array("2721873715@qq.com","HDD20150126@qq.com"),
//    array("2721873715@qq.com","xionggin2005@qq.com")
//);

//$emailArray = array(
//    array("Jennyteh22@gmail.com","sandyqu1984@gmail.com")
//);

//$emailArray = array(
//    array("happy_yap@hotmail.com","angpookeong5439@gmail.com"),
//    array("irenecly3393@gmail.com","davidbit8702@gmail.com")
//);

//$emailArray = array(
//    array("13981920217@163.com","171818583@qq.com")
//);

//$emailArray = array(
//    array("573827152@qq.com","530376625@qq.com")
//);

//$emailArray = array(
//    array("949389995@qq.com","2387835884@qq.com"),
//    array("949389995@qq.com","239079925@qq.com"),
//    array("949389995@qq.com","824383248@qq.com"),
//    array("949389995@qq.com","294328829@qq.com")
//);

//$emailArray = array(
//    array("1075788256@qq.com","906616947@qq.com")
//);

//$emailArray = array(
//    array("13671060502@139.com","526235883@qq.com")
//);

//$emailArray = array(
//    array("13671060502@139.com","244347829@qq.com")
//);

//$emailArray = array(
//    array("2537366767@qq.com","6966606@qq.com")
//);

//$emailArray = array(
//    array("2427238478@qq.com","1092290500@qq.com")
//);

//$emailArray = array(
//    array("m13307331216@163.com","18907338686@163.com")
//);

$emailArray = array(
    array("marywong0217@gmail.com","meng20X@gmail.com")
);

$no = 1;
foreach($emailArray as $bothEmail){
    $uplineUserRows = getUser($conn," WHERE email = ? ",array("email"),array($bothEmail[0]),"s");
    $downlineUserRows = getUser($conn," WHERE email = ? ",array("email"),array($bothEmail[1]),"s");

    if(!$uplineUserRows){
        echoError("$no) No such upline user found with this email " . $bothEmail[0]);
        continue;
    }

    if(!$downlineUserRows){
        echoError("$no) No such downline user found with this email " . $bothEmail[1]);
        continue;
    }

    changeUplineForAnUpline($conn,$uplineUserRows[0]->getUid(),$downlineUserRows[0]->getUid(),$no);

    $no++;
}

$conn->close();

function changeUplineForAnUpline($conn,$uplineUid,$downlineUid,$no){
    //$downlineType has 4 scenario:
    // 1 - can insert directly,
    // 2 - downline has referred someone,
    // 3 - downline already referred by someone else,
    // 4 - both
    $downlineType = 0; // 0 = error

    $downlineUplineRows = getUplineDetails($conn,$downlineUid);//got referred by anyone?
    $downlineDownlineRows = getDownlineDetails($conn,$downlineUid);//has this user referred anyone before?

    if(!$downlineUplineRows && !$downlineDownlineRows){
        $downlineType = 1;
    }else if($downlineUplineRows && $downlineDownlineRows){
        $downlineType = 4;
    }else if($downlineUplineRows){
        $downlineType = 3;
    }else if($downlineDownlineRows){
        $downlineType = 2;
    }

    $newReferrerHistoryRef = null;

    $uplineUplineRows = getUplineDetails($conn,$uplineUid);
    $uplineDownlineRows = getDownlineDetails($conn,$uplineUid);

    if($uplineDownlineRows){
        $newReferrerHistoryRef = new ReferralHistory();
        $newReferrerHistoryRef->setTopReferrerId($uplineDownlineRows[0]->getTopReferrerId());
        $newReferrerHistoryRef->setCurrentLevel($uplineDownlineRows[0]->getCurrentLevel());
    }else if($uplineUplineRows){
        $newReferrerHistoryRef = new ReferralHistory();
        $newReferrerHistoryRef->setTopReferrerId($uplineUplineRows[0]->getTopReferrerId());
        $currentLevel = $uplineUplineRows[0]->getCurrentLevel() + 1;
        $newReferrerHistoryRef->setCurrentLevel($currentLevel);
    }else if(!$uplineDownlineRows && !$uplineUplineRows){
        $newReferrerHistoryRef = new ReferralHistory();
        $newReferrerHistoryRef->setTopReferrerId($uplineUid);
        $newReferrerHistoryRef->setCurrentLevel(1);
    }

    if(!$newReferrerHistoryRef){
        echoError("$no) EEEEEERROR dont have newReferrerHistoryRef : upline - $uplineUid --- downline - $downlineUid");
        return;
    }

    switch ($downlineType){
        case 0:
            echoError("$no) EEEEEERROR 0 : upline - $uplineUid --- downline - $downlineUid");
            break;
        case 1:
            $newTableId = insertIntoReferralHistory($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef->getCurrentLevel(),$newReferrerHistoryRef->getTopReferrerId());
            if($newTableId){
                echoError("$no) success 1 : upline - $uplineUid --- downline - $downlineUid");
            }else{
                echoError("$no) EEEEEERROR 1 can insert directly: upline - $uplineUid --- downline - $downlineUid");
            }
            break;
        case 2:
            updateForDownlineThatHasReferredSomeone($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef,$no,$downlineType);
            break;
        case 3:
            updateForDownlineThatHasReferredSomeone($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef,$no,$downlineType);
            break;
        case 4:
            updateForDownlineThatHasReferredSomeone($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef,$no,$downlineType);
            break;
        default:
            echoError("$no) EEEEEERROR default: upline - $uplineUid --- downline - $downlineUid");
            break;
    }
}

function insertIntoReferralHistory($conn,$referrerId,$referralId,$currentLevel,$topReferrerId){
    return insertDynamicData($conn,"referral_history",
        array("referrer_id","referral_id","current_level","top_referrer_id"),
        array($referrerId,$referralId,$currentLevel,$topReferrerId),
        "ssis");
}

function updateForDownlineThatHasReferredSomeone($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef,$no,$downlineType){
    $hasError = false;

    if($downlineType === 4){
        if(!updateIntoReferralHistoryFIRST_TARGET($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef->getCurrentLevel(),$newReferrerHistoryRef->getTopReferrerId())){
            $hasError = true;
            echoError("$no) EEEEEERROR1 $downlineType downline has referred someone: upline - $uplineUid --- downline - $downlineUid " . $newReferrerHistoryRef->getCurrentLevel() . " " . $newReferrerHistoryRef->getTopReferrerId());
            return;
        }
    }else if($downlineType === 3){//3 means this referral/downline guy has not refer anyone
        if(!updateIntoReferralHistoryFIRST_TARGET($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef->getCurrentLevel(),$newReferrerHistoryRef->getTopReferrerId())){
            $hasError = true;
            echoError("$no) EEEEEERROR2 $downlineType downline has referred someone: upline - $uplineUid --- downline - $downlineUid");
            return;
        }
    }else if($downlineType === 2){//2 means this referral/downline guy is not referred by anyone
        if(!insertIntoReferralHistory($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef->getCurrentLevel(),$newReferrerHistoryRef->getTopReferrerId())){
            $hasError = true;
            echoError("$no) EEEEEERROR3 $downlineType downline has referred someone: upline - $uplineUid --- downline - $downlineUid");
            return;
        }
    }else{
        echoError(" NOTHING SELECTED");
        return;
    }

    $wholeDownlineTree = getWholeDownlineTree($conn,$downlineUid,false);
    if($wholeDownlineTree && count($wholeDownlineTree) > 0){
        $firstLevel = $wholeDownlineTree[0]->getCurrentLevel();
        $count = 1;
        foreach ($wholeDownlineTree as $thisReferral) {
            if($firstLevel !== $thisReferral->getCurrentLevel()){
                $firstLevel++;
                $count++;
            }

            $thisReferralCurrentLevel = $newReferrerHistoryRef->getCurrentLevel() + $count;
            if(!updateIntoReferralHistoryALL_FOLLOWING_DOWNLINES($conn,$thisReferral->getReferralId(),$thisReferralCurrentLevel,$newReferrerHistoryRef->getTopReferrerId())){
                echoError("$no) EEEEEERROR4 $downlineType downline has referred someone FOLLOWING: upline - $downlineUid --- downline - ".$thisReferral->getReferralId());
                $hasError = true;
            }
        }
    }

    if(!$hasError){
        echoError("$no) success $downlineType : upline - $uplineUid --- downline - $downlineUid");
    }
}

//this is only for the USER THAT WANTS TO CHANGE UPLINE (NOT FOR UPDATING ALL HIS FOLLOWING DOWNLINES)
function updateIntoReferralHistoryFIRST_TARGET($conn,$referrerId,$referralId,$currentLevel,$topReferrerId){
    return updateDynamicData($conn,"referral_history"," WHERE referral_id = ? ",
        array("referrer_id","current_level","top_referrer_id"),
        array($referrerId,$currentLevel,$topReferrerId,$referralId),
        "siss");
}

//this is only for updating all the user's (user = USER THAT WANTS TO CHANGE UPLINE) following downlines
//current level need++ depend on how many loop
function updateIntoReferralHistoryALL_FOLLOWING_DOWNLINES($conn,$referralId,$currentLevel,$topReferrerId){
    return updateDynamicData($conn,"referral_history"," WHERE referral_id = ? ",
        array("current_level","top_referrer_id"),
        array($currentLevel,$topReferrerId,$referralId),
        "iss");
}

function getUplineDetails($conn,$uid){
    //check if this suer has been referred by anyone
    return getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
}

function getDownlineDetails($conn,$uid){
    //check if this user has referred anyone or not

    return getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uid),"s");
}

function echoError($message){
    echo "<p> $message </p>";
}