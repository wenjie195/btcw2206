<?php

//dynamic directory require from here https://stackoverflow.com/questions/4061879/how-do-i-require-files-in-other-directory-branches
require_once dirname(__FILE__) . '/../classes/AchievementReward.php';
require_once dirname(__FILE__) . '/../classes/ApplicationHistory.php';
require_once dirname(__FILE__) . '/../classes/BtcTrades.php';
require_once dirname(__FILE__) . '/../classes/BtcTradesGainLoss.php';
require_once dirname(__FILE__) . '/../classes/BtcType.php';
require_once dirname(__FILE__) . '/../classes/BtcUsdPairGraph.php';
require_once dirname(__FILE__) . '/../classes/BtcUsdRate.php';
require_once dirname(__FILE__) . '/../classes/Countries.php';
require_once dirname(__FILE__) . '/../classes/FreeBtc.php';
require_once dirname(__FILE__) . '/../classes/MonthlyReward.php';
require_once dirname(__FILE__) . '/../classes/Plan.php';
require_once dirname(__FILE__) . '/../classes/ReferralCommission.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/States.php';
require_once dirname(__FILE__) . '/../classes/TempNewReinvest.php';
require_once dirname(__FILE__) . '/../classes/TemporaryPlanB.php';
require_once dirname(__FILE__) . '/../classes/TopupCriteria.php';
require_once dirname(__FILE__) . '/../classes/TransactionHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/UserPlan.php';

//***************************************SQL BUILDER START****************************************************/

function sqlSelectSimpleBuilder($columnNames,$tableName){
    $sql = "SELECT";
    $columnSize = count($columnNames);
    for ($i = 0; $i < $columnSize; $i++) {
        $sql .= " " . $columnNames[$i];
        if(($i + 1) >= $columnSize){
            $sql .=  " ";
        }else{
            $sql .=  ", ";
        }
    }

    $sql .= " FROM " . $tableName . " ";

//    if($queryColumns === NULL){
//
//    }else{
//        $whereSize = count($queryColumns);
//        for($x = 0; $x < $whereSize; $x++){
//            if($x === 0){
//                $sql .= " WHERE ";
//            }
//            $sql .= $queryColumns[$x] . " = ? ";
//            if(($x + 1) >= $whereSize){
//
//            }else{
//                $sql .= " AND ";
//            }
//        }
//    }

    return $sql;
}

function sqlInsertSimpleBuilder($columnNames,$tableName){
    $sql = "INSERT INTO $tableName (";
    $columnSize = count($columnNames);

    for ($i = 0; $i < $columnSize; $i++) {
        $sql .= " " . $columnNames[$i];
        if(($i + 1) >= $columnSize){
            $sql .=  " ";
        }else{
            $sql .=  ", ";
        }
    }

    $sql .= ") VALUES (";

    for ($x = 0; $x < $columnSize; $x++) {
        if(($x + 1) >= $columnSize){
            $sql .=  " ? ";
        }else{
            $sql .=  " ? , ";
        }
    }

    $sql .= ")";

    return $sql;
}

function sqlUpdateSimpleBuilder($columnNames,$tableName){
    $sql = "UPDATE $tableName SET";
    $columnSize = count($columnNames);

    for ($i = 0; $i < $columnSize; $i++) {
        $sql .= " " . $columnNames[$i];
        if(($i + 1) >= $columnSize){
            $sql .=  "  = ? ";
        }else{
            $sql .=  "  = ?, ";
        }
    }

    return $sql;
}

//***************************************SQL BUILDER END*******************************************************/


//***************************************SQL STATEMENT DYNAMIC BIND START****************************************************/

function returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes){
//    dynamically binding statement with values from here https://stackoverflow.com/questions/21896953/dynamically-bind-params-in-prepared-statements-with-mysqli
    //example
    //    $queryColumns = array("uid");
    //    $queryValues = array($uid);
    //    $queryTypes = 's';
    //    $user = getUser($conn,$queryColumns,$queryValues,$queryTypes);
    //    $user1 = getUser($conn);
    //    echo "should have result: " . $user->getEmail();
    //    echo "should have result: " . $user1->getEmail();
    $bind_names[] = $queryTypes;
    for ($i=0; $i<count($queryValues);$i++){
        $bind_name = 'bind' . $i;
        $$bind_name = $queryValues[$i];
        $bind_names[] = &$$bind_name;
    }
    $return = call_user_func_array(array($stmt,'bind_param'),$bind_names);

    return $stmt;
}

//***************************************SQL STATEMENT DYNAMIC BIND END******************************************************/


//***************************************GET CLASSES DATA FROM DATABASE START****************************************************/

function getAchievementReward($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","plan_id","min_total_btc","min_total_usd","percentage","level","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"achievement_reward");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $planId, $minTotalBtc,$minTotalUsd, $percentage, $level, $dateCreated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $achievementReward = new AchievementReward;
            $achievementReward->setId($id);
            $achievementReward->setPlanId($planId);
            $achievementReward->setMinTotalBtc($minTotalBtc);
            $achievementReward->setMinTotalUsd($minTotalUsd);
            $achievementReward->setPercentage($percentage);
            $achievementReward->setLevel($level);
            $achievementReward->setDateCreated($dateCreated);

            array_push($resultRows,$achievementReward);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getApplicationHistory($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","type","amount","date_created","date_updated","status","admin_remark","user_remark","uid","coinbase_account_id","coinbase_address_id","coinbase_receive_address","user_send_address","plan_id");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"application_history");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $type, $amount, $dateCreated, $dateUpdated, $status, $adminRemark, $userRemark, $uid, $coinbaseAccountId,$coinbaseAddressId,$coinbaseReceiveAddress,$userSendAddress,$planId);

        $resultRows = array();
        while ($stmt->fetch()) {
            $applicationHistory = new ApplicationHistory();
            $applicationHistory->setId($id);
            $applicationHistory->setType($type);
            $applicationHistory->setAmount($amount);
            $applicationHistory->setDateCreated($dateCreated);
            $applicationHistory->setDateUpdated($dateUpdated);
            $applicationHistory->setStatus($status);
            $applicationHistory->setAdminRemark($adminRemark);
            $applicationHistory->setUserRemark($userRemark);
            $applicationHistory->setUid($uid);
            $applicationHistory->setCoinbaseAccountId($coinbaseAccountId);
            $applicationHistory->setCoinbaseAddressId($coinbaseAddressId);
            $applicationHistory->setCoinbaseReceiveAddress($coinbaseReceiveAddress);
            $applicationHistory->setUserSendAddress($userSendAddress);
            $applicationHistory->setPlanId($planId);

            array_push($resultRows,$applicationHistory);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getBtcTrades($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","amount","price","api_type","timestamp","direction","api_id","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"btc_trades");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$amount,$price,$apiType,$timestamp,$direction,$apiId,$dateCreated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new BtcTrades();
            $class->setId($id);
            $class->setAmount(removeUselessZero($amount));
            $class->setPrice(removeUselessZero($price));
            $class->setApiType($apiType);
            $class->setTimestamp($timestamp);
            $class->setDirection($direction);
            $class->setApiId($apiId);
            $class->setDateCreated($dateCreated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getBtcTradesGainLoss($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","api_type","latest_buy","latest_sell","percent","is_gain","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"btc_trades_gain_loss");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$apiType,$latestBuy,$latestSell,$percent,$isGain,$dateCreated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new BtcTradesGainLoss();
            $class->setId($id);
            $class->setApiType($apiType);
            $class->setLatestBuy($latestBuy);
            $class->setLatestSell($latestSell);
            $class->setPercent($percent);
            $class->setIsGain($isGain);
            $class->setDateCreated($dateCreated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getBtcType($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","name","date_created","date_updated","amount","min_ppl_intro");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"btc_type");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $name, $dateCreated, $dateUpdated, $amount, $minPplIntro);

        $resultRows = array();
        while ($stmt->fetch()) {
            $btcType = new BtcType();
            $btcType->setId($id);
            $btcType->setName($name);
            $btcType->setDateCreated($dateCreated);
            $btcType->setDateUpdated($dateUpdated);
            $btcType->setAmount($amount);
            $btcType->setMinPplIntro($minPplIntro);

            array_push($resultRows,$btcType);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getBtcUsdPairGraph($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","btc_transaction","usd_transaction","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"btc_usd_pair_graph");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$btcTransaction,$usdTransaction,$dateCreated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new BtcUsdPairGraph();
            $class->setId($id);
            $class->setBtcTransaction($btcTransaction);
            $class->setUsdTransaction($usdTransaction);
            $class->setDateCreated($dateCreated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getBtcUsdRate($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","btc","usd","rate","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"btc_usd_rate");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $btc, $usd, $rate, $dateCreated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $btcUsdRate = new BtcUsdRate();
            $btcUsdRate->setId($id);
            $btcUsdRate->setBtc($btc);
            $btcUsdRate->setUsd($usd);
            $btcUsdRate->setRate($rate);
            $btcUsdRate->setDateCreated($dateCreated);

            array_push($resultRows,$btcUsdRate);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getCountries($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","sortname","name","phonecode");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"countries");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $sortname, $name, $phonecode);

        $resultRows = array();
        while ($stmt->fetch()) {
            $countries = new Countries();
            $countries->setId($id);
            $countries->setSortname($sortname);
            $countries->setName($name);
            $countries->setPhonecode($phonecode);

            array_push($resultRows,$countries);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getFreeBtc($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","total_amount","current_day","is_finished","date_created","uid","btc_type_id","transaction_id","referrer_id","monthly_reward_id","achievement_reward_id","plan_id","is_withdrawed");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"free_btc");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $totalAmount, $currentDay, $isFinished, $dateCreated, $uid, $btcTypeId, $transactionId, $referrerId, $monthlyRewardId, $achievementRewardId,$planId,$isWithdrawed);

        $resultRows = array();
        while ($stmt->fetch()) {
            $freeBtc = new FreeBtc();
            $freeBtc->setId($id);
            $freeBtc->setTotalAmount($totalAmount);
            $freeBtc->setCurrentDay($currentDay);
            $freeBtc->setIsFinished($isFinished);
            $freeBtc->setDateCreated($dateCreated);
            $freeBtc->setUid($uid);
            $freeBtc->setBtcTypeId($btcTypeId);
            $freeBtc->setTransactionId($transactionId);
            $freeBtc->setReferrerId($referrerId);
            $freeBtc->setMonthlyRewardId($monthlyRewardId);
            $freeBtc->setAchievementRewardId($achievementRewardId);
            $freeBtc->setPlanId($planId);
            $freeBtc->setIsWithdrawed($isWithdrawed);

            array_push($resultRows,$freeBtc);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getMonthlyReward($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","plan_id","month","percentage","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"monthly_reward");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $planId, $month, $percentage, $dateCreated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $monthlyReward = new MonthlyReward();
            $monthlyReward->setId($id);
            $monthlyReward->setPlanId($planId);
            $monthlyReward->setMonth($month);
            $monthlyReward->setPercentage($percentage);
            $monthlyReward->setDateCreated($dateCreated);

            array_push($resultRows,$monthlyReward);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getPlan($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id", "name", "max_btc", "min_btc", "investment_return", "date_created", "date_updated", "withdrawal_percent", "withdrawal_period_start", "withdrawal_period_end", "percentage_given", "min_btc_to_withdraw");

    $sql = sqlSelectSimpleBuilder($dbColumnNames, "plan");
    if($whereClause){
        $sql .= $whereClause;
    }

    if ($stmt = $conn->prepare($sql)) {
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('i', $queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $name, $maxBtc, $minBtc, $investmentReturn, $dateCreated, $dateUpdated, $withdrawalPercent, $withdrawalPeriodStart, $withdrawalPeriodEnd, $percentageGiven, $minBtcToWithdraw);

        $resultRows = array();
        while ($stmt->fetch()) {
            $plan = new Plan;
            $plan->setId($id);
            $plan->setName($name);
            $plan->setMaxBtc($maxBtc);
            $plan->setMinBtc($minBtc);
            $plan->setInvestmentReturn($investmentReturn);
            $plan->setDateCreated($dateCreated);
            $plan->setDateUpdated($dateUpdated);
            $plan->setWithdrawalPercent($withdrawalPercent);
            $plan->setWithdrawalPeriodStart($withdrawalPeriodStart);
            $plan->setWithdrawalPeriodEnd($withdrawalPeriodEnd);
            $plan->setPercentageGiven($percentageGiven);
            $plan->setMinBtcToWithdraw($minBtcToWithdraw);

            array_push($resultRows,$plan);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if ($num_of_rows <= 0) {
            return null;
        } else {
            return $resultRows;
        }
    } else {
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getReferralCommission($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","date_created","commission","level","btc_needed","usd_needed","downline_reward_commission","plan_id");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"referral_commision");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $dateCreated, $commission, $level, $btcNeeded,$usdNeeded, $downlineRewardCommission, $planId);

        $resultRows = array();
        while ($stmt->fetch()) {
            $referralCommission = new ReferralCommission();
            $referralCommission->setId($id);
            $referralCommission->setDateCreated($dateCreated);
            $referralCommission->setCommission($commission);
            $referralCommission->setLevel($level);
            $referralCommission->setBtcNeeded($btcNeeded);
            $referralCommission->setUsdNeeded($usdNeeded);
            $referralCommission->setDownlineRewardCommission($downlineRewardCommission);
            $referralCommission->setPlanId($planId);

            array_push($resultRows,$referralCommission);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getReferralHistory($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","referrer_id","referral_id","current_level","top_referrer_id","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"referral_history");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $referrerId, $referralId, $currentLevel, $topReferrerId, $dateCreated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $referralHistory = new ReferralHistory();
            $referralHistory->setId($id);
            $referralHistory->setReferrerId($referrerId);
            $referralHistory->setReferralId($referralId);
            $referralHistory->setCurrentLevel($currentLevel);
            $referralHistory->setTopReferrerId($topReferrerId);
            $referralHistory->setDateCreated($dateCreated);

            array_push($resultRows,$referralHistory);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getStates($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","name","country_id");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"states");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $name, $countryId);

        $resultRows = array();
        while ($stmt->fetch()) {
            $states = new States();
            $states->setId($id);
            $states->setName($name);
            $states->setCountryId($countryId);

            array_push($resultRows,$states);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getTempNewReinvest($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","dateCreated","dateCanceled","reinvestType","reinvestStatus","reinvestPlanType","reinvestPosition","isReinvested");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"temp_new_reinvest");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('i',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$dateCreated,$dateCanceled,$reinvestType,$reinvestStatus,$reinvestPlanType,$reinvestPosition,$isReinvested);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new TempNewReinvest();
            $class->setId($id);
            $class->setUid($uid);
            $class->setDateCreated($dateCreated);
            $class->setDateCanceled($dateCanceled);
            $class->setReinvestType($reinvestType);
            $class->setReinvestStatus($reinvestStatus);
            $class->setReinvestPlanType($reinvestPlanType);
            $class->setReinvestPosition($reinvestPosition);
            $class->setIsReinvested($isReinvested);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getTempNewReinvestSpecialGroupBy($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("uid","reinvestPosition","reinvestPlanType");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"temp_new_reinvest");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('i',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($uid,$reinvestPosition,$reinvestPlanType);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new TempNewReinvest();
            $class->setUid($uid);
            $class->setReinvestPosition($reinvestPosition);
            $class->setReinvestPlanType($reinvestPlanType);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getTemporaryPlanB($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","planType","btcSetToThisUser","isPromotion","promotionMultiplier","dateUpdated","dateCreated","dateTransactionReceive","isMovedToUserPlan");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"temporary_plan_b");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('i',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$planType,$btcSetToThisUser,$isPromotion,$promotionMultiplier,$dateUpdated,$dateCreated,$dateTransactionReceive,$isMovedToUserPlan);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new TemporaryPlanB();
            $class->setId($id);
            $class->setUid($uid);
            $class->setPlanType($planType);
            $class->setBtcSetToThisUser($btcSetToThisUser);
            $class->setIsPromotion($isPromotion);
            $class->setPromotionMultiplier($promotionMultiplier);
            $class->setDateUpdated($dateUpdated);
            $class->setDateCreated($dateCreated);
            $class->setDateTransactionReceive($dateTransactionReceive);
            $class->setIsMovedToUserPlan($isMovedToUserPlan);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getTopupCriteria($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","require_month","max_btc","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"topup_criteria");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('i',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $requireMonth, $maxBtc, $dateCreated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $topupCriteria = new TopupCriteria;
            $topupCriteria->setId($id);
            $topupCriteria->setRequireMonth($requireMonth);
            $topupCriteria->setMaxBtc($maxBtc);
            $topupCriteria->setDateCreated($dateCreated);

            array_push($resultRows,$topupCriteria);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getTransactionHistory($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","free_btc_amount_in","free_btc_amount_out","capital_btc_amount_in","capital_btc_amount_out",
                            "returned_btc_amount_in","returned_btc_amount_out","current_total_returned_btc",
                            "current_total_free_btc","current_total_capital_btc","day","date_created","uid","percentage_given",
                            "free_btc_id","total_percentage_given","application_history_id","btc_type_id","description","plan_id","btc_invested_in_usd",
                            "withdraw_position_type","withdraw_status","withdrawal_id");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"transaction_history");
    if($whereClause){
        $sql .= $whereClause;
    }



    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('i',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$freeBtcAmountIn,$freeBtcAmountOut,$capitalBtcAmountIn,$capitalBtcAmountOut,
                            $returnedBtcAmountIn,$returnedBtcAmountOut,$currentTotalReturnedBtc,
                            $currentTotalFreeBtc,$currentTotalCapitalBtc,$day,$dateCreated,$uid,$percentageGiven,
                            $freeBtcId,$totalPercentageGiven,$applicationHistoryId,$btcTypeId,$description,$planId,
                            $btcInvestedInUsd,$withdrawPositionType,$withdrawStatus,$withdrawalId);

        $resultRows = array();
        while ($stmt->fetch()) {
            $transactionHistory = new TransactionHistory();
            $transactionHistory->setId($id);
            $transactionHistory->setFreeBtcAmountIn($freeBtcAmountIn);
            $transactionHistory->setFreeBtcAmountOut($freeBtcAmountOut);
            $transactionHistory->setCapitalBtcAmountIn($capitalBtcAmountIn);
            $transactionHistory->setCapitalBtcAmountOut($capitalBtcAmountOut);
            $transactionHistory->setReturnedBtcAmountIn($returnedBtcAmountIn);
            $transactionHistory->setReturnedBtcAmountOut($returnedBtcAmountOut);
            $transactionHistory->setCurrentTotalReturnedBtc($currentTotalReturnedBtc);
            $transactionHistory->setCurrentTotalFreeBtc($currentTotalFreeBtc);
            $transactionHistory->setCurrentTotalCapitalBtc($currentTotalCapitalBtc);
            $transactionHistory->setDay($day);
            $transactionHistory->setDateCreated($dateCreated);
            $transactionHistory->setUid($uid);
            $transactionHistory->setPercentageGiven($percentageGiven);
            $transactionHistory->setFreeBtcId($freeBtcId);
            $transactionHistory->setTotalPercentageGiven($totalPercentageGiven);
            $transactionHistory->setApplicationHistoryId($applicationHistoryId);
            $transactionHistory->setBtcTypeId($btcTypeId);
            $transactionHistory->setDescription($description);
            $transactionHistory->setPlanId($planId);
            $transactionHistory->setBtcInvestedInUsd($btcInvestedInUsd);
            $transactionHistory->setWithdrawPositionType($withdrawPositionType);
            $transactionHistory->setWithdrawStatus($withdrawStatus);
            $transactionHistory->setWithdrawalId($withdrawalId);

            array_push($resultRows,$transactionHistory);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("uid","username","email","password","salt","phone_no","ic_no","country_id","full_name","is_phone_verified","login_type","user_type","referral_link","date_created","date_updated","downline_accumulated_btc","downline_accumulated_usd","can_send_newsletter","topup_criteria_id","is_auto_reinvest");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($uid, $username, $email, $password, $salt, $phoneNo, $icNo, $countryId, $fullName, $isPhoneVerified, $loginType, $userType, $referralLink, $dateCreated, $dateUpdated, $downlineAccumulatedBtc,$downlineAccumulatedUsd, $canSendNewsletter, $topupCriteriaId, $isAutoReinvest);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setUid($uid);
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setPhoneNo($phoneNo);
            $user->setIcNo($icNo);
            $user->setCountryId($countryId);
            $user->setFullName($fullName);
            $user->setIsPhoneVerified($isPhoneVerified);
            $user->setLoginType($loginType);
            $user->setUserType($userType);
            $user->setReferralLink($referralLink);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);
            $user->setDownlineAccumulatedBtc($downlineAccumulatedBtc);
            $user->setDownlineAccumulatedUsd($downlineAccumulatedUsd);
            $user->setCanSendNewsletter($canSendNewsletter);
            $user->setTopupCriteriaId($topupCriteriaId);
            $user->setIsAutoReinvest($isAutoReinvest);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getUserPlan($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","plan_id","capital_btc","returned_btc","free_btc","date_created","downline_accumulated_btc","downline_accumulated_usd","is_auto_reinvest");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user_plan");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $uid, $planId, $capitalBtc, $returnedBtc, $freeBtc, $dateCreated,$downlineAccumulatedBtc,$downlineAccumulatedUsd,$isAutoReinvest);

        $resultRows = array();
        while ($stmt->fetch()) {
            $userPlan = new UserPlan;
            $userPlan->setId($id);
            $userPlan->setUid($uid);
            $userPlan->setPlanId($planId);
            $userPlan->setCapitalBtc($capitalBtc);
            $userPlan->setReturnedBtc($returnedBtc);
            $userPlan->setFreeBtc($freeBtc);
            $userPlan->setDateCreated($dateCreated);
            $userPlan->setDownlineAccumulatedBtc($downlineAccumulatedBtc);
            $userPlan->setDownlineAccumulatedUsd($downlineAccumulatedUsd);
            $userPlan->setIsAutoReinvest($isAutoReinvest);

            array_push($resultRows,$userPlan);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

//***************************************GET CLASSES DATA FROM DATABASE END*******************************************************/


//***************************************COUNT START****************************************************/

function getCount($conn, $tableName = null, $countColumn = null, $whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    if(!$tableName || !$countColumn){
        return 0;
    }

    $sql = "SELECT COUNT($countColumn) AS total FROM $tableName ";
//    $sql = "SELECT COUNT(top_referrer_id) AS total FROM referral_history WHERE top_referrer_id = ?";

    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($totalCount);

        $stmt->fetch();

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return 0;
        }else{
            return $totalCount;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return 0;
    }
}

function getSum($conn, $tableName = null, $sumColumn = null, $whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    if(!$tableName || !$sumColumn){
        return 0;
    }

    $sql = "SELECT SUM($sumColumn) AS total FROM $tableName ";
//    $sql = "SELECT COUNT(top_referrer_id) AS total FROM referral_history WHERE top_referrer_id = ?";

    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($totalSum);

        $stmt->fetch();

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return 0;
        }else{
            return $totalSum;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return 0;
    }
}

//***************************************COUNT END****************************************************/


//***************************************INSERT START****************************************************/

function insertDynamicData($conn,$tableName = null,$columnNames = null,$columnValues = null,$columnTypes = null){
    if(!$tableName || !$columnNames || !$columnValues || !$columnTypes){
        return null;
    }

    $sql = sqlInsertSimpleBuilder($columnNames,$tableName);

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        $stmt = returnStmtWithDynamicBinding($stmt,$columnValues,$columnTypes);

        /* execute query */
        if(!$stmt->execute()){
//            echo $stmt->error;
            return null;
        }

        $newTableId = $stmt->insert_id;

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if(isset($newTableId) && $newTableId !== ''){
            return $newTableId;
        }else{
//            echo $conn->error;
            return null;
        }
    }else{
//        echo $conn->error;
        return null;
    }
}

//***************************************INSERT END****************************************************/


//***************************************UPDATE START****************************************************/

function updateDynamicData($conn,$tableName = null,$whereClause = null,$setColumnNamesOnly = null,$columnValuesAll = null,$columnTypesAll = null){
    if(!$setColumnNamesOnly || !$columnValuesAll || !$columnTypesAll){
        return null;
    }

    $sql = sqlUpdateSimpleBuilder($setColumnNamesOnly,$tableName);

    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        $stmt = returnStmtWithDynamicBinding($stmt,$columnValuesAll,$columnTypesAll);

        /* execute query */
        if(!$stmt->execute()){
//            echo $stmt->error;
            return null;
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        return true;
    }else{
//        echo $conn->error;
        return null;
    }
}

//***************************************UPDATE END****************************************************/


//****************************************INSERT INTO TRANSACTION HISTORY START****************************************************/

function investCapitalToPlan($conn,$amount,$planId,$applicationHistoryId,$uid,$isReinvest = false){
    $usdRateRows = getBtcUsdRate($conn," ORDER BY date_created DESC LIMIT 1 ");
    if(!$usdRateRows){
        //return "failed to get usdRateRows details for plan B";
        return "对不起,未知错误发生了！！";
    }

    $usdRate = $usdRateRows[0];
    $btcInvestedInUsd = bcmul($usdRate->getRate(),$amount,25);

    $userPlanRows = getUserPlan($conn," WHERE uid = ? && plan_id = ? ORDER BY date_created DESC LIMIT 1",array("uid","plan_id"),array($uid,$planId),"si");
    if($userPlanRows !== null && count($userPlanRows) > 0){
        $planRows = getPlan($conn," WHERE id = ? ORDER BY date_created DESC LIMIT 1",array("id"),array($userPlanRows[0]->getPlanId()),"i");
        if($planRows !== null && count($planRows) > 0){
            $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid),"s");
            if($userRows && count($userRows) > 0){

                $userTopupCriteriaRows = getTopupCriteria($conn," WHERE id = ? ORDER BY date_created DESC LIMIT 1",array("id"),array($userRows[0]->getTopupCriteriaId()),"i");
                if($userTopupCriteriaRows && count($userTopupCriteriaRows) > 0){
                    $maxBtc = $userTopupCriteriaRows[0]->getMaxBtc();
                }else{
                    $maxBtc = $planRows[0]->getMaxBtc();
                }

                if($amount < $planRows[0]->getMinBtc() && !$isReinvest && false){
//                    echo "too little cant invest";
                    $minBtc = $planRows[0]->getMinBtc();
                    $minBtcString = removeUselessZero($minBtc)." BTC";
                    return "捐赠数额太低了,</br>请挑高您的捐赠数额(最低: $minBtcString)";
                }else{
                    if(bcadd($amount,$userPlanRows[0]->getCapitalBtc(),25)  > $maxBtc && !$isReinvest && false){
//                        echo "too much cant invest";
                        $maxBtcString = removeUselessZero($maxBtc)." BTC)</br>您之前总共已捐赠了: ".removeUselessZero($userPlanRows[0]->getCapitalBtc())." BTC";
                        return "捐赠数额超出了您的最高捐赠限制,</br>请降低捐赠数额(最高: $maxBtcString";
                    }else{
                        $freeBtcAmountIn = bcmul($amount,$planRows[0]->getInvestmentReturn(),25);
                        //insert user investment first
                        $descriptionTH = "捐赠计划A";
                        $capitalInTHId = insertIntoTransactionHistoryWithUsd($conn,0,0,$amount,0,
                            0,0,$userPlanRows[0]->getReturnedBtc(),$userPlanRows[0]->getFreeBtc(),
                            bcadd($userPlanRows[0]->getCapitalBtc(),$amount,25),0,$uid,0,null,
                            0,$applicationHistoryId,2,$descriptionTH,$planId,$btcInvestedInUsd);

                        if(!$capitalInTHId){
//                            return "capital in error";
                            return "对不起,未知错误发生了！！";
                        }

                        //then insert the free btc
                        //now insert the free btc into free_btc table for everyday 0.2% drop. the above insert is just for transaction_history
                        $newFreeBtcTableId = insertDynamicData($conn,"free_btc",array("total_amount","uid","btc_type_id","transaction_id"),
                            array($freeBtcAmountIn,$uid,1,null),"dsii");
                        if(!$newFreeBtcTableId){
//                            return "insert free btc table error";
                            return "对不起,未知错误发生了！！";
                        }

                        updateTransactionHistoryWithNewFreeBtcId($conn,$newFreeBtcTableId,$capitalInTHId);

                        $descriptionTH = "计划A收益";
                        $freeBtcInTHId = insertIntoTransactionHistory($conn,$freeBtcAmountIn,0,0,0,
                            0,0,$userPlanRows[0]->getReturnedBtc(),bcadd($userPlanRows[0]->getFreeBtc(),$freeBtcAmountIn,25),
                            bcadd($userPlanRows[0]->getCapitalBtc(),$amount,25),0,$uid,0,$newFreeBtcTableId,
                            0,$applicationHistoryId,1,$descriptionTH,$planId);
                        if(!$freeBtcInTHId){
//                            return "free btc in error";
                            return "对不起,未知错误发生了！！";
                        }

                        //update free btc table with transaction id
                        if(!updateDynamicData($conn,"free_btc"," WHERE id = ? ",array("transaction_id"),
                            array($freeBtcInTHId,$newFreeBtcTableId),
                            "ii")){
//                            return "update free btc table's transaction id error";
                            return "对不起,未知错误发生了！！";
                        }

                        //update user's current total free btc and capital btc into user_plan table
                        if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = ? ",array("capital_btc","free_btc"),
                            array(bcadd($amount,$userPlanRows[0]->getCapitalBtc(),25),bcadd($userPlanRows[0]->getFreeBtc(),$freeBtcAmountIn,25),
                                $uid,$userPlanRows[0]->getPlanId()),
                            "ddsi")){
//                            return "update free and capital btc error";
                            return "对不起,未知错误发生了！！";
                        }

                        //update all his referral up to level 10's downlineAccumulatedbtc value
                        if(!updateAllReferrerLevelsUplineBtcAmountOfThisUserPlanA($conn,$amount,$uid,$btcInvestedInUsd)){
//                            return "update all referrer's accumulated btc error";
                            return "对不起,未知错误发生了！！";
                        }

                        //return null means success because no error message
                        return null;
                    }
                }

            }else{
                return "对不起,未知错误发生了！！";
            }
        }else{
            return "对不起,未知错误发生了！！";
        }
    }else{
        return "对不起,未知错误发生了！！";
    }
}

function reinvestFreeBtcToCapital($conn,$uid,$amount,$withdrawPositionType,$planId,$isReinvest){
    if(isAllowReinvestAndWithdraw()){
        //can let user reinvest
    }else{
        //should not allow reinvest
        return "对不起,再投资只能在每个月的10号20号或者是30号而已";
    }

    $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid),"s");
    if(!$userRows){
        return "对不起,未知错误发生了！！";
    }

    //plan A user plan
    $userPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,1),"si");
    if(!$userPlanRows){
        return "对不起,未知错误发生了！！";
    }

    $totalInvestedReturnFreeBtc = getSum($conn,"free_btc","total_amount"," WHERE uid = ? AND btc_type_id = 1 AND plan_id = 1 AND is_withdrawed = 0 ", array("uid"), array($uid),"s");
    $totalFreeBtcReceived = getSum($conn,"free_btc","total_amount"," WHERE uid = ? AND plan_id = 1 AND is_withdrawed = 0 AND current_day != 0", array("uid"), array($uid),"s");
    $totalFreeBtcReceivedExcludingInvestment = getSum($conn,"free_btc","total_amount"," WHERE uid = ? AND (btc_type_id = 4 OR btc_type_id = 5) AND plan_id = 1 ", array("uid"), array($uid),"s");
    $totalReturnedBtc = 0;
    $totalCapitalBtc = 0;
    $totalFreeBtc = 0;
    $allCombinedTotalBtc = 0;

    $totalReturnedBtc = $userPlanRows[0]->getReturnedBtc();
    $totalCapitalBtc = $userPlanRows[0]->getCapitalBtc();
    $totalFreeBtc = $userPlanRows[0]->getFreeBtc();
    $allCombinedTotalBtc = $totalReturnedBtc + $totalCapitalBtc + $totalFreeBtc;

    $planARows = getPlan($conn," WHERE id = ? ORDER BY date_updated DESC LIMIT 1",array("id"),array(1),"i");

    $planA = null;
    if($planARows){
        $planA = $planARows[0];
    }else{
        return "对不起,未知错误发生了！！";
    }

    $totalReleasedBtc = 0;

    $totalCapitalBtcPlanB = 0;
    $totalRewardCapitalBtcPlanB = 0;
    $totalReleasedBtcFasterPlanB = "-";
    $totalDownlineAccumulatedUsdPlanB = "-";
    $totalReferralCommissionPlanB = "-";
    $totalReleasedBtcNormalPlanB = "-";
    $totalInvestmentOfLevelOneDownline = 0;

    $everydayReleaseAmountOfBtc = 0;
    if($planA && $totalFreeBtcReceived > 0){
        $everydayReleaseAmountOfBtc = bcdiv(bcmul($totalFreeBtcReceived,$planA->getPercentageGiven(),25),100,25);
        //this is for display only. To let user see that his capital also dropped 0.2% but in actual fact, it didn't. When withdraw, need to manually deduct capital amount and returned btc amount
        $everydayReleaseAmountOfBtc = bcadd($everydayReleaseAmountOfBtc,bcdiv(bcmul($totalCapitalBtc,$planA->getPercentageGiven(),25),100,25),25);

        $freeBtcRows = getFreeBtc($conn," WHERE uid = ? AND plan_id = 1 ",array("uid"),array($uid),"s");
        if($freeBtcRows){
            foreach ($freeBtcRows as $freeBtc){
                //$totalReleaseBtc += (totalAmount * percentageGiven / 100 ) * totalDays;
                $totalReleasedBtc = bcadd($totalReleasedBtc,bcmul(bcdiv(bcmul($freeBtc->getTotalAmount(),$planA->getPercentageGiven(),25),100,25),$freeBtc->getCurrentDay(),25),25);

                $investCapitalPlanATHRows = getTransactionHistory($conn," WHERE free_btc_id = ? AND btc_type_id = 2 ",array("free_btc_id"),array($freeBtc->getId()),"i");
                if($investCapitalPlanATHRows){
                    //this is for display only. To let user see that his capital also dropped 0.2% but in actual fact, it didn't. When withdraw, need to manually deduct capital amount and returned btc amount
                    $totalReleasedBtc = bcadd($totalReleasedBtc,bcmul($freeBtc->getCurrentDay(),bcdiv(bcmul($investCapitalPlanATHRows[0]->getCapitalBtcAmountIn(),$planA->getPercentageGiven(),25),100,25),25),25);
                }
            }
        }

        //this is to minus the btc he withdraw previously
        $totalReleasedBtc = bcsub($totalReleasedBtc,getPreviouslyWithdrewBtc($conn,$uid,2,1),25);
    }

    $thisUserLevelOneReferralRows = getReferralHistory($conn," WHERE referrer_id = ? ",array("uid"),array($uid),"s");
    if($thisUserLevelOneReferralRows){
        foreach ($thisUserLevelOneReferralRows as $tempLevelOneReferral){
            $tempLevelOneUserPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = 1 ",array("uid"),array($tempLevelOneReferral->getReferralId()),"s");
            if($tempLevelOneUserPlanRows){
                $totalInvestmentOfLevelOneDownline = bcadd($totalInvestmentOfLevelOneDownline,$tempLevelOneUserPlanRows[0]->getCapitalBtc(),25);
            }

            $tempLevelOneUserPlanRowsPlanB = getUserPlan($conn," WHERE uid = ? AND plan_id = 2 ",array("uid"),array($tempLevelOneReferral->getReferralId()),"s");
            if($tempLevelOneUserPlanRowsPlanB){
                $totalInvestmentOfLevelOneDownline = bcadd($totalInvestmentOfLevelOneDownline,$tempLevelOneUserPlanRowsPlanB[0]->getCapitalBtc(),25);
            }
        }
        //add his own plan B capital also
        $thisUserPlanBRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,2),"si");
        if($thisUserPlanBRows){
            $totalInvestmentOfLevelOneDownline = bcadd($totalInvestmentOfLevelOneDownline,$thisUserPlanBRows[0]->getCapitalBtc(),25);
        }
    }

    $totalReferralCommissionReceived = getSum($conn,"transaction_history","returned_btc_amount_in"," WHERE uid = ? AND btc_type_id = 3 AND plan_id = 1 ",array("uid"),array($uid),"s");
    //this is to minus the btc he withdraw previously
    $totalReferralCommissionReceived = bcsub($totalReferralCommissionReceived,getPreviouslyWithdrewBtc($conn,$uid,3,1),25);

    //for Plan B only
    $userPlanBRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,2),"si");

    if(!$userPlanBRows){
        return "对不起,未知错误发生了！！";
    }

    $userPlanB = $userPlanBRows[0];

    $totalCapitalBtcPlanB = getSum($conn,"transaction_history","capital_btc_amount_in",
        " WHERE uid = ? AND btc_type_id = 2 AND plan_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ",
        array("uid"),array($uid),"s");

    $totalRewardCapitalBtcPlanB = getSum($conn,"transaction_history","free_btc_amount_in",
        " WHERE uid = ? AND btc_type_id = 1 AND plan_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ",
        array("uid"),array($uid),"s");

    $totalReleasedBtcFasterPlanB = getSum($conn,"transaction_history","returned_btc_amount_in",
        " WHERE uid = ? AND btc_type_id = 12 AND plan_id = 2 ",
        array("uid"),array($uid),"s");
    //this is to minus the btc he withdraw previously
    $totalReleasedBtcFasterPlanB = bcsub($totalReleasedBtcFasterPlanB,getPreviouslyWithdrewBtc($conn,$uid,4,2),25);

    $totalDownlineAccumulatedUsdPlanB = $userPlanB->getDownlineAccumulatedUsd();

    $totalReferralCommissionPlanB = getSum($conn,"transaction_history","returned_btc_amount_in",
        " WHERE uid = ? AND (btc_type_id = 3 OR btc_type_id = 11) AND plan_id = 2  ",
        array("uid"),array($uid),"s");
    //this is to minus the btc he withdraw previously
    $totalReferralCommissionPlanB = bcsub($totalReferralCommissionPlanB,getPreviouslyWithdrewBtc($conn,$uid,5,2),25);

    $totalReleasedBtcNormalPlanB = getSum($conn,"transaction_history","returned_btc_amount_in",
        " WHERE uid = ? AND (btc_type_id = 6 OR btc_type_id = 13) AND plan_id = 2  ",
        array("uid"),array($uid),"s");
    //this is to minus the btc he withdraw previously
    $totalReleasedBtcNormalPlanB = bcsub($totalReleasedBtcNormalPlanB,getPreviouslyWithdrewBtc($conn,$uid,6,2),25);

    if($totalCapitalBtcPlanB <= 0 || $totalCapitalBtcPlanB === "-"){
        $totalCapitalBtcPlanB = 0;
    }

    if($totalRewardCapitalBtcPlanB <= 0 || $totalRewardCapitalBtcPlanB === "-"){
        $totalRewardCapitalBtcPlanB = 0;
    }

    if($totalReleasedBtcFasterPlanB <= 0 || $totalReleasedBtcFasterPlanB === "-"){
        $totalReleasedBtcFasterPlanB = 0;
    }
    if($totalDownlineAccumulatedUsdPlanB <= 0 || $totalDownlineAccumulatedUsdPlanB === "-"){
        $totalDownlineAccumulatedUsdPlanB = 0;
    }

    if($totalReferralCommissionPlanB <= 0 || $totalReferralCommissionPlanB === "-"){
        $totalReferralCommissionPlanB = 0;
    }

    if($totalReleasedBtcNormalPlanB <= 0 || $totalReleasedBtcNormalPlanB === "-"){
        $totalReleasedBtcNormalPlanB = 0;
    }

    switch ($withdrawPositionType){
        case 1:
            return "对不起,未知错误发生了！！";
            break;
        case 2:
            if($isReinvest){
                $amount = $totalReleasedBtc;
            }

            if($amount > $totalReleasedBtc){
                return "再投资的数额不可以超过您现在所拥有的拥有的数额!";
            }
            break;
        case 3:
            if($isReinvest){
                $amount = $totalReferralCommissionReceived;
            }

            if($amount > $totalReferralCommissionReceived){
                return "再投资的数额不可以超过您现在所拥有的拥有的数额!";
            }
            break;
        case 4:
            if($isReinvest){
                $amount = $totalReleasedBtcFasterPlanB;
            }

            if($amount > $totalReleasedBtcFasterPlanB){
                return "再投资的数额不可以超过您现在所拥有的拥有的数额!";
            }
            break;
        case 5:
            if($isReinvest){
                $amount = $totalReferralCommissionPlanB;
            }

            if($amount > $totalReferralCommissionPlanB){
                return "再投资的数额不可以超过您现在所拥有的拥有的数额!";
            }
            break;
        case 6:
            if($isReinvest){
                $amount = $totalReleasedBtcNormalPlanB;
            }

            if($amount > $totalReleasedBtcNormalPlanB){
                return "再投资的数额不可以超过您现在所拥有的拥有的数额!";
            }
            break;
        default:
            return "对不起,未知错误发生了！！";
            break;
    }

    if($amount < 0.0000000001){
        return "再投资的数额不可以等于0!";
    }

    $userPlanConfirmRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,$planId),"si");
    if(!$userPlanConfirmRows){
        return "对不起,未知错误发生了！！";
    }
    $userPlanConfirm = $userPlanConfirmRows[0];

    $descriptionTH = "再捐赠";
    $reinvestTHId = insertIntoTransactionHistoryWithWithdraw($conn,0,0,0,0,0,
        $amount,bcsub($userPlanConfirm->getReturnedBtc(),$amount,25),$userPlanConfirm->getFreeBtc(),
        $userPlanConfirm->getCapitalBtc(),0,$uid,0,null,0,null,10,$descriptionTH,$planId,
        $withdrawPositionType,2,null);

    if(!$reinvestTHId){
        return "对不起,未知错误发生了！！";
    }

    if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = ? ",array("returned_btc"),
        array(bcsub($userPlanConfirm->getReturnedBtc(),$amount,25),
            $uid,$planId),
        "dsi")){
        echo "对不起,未知错误发生了！！";
    }

    if($planId === 1){
        return investCapitalToPlan($conn,$amount,$planId,null,$uid,true);
    }else if($planId === 2){
        return investCapitalToPlanB($conn,$amount,null,$uid,true);
    }else{
        return "对不起,未知错误发生了！！";
    }
}

function getPreviouslyWithdrewBtc($conn,$uid,$withdrawPositionType,$planId){
// 1 = Capital(A),
// 2 = Released/Drop(A),
// 3 = ReferralCommision(A),
// 4 - FasterRelease(B),
// 5 - ReferralCommission(B),
// 6 - MonthlyDrop(B)
    return getSum($conn,"transaction_history","returned_btc_amount_out",
        " WHERE uid = ? AND withdraw_position_type = $withdrawPositionType AND (withdraw_status = 1 OR withdraw_status = 2) AND plan_id = $planId ",
        array("uid"),array($uid),"s");
}

function updateAllReferrerLevelsUplineBtcAmountOfThisUserPlanA($conn,$investAmount,$uid,$btcInvestedInUsd){
    //update all his referral up to level 10's downlineAccumulatedbtc value
    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
    if(!$referralHistoryRows){
        //meaning this user is not referred so just return true, because no need update anything also
        return true;
    }else{
        $isAllUpdateCompleted = true;

        $allTop10ReferrerId = getTop10ReferrerOfUser($conn,$uid);
        foreach ($allTop10ReferrerId as $thisReferrerId){
//            echo "</br>" . $currentReferralHistoryRows[0]->getReferrerId() . " level " . $currentLevel . " max " . $maxLevel;
//            echo "</br>";
            $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($thisReferrerId),"s");
            if($referrerUserRows){
                if(!updateDynamicData($conn,"user"," WHERE uid = ? ",array("downline_accumulated_btc","downline_accumulated_usd"),
                    array(bcadd($referrerUserRows[0]->getDownlineAccumulatedBtc(),$investAmount,25),bcadd($referrerUserRows[0]->getDownlineAccumulatedUsd(),$btcInvestedInUsd,25),$thisReferrerId)
                    ,"dds")){
                    $isAllUpdateCompleted = false;
                }
            }
        }
        return $isAllUpdateCompleted;
    }
    //SELECT uid,username,downline_accumulated_btc FROM `user` WHERE uid = 'vlZAxQtxguXvOWZe1glStjPPLC42' || uid = 'CUSTOM-ID-cb61df544a4b209dfe9d07c30991100525f82665' || uid = 'CUSTOM-ID-da7d6e631607ad100ccb845f5ec0da16cc9dc423' || uid = 'CUSTOM-ID-31b1459c18ee7cad9fe08d5223a36b707ddfa763' || uid ='lZGYMyfT5HU3WAcQ59wyKzuHuDv1' ORDER BY `date_created` ASC
}

function investCapitalToPlanB($conn,$amount,$applicationHistoryId,$uid,$isReinvest = false){
    $planId = 2;

    $userPlanRows = getUserPlan($conn," WHERE uid = ? && plan_id = ? ORDER BY date_created DESC LIMIT 1",array("uid","plan_id"),array($uid,$planId),"si");

    if(!$userPlanRows){
        //return "failed to get user plan details for plan B";
        return "对不起,未知错误发生了！！";
    }

    $planRows = getPlan($conn," WHERE id = ? ORDER BY date_created DESC LIMIT 1",array("id"),array($planId),"i");
    if(!$planRows){
        //return "failed to get plan details for plan B";
        return "对不起,未知错误发生了！！";
    }

//    $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($uid),"s");
//    if(!$userRows){
//        //return "failed to get user details for plan B";
//        return "对不起,未知错误发生了！！";
//    }

    $usdRateRows = getBtcUsdRate($conn," ORDER BY date_created DESC LIMIT 1 ");
    if(!$usdRateRows){
        //return "failed to get usdRateRows details for plan B";
        return "对不起,未知错误发生了！！";
    }

    $usdRate = $usdRateRows[0];
//    $thisUser = $userRows[0];
    $planB = $planRows[0];
    $thisUserPlan = $userPlanRows[0];
    $maxBtc = $planRows[0]->getMaxBtc();
    $minBtc = $planRows[0]->getMinBtc();

    if($amount < $minBtc && !$isReinvest && false){
//                    echo "too little cant invest";
        $minBtc = $planB->getMinBtc();
        $minBtcString = removeUselessZero($minBtc)." BTC";
        return "捐赠数额太低了,</br>请挑高您的捐赠数额(最低: $minBtcString)";
    }

    if(bcadd($amount,$thisUserPlan->getCapitalBtc(),25)  > $maxBtc && !$isReinvest && false){
//                        echo "too much cant invest";
        $maxBtcString = removeUselessZero($maxBtc)." BTC)</br>您之前总共已捐赠了: ".removeUselessZero($thisUserPlan->getCapitalBtc())." BTC";
        return "捐赠数额超出了您的最高捐赠限制,</br>请降低捐赠数额(最高: $maxBtcString";
    }

    $freeBtcAmountIn = bcmul($amount,($planRows[0]->getInvestmentReturn() + 1),25);
    //insert user investment first
    $descriptionTH = "捐赠计划B";
    $currentBtcInUsd = bcmul($usdRate->getRate(),$amount,25);

    $capitalInTHId = insertIntoTransactionHistoryWithUsd($conn,0,0,$amount,0,
        0,0,$thisUserPlan->getReturnedBtc(),$thisUserPlan->getFreeBtc(),
        bcadd($thisUserPlan->getCapitalBtc(),$amount,25),0,$uid,0,null,
        0,$applicationHistoryId,2,$descriptionTH,$planId,$currentBtcInUsd);

    if(!$capitalInTHId){
//                            return "capital in error plan b";
        return "对不起,未知错误发生了！！";
    }

    //then insert the free btc
    //now insert the free btc into free_btc table for everyday 0.2% drop. the above insert is just for transaction_history
    $newFreeBtcTableId = insertDynamicData($conn,"free_btc",array("total_amount","uid","btc_type_id","transaction_id","plan_id"),
        array($freeBtcAmountIn,$uid,1,null,$planId),"dsiii");
    if(!$newFreeBtcTableId){
//                            return "insert free btc table error plan b";
        return "对不起,未知错误发生了！！";
    }

    updateTransactionHistoryWithNewFreeBtcId($conn,$newFreeBtcTableId,$capitalInTHId);

    $descriptionTH = "计划B收益";
    $freeBtcInTHId = insertIntoTransactionHistory($conn,$freeBtcAmountIn,0,0,0,
        0,0,$thisUserPlan->getReturnedBtc(),bcadd($thisUserPlan->getFreeBtc(),$freeBtcAmountIn,25),
        bcadd($thisUserPlan->getCapitalBtc(),$amount,25),0,$uid,0,$newFreeBtcTableId,
        0,$applicationHistoryId,1,$descriptionTH,$planId);
    if(!$freeBtcInTHId){
//                            return "free btc in error plan b";
        return "对不起,未知错误发生了！！";
    }

    //update free btc table with transaction id
    if(!updateDynamicData($conn,"free_btc"," WHERE id = ? ",array("transaction_id"),
        array($freeBtcInTHId,$newFreeBtcTableId),
        "ii")){
//                            return "update free btc table's transaction id error plan b";
        return "对不起,未知错误发生了！！";
    }

    //update user's current total free btc and capital btc into user_plan table
    if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = ? ",array("capital_btc","free_btc"),
        array(bcadd($amount,$userPlanRows[0]->getCapitalBtc(),25),bcadd($userPlanRows[0]->getFreeBtc(),$freeBtcAmountIn,25),
            $uid,$planId),
        "ddsi")){
//                            return "update free and capital btc error plan b";
        return "对不起,未知错误发生了！！";
    }

    //update all his referral up to level 10's downlineAccumulatedbtc value
    if(!updateAllReferrerLevelsUplineBtcAmountOfThisUserPlanB($conn,$amount,$currentBtcInUsd,$uid)){
//                            return "update all referrer's accumulated btc error plan b";
        return "对不起,未知错误发生了！！";
    }

    //return null means success because no error message
    return null;
}

function updateAllReferrerLevelsUplineBtcAmountOfThisUserPlanB($conn,$investAmount,$currentBtcInUsd,$uid){
    //update all his referral up to level 10's downlineAccumulatedbtc value
    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
    if(!$referralHistoryRows){
        //meaning this user is not referred so just return true, because no need update anything also
        return true;
    }else{
        $isAllUpdateCompleted = true;
        $allTop10ReferrerId = getTop10ReferrerOfUser($conn,$uid);
        $level = 0;

        $achievementRewardRows = getAchievementReward($conn," WHERE plan_id = 2 ORDER BY level ASC ");
        if(!$achievementRewardRows){
//            return "failed to get achievement reward at plan b";
            return "对不起,未知错误发生了！！";
        }

        $referralCommissionRows = getReferralCommission($conn," WHERE plan_id = 2 ORDER BY level ASC ");
        if(!$referralCommissionRows){
//            return "failed to get achievement reward at plan b";
            return "对不起,未知错误发生了！！";
        }

        foreach ($allTop10ReferrerId as $thisReferrerId){
//            echo "</br>" . $currentReferralHistoryRows[0]->getReferrerId() . " level " . $currentLevel . " max " . $maxLevel;
//            echo "</br>";
            $referrerUserPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = 2 ",array("uid"),array($thisReferrerId),"s");
            if($referrerUserPlanRows){
                if($referrerUserPlanRows[0]->getCapitalBtc() <= 0){
                    continue;
                }

                $thisReferrerUserPlan = $referrerUserPlanRows[0];
                $thisReferrerTotalDownlineBtc = bcadd($thisReferrerUserPlan->getDownlineAccumulatedBtc(),$investAmount,25);
                $thisReferrerTotalDownlineUsd = bcadd($thisReferrerUserPlan->getDownlineAccumulatedUsd(),$currentBtcInUsd,25);
                if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = 2 ",array("downline_accumulated_btc","downline_accumulated_usd"),array($thisReferrerTotalDownlineBtc,$thisReferrerTotalDownlineUsd,$thisReferrerId),"dds")){
                    $isAllUpdateCompleted = false;
                }else{
                    //this is for 社区奖金
                    $rewardCommissionInsertStatus = rewardUserWithReferralCommissionPlanB($conn,$level,$thisReferrerTotalDownlineUsd,$thisReferrerId,$investAmount,$thisReferrerUserPlan,$referralCommissionRows[$level]);
                    if($rewardCommissionInsertStatus !== null){
//                        return $rewardCommissionInsertStatus;
                    }

                    //get user_plan with newest value again
                    $referrerUserPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = 2 ",array("uid"),array($thisReferrerId),"s");
                    $thisReferrerUserPlan = $referrerUserPlanRows[0];
                    $thisReferrerTotalDownlineBtc = $thisReferrerUserPlan->getDownlineAccumulatedBtc();
                    $thisReferrerTotalDownlineUsd = $thisReferrerUserPlan->getDownlineAccumulatedUsd();

                    //this is for the achievement reward 卓越奖金
                    $rewardInsertStatus = rewardUserAchievementReward($conn,$achievementRewardRows,$thisReferrerTotalDownlineUsd,$thisReferrerId,$investAmount,$thisReferrerUserPlan);
                    if($rewardInsertStatus !== null){
//                        return $rewardInsertStatus;
                    }
                }
            }
            $level++;
        }
        return $isAllUpdateCompleted;
    }
    //SELECT uid,username,downline_accumulated_btc FROM `user` WHERE uid = 'vlZAxQtxguXvOWZe1glStjPPLC42' || uid = 'CUSTOM-ID-cb61df544a4b209dfe9d07c30991100525f82665' || uid = 'CUSTOM-ID-da7d6e631607ad100ccb845f5ec0da16cc9dc423' || uid = 'CUSTOM-ID-31b1459c18ee7cad9fe08d5223a36b707ddfa763' || uid ='lZGYMyfT5HU3WAcQ59wyKzuHuDv1' ORDER BY `date_created` ASC
}

function rewardUserWithReferralCommissionPlanB($conn,$level,$thisReferrerTotalDownlineUsd,$thisReferrerId,$investAmount,$thisReferrerUserPlan,$referralCommission){
    //if everything dropped finished dont reward him already
    if($thisReferrerUserPlan->getFreeBtc() <= 0){
        return null;
    }

    $planId = 2;
    // if level 1, this is this user get 10% of his level 1 downline invested amount and let his freeBtc drop faster
    // (THIS IS NOT BONUS,IS A WAY TO LET THIS USER FASTER DROP FINISH HIS X9 OR 7 FREE BTC)
    if($level === 0){
        $droppedBtc = bcdiv(bcmul($investAmount,10,25),100,25);
        if($thisReferrerUserPlan->getFreeBtc() > 0){
            if(bcsub($thisReferrerUserPlan->getFreeBtc(),$droppedBtc,25) <= 0){
                //d - (d - f);
                $droppedBtc = bcsub($droppedBtc,bcsub($droppedBtc,$thisReferrerUserPlan->getFreeBtc(),25),25);
            }

            $descriptionTH = "获得下线投资的10%加速释放";
            $currentTotalReturnedBtc = bcadd($thisReferrerUserPlan->getReturnedBtc(),$droppedBtc,25);
            $currentTotalFreeBtc = bcsub($thisReferrerUserPlan->getFreeBtc(),$droppedBtc,25);
            $droppedBtcInTHId = insertIntoTransactionHistory($conn,0,$droppedBtc,0,0,
                $droppedBtc,0,$currentTotalReturnedBtc,$currentTotalFreeBtc,
                $thisReferrerUserPlan->getCapitalBtc(),0,$thisReferrerId,10,null,
                0,null,12,$descriptionTH,$planId);

            if(!$droppedBtcInTHId){
//                            return "insert 加速释放 dropped btc error plan b";
                echo "对不起,未知错误发生了！！";
            }

            $droppedBtcInFreeBtcId = insertDynamicData($conn,"free_btc",
                array("total_amount","is_finished","uid","btc_type_id","transaction_id","plan_id"),
                array($droppedBtc,1,$thisReferrerId,12,$droppedBtcInTHId,$planId),
                "disiii");

            if(!$droppedBtcInFreeBtcId){
//                            return "insert 加速释放 dropped btc into free_btc error plan b";
                echo "对不起,未知错误发生了！！";
            }

            if(!updateTransactionHistoryWithNewFreeBtcId($conn,$droppedBtcInFreeBtcId,$droppedBtcInTHId)){
                echo "对不起,未知错误发生了！！";
            }

            //update user's current total free btc and capital btc into user_plan table
            if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = ? ",array("returned_btc","free_btc"),
                array($currentTotalReturnedBtc,$currentTotalFreeBtc,$thisReferrerId,$planId),
                "ddsi")){
//                            return "update free and capital btc error plan b";
                echo "对不起,未知错误发生了！！";
            }

            //get user_plan with newest value again
            $referrerUserPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = 2 ",array("uid"),array($thisReferrerId),"s");
            $thisReferrerUserPlan = $referrerUserPlanRows[0];
            $thisReferrerTotalDownlineBtc = $thisReferrerUserPlan->getDownlineAccumulatedBtc();
            $thisReferrerTotalDownlineUsd = $thisReferrerUserPlan->getDownlineAccumulatedUsd();
        }

    }

    if($thisReferrerTotalDownlineUsd >= $referralCommission->getUsdNeeded()){
        $commissionEarnedBtc = bcdiv(bcmul($investAmount,$referralCommission->getCommission(),25),100,25);
        $currentTotalReturnedBtc = bcadd($commissionEarnedBtc,$thisReferrerUserPlan->getReturnedBtc(),25);
        $currentTotalFreeBtc = $thisReferrerUserPlan->getFreeBtc();
        $currentTotalCapitalBtc = $thisReferrerUserPlan->getCapitalBtc();
        $descriptionTH = "获得下线第".($level + 1)."代投资的".$referralCommission->getCommission()."%";

        $commissionBtcInTHId = insertIntoTransactionHistory($conn,0,0,0,0,
            $commissionEarnedBtc,0,$currentTotalReturnedBtc,$currentTotalFreeBtc,
            $currentTotalCapitalBtc,0,$thisReferrerId,$referralCommission->getCommission(),null,
            0,null,3,$descriptionTH,$planId);

        if(!$commissionBtcInTHId){
            echo "对不起,未知错误发生了！！";
        }

        //free_btc
        $commissionFreeBtcTableId = insertDynamicData($conn,"free_btc",
            array("total_amount","is_finished","uid","btc_type_id","transaction_id","plan_id"),
            array($commissionEarnedBtc,1,$thisReferrerId,3,$commissionBtcInTHId,$planId),
            "disiii");

        if(!$commissionFreeBtcTableId){
            echo "对不起,未知错误发生了！！";
        }

        if(!updateTransactionHistoryWithNewFreeBtcId($conn,$commissionFreeBtcTableId,$commissionBtcInTHId)){
            echo "对不起,未知错误发生了！！";
        }

        //update user's current total free btc and capital btc into user_plan table
        if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = ? ",array("returned_btc"),
            array($currentTotalReturnedBtc,$thisReferrerId,$planId),
            "dsi")){
//                            return "update free and capital btc error plan b";
            echo "对不起,未知错误发生了！！";
        }

    }

    return null;
}

function rewardUserAchievementReward($conn,$achievementRewardRows,$thisReferrerTotalDownlineUsd,$thisReferrerId,$investAmount,$thisReferrerUserPlan){
    //this is for the achievement reward 卓越奖金
    $currentAchievementReward = null;
    //this for loop is to get the topmost achievement this user/upline can get
    foreach ($achievementRewardRows as $tempAchievementReward){
        //check whether this user's total downline investment in usd is fulfilling the total usd needed
        if($thisReferrerTotalDownlineUsd >= $tempAchievementReward->getMinTotalUsd()){
            $totalLevelThisUserHas = getWholeDownlineTree($conn,$thisReferrerId,true);
            if($totalLevelThisUserHas >= $tempAchievementReward->getLevel())
                $currentAchievementReward = $tempAchievementReward;
        }else{
            break;
        }
    }

    if($currentAchievementReward && $currentAchievementReward->getPercentage() > 0){
        //get only once, need check transaction if this user already received this reward before for eg. (if he received reward no.3, then 1 - 3 he should not receive again)
        $achievementFreeBtcRows = getFreeBtc($conn," WHERE uid = ? AND achievement_reward_id >= ? AND plan_id = 2 ",array("uid","achievement_reward_id"),array($thisReferrerId,$currentAchievementReward->getId()),"si");
        if(!$achievementFreeBtcRows){
            //ok to give him reward

            $rewardFreeBtcAmountIn = bcdiv(bcmul($investAmount,$currentAchievementReward->getPercentage(),25),100,25);
            $descriptionTH = "获得第".$currentAchievementReward->getLevel()."代".$currentAchievementReward->getPercentage()."%的卓越奖金";

            // 1) put into free_btc table
            $rewardBtcFreeBtcId = insertDynamicData($conn,"free_btc"
                ,array("total_amount","uid","btc_type_id","achievement_reward_id","plan_id")
                ,array($rewardFreeBtcAmountIn,$thisReferrerId,11,$currentAchievementReward->getId(),2)
                ,"dsiii");

            if(!$rewardBtcFreeBtcId){
//                                return "failed to insert into free_btc for achievement reward at plan b for user $thisReferrerId";
                return "对不起,未知错误发生了！！";
            }

            // 2) put into transaction history
            $rewardBtcTHId = insertIntoTransactionHistory($conn,0,0,0,0,
                $rewardFreeBtcAmountIn,0,bcadd($rewardFreeBtcAmountIn,$thisReferrerUserPlan->getReturnedBtc(),25),$thisReferrerUserPlan->getFreeBtc(),
                $thisReferrerUserPlan->getCapitalBtc(),0,$thisReferrerId,$currentAchievementReward->getPercentage(),$rewardBtcFreeBtcId,
                0,null,11,$descriptionTH,2);

            if(!$rewardBtcTHId){
//                                return "failed to insert transaction history for reward at plan b";
                return "对不起,未知错误发生了！！";
            }

            // 3) update free btc table with transaction id
            if(!updateDynamicData($conn,"free_btc"," WHERE id = ? ",array("transaction_id"),
                array($rewardBtcTHId,$rewardBtcFreeBtcId),
                "ii")){
//                            return "update free btc table's transaction id for reward error plan b";
                return "对不起,未知错误发生了！！";
            }

            // 4) finally update user_plan with new value
            if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = 2 ",array("returned_btc"),array(bcadd($rewardFreeBtcAmountIn,$thisReferrerUserPlan->getReturnedBtc(),25),$thisReferrerId),"ds")){
//                                return "failed to update referrer user: $thisReferrerId into achievement reward received into user_plan at plan b";
                return "对不起,未知错误发生了！！";
            }
        }
    }

    return null;
}

function getTop10ReferrerOfUser($conn,$uid){
    $all10ReferrerId = array();

    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");

    if(!$referralHistoryRows){
        //meaning this user is not referred by anyone
    }else{
        $currentLevel = $referralHistoryRows[0]->getCurrentLevel();
        $maxLevel = $currentLevel - 10;//minus ten because i want update the people above me not the 1 below me and update all the people above me

        $currentReferralId = $uid;
        while($currentLevel > 0 && $maxLevel < $currentLevel){
            $currentReferralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($currentReferralId),"s");

            if($currentReferralHistoryRows){
                $currentReferralId = $currentReferralHistoryRows[0]->getReferrerId();
                array_push($all10ReferrerId,$currentReferralId);
            }else{
                break;
            }
            $currentLevel--;
        }
    }
    return $all10ReferrerId;
}

function getTop3ReferrerOfUserFOR_TESTING($conn,$uid){
    $all10ReferrerId = array();

    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");

    if(!$referralHistoryRows){
        //meaning this user is not referred by anyone
    }else{
        $currentLevel = $referralHistoryRows[0]->getCurrentLevel();
        $maxLevel = $currentLevel - 3;//minus ten because i want update the people above me not the 1 below me and update all the people above me

        $currentReferralId = $uid;
        while($currentLevel > 0 && $maxLevel < $currentLevel){
            $currentReferralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($currentReferralId),"s");

            if($currentReferralHistoryRows){
                $currentReferralId = $currentReferralHistoryRows[0]->getReferrerId();
                array_push($all10ReferrerId,$currentReferralId);
            }else{
                break;
            }
            $currentLevel--;
        }
    }
    return $all10ReferrerId;
}

function insertIntoTransactionHistory($conn,$freeBtcAmountIn,$freeBtcAmountOut,$capitalBtcAmountIn,$capitalBtcAmountOut,
                                      $returnedBtcAmountIn,$returnedBtcAmountOut,$currentTotalReturnedBtc,
                                      $currentTotalFreeBtc,$currentTotalCapitalBtc,$day,$uid,$percentageGiven,$freeBtcId,
                                      $totalPercentageGiven,$applicationHistoryId,$btcTypeId,$description,$planId){
    $tableName = "transaction_history";
    $columnNames = array("free_btc_amount_in","free_btc_amount_out","capital_btc_amount_in","capital_btc_amount_out",
                         "returned_btc_amount_in","returned_btc_amount_out","current_total_returned_btc","current_total_free_btc",
                         "current_total_capital_btc","day","uid","percentage_given","free_btc_id","total_percentage_given",
                         "application_history_id","btc_type_id","description","plan_id");
    $columnValues = array($freeBtcAmountIn,$freeBtcAmountOut,$capitalBtcAmountIn,$capitalBtcAmountOut,
                          $returnedBtcAmountIn,$returnedBtcAmountOut,$currentTotalReturnedBtc,
                          $currentTotalFreeBtc,$currentTotalCapitalBtc,
                          $day,$uid,$percentageGiven,$freeBtcId,$totalPercentageGiven,$applicationHistoryId,$btcTypeId,$description,$planId);
    $columnTypes = "dddddddddisdidiisi";

    //this returns either null (error) or new row id (success)
    return insertDynamicData($conn,$tableName,$columnNames,$columnValues,$columnTypes);
}

function insertIntoTransactionHistoryWithUsd($conn,$freeBtcAmountIn,$freeBtcAmountOut,$capitalBtcAmountIn,$capitalBtcAmountOut,
                                      $returnedBtcAmountIn,$returnedBtcAmountOut,$currentTotalReturnedBtc,
                                      $currentTotalFreeBtc,$currentTotalCapitalBtc,$day,$uid,$percentageGiven,$freeBtcId,
                                      $totalPercentageGiven,$applicationHistoryId,$btcTypeId,$description,$planId,$btcInvestedInUsd){
    $tableName = "transaction_history";
    $columnNames = array("free_btc_amount_in","free_btc_amount_out","capital_btc_amount_in","capital_btc_amount_out",
        "returned_btc_amount_in","returned_btc_amount_out","current_total_returned_btc","current_total_free_btc",
        "current_total_capital_btc","day","uid","percentage_given","free_btc_id","total_percentage_given",
        "application_history_id","btc_type_id","description","plan_id","btc_invested_in_usd");
    $columnValues = array($freeBtcAmountIn,$freeBtcAmountOut,$capitalBtcAmountIn,$capitalBtcAmountOut,
        $returnedBtcAmountIn,$returnedBtcAmountOut,$currentTotalReturnedBtc,
        $currentTotalFreeBtc,$currentTotalCapitalBtc,
        $day,$uid,$percentageGiven,$freeBtcId,$totalPercentageGiven,$applicationHistoryId,$btcTypeId,$description,$planId,
        $btcInvestedInUsd);
    $columnTypes = "dddddddddisdidiisid";

    //this returns either null (error) or new row id (success)
    return insertDynamicData($conn,$tableName,$columnNames,$columnValues,$columnTypes);
}

function insertIntoTransactionHistoryWithWithdraw($conn,$freeBtcAmountIn,$freeBtcAmountOut,$capitalBtcAmountIn,$capitalBtcAmountOut,
                                      $returnedBtcAmountIn,$returnedBtcAmountOut,$currentTotalReturnedBtc,
                                      $currentTotalFreeBtc,$currentTotalCapitalBtc,$day,$uid,$percentageGiven,$freeBtcId,
                                      $totalPercentageGiven,$applicationHistoryId,$btcTypeId,$description,$planId,
                                      $withdrawPositionType,$withdrawStatus,$withdrawalId){
    $tableName = "transaction_history";
    $columnNames = array("free_btc_amount_in","free_btc_amount_out","capital_btc_amount_in","capital_btc_amount_out",
        "returned_btc_amount_in","returned_btc_amount_out","current_total_returned_btc","current_total_free_btc",
        "current_total_capital_btc","day","uid","percentage_given","free_btc_id","total_percentage_given",
        "application_history_id","btc_type_id","description","plan_id","withdraw_position_type","withdraw_status","withdrawal_id");
    $columnValues = array($freeBtcAmountIn,$freeBtcAmountOut,$capitalBtcAmountIn,$capitalBtcAmountOut,
        $returnedBtcAmountIn,$returnedBtcAmountOut,$currentTotalReturnedBtc,
        $currentTotalFreeBtc,$currentTotalCapitalBtc,
        $day,$uid,$percentageGiven,$freeBtcId,$totalPercentageGiven,$applicationHistoryId,$btcTypeId,$description,$planId,
        $withdrawPositionType,$withdrawStatus,$withdrawalId);
    $columnTypes = "dddddddddisdidiisiiii";

    //this returns either null (error) or new row id (success)
    return insertDynamicData($conn,$tableName,$columnNames,$columnValues,$columnTypes);
}

function updateTransactionHistoryWithNewFreeBtcId($conn,$newFreeBtcTableId,$transactionHistoryId){
    return updateDynamicData($conn,"transaction_history"," WHERE id = ? ",array("free_btc_id"),array($newFreeBtcTableId,$transactionHistoryId),"ii");
}

                                            //****************DISPLAY RESULT*******************/
function displayTransactionHistory($conn,$uid,$planId,$limit = null){
    $whereClause = " WHERE uid = ? AND plan_id = ? ORDER BY date_created DESC, id DESC ";
    if($limit){
        $whereClause .= " LIMIT $limit ";
    }
    $transactionHistoryRows = getTransactionHistory($conn,$whereClause, array("uid","plan_id"),array($uid,$planId),"si");
    if($transactionHistoryRows && count($transactionHistoryRows) > 0){
        $results = "";

        foreach ($transactionHistoryRows as $transactionHistory) {
            $transactionDate = date( 'd/m/Y', strtotime($transactionHistory->getDateCreated()) );
            $description = $transactionHistory->getDescription();

            $btcType = $transactionHistory->getBtcTypeId();

            $btcIn = "-";
            $btcInCss = "";
            if($transactionHistory->getFreeBtcAmountIn() > 0){
                $btcIn = $transactionHistory->getFreeBtcAmountIn();
            }else if($transactionHistory->getCapitalBtcAmountIn() > 0){
                $btcIn = $transactionHistory->getCapitalBtcAmountIn();
            }else if($transactionHistory->getReturnedBtcAmountIn() > 0){
                $btcIn = $transactionHistory->getReturnedBtcAmountIn();
            }
            if($btcIn !== "-"){
                $btcIn = removeUselessZero($btcIn);
                $btcInCss = getBtcInOutCss($btcType);
            }

            $btcOut = "-";
            $btcOutCss = "";
            if($transactionHistory->getFreeBtcAmountOut() > 0){
                $btcOut = $transactionHistory->getFreeBtcAmountOut();
            }else if($transactionHistory->getCapitalBtcAmountOut() > 0){
                $btcOut = $transactionHistory->getCapitalBtcAmountOut();
            }else if($transactionHistory->getReturnedBtcAmountOut() > 0){
                $btcOut = $transactionHistory->getReturnedBtcAmountOut();
            }
            if($btcOut !== "-"){
                $btcOut = removeUselessZero($btcOut);
                $btcOutCss = getBtcInOutCss($btcType);
            }

            $returnedBtc = $transactionHistory->getCurrentTotalReturnedBtc();
            $totalFreeBtc = bcadd($transactionHistory->getCurrentTotalFreeBtc(),$returnedBtc,25);
            $totalCapitalBtc = $transactionHistory->getCurrentTotalCapitalBtc();
            $allCombinedTotalBtc = bcadd($totalFreeBtc,$totalCapitalBtc,25);

            $results .= '
                    <tr class="transparent-tr">
                        <td class="th1">'.$transactionDate.'</td>
                        <td class="th2">'.$description.'</td>
                        <td class="th3 '.$btcInCss.' ">'.$btcIn.'</td>
                        <td class="th4 '.$btcOutCss.' ">'.$btcOut.'</td>
                        <td class="th5">'.removeUselessZero($totalFreeBtc).'</td>
                        <td class="th6">'.removeUselessZero($totalCapitalBtc).'</td>
                        <td class="th7">'.removeUselessZero($allCombinedTotalBtc).'</td>
                    </tr>  
                ';
        }

        return $results;
    }else{
        return '
            <tr class="transparent-tr">
                <td class="th1">-</td>
                <td class="th2">-</td>
                <td class="th3">-</td>
                <td class="th4">-</td>
                <td class="th5">-</td>
                <td class="th6">-</td>
                <td class="th7">-</td>
            </tr>  
            ';
    }
}

function getBtcInOutCss($btcType){
    switch ($btcType){
        case 1:
            return "color-reward";
            break;
        case 2:
            return "color-invest";
            break;
        case 3:
            return "color-downline";
            break;
        case 4:
            return "color-freebtc";
            break;
        case 5:
            return "color-freebtc";
            break;
        case 6:
            return "color-downline";
            break;
        case 7:
            return "color-other";
            break;
        case 8:
            return "color-other";
            break;
        case 9:
            return "color-reward";
            break;
        case 10:
            return "color-invest";
            break;
        case 11:
            return "color-reward";
            break;
        default:
            return "color-other";
            break;
    }
}

//****************************************INSERT INTO TRANSACTION HISTORY END****************************************************/


//****************************************WITHDRAW TRANSACTION HISTORY START****************************************************/

function getCurrentTimeInMalaysiaTimeMsWithDBFunction($minusMs){
    $currentTime = idate("U");//unix
    $timezoneOffsetInSecond = idate("Z");//get this server's timezone offset
    $ourTimeZoneMalaysiaOffset = 60 * 60 * 8;//our malaysia timezone +8 GMT
    $finalTimeInMalaysiaTimeZone = $currentTime - $timezoneOffsetInSecond + $ourTimeZoneMalaysiaOffset;
    return ($finalTimeInMalaysiaTimeZone - $minusMs);
}

function withdrawCapitalPlanA($conn,$uid,$withdrawalId){
    //0 is nothing 3 is rejected, so can get
    //only allow withdrawal of investment that is less than or equal to 90 days
    $threeMonthInMs = 60 * 60 * 24 * 90;//90 days
    $initialDate = date("Y-m-d H:i:s",getCurrentTimeInMalaysiaTimeMsWithDBFunction($threeMonthInMs));
    $transactionHistoryRows = getTransactionHistory($conn," WHERE uid = ? AND date_created >= ? AND btc_type_id = 2 AND plan_id = 1 AND (withdraw_status = 0 OR withdraw_status = 3) ",array("uid","date_created"),array($uid,$initialDate),"ss");

    if($transactionHistoryRows){
        $totalCapitalWithdrawnBtc = 0;
        $totalFreeBtcToDeduct = 0;
        $totalCapitalBtcDropped = 0;

        foreach ($transactionHistoryRows as $transactionHistory){
            if(!updateDynamicData($conn,"transaction_history"," WHERE id = ?",
                array("withdraw_position_type","withdraw_status","withdrawal_id"),
                array(1,1,$withdrawalId,$transactionHistory->getId()),
                "iiii")){
                return "对不起,未知错误发生了！！";
            }

            if(!updateDynamicData($conn,"free_btc"," WHERE id = ? ",
                array("is_finished","is_withdrawed"),
                array(1,1,$transactionHistory->getFreeBtcId()),
                "iis")){
                return "对不起,未知错误发生了！！";
            }

            $totalReturnedBtc = getSum($conn,"transaction_history","returned_btc_amount_in",
                " WHERE uid = ? AND free_btc_id = ? AND plan_id = 1 AND btc_type_id = 9 ",
                array("uid","free_btc_id"),
                array($uid,$transactionHistory->getFreeBtcId()),
                "si");

            $freeBtcRows = getFreeBtc($conn," WHERE id = ? ",array("id"),array($transactionHistory->getFreeBtcId()),"i");

            //need calculate how many already dropped before proceed, need total capital - all dropped btc from capital
            $realDivideNumber = bcdiv($freeBtcRows[0]->getTotalAmount(),$transactionHistory->getCapitalBtcAmountIn(),25);
            $totalCapitalBtcDropped = bcadd($totalCapitalBtcDropped,bcdiv($totalReturnedBtc,$realDivideNumber,25),25);

            if($freeBtcRows){
                $totalFreeBtcToDeduct = bcadd($totalFreeBtcToDeduct,$freeBtcRows[0]->getTotalAmount(),25);
                $totalFreeBtcToDeduct = bcsub($totalFreeBtcToDeduct,$totalReturnedBtc,25);
            }

            $totalCapitalWithdrawnBtc = bcadd($totalCapitalWithdrawnBtc,$transactionHistory->getCapitalBtcAmountIn(),25);
        }

        $userPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = 1 ",array("uid"),array($uid),"s");

        if(!$userPlanRows){
            return "对不起,未知错误发生了！！";
        }

        //need calculate how many already dropped before proceed, need total capital - all dropped btc from capital
        $descriptionTH = "提取捐赠";
        $withdrawTHId = insertIntoTransactionHistoryWithWithdraw(
            $conn,0,$totalFreeBtcToDeduct,0,bcsub($totalCapitalWithdrawnBtc,$totalCapitalBtcDropped,25),
            0,0,$userPlanRows[0]->getReturnedBtc(),bcsub($userPlanRows[0]->getFreeBtc(),$totalFreeBtcToDeduct,25),
            bcsub($userPlanRows[0]->getCapitalBtc(),$totalCapitalWithdrawnBtc,25),0,$uid,0,null,
            0,null,7,$descriptionTH,1,1,1,$withdrawalId);

        if(!$withdrawTHId){
            return "对不起,未知错误发生了！！";
        }

        //make capital btc become 0 because withdrew everything already
        if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = 1 ",
            array("capital_btc","free_btc"),
            array(bcsub($userPlanRows[0]->getCapitalBtc(),$totalCapitalWithdrawnBtc,25),bcsub($userPlanRows[0]->getFreeBtc(),$totalFreeBtcToDeduct,25),$uid),
            "dds")){
            return "对不起,未知错误发生了！！";
        }

        //need to deduct all upline's total $downlineAccumulatedBtc due to his downline withdrawn some btc
        $allTop10ReferrerId = getTop10ReferrerOfUser($conn,$uid);
        foreach ($allTop10ReferrerId as $thisReferrerId){
            $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($thisReferrerId),"s");
            if($referrerUserRows){
                $thisReferrerUser = $referrerUserRows[0];
                if(!updateDynamicData($conn,"user"," WHERE uid = ? ",
                    array("downline_accumulated_btc"),
                    array(bcsub($thisReferrerUser->getDownlineAccumulatedBtc(),$totalCapitalWithdrawnBtc,25),$thisReferrerId),
                    "ds")){
                    return "对不起,未知错误发生了！！";
                }
            }
        }

        return null;
    }else{
        return "没有捐赠可以提取";
    }
}

function withdrawReturnedBtc($conn,$uid,$amount,$planId,$withdrawPositionType,$withdrawalId){
    if($withdrawPositionType === 1){
        return "对不起,未知错误发生了！！";
    }
    //after confirm only deduct the user_plan's returned_btc amount, for now just record as pending only

    $userPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,$planId),"si");
    $userPlan = $userPlanRows[0];

    $descriptionTH = "提取收益";
    $withdrawTHId = insertIntoTransactionHistoryWithWithdraw(
        $conn,0,0,0,0,
        0,$amount,bcsub($userPlan->getReturnedBtc(),$amount,25),$userPlan->getFreeBtc(),
        $userPlan->getCapitalBtc(),0,$uid,0,null,
        0,null,8,$descriptionTH,$planId,$withdrawPositionType,1,$withdrawalId);

    if(!$withdrawTHId){
        return "对不起,未知错误发生了！！";
    }else{
        return null;
    }
}

//****************************************WITHDRAW TRANSACTION HISTORY END******************************************************/


//****************************************GET WHOLE DOWNLINE START*************************************************************/

function getWholeDownlineTree($conn, $uid, $isReturnAsTotalNumberOfLevels){
    $referralHistoryRows = array();
    $thisUserFirstTimeReferralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ORDER BY id ASC LIMIT 1 ",array("referrer_id"),array($uid),"s");
    if($thisUserFirstTimeReferralHistoryRows){
        $topReferrerId = $thisUserFirstTimeReferralHistoryRows[0]->getTopReferrerId();
        if($topReferrerId === $uid){
            $thisUserFirstLevel = 1;
            $thisUserLastLevel = 10;
        }else{
            $thisUserFirstLevel = $thisUserFirstTimeReferralHistoryRows[0]->getCurrentLevel();
            $thisUserLastLevel = $thisUserFirstLevel + 9;
        }

        $downlineReferralIdArray = array();
        $downlineReferralIdArray[$uid]  = true ;
        for($loopCurrentLevel = $thisUserFirstLevel; $loopCurrentLevel <= $thisUserLastLevel; $loopCurrentLevel++){
            $thisLevelReferralHistoryRows = getReferralHistory($conn," WHERE top_referrer_id = ? AND current_level = $loopCurrentLevel ORDER BY id ",array("top_referrer_id"),array($topReferrerId),"s");
            if($thisLevelReferralHistoryRows){
                $newDownlineReferralIdArray = array();
                foreach ($thisLevelReferralHistoryRows as $tempReferral) {
                    $canAdd = false;
                    foreach ($downlineReferralIdArray as $key => $value) {
                        if($tempReferral->getReferrerId() === $key){
                            $newDownlineReferralIdArray[$tempReferral->getReferralId()]  = true ;
                            $canAdd = true;
                            break;
                        }
                    }
                    if($canAdd){
                        array_push($referralHistoryRows,$tempReferral);
                    }
                }
                unset($downlineReferralIdArray);
                $downlineReferralIdArray = array();
                foreach ($newDownlineReferralIdArray as $newDownlineReferralId => $value) {
                    $downlineReferralIdArray[$newDownlineReferralId]  = true ;
                }
                unset($newDownlineReferralIdArray);
            }
        }
    }

    if($isReturnAsTotalNumberOfLevels){
        if(count($referralHistoryRows) <= 0){
            return 0;
        }else{
            return $referralHistoryRows[count($referralHistoryRows) - 1]->getCurrentLevel() - $thisUserFirstLevel + 1;
        }
    }else{
        return $referralHistoryRows;
    }
}

//****************************************GET WHOLE DOWNLINE END***************************************************************/

function removeUselessZero($decimalValue){
//    return floatval(sprintf("%.3f", $decimalValue + 0));
//    return rtrim(sprintf('%f',$decimalValue),'0');
//    return sprintf('%f', $decimalValue + 0);

    //from here https://stackoverflow.com/questions/1471674/why-is-php-printing-my-number-in-scientific-notation-when-i-specified-it-as-00
    if($decimalValue !== "-" && $decimalValue > 0){
        return rtrim(rtrim(sprintf('%.8F', $decimalValue), '0'), ".");
    }else{
        return 0;
    }
}

function getTotalMonthDifference($startDate,$endDate){
//    $startDate = '2000-01-25';
//    $endDate = '2010-02-20';

//    $ts1 = strtotime($startDate);
//    $ts2 = strtotime($endDate);
//
//    $year1 = date('Y', $ts1);
//    $year2 = date('Y', $ts2);
//
//    $month1 = date('m', $ts1);
//    $month2 = date('m', $ts2);
//
//    $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
//
//    echo "<p>start $startDate end $endDate diff $diff</p>";
//    return $diff;

    //better one from here https://stackoverflow.com/questions/4233605/elegant-way-to-get-the-count-of-months-between-two-dates
    $d1 = new DateTime($startDate);
    $d2 = new DateTime($endDate);

//    echo($d1->diff($d2)->m); // int(4)
    $diff = ($d1->diff($d2)->m + ($d1->diff($d2)->y*12)); // int(8)
    return $diff;
}

function isAllowReinvestAndWithdraw(){
    //only allow 10,20,30 12pm until next day's 11.59am to withdraw or reinvest

    $currentTime = idate("U");//unix
    $timezoneOffsetInSecond = idate("Z");//get this server's timezone offset
    $ourTimeZoneMalaysiaOffset = 60 * 60 * 8;//our malaysia timezone +8 GMT
    $finalTimeInMalaysiaTimeZone = $currentTime - $timezoneOffsetInSecond + $ourTimeZoneMalaysiaOffset;

    $day = idate("d",$finalTimeInMalaysiaTimeZone);
    $previousDay = idate("d",$finalTimeInMalaysiaTimeZone - (60 * 60 * 24));
    $hour = idate("H",$finalTimeInMalaysiaTimeZone);

//    echo "<script>alert('$day $hour $previousDay')</script>";

    if($day == 10 && ($hour >= 12 && $hour <= 23)){
        return true;
    }else if($day == 11 && ($hour < 12 && $hour >= 00)){
        return true;
    }

    if($day == 20 && ($hour >= 12 && $hour <= 23)){
        return true;
    }else if($day == 21 && ($hour < 12 && $hour >= 00)){
        return true;
    }

    if($day == 30 && ($hour >= 12 && $hour <= 23)){
        return true;
    }else if($day == 31 && ($hour < 12 && $hour >= 00)){
        return true;
    }else if($day == 1 && $previousDay == 30 && ($hour < 12 && $hour >= 00)){
        return true;
    }

    return false;
}

function checkIsAllowWithdraw($conn,$amount){
    if(!isset($amount) || !$amount){
        return _profile_error_try_again;
    }

    $usdRateRows = getBtcUsdRate($conn," ORDER BY date_created DESC LIMIT 1 ");
    if(!$usdRateRows){
        return _profile_error_try_again;
    }

    $rate = $usdRateRows[0]->getRate();
    $amountInUsd = bcmul($amount,$rate,25);

    if($amountInUsd < 20){
//        $minAmountInBtc = bcdiv(20,$rate,25);
//        return "提取数额必须超过20USD或".removeUselessZero($minAmountInBtc)."btc";
        return _profile_withdraw_too_low_err;
    }

    return null;
}

function getCommunityLevel($conn,$uid,$planId){
    //this function returns level which start at 1 not 0, so if error will return default 1
    $totalDownlineAccumulatedUsd = 0;
    if($planId === 1){
        $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
        if($userRows){
            $totalDownlineAccumulatedUsd = $userRows[0]->getDownlineAccumulatedUsd();
        }else{
            return 1;
        }
    }else if($planId === 2){
        $userPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = 2 ",array("uid"),array($uid),"s");
        if($userPlanRows){
            $totalDownlineAccumulatedUsd = $userPlanRows[0]->getDownlineAccumulatedUsd();
        }else{
            return 1;
        }
    }

    $referralCommissionRows = getReferralCommission($conn, " WHERE usd_needed <= ? AND plan_id = ? ORDER BY level DESC LIMIT 1 ",
        array("usd_needed","plan_id"),array($totalDownlineAccumulatedUsd,$planId),"di");

    if($referralCommissionRows){
        return $referralCommissionRows[0]->getLevel();
    }

    return 1;
}

function getCommunityLevelStarsImg($level){
    $echoString = "";
    for($i = 1; $i <= 10; $i++){
        if(($i % 2 == 0)){
            continue;
        }

        if($level >= $i){
            $echoString .= '<img src="img/sherry/star1.png" class="star-img" alt="社区等级奖励" title="社区等级奖励">';
        }else{
            $echoString .= '<img src="img/sherry/star0.png" class="star-img" alt="社区等级奖励" title="社区等级奖励">';
        }
    }
    return $echoString;
}

//DELETE FROM free_btc;
//ALTER TABLE free_btc AUTO_INCREMENT = 1;
//DELETE FROM transaction_history;
//ALTER TABLE transaction_history AUTO_INCREMENT = 1;
//UPDATE user_plan SET free_btc = 0;