<?php
//Start the session
session_start();

//Check if the session uid is empty/exist or not
if(!empty($_SESSION['uid']))
{
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:profile.php');
}
?>