<!doctype html>
<html>
<head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/splashscreen.php/" />
        <meta property="og:title" content="BTCW - Splash Screen" />
        <meta name="description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Join us now!">
        <meta property="og:description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Join us now!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, register, member, 会员登入,登录, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
       
        <title>BTCW - Splash Screen</title>
        <link rel="canonical" href="https://btcworg.com/splashscreen.php/" />

</head>

<body>
<div class="splash-div">
	<img src="img/sherry/bitcoin2.gif" class="splash-img" alt="比特基金" title="比特基金">
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script>
setTimeout(function () {
   window.location.href= 'https://www.btcworg.com/indexLogin.php'; // the redirect goes here

},4000);
</script>
</body>
</html>