<?php
class ReferralCommission {
    /* Member variables */
    var $id,$dateCreated,$commission,$level,$btcNeeded,$usdNeeded,$downlineRewardCommission,$planId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $commission
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getBtcNeeded()
    {
        return $this->btcNeeded;
    }

    /**
     * @param mixed $btcNeeded
     */
    public function setBtcNeeded($btcNeeded)
    {
        $this->btcNeeded = $btcNeeded;
    }

    /**
     * @return mixed
     */
    public function getUsdNeeded()
    {
        return $this->usdNeeded;
    }

    /**
     * @param mixed $usdNeeded
     */
    public function setUsdNeeded($usdNeeded)
    {
        $this->usdNeeded = $usdNeeded;
    }

    /**
     * @return mixed
     */
    public function getDownlineRewardCommission()
    {
        return $this->downlineRewardCommission;
    }

    /**
     * @param mixed $downlineRewardCommission
     */
    public function setDownlineRewardCommission($downlineRewardCommission)
    {
        $this->downlineRewardCommission = $downlineRewardCommission;
    }

    /**
     * @return mixed
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * @param mixed $planId
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;
    }

}