<?php
class User {
    /* Member variables */
    var $uid,$username,$email,$password,$salt,$phoneNo,$icNo,$countryId,$fullName,$isPhoneVerified,$loginType,
        $userType,$referralLink,$dateCreated,$dateUpdated,$downlineAccumulatedBtc,$downlineAccumulatedUsd,$isReferred,
        $canSendNewsletter,$topupCriteriaId,$isAutoReinvest;

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getIcNo()
    {
        return $this->icNo;
    }

    /**
     * @param mixed $icNo
     */
    public function setIcNo($icNo)
    {
        $this->icNo = $icNo;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param mixed $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getisPhoneVerified()
    {
        return $this->isPhoneVerified;
    }

    /**
     * @param mixed $isPhoneVerified
     */
    public function setIsPhoneVerified($isPhoneVerified)
    {
        $this->isPhoneVerified = $isPhoneVerified;
    }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getReferralLink()
    {
        return $this->referralLink;
    }

    /**
     * @param mixed $referralLink
     */
    public function setReferralLink($referralLink)
    {
        $this->referralLink = $referralLink;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getDownlineAccumulatedBtc()
    {
        return $this->downlineAccumulatedBtc;
    }

    /**
     * @param mixed $downlineAccumulatedBtc
     */
    public function setDownlineAccumulatedBtc($downlineAccumulatedBtc)
    {
        $this->downlineAccumulatedBtc = $downlineAccumulatedBtc;
    }

    /**
     * @return mixed
     */
    public function getDownlineAccumulatedUsd()
    {
        return $this->downlineAccumulatedUsd;
    }

    /**
     * @param mixed $downlineAccumulatedUsd
     */
    public function setDownlineAccumulatedUsd($downlineAccumulatedUsd)
    {
        $this->downlineAccumulatedUsd = $downlineAccumulatedUsd;
    }

    /**
     * @return mixed
     */
    public function getisReferred()
    {
        return $this->isReferred;
    }

    /**
     * @param mixed $isReferred
     */
    public function setIsReferred($isReferred)
    {
        $this->isReferred = $isReferred;
    }

    /**
     * @return mixed
     */
    public function getCanSendNewsletter()
    {
        return $this->canSendNewsletter;
    }

    /**
     * @param mixed $canSendNewsletter
     */
    public function setCanSendNewsletter($canSendNewsletter)
    {
        $this->canSendNewsletter = $canSendNewsletter;
    }

    /**
     * @return mixed
     */
    public function getTopupCriteriaId()
    {
        return $this->topupCriteriaId;
    }

    /**
     * @param mixed $topupCriteriaId
     */
    public function setTopupCriteriaId($topupCriteriaId)
    {
        $this->topupCriteriaId = $topupCriteriaId;
    }

    /**
     * @return mixed
     */
    public function getisAutoReinvest()
    {
        return $this->isAutoReinvest;
    }

    /**
     * @param mixed $isAutoReinvest
     */
    public function setIsAutoReinvest($isAutoReinvest)
    {
        $this->isAutoReinvest = $isAutoReinvest;
    }


}