<?php
class Register 
{
     var $username;
     var $email;
     var $country_id;
     var $phone;
     var $password;
     var $confirmPassword;
     var $termNCondition;
     var $receiveNotification;
     var $canRegister = FALSE;
     var $referral_Id;
     var $registerErrorMessage;
     var $editprofileType = 0;
     var $icNo;
     

     //////////   SET
     /**
     * @param mixed $username
     */
     public function setUsername($username)
     {
          $this->username = $username;
     }
     /**
     * @param mixed $email
     */
     public function setEmail($email)
     {
         $this->email = $email;
     }
     /**
     * @param mixed $country_id
     */
     public function setCountryID($country_id)
     {
          $this->country_id = $country_id;
     }
     /**
     * @param mixed $phone
     */
     public function setPhone($phone)
     {
         $this->phone = $phone;
     }
     /**
     * @param mixed $password
     */
     public function setPassword($password)
     {
          $this->password = $password;
     }
     /**
     * @param mixed $confirmPassword
     */
     public function setConfirmPassword($confirmPassword)
     {
         $this->confirmPassword = $confirmPassword;
     }
     /**
     * @param mixed $termNCondition
     */
     public function setTermNCondition($termNCondition)
     {
          $this->termNCondition = $termNCondition;
     }
     /**
     * @param mixed $receiveNotification
     */
     public function setReceiveNotification($receiveNotification)
     {
         $this->receiveNotification = $receiveNotification;
     }
     /**
     * @param mixed $canRegister
     */
     public function setCanRegister($canRegister)
     {
         $this->canRegister = $canRegister;
     }
     /**
     * @param mixed $referral_Id
     */
     public function setReferralID($referral_Id)
     {
        $this->referral_Id = $referral_Id;
     }
   /**
     * @param mixed $registerErrorMessage
     */
     public function setRegisterErrorMessage($registerErrorMessage)
     {
        $this->registerErrorMessage = $registerErrorMessage;
     }
     /**
     * @param mixed $editprofileType
     */
     public function setEditProfileType($editprofileType)
     {
        $this->editprofileType = $editprofileType;
     }
    /**
     * @param mixed $editprofileType
     */
    public function setIcNo($icNo)
    {
        $this->icNo = $icNo;
    }
     //////////   GET
     /**
     * @return mixed
     */
     public function getUsername()
     {
          return $this->username;
     }
     /**
     * @return mixed
     */
     public function getEmail()
     {
         return $this->email;
     }
     /**
     * @return mixed
     */
     public function getCountryID()
     {
          return $this->country_id;
     }
     /**
     * @return mixed
     */
     public function getPhone()
     {
         return $this->phone;
     }

     /**
     * @return mixed
     */
     public function getPassword()
     {
          return $this->password;
     }
     /**
     * @return mixed
     */
     public function getConfirmPassword()
     {
         return $this->confirmPassword;
     }

     /**
     * @return mixed
     */
     public function getTermNCondition()
     {
          return $this->termNCondition;
     }
     /**
     * @return mixed
     */
     public function getReceiveNotification()
     {
         return $this->receiveNotification;
     }
       /**
     * @return mixed
     */
     public function getCanRegister()
     {
         return $this->canRegister;
     }
     /**
     * @return mixed
     */
     public function getReferralID()
     {
        return $this->referral_Id;
     }
      /**
     * @return mixed
     */
     public function getRegisterErrorMessage()
     {
        return $this->registerErrorMessage;
     }
      /**
     * @return mixed
     */
     public function getEditProfileType()
     {
        return $this->editprofileType;
     }
    /**
     * @return mixed
     */
    public function getIcNo()
    {
        return $this->icNo;
    }



}