<?php
class Countries {
    /* Member variables */
    var $id,$sortname,$name,$phonecode;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSortname()
    {
        return $this->sortname;
    }

    /**
     * @param mixed $sortname
     */
    public function setSortname($sortname)
    {
        $this->sortname = $sortname;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPhonecode()
    {
        return $this->phonecode;
    }

    /**
     * @param mixed $phonecode
     */
    public function setPhonecode($phonecode)
    {
        $this->phonecode = $phonecode;
    }
}