<?php
class AchievementReward {
    /* Member variables */
    var $id,$planId,$minTotalBtc,$minTotalUsd,$percentage,$level,$dateCreated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * @param mixed $planId
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;
    }

    /**
     * @return mixed
     */
    public function getMinTotalBtc()
    {
        return $this->minTotalBtc;
    }

    /**
     * @param mixed $minTotalBtc
     */
    public function setMinTotalBtc($minTotalBtc)
    {
        $this->minTotalBtc = $minTotalBtc;
    }

    /**
     * @return mixed
     */
    public function getMinTotalUsd()
    {
        return $this->minTotalUsd;
    }

    /**
     * @param mixed $minTotalUsd
     */
    public function setMinTotalUsd($minTotalUsd)
    {
        $this->minTotalUsd = $minTotalUsd;
    }

    /**
     * @return mixed
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param mixed $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

}