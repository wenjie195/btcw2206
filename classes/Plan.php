<?php
class Plan {
    /* Member variables */
    var $id,$name,$description,$maxBtc,$minBtc,$investmentReturn,$dateCreated,$dateUpdated,
        $withdrawalPercent,$withdrawalPeriodStart,$withdrawalPeriodEnd,$percentageGiven,
        $minBtcToWithdraw;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getMaxBtc()
    {
        return $this->maxBtc;
    }

    /**
     * @param mixed $maxBtc
     */
    public function setMaxBtc($maxBtc)
    {
        $this->maxBtc = $maxBtc;
    }

    /**
     * @return mixed
     */
    public function getMinBtc()
    {
        return $this->minBtc;
    }

    /**
     * @param mixed $minBtc
     */
    public function setMinBtc($minBtc)
    {
        $this->minBtc = $minBtc;
    }

    /**
     * @return mixed
     */
    public function getInvestmentReturn()
    {
        return $this->investmentReturn;
    }

    /**
     * @param mixed $investmentReturn
     */
    public function setInvestmentReturn($investmentReturn)
    {
        $this->investmentReturn = $investmentReturn;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalPercent()
    {
        return $this->withdrawalPercent;
    }

    /**
     * @param mixed $withdrawalPercent
     */
    public function setWithdrawalPercent($withdrawalPercent)
    {
        $this->withdrawalPercent = $withdrawalPercent;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalPeriodStart()
    {
        return $this->withdrawalPeriodStart;
    }

    /**
     * @param mixed $withdrawalPeriodStart
     */
    public function setWithdrawalPeriodStart($withdrawalPeriodStart)
    {
        $this->withdrawalPeriodStart = $withdrawalPeriodStart;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalPeriodEnd()
    {
        return $this->withdrawalPeriodEnd;
    }

    /**
     * @param mixed $withdrawalPeriodEnd
     */
    public function setWithdrawalPeriodEnd($withdrawalPeriodEnd)
    {
        $this->withdrawalPeriodEnd = $withdrawalPeriodEnd;
    }

    /**
     * @return mixed
     */
    public function getPercentageGiven()
    {
        return $this->percentageGiven;
    }

    /**
     * @param mixed $percentageGiven
     */
    public function setPercentageGiven($percentageGiven)
    {
        $this->percentageGiven = $percentageGiven;
    }

    /**
     * @return mixed
     */
    public function getMinBtcToWithdraw()
    {
        return $this->minBtcToWithdraw;
    }

    /**
     * @param mixed $minBtcToWithdraw
     */
    public function setMinBtcToWithdraw($minBtcToWithdraw)
    {
        $this->minBtcToWithdraw = $minBtcToWithdraw;
    }
}