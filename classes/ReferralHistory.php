<?php
class ReferralHistory {
    /* Member variables */
    var $id,$referrerId,$referralId,$currentLevel,$topReferrerId,$dateCreated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReferrerId()
    {
        return $this->referrerId;
    }

    /**
     * @param mixed $referrerId
     */
    public function setReferrerId($referrerId)
    {
        $this->referrerId = $referrerId;
    }

    /**
     * @return mixed
     */
    public function getReferralId()
    {
        return $this->referralId;
    }

    /**
     * @param mixed $referralId
     */
    public function setReferralId($referralId)
    {
        $this->referralId = $referralId;
    }

    /**
     * @return mixed
     */
    public function getCurrentLevel()
    {
        return $this->currentLevel;
    }

    /**
     * @param mixed $currentLevel
     */
    public function setCurrentLevel($currentLevel)
    {
        $this->currentLevel = $currentLevel;
    }

    /**
     * @return mixed
     */
    public function getTopReferrerId()
    {
        return $this->topReferrerId;
    }

    /**
     * @param mixed $topReferrerId
     */
    public function setTopReferrerId($topReferrerId)
    {
        $this->topReferrerId = $topReferrerId;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

}