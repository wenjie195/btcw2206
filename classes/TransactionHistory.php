<?php
class TransactionHistory {
    /* Member variables */
    var $id,$freeBtcAmountIn,$freeBtcAmountOut,$capitalBtcAmountIn,$capitalBtcAmountOut,$currentTotalFreeBtc,$currentTotalCapitalBtc,
        $day,$dateCreated,$uid,$percentageGiven,$freeBtcId,$totalPercentageGiven,$applicationHistoryId,$btcTypeId,$description,
        $planId,$returnedBtcAmountIn,$returnedBtcAmountOut,$currentTotalReturnedBtc,$btcInvestedInUsd,
        $withdrawPositionType,$withdrawStatus,$withdrawalId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFreeBtcAmountIn()
    {
        return $this->freeBtcAmountIn;
    }

    /**
     * @param mixed $freeBtcAmountIn
     */
    public function setFreeBtcAmountIn($freeBtcAmountIn)
    {
        $this->freeBtcAmountIn = $freeBtcAmountIn;
    }

    /**
     * @return mixed
     */
    public function getFreeBtcAmountOut()
    {
        return $this->freeBtcAmountOut;
    }

    /**
     * @param mixed $freeBtcAmountOut
     */
    public function setFreeBtcAmountOut($freeBtcAmountOut)
    {
        $this->freeBtcAmountOut = $freeBtcAmountOut;
    }

    /**
     * @return mixed
     */
    public function getCapitalBtcAmountIn()
    {
        return $this->capitalBtcAmountIn;
    }

    /**
     * @param mixed $capitalBtcAmountIn
     */
    public function setCapitalBtcAmountIn($capitalBtcAmountIn)
    {
        $this->capitalBtcAmountIn = $capitalBtcAmountIn;
    }

    /**
     * @return mixed
     */
    public function getCapitalBtcAmountOut()
    {
        return $this->capitalBtcAmountOut;
    }

    /**
     * @param mixed $capitalBtcAmountOut
     */
    public function setCapitalBtcAmountOut($capitalBtcAmountOut)
    {
        $this->capitalBtcAmountOut = $capitalBtcAmountOut;
    }

    /**
     * @return mixed
     */
    public function getReturnedBtcAmountIn()
    {
        return $this->returnedBtcAmountIn;
    }

    /**
     * @param mixed $returnedBtcAmountIn
     */
    public function setReturnedBtcAmountIn($returnedBtcAmountIn)
    {
        $this->returnedBtcAmountIn = $returnedBtcAmountIn;
    }

    /**
     * @return mixed
     */
    public function getReturnedBtcAmountOut()
    {
        return $this->returnedBtcAmountOut;
    }

    /**
     * @param mixed $returnedBtcAmountOut
     */
    public function setReturnedBtcAmountOut($returnedBtcAmountOut)
    {
        $this->returnedBtcAmountOut = $returnedBtcAmountOut;
    }

    /**
     * @return mixed
     */
    public function getCurrentTotalReturnedBtc()
    {
        return $this->currentTotalReturnedBtc;
    }

    /**
     * @param mixed $currentTotalReturnedBtc
     */
    public function setCurrentTotalReturnedBtc($currentTotalReturnedBtc)
    {
        $this->currentTotalReturnedBtc = $currentTotalReturnedBtc;
    }

    /**
     * @return mixed
     */
    public function getCurrentTotalFreeBtc()
    {
        return $this->currentTotalFreeBtc;
    }

    /**
     * @param mixed $currentTotalFreeBtc
     */
    public function setCurrentTotalFreeBtc($currentTotalFreeBtc)
    {
        $this->currentTotalFreeBtc = $currentTotalFreeBtc;
    }

    /**
     * @return mixed
     */
    public function getCurrentTotalCapitalBtc()
    {
        return $this->currentTotalCapitalBtc;
    }

    /**
     * @param mixed $currentTotalCapitalBtc
     */
    public function setCurrentTotalCapitalBtc($currentTotalCapitalBtc)
    {
        $this->currentTotalCapitalBtc = $currentTotalCapitalBtc;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getPercentageGiven()
    {
        return $this->percentageGiven;
    }

    /**
     * @param mixed $percentageGiven
     */
    public function setPercentageGiven($percentageGiven)
    {
        $this->percentageGiven = $percentageGiven;
    }

    /**
     * @return mixed
     */
    public function getFreeBtcId()
    {
        return $this->freeBtcId;
    }

    /**
     * @param mixed $freeBtcId
     */
    public function setFreeBtcId($freeBtcId)
    {
        $this->freeBtcId = $freeBtcId;
    }

    /**
     * @return mixed
     */
    public function getTotalPercentageGiven()
    {
        return $this->totalPercentageGiven;
    }

    /**
     * @param mixed $totalPercentageGiven
     */
    public function setTotalPercentageGiven($totalPercentageGiven)
    {
        $this->totalPercentageGiven = $totalPercentageGiven;
    }

    /**
     * @return mixed
     */
    public function getApplicationHistoryId()
    {
        return $this->applicationHistoryId;
    }

    /**
     * @param mixed $applicationHistoryId
     */
    public function setApplicationHistoryId($applicationHistoryId)
    {
        $this->applicationHistoryId = $applicationHistoryId;
    }

    /**
     * @return mixed
     */
    public function getBtcTypeId()
    {
        return $this->btcTypeId;
    }

    /**
     * @param mixed $btcTypeId
     */
    public function setBtcTypeId($btcTypeId)
    {
        $this->btcTypeId = $btcTypeId;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * @param mixed $planId
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;
    }

    /**
     * @return mixed
     */
    public function getBtcInvestedInUsd()
    {
        return $this->btcInvestedInUsd;
    }

    /**
     * @param mixed $btcInvestedInUsd
     */
    public function setBtcInvestedInUsd($btcInvestedInUsd)
    {
        $this->btcInvestedInUsd = $btcInvestedInUsd;
    }

    /**
     * @return mixed
     */
    public function getWithdrawPositionType()
    {
        return $this->withdrawPositionType;
    }

    /**
     * @param mixed $withdrawPositionType
     */
    public function setWithdrawPositionType($withdrawPositionType)
    {
        $this->withdrawPositionType = $withdrawPositionType;
    }

    /**
     * @return mixed
     */
    public function getWithdrawStatus()
    {
        return $this->withdrawStatus;
    }

    /**
     * @param mixed $withdrawStatus
     */
    public function setWithdrawStatus($withdrawStatus)
    {
        $this->withdrawStatus = $withdrawStatus;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalId()
    {
        return $this->withdrawalId;
    }

    /**
     * @param mixed $withdrawalId
     */
    public function setWithdrawalId($withdrawalId)
    {
        $this->withdrawalId = $withdrawalId;
    }

}