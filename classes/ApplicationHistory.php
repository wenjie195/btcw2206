<?php
class ApplicationHistory {
    /* Member variables */
    var $id,$type,$amount,$dateCreated,$dateUpdated,$status,$adminRemark,$userRemark,$uid,$coinbaseAccountId,$coinbaseAddressId,$coinbaseReceiveAddress,$userSendAddress,$planId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getAdminRemark()
    {
        return $this->adminRemark;
    }

    /**
     * @param mixed $adminRemark
     */
    public function setAdminRemark($adminRemark)
    {
        $this->adminRemark = $adminRemark;
    }

    /**
     * @return mixed
     */
    public function getUserRemark()
    {
        return $this->userRemark;
    }

    /**
     * @param mixed $userRemark
     */
    public function setUserRemark($userRemark)
    {
        $this->userRemark = $userRemark;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCoinbaseAccountId()
    {
        return $this->coinbaseAccountId;
    }

    /**
     * @param mixed $coinbaseAccountId
     */
    public function setCoinbaseAccountId($coinbaseAccountId)
    {
        $this->coinbaseAccountId = $coinbaseAccountId;
    }

    /**
     * @return mixed
     */
    public function getCoinbaseAddressId()
    {
        return $this->coinbaseAddressId;
    }

    /**
     * @param mixed $coinbaseAddressId
     */
    public function setCoinbaseAddressId($coinbaseAddressId)
    {
        $this->coinbaseAddressId = $coinbaseAddressId;
    }

    /**
     * @return mixed
     */
    public function getCoinbaseReceiveAddress()
    {
        return $this->coinbaseReceiveAddress;
    }

    /**
     * @param mixed $coinbaseReceiveAddress
     */
    public function setCoinbaseReceiveAddress($coinbaseReceiveAddress)
    {
        $this->coinbaseReceiveAddress = $coinbaseReceiveAddress;
    }

    /**
     * @return mixed
     */
    public function getUserSendAddress()
    {
        return $this->userSendAddress;
    }

    /**
     * @param mixed $userSendAddress
     */
    public function setUserSendAddress($userSendAddress)
    {
        $this->userSendAddress = $userSendAddress;
    }

    /**
     * @return mixed
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * @param mixed $planId
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;
    }

}