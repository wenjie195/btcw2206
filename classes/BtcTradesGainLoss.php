<?php
class BtcTradesGainLoss {
    /* Member variables */
    var $id,$apiType,$latestBuy,$latestSell,$percent,$isGain,$dateCreated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getApiType()
    {
        return $this->apiType;
    }

    /**
     * @param mixed $apiType
     */
    public function setApiType($apiType)
    {
        $this->apiType = $apiType;
    }

    /**
     * @return mixed
     */
    public function getLatestBuy()
    {
        return $this->latestBuy;
    }

    /**
     * @param mixed $latestBuy
     */
    public function setLatestBuy($latestBuy)
    {
        $this->latestBuy = $latestBuy;
    }

    /**
     * @return mixed
     */
    public function getLatestSell()
    {
        return $this->latestSell;
    }

    /**
     * @param mixed $latestSell
     */
    public function setLatestSell($latestSell)
    {
        $this->latestSell = $latestSell;
    }

    /**
     * @return mixed
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @param mixed $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return mixed
     */
    public function getisGain()
    {
        return $this->isGain;
    }

    /**
     * @param mixed $isGain
     */
    public function setIsGain($isGain)
    {
        $this->isGain = $isGain;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

}