<?php
class UserPlan {
    /* Member variables */
    var $id,$uid,$planId,$capitalBtc,$returnedBtc,$freeBtc,$dateCreated,$downlineAccumulatedBtc,$downlineAccumulatedUsd,$isAutoReinvest;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * @param mixed $planId
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;
    }

    /**
     * @return mixed
     */
    public function getCapitalBtc()
    {
        return $this->capitalBtc;
    }

    /**
     * @param mixed $capitalBtc
     */
    public function setCapitalBtc($capitalBtc)
    {
        $this->capitalBtc = $capitalBtc;
    }

    /**
     * @return mixed
     */
    public function getReturnedBtc()
    {
        return $this->returnedBtc;
    }

    /**
     * @param mixed $returnedBtc
     */
    public function setReturnedBtc($returnedBtc)
    {
        $this->returnedBtc = $returnedBtc;
    }

    /**
     * @return mixed
     */
    public function getFreeBtc()
    {
        return $this->freeBtc;
    }

    /**
     * @param mixed $freeBtc
     */
    public function setFreeBtc($freeBtc)
    {
        $this->freeBtc = $freeBtc;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDownlineAccumulatedBtc()
    {
        return $this->downlineAccumulatedBtc;
    }

    /**
     * @param mixed $downlineAccumulatedBtc
     */
    public function setDownlineAccumulatedBtc($downlineAccumulatedBtc)
    {
        $this->downlineAccumulatedBtc = $downlineAccumulatedBtc;
    }

    /**
     * @return mixed
     */
    public function getDownlineAccumulatedUsd()
    {
        return $this->downlineAccumulatedUsd;
    }

    /**
     * @param mixed $downlineAccumulatedUsd
     */
    public function setDownlineAccumulatedUsd($downlineAccumulatedUsd)
    {
        $this->downlineAccumulatedUsd = $downlineAccumulatedUsd;
    }

    /**
     * @return mixed
     */
    public function getisAutoReinvest()
    {
        return $this->isAutoReinvest;
    }

    /**
     * @param mixed $isAutoReinvest
     */
    public function setIsAutoReinvest($isAutoReinvest)
    {
        $this->isAutoReinvest = $isAutoReinvest;
    }

}