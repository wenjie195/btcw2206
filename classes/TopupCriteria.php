<?php
class TopupCriteria {
    /* Member variables */
    var $id,$requireMonth,$maxBtc,$dateCreated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRequireMonth()
    {
        return $this->requireMonth;
    }

    /**
     * @param mixed $requireMonth
     */
    public function setRequireMonth($requireMonth)
    {
        $this->requireMonth = $requireMonth;
    }

    /**
     * @return mixed
     */
    public function getMaxBtc()
    {
        return $this->maxBtc;
    }

    /**
     * @param mixed $maxBtc
     */
    public function setMaxBtc($maxBtc)
    {
        $this->maxBtc = $maxBtc;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }



}