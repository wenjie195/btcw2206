<?php
class BtcUsdPairGraph {
    /* Member variables */
    var $id,$btcTransaction,$usdTransaction,$dateCreated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBtcTransaction()
    {
        return $this->btcTransaction;
    }

    /**
     * @param mixed $btcTransaction
     */
    public function setBtcTransaction($btcTransaction)
    {
        $this->btcTransaction = $btcTransaction;
    }

    /**
     * @return mixed
     */
    public function getUsdTransaction()
    {
        return $this->usdTransaction;
    }

    /**
     * @param mixed $usdTransaction
     */
    public function setUsdTransaction($usdTransaction)
    {
        $this->usdTransaction = $usdTransaction;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

}