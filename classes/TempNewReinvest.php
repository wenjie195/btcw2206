<?php

class TempNewReinvest{
    var $id,$uid,$dateCreated,$dateCanceled,$reinvestType,$reinvestStatus,$reinvestPlanType,$reinvestPosition,$isReinvested;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateCanceled()
    {
        return $this->dateCanceled;
    }

    /**
     * @param mixed $dateCanceled
     */
    public function setDateCanceled($dateCanceled)
    {
        $this->dateCanceled = $dateCanceled;
    }

    /**
     * @return mixed
     */
    public function getReinvestType()
    {
        return $this->reinvestType;
    }

    /**
     * @param mixed $reinvestType
     */
    public function setReinvestType($reinvestType)
    {
        $this->reinvestType = $reinvestType;
    }

    /**
     * @return mixed
     */
    public function getReinvestStatus()
    {
        return $this->reinvestStatus;
    }

    /**
     * @param mixed $reinvestStatus
     */
    public function setReinvestStatus($reinvestStatus)
    {
        $this->reinvestStatus = $reinvestStatus;
    }

    /**
     * @return mixed
     */
    public function getReinvestPlanType()
    {
        return $this->reinvestPlanType;
    }

    /**
     * @param mixed $reinvestPlanType
     */
    public function setReinvestPlanType($reinvestPlanType)
    {
        $this->reinvestPlanType = $reinvestPlanType;
    }

    /**
     * @return mixed
     */
    public function getReinvestPosition()
    {
        return $this->reinvestPosition;
    }

    /**
     * @param mixed $reinvestPosition
     */
    public function setReinvestPosition($reinvestPosition)
    {
        $this->reinvestPosition = $reinvestPosition;
    }

    /**
     * @return mixed
     */
    public function getisReinvested()
    {
        return $this->isReinvested;
    }

    /**
     * @param mixed $isReinvested
     */
    public function setIsReinvested($isReinvested)
    {
        $this->isReinvested = $isReinvested;
    }

}