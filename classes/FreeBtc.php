<?php
class FreeBtc {
    /* Member variables */
    var $id,$totalAmount,$currentDay,$isFinished,$dateCreated,$uid,$btcTypeId,$transactionId,$referrerId,$monthlyRewardId,$achievementRewardId,$planId,$isWithdrawed;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param mixed $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * @return mixed
     */
    public function getCurrentDay()
    {
        return $this->currentDay;
    }

    /**
     * @param mixed $currentDay
     */
    public function setCurrentDay($currentDay)
    {
        $this->currentDay = $currentDay;
    }

    /**
     * @return mixed
     */
    public function getisFinished()
    {
        return $this->isFinished;
    }

    /**
     * @param mixed $isFinished
     */
    public function setIsFinished($isFinished)
    {
        $this->isFinished = $isFinished;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getBtcTypeId()
    {
        return $this->btcTypeId;
    }

    /**
     * @param mixed $btcTypeId
     */
    public function setBtcTypeId($btcTypeId)
    {
        $this->btcTypeId = $btcTypeId;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getReferrerId()
    {
        return $this->referrerId;
    }

    /**
     * @param mixed $referrerId
     */
    public function setReferrerId($referrerId)
    {
        $this->referrerId = $referrerId;
    }

    /**
     * @return mixed
     */
    public function getMonthlyRewardId()
    {
        return $this->monthlyRewardId;
    }

    /**
     * @param mixed $monthlyRewardId
     */
    public function setMonthlyRewardId($monthlyRewardId)
    {
        $this->monthlyRewardId = $monthlyRewardId;
    }

    /**
     * @return mixed
     */
    public function getAchievementRewardId()
    {
        return $this->achievementRewardId;
    }

    /**
     * @param mixed $achievementRewardId
     */
    public function setAchievementRewardId($achievementRewardId)
    {
        $this->achievementRewardId = $achievementRewardId;
    }

    /**
     * @return mixed
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * @param mixed $planId
     */
    public function setPlanId($planId)
    {
        $this->planId = $planId;
    }

    /**
     * @return mixed
     */
    public function getisWithdrawed()
    {
        return $this->isWithdrawed;
    }

    /**
     * @param mixed $isWithdrawed
     */
    public function setIsWithdrawed($isWithdrawed)
    {
        $this->isWithdrawed = $isWithdrawed;
    }

}