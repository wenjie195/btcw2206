<?php

class TemporaryPlanB{
    var $id,$uid,$planType,$btcSetToThisUser,$isPromotion,$promotionMultiplier,$dateUpdated,$dateCreated,$dateTransactionReceive,$isMovedToUserPlan;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getPlanType()
    {
        return $this->planType;
    }

    /**
     * @param mixed $planType
     */
    public function setPlanType($planType)
    {
        $this->planType = $planType;
    }

    /**
     * @return mixed
     */
    public function getBtcSetToThisUser()
    {
        return $this->btcSetToThisUser;
    }

    /**
     * @param mixed $btcSetToThisUser
     */
    public function setBtcSetToThisUser($btcSetToThisUser)
    {
        $this->btcSetToThisUser = $btcSetToThisUser;
    }

    /**
     * @return mixed
     */
    public function getisPromotion()
    {
        return $this->isPromotion;
    }

    /**
     * @param mixed $isPromotion
     */
    public function setIsPromotion($isPromotion)
    {
        $this->isPromotion = $isPromotion;
    }

    /**
     * @return mixed
     */
    public function getPromotionMultiplier()
    {
        return $this->promotionMultiplier;
    }

    /**
     * @param mixed $promotionMultiplier
     */
    public function setPromotionMultiplier($promotionMultiplier)
    {
        $this->promotionMultiplier = $promotionMultiplier;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateTransactionReceive()
    {
        return $this->dateTransactionReceive;
    }

    /**
     * @param mixed $dateTransactionReceive
     */
    public function setDateTransactionReceive($dateTransactionReceive)
    {
        $this->dateTransactionReceive = $dateTransactionReceive;
    }

    /**
     * @return mixed
     */
    public function getisMovedToUserPlan()
    {
        return $this->isMovedToUserPlan;
    }

    /**
     * @param mixed $isMovedToUserPlan
     */
    public function setIsMovedToUserPlan($isMovedToUserPlan)
    {
        $this->isMovedToUserPlan = $isMovedToUserPlan;
    }

}