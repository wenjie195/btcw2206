<?php
	//Start the session
	session_start();
	
	//Check f the session is empty/exist or not
	if(!empty($_SESSION))
	{
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        
        <meta property="og:url" content="https://btcworg.com/scanQr.php" />
        <meta property="og:title" content="比特基金 - 扫描二维码" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
        
        <title>比特基金 - 扫描二维码</title>
        <link rel="canonical" href="https://btcworg.com/scanQr.php" />
  </head>
  <body>
  	 <?php require 'mainNavbar.php';?>
     <div id="firefly" class="firefly-class min-height">   
        
        <div class="width100 same-padding more-separate-margin-top">
        	
            	
                <h2 class="bit-h2 white-text"><img src="img/sherry/btcw.png" alt="BTCW" title="BTCW" class="bitcoin-icon"> 比特基金</h2>
          
        </div>
        <div class="width100 same-padding more-separate-margin-top">
        	
                <h2 class="bit2-h2 white-text">比特基金是全球唯一由官方打造的比特币创富平台。</h2>
      
        </div>        
        <div class="width100 same-padding more-separate-margin-top">  
        	<p class="qr-p white-text">
            	<img src="img/qrCode.png" class="qr-img2" alt="二维码" title="二维码"><br>
                长按图片识别二维码
            </p>
        </div> 
        <div class="width100 same-padding more-separate-margin-top">  
        	<p class="qr-p2 white-text">
            	免费注册，平台即送价值200美金的btc 0.05个，直推5人，再送0.05个。
            </p>
        </div>              
	<div class="width100 element-div more-separate-margin-top">
        	<img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
    </div> 
   </div>          
         <?php require 'mainFooter.php';?> 


  </body>
</html>
<?php
        
    }
    else
	{
		// Go back to index page 
		// NOTE : MUST PROMPT ERROR
		header('Location:index.php');
	}
?>