<?php
//give user 0.2% of total free btc everyday
//reinvest returned_btc everyday if user selects auto-reinvest
//for plan A
ini_set('max_execution_time', 0);
require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

upgradeUserTopupCriteria($conn);

reinvestFreeBtcForBothPlans($conn);

/********************************************PLAN A START****************************************************/

dropReturnBtcForPlanA($conn);

/********************************************PLAN A END****************************************************/


/********************************************PLAN B START****************************************************/

dropExtraBtcForPlanBEveryOneMonth($conn);
//UPDATE `transaction_history` SET date_created = "2019-05-20 08:12:31" WHERE (date_created >=  "2019-05-10 00:00:00" AND date_created <= "2019-05-10 23:59:59") AND plan_id = 2
//UPDATE `transaction_history` SET date_created = "2019-05-23 08:12:31" WHERE (date_created =  "2019-05-23 08:12:31") AND plan_id = 2

/********************************************PLAN B END****************************************************/

$conn->close();

/********************************************PLAN A START****************************************************/

function dropReturnBtcForPlanA($conn){
    $freeBtcRows = getFreeBtc($conn," WHERE monthly_reward_id IS NULL AND achievement_reward_id IS NULL AND is_finished != 1 AND plan_id = 1 ORDER BY date_created ASC");

    if(!$freeBtcRows){
        logError("PLAN A no free btc are found");
        return false;
    }

    $planRows = getPlan($conn," WHERE id = ? ORDER BY date_updated DESC LIMIT 1",array("id"),array(1),"i");

    if(!$planRows){
        logError("Plan A not found");
        return false;
    }
    $planA = $planRows[0];
    $percentageGiven = $planA->getPercentageGiven();

    $referralCommissionRows = getReferralCommission($conn," WHERE plan_id = ? ORDER BY level ASC ",array("plan_id"),array(1),"i");
    if(!$referralCommissionRows){
        logError("PLAN A Referral Commission is null");
        return false;
    }

    foreach ($freeBtcRows as $freeBtc) {
        $transactionHistoryRows = getTransactionHistory($conn," WHERE free_btc_id = ? ORDER BY id DESC LIMIT 1 ", array("free_btc_id"),array($freeBtc->getId()),"i");
        if(!$transactionHistoryRows){
            logError("PLAN A transaction history for free_btc id (".$freeBtc->getId().") not found");
            continue;
        }

        $divideNumber = null;
        if($freeBtc->getBtcTypeId() === 1){
            $transactionHistoryInvestedRows = getTransactionHistory($conn," WHERE free_btc_id = ? AND btc_type_id = 2 AND plan_id = 1 ",array("free_btc_id"),array($freeBtc->getId()),"i");
            if(!$transactionHistoryInvestedRows){
                logError("PLAN A transaction history Original for free_btc id (".$freeBtc->getId().") not found");
                continue;
            }

            if($transactionHistoryInvestedRows[0]->getCapitalBtcAmountIn() > 0){
                $divideNumber =  bcdiv($freeBtc->getTotalAmount(),$transactionHistoryInvestedRows[0]->getCapitalBtcAmountIn(),25);
            }
            if(!$divideNumber){
                logError("PLAN A transaction divide number for transaction_history id (".$transactionHistoryInvestedRows[0]->getId().") not found");
                continue;
            }
        }

        $userPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($freeBtc->getUid(),1),"si");
        if(!$userPlanRows){
            logError("PLAN A user_plan for free_btc id (".$freeBtc->getId().") not found");
            continue;
        }

        $userRows = getUser($conn, " WHERE uid = ? ",array("uid"),array($freeBtc->getUid()),"s");
        if(!$userRows){
            logError("PLAN A user for free_btc id (".$freeBtc->getId().") not found");
            continue;
        }

        $userPlan = $userPlanRows[0];

        $totalInvestmentOfLevelOneDownline = 0;
        $thisUserLevelOneReferralRows = getReferralHistory($conn," WHERE referrer_id = ? ",array("uid"),array($freeBtc->getUid()),"s");
        if($thisUserLevelOneReferralRows){
            foreach ($thisUserLevelOneReferralRows as $tempLevelOneReferral){
                $tempLevelOneUserPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = 1 ",array("uid"),array($tempLevelOneReferral->getReferralId()),"s");
                if($tempLevelOneUserPlanRows){
                    $totalInvestmentOfLevelOneDownline = bcadd($totalInvestmentOfLevelOneDownline,$tempLevelOneUserPlanRows[0]->getCapitalBtc(),25);
                }

                $tempLevelOneUserPlanRowsPlanB = getUserPlan($conn," WHERE uid = ? AND plan_id = 2 ",array("uid"),array($tempLevelOneReferral->getReferralId()),"s");
                if($tempLevelOneUserPlanRowsPlanB){
                    $totalInvestmentOfLevelOneDownline = bcadd($totalInvestmentOfLevelOneDownline,$tempLevelOneUserPlanRowsPlanB[0]->getCapitalBtc(),25);
                }
            }
        }
        //add his own plan B capital also
        $thisUserPlanBRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($freeBtc->getUid(),2),"si");
        if($thisUserPlanBRows){
            $totalInvestmentOfLevelOneDownline = bcadd($totalInvestmentOfLevelOneDownline,$thisUserPlanBRows[0]->getCapitalBtc(),25);
        }

        //if equal to or more than 0.15, then allow dropping
        //0.15 requirement can only count user himself and his level 1's total investment ONLY not all 10 levels
        if(bcadd($totalInvestmentOfLevelOneDownline,$userPlan->getCapitalBtc(),25) < $planA->getMinBtcToWithdraw() && $freeBtc->getBtcTypeId() !== 1){
            continue;
        }

        $currentTotalReturnedBtc = $userPlan->getReturnedBtc();
        $currentTotalFreeBtc = $userPlan->getFreeBtc();
        $transactionHistory = $transactionHistoryRows[0];
        $totalPercentageGiven = $transactionHistory-> getTotalPercentageGiven();
        //to make sure that total percentage given never exceeds 100
        if(($totalPercentageGiven + $percentageGiven) > 100){
            $percentageGiven = 100 - $totalPercentageGiven;
        }

        $droppedBtc = bcdiv(bcmul($freeBtc->getTotalAmount(),$percentageGiven,25),100,25);
        $currentTotalReturnedBtc = bcadd($currentTotalReturnedBtc,$droppedBtc,25);
        $currentTotalFreeBtc = bcsub($currentTotalFreeBtc,$droppedBtc,25) ;
        $currentDay = $transactionHistory->getDay() + 1;
        $totalPercentageGiven += $percentageGiven;
        $descriptionTH =  "投资收益的".$percentageGiven."%";
        $isFinished = false;
        if($totalPercentageGiven >= 100){
            $isFinished = true;
        }

        //order:
        //1. drop btc for this user
        //2. reward btc to this user's top 10 referral
        //3. reinvest (every 10 day)

        //1) drop btc
        //1-1) put dropped btc into transaction history
        $droppedBtcTHId = insertIntoTransactionHistory($conn,0,$droppedBtc,0,0,
            $droppedBtc,0,$currentTotalReturnedBtc,$currentTotalFreeBtc,
            $userPlan->getCapitalBtc(),$currentDay,$freeBtc->getUid(),$percentageGiven,$freeBtc->getId(),
            $totalPercentageGiven,$transactionHistory->getApplicationHistoryId(),9,$descriptionTH,$transactionHistory->getPlanId());

        if(!$droppedBtcTHId){
            logError("error PLAN A in giving dropped btc. uid: " . $freeBtc->getUid() . " current day supposed to insert: $currentDay last transaction id: " . $transactionHistory->getId() . " free btc id: " . $freeBtc->getId());
        }

        //1-2) update dropped btc into free btc table
        if(!updateDynamicData($conn,"free_btc"," WHERE id = ? ",array("current_day","is_finished"),array($currentDay,$isFinished,$freeBtc->getId()),"iii")){
            logError("error PLAN A in updating dropped btc. into free_btc. uid: " . $freeBtc->getUid() . " current day supposed to insert: $currentDay last transaction id: " . $transactionHistory->getId() . " free btc id: " . $freeBtc->getId());
        }

        //1-3) update user_plan's free btc and returned btc amount
        if(!updateDynamicData($conn,"user_plan"," WHERE id = ? ",array("returned_btc","free_btc"),array($currentTotalReturnedBtc,$currentTotalFreeBtc,$userPlan->getId()),"ddi")){
            logError("error PLAN A in updating user plan's returned btc and free btc. uid: " . $freeBtc->getUid() . " current day supposed to insert: $currentDay last transaction id: " . $transactionHistory->getId() . " free btc id: " . $freeBtc->getId());
        }

        //2) loop 10 levels just like this function -> updateAllReferrerLevelsUplineBtcAmountOfThisUser to reward this user's top 10 uplines' returned_btc with [REFERRAL_COMMISION_LEVEL_%] of 0.2% dropping
        //2-1) insert into transaction history of x% of droppedBtc amount
        //2-2) update upline's user_plan's returned_btc
        //4 is entry and 5 is intro
        //cant get entry and intro type btc commission
        if($freeBtc->getBtcTypeId() != 4 && $freeBtc->getBtcTypeId() != 5){
            if(!updateAllReferrerLevelsUplineBtcAmountOfThisUserEverydayWithCommissionEarnedFromEachUserDroppedBtcPlanA($conn,$freeBtc->getUid(),$droppedBtc,$currentDay,$referralCommissionRows,$freeBtc->getId(),$percentageGiven,$totalPercentageGiven,$divideNumber)){
                logError("error PLAN A in updating referrer's commission earned btc. uid: " . $freeBtc->getUid() . " current day supposed to insert: $currentDay last transaction id: " . $transactionHistory->getId() . " free btc id: " . $freeBtc->getId());
            }
        }

        //3) reinvest (every 10 day)

    }

    return true;
}

//ONLY INVESTMENT FREE BTC CAN LET UPLINE GET 0.2% OF THE DROPPING
function updateAllReferrerLevelsUplineBtcAmountOfThisUserEverydayWithCommissionEarnedFromEachUserDroppedBtcPlanA($conn,$uid,$droppedBtc,$currentDay,$referralCommissionRows,$freeBtcId,$percentageGiven,$totalPercentageGiven,$divideNumber){
    //update all his referral up to level 10's commission earned value
    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
    if(!$referralHistoryRows){
        //meaning this user is not referred so just return true, because no need update anything also
        return true;
    }else{
        $isAllUpdateCompleted = true;
        $currentLevel = $referralHistoryRows[0]->getCurrentLevel();
        $maxLevel = $currentLevel - 10;//minus ten because i want update the people above me not the 1 below me and update all the people above me

        $allTop10ReferrerId = getTop10ReferrerOfUser($conn,$uid);
        $commissionLevel = 0;
        foreach ($allTop10ReferrerId as $thisReferrerId){
            $referrerUserPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($thisReferrerId,1),"si");
            //this user must invest btc (any amount also can as long as got invest) to get commission
            if($referrerUserPlanRows && $referrerUserPlanRows[0]->getCapitalBtc() > 0){
                $thisUserPlan = $referrerUserPlanRows[0];

                $thisReferralUserCommission = $referralCommissionRows[$commissionLevel];
                $usdNeeded = $thisReferralUserCommission->getUsdNeeded();
                $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($thisReferrerId),"s");

                //2-1) check if this referrer's tota lDownline Invested Capital is more than the referral commission's requirement
                if($referrerUserRows && $referrerUserRows[0]->getDownlineAccumulatedUsd() >= $usdNeeded){
                    $commissionPercent = $thisReferralUserCommission->getCommission();
                    $descriptionTH =  "获得下线释放收益的".$commissionPercent."%";
                    $commissionEarnedBtc = bcdiv(bcmul($droppedBtc,$commissionPercent,25),100,25);
                    if($divideNumber){
                        $commissionEarnedBtc = bcadd($commissionEarnedBtc,bcdiv($commissionEarnedBtc,$divideNumber,25),25);
                    }
//                    echo " </br> UID ".$thisReferrerId." ::: Commission: $commissionEarnedBtc ::: droppedBtc $droppedBtc ::: commissionPercent $commissionPercent </br>";
                    $currentTotalReturnedBtc = bcadd($thisUserPlan->getReturnedBtc(),$commissionEarnedBtc,25);
//                    echo "current returned btc = " .$thisUserPlan->getReturnedBtc()." - " . $currentTotalReturnedBtc . " current level: $currentLevel" . " max level $maxLevel commission level $commissionLevel $thisReferrerId";
//                    echo "<p> ".$thisUserPlan->getReturnedBtc()." </p>";
//                    echo "<p> ".$thisUserPlan->getUid()." </p>";

                    //2-2) insert into transaction history of x% of droppedBtc amount
                    $commissionEarnedBtcTHId = insertIntoTransactionHistory($conn,0,0,0,0,
                        $commissionEarnedBtc,0,$currentTotalReturnedBtc,$thisUserPlan->getFreeBtc(),
                        $thisUserPlan->getCapitalBtc(),$currentDay,$thisReferrerId,$percentageGiven,$freeBtcId,
                        $totalPercentageGiven,null,3,$descriptionTH,1);

                    if(!$commissionEarnedBtcTHId){
                        logError("error PLAN A in inserting referrer's commission earned btc into transaction history. uid: " . $thisReferrerId );
                        $isAllUpdateCompleted = false;
                    }

                    //2-3) update upline's user_plan's returned_btc
                    if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = ? ",array("returned_btc"),array($currentTotalReturnedBtc,$thisReferrerId ,1),"dsi")){
                        logError("error PLAN A in updating referrer's commission earned btc into user_plan. uid: " . $thisReferrerId . " transaction history id: " . $commissionEarnedBtcTHId);
                        $isAllUpdateCompleted = false;
                    }
                }
            }
            $commissionLevel++;
        }

        return $isAllUpdateCompleted;
    }
}

/********************************************PLAN A END****************************************************/


/********************************************PLAN B START****************************************************/

//代数分红 (Free/profit btc)
function dropExtraBtcForPlanBEveryOneMonth($conn){
    $monthlyRewardRows = getMonthlyReward($conn," WHERE plan_id = 2 ");

    $investedPlanBTHRows = getTransactionHistory($conn," WHERE btc_type_id = 2 AND plan_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ");

    if($investedPlanBTHRows){
        foreach ($investedPlanBTHRows as $investedTransactionHistory){
            $freeBtcId = $investedTransactionHistory->getFreeBtcId();
            $uid = $investedTransactionHistory->getUid();

            $percentage = 5;
            $totalMonth = getTotalMonthDifference(date('Y/m/d', strtotime($investedTransactionHistory->getDateCreated())),date('Y/m/d'));

            if($totalMonth === 0){
                continue;
            }

            $thisUserPlanRows = getUserPlan($conn, " WHERE uid = ? AND plan_id = 2 ",array("uid"),array($uid),"s");
            if(!$thisUserPlanRows){
                logError("cant get user plan in reward uid: $uid    free btc id: $freeBtcId");
                continue;
            }
            $thisUserPlan = $thisUserPlanRows[0];

            if($thisUserPlan->getFreeBtc() <= 0){
                continue;
            }

            $previousDroppedTHRows = getTransactionHistory($conn," WHERE uid = ? AND free_btc_id = ? AND day = ? AND plan_id = 2 AND btc_type_id = 6 ",
                array("uid","free_btc_id","day"),array($uid,$freeBtcId,$totalMonth),"sii");

            //if previously already dropped, dont drop again, need wait until next month to drop
            if($previousDroppedTHRows){
                continue;
            }

            //this gets the percentage from reward table
            foreach ($monthlyRewardRows as $monthlyReward){
                if($totalMonth === $monthlyReward->getMonth()){
                    $percentage = $monthlyReward->getPercentage();
                    break;
                }
            }

            //get the amount of btc that is supposed to reward to users
            // C * P /100
            $rewardBtc = bcdiv(bcmul($investedTransactionHistory->getCapitalBtcAmountIn(),$percentage,25),100,25);

            $currentTotalReturnedBtc = bcadd($rewardBtc,$thisUserPlan->getReturnedBtc(),25);
            $descriptionTH = "第".$totalMonth."个月的".$percentage."%代数分红";
            $newRewardTHId = insertIntoTransactionHistory($conn,0,0,0,0,$rewardBtc,0,
                $currentTotalReturnedBtc,$thisUserPlan->getFreeBtc(),$thisUserPlan->getCapitalBtc(),$totalMonth,$uid,$percentage,$freeBtcId,
                0,null,6,$descriptionTH,2);
            if(!$newRewardTHId){
                logError("cant insert transaction history in reward uid: $uid    free btc id: $freeBtcId");
                continue;
            }

            if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = 2 ",
                array("returned_btc"),array($currentTotalReturnedBtc,$uid),"ds")){
                logError("cant update user plan in reward uid: $uid    free btc id: $freeBtcId   new reward TH id: $newRewardTHId");
                continue;
            }

            //reward to his top 10 referrer with 10% of this guy's $rewardBtc;
            if(!rewardTop10ReferrerWithMonthlyDroppingPlanB($conn,$uid,$rewardBtc,$totalMonth,$freeBtcId)){
                logError("cant reward this user his top 10 referrer with 10% of his rewardBtc uid: $uid    free btc id: $freeBtcId   new reward TH id: $newRewardTHId");
                continue;
            }

            forceWithdrawOfPlanBEarning($conn,$investedTransactionHistory,$totalMonth);
        }
    }

    return null;
}

function forceWithdrawOfPlanBEarning($conn,$investedTransactionHistory,$totalMonth){
    //if more than 1 year already, stop the dropping by withdraw it with pending status and minus the free btc in user_plan //TESTED AND WORKS AS INTENDED ALREADY

    if($totalMonth >= 12){
        $freeBtcId = $investedTransactionHistory->getFreeBtcId();
        $uid = $investedTransactionHistory->getUid();

        //get user plan again with newest value
        $thisUserPlanRows = getUserPlan($conn, " WHERE uid = ? AND plan_id = 2 ",array("uid"),array($uid),"s");
        if(!$thisUserPlanRows){
            logError("cant get user plan in reward uid force withdraw: $uid    free btc id: $freeBtcId");
            return;
        }
        $thisUserPlan = $thisUserPlanRows[0];

        if(!updateDynamicData($conn,"free_btc"," WHERE id = ? ",array("is_finished","is_withdrawed"),array(1,1,$freeBtcId),"iii")){
            logError("failed to set withdraw status for free btc with id : $freeBtcId uid: $uid");
            return;
        }

        $relatedTHRows = getTransactionHistory($conn," WHERE free_btc_id = ? AND plan_id = 2 ",array("free_btc_id"),array($freeBtcId),"i");
        if(!$relatedTHRows){
            logError("failed to find related transaction history rows after set withdraw status for free btc with id : $freeBtcId uid: $uid");
            return;
        }

        foreach ($relatedTHRows as $tempTH){
            if(!updateDynamicData($conn,"transaction_history"," WHERE id = ? AND plan_id = 2 AND (btc_type_id = 1 OR btc_type_id = 2) ",
                array("withdraw_position_type","withdraw_status"),array(7,1,$tempTH->getId()),"iii")){
                logError("failed to set related transaction history to withdraw free btc  free_btc_id : $freeBtcId uid: $uid th Id: " . $tempTH->getId());
                return;
            }
        }

        $totalInvestedCapitalIncludingFreeBtcAmount = getSum($conn,"transaction_history","free_btc_amount_in",
            " WHERE uid = ? AND plan_id = 2 AND btc_type_id = 1 ",array("uid"),array($uid),"s");

        //the 10% 加速释放 of direct downline only
        $totalDroppedFasterAmount = getSum($conn,"transaction_history","returned_btc_amount_in",
            " WHERE uid = ? AND plan_id = 2 AND btc_type_id = 12 ",array("uid"),array($uid),"s");

        $totalPreviousWithdrawAmountAll = getSum($conn,"transaction_history","free_btc_amount_in",
            " WHERE uid = ? AND plan_id = 2 AND btc_type_id = 1 AND withdraw_position_type = 7 AND (withdraw_status = 1 OR withdraw_status = 2) ",array("uid"),array($uid),"s");

        $currentTotalCapitalBtc = bcsub($thisUserPlan->getCapitalBtc(),$investedTransactionHistory->getCapitalBtcAmountIn(),25);
        $currentTotalFreeBtc = 0;

        if($totalDroppedFasterAmount > $totalPreviousWithdrawAmountAll){
            $currentTotalFreeBtc = bcsub($totalInvestedCapitalIncludingFreeBtcAmount,$totalDroppedFasterAmount,25);
        }else{
            $currentTotalFreeBtc = bcsub($totalInvestedCapitalIncludingFreeBtcAmount,$totalPreviousWithdrawAmountAll,25);
        }

        //current total free btc (before calculate this withdrawal) - current total free btc (already included every withdrawal including this)
        $withdrawableAmount = bcsub($thisUserPlan->getFreeBtc(),$currentTotalFreeBtc,25);

        if($withdrawableAmount < 0){
//            logError("withdrawable amount should not become negative. free_btc_id : $freeBtcId uid: $uid th_Id: " . $tempTH->getId());
            $withdrawableAmount = 0;
        }

        $descriptionTH = "提取定期资产";
        $newWithdrawTHId = insertIntoTransactionHistoryWithWithdraw($conn,0,$withdrawableAmount,0,$investedTransactionHistory->getCapitalBtcAmountIn(),0,0,
            $thisUserPlan->getReturnedBtc(),$currentTotalFreeBtc,$currentTotalCapitalBtc,$totalMonth,$uid,0,$freeBtcId,100,null,
            8,$descriptionTH,2,7,1,null);

        if(!$newWithdrawTHId){
            logError("failed to withdraw designated all free btc for plan B. free_btc_id : $freeBtcId uid: $uid th_Id: " . $tempTH->getId());
            return;
        }

        if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = 2 ",array("capital_btc","free_btc"),
            array($currentTotalCapitalBtc,$currentTotalFreeBtc,$uid),"dds")){
            logError("failed to update user plan for plan B after withdraw capital and free btc. withdrawal TH Id: $newWithdrawTHId free_btc_id : $freeBtcId uid: $uid th_Id: " . $tempTH->getId());
            return;
        }
    }
}

function rewardTop10ReferrerWithMonthlyDroppingPlanB($conn,$uid,$rewardBtc,$totalMonth,$freeBtcId){
    $level = 0;
    $allTop10ReferrerId = getTop10ReferrerOfUser($conn,$uid);

    //10% only for everyone
    $allReferrerRewardBtc = bcdiv(bcmul($rewardBtc,10,25),100,25);

    $referralCommissionRows = getReferralCommission($conn," WHERE plan_id = 2 ORDER BY level ASC ");
    if(!$referralCommissionRows){
        logError("failed to get referral commission at plan b monthly drop");
        return null;
    }

    foreach ($allTop10ReferrerId as $thisReferrerId){
        $referralCommission = $referralCommissionRows[$level];
        $level++;

        $referrerUserPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = 2 ",array("uid"),array($thisReferrerId),"s");
        if($referrerUserPlanRows){
            $thisReferrerUserPlan = $referrerUserPlanRows[0];

            //need to invest at least some btc only can get commission
            //if total accumulated usd less than usd needed, cant proceed
            if($thisReferrerUserPlan->getCapitalBtc() <= 0 || $thisReferrerUserPlan->getDownlineAccumulatedUsd() < $referralCommission->getUsdNeeded()){
                continue;
            }

            $thisReferrerUserCurrentTotalReturnedBtc = bcadd($thisReferrerUserPlan->getReturnedBtc(),$allReferrerRewardBtc,25);
            $descriptionTH = "获得下线第".$level."代第".$totalMonth."个月的10%代数分红";
            $newMonthlyRewardDropForReferrerTHId = insertIntoTransactionHistory($conn,0,0,0,0,$allReferrerRewardBtc,0,
                $thisReferrerUserCurrentTotalReturnedBtc,$thisReferrerUserPlan->getFreeBtc(),$thisReferrerUserPlan->getCapitalBtc(),
                $totalMonth,$thisReferrerId,10,$freeBtcId,0,null,13,$descriptionTH,2);

            if(!$newMonthlyRewardDropForReferrerTHId){
                logError("failed inserting monthly reward transaction history to this referrer user uid: $thisReferrerId referralId: $uid freeBtcId: $freeBtcId Month: $totalMonth");
                continue;
            }

            if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = 2 ",array("returned_btc"),
                array($thisReferrerUserCurrentTotalReturnedBtc,$thisReferrerId),"ds")){
                logError("failed updating monthly reward user plan to this referrer user uid: $thisReferrerId referralId: $uid freeBtcId: $freeBtcId Month: $totalMonth");
                continue;
            }

        }else{
            logError("failed reward referral user with monthly drop plan B uid: $thisReferrerId No user plan found!");
        }
    }

    return true;
}

/********************************************PLAN B END****************************************************/

function reinvestFreeBtcForBothPlans($conn){
    if(isAllowReinvestAndWithdraw()){
        $tempReinvestRows = getTempNewReinvestSpecialGroupBy($conn," WHERE reinvestType = 1 GROUP BY uid,reinvestPosition,reinvestPlanType ");

        if($tempReinvestRows){
            foreach ($tempReinvestRows as $tempInvest){
                $checkIsCanceledRows = getTempNewReinvest($conn," WHERE uid = ? AND reinvestPlanType = ? AND reinvestPosition = ? AND reinvestType = 1 ORDER BY id DESC LIMIT 1 ",
                    array("uid","reinvestPlanType","reinvestPosition"),
                    array($tempInvest->getUid(),$tempInvest->getReinvestPlanType(),$tempInvest->getReinvestPosition()),
                    "sii");

                if($checkIsCanceledRows){
                    $realTempReinvest = $checkIsCanceledRows[0];

                    if($realTempReinvest->getDateCanceled() === null){
                        executeReinvest($conn,$realTempReinvest->getUid(),$realTempReinvest->getReinvestPlanType(),$realTempReinvest->getReinvestPosition());
                    }else{
                        //it is not null, means user stopped the reinvest already at the latest update
                    }
                }else{
                    logError("something wrong here");
                }
            }
        }else{
            logError("No record found for getTempNewReinvest");
        }
    }
}

function executeReinvest($conn,$uid,$planId,$reinvestPosition){
    $amount = 0;
    $isReinvested = true;

    $errorMsg = null;

    $errorMsg = reinvestFreeBtcToCapital($conn,$uid,$amount,$reinvestPosition + 1,$planId,$isReinvested);

    if(isset($errorMsg) && $errorMsg !== "再投资的数额不可以等于0!" && $errorMsg !== "对不起,再投资只能在每个月的10号20号或者是30号而已"){
        logError("THIS USER ERROR: $uid -->> $errorMsg");
    }
}


function upgradeUserTopupCriteria($conn){
    $topupCriteriaRows = getTopupCriteria($conn);
    if(!$topupCriteriaRows){
        logError("Topup Criteria is null");
        return;
    }

    foreach ($topupCriteriaRows as $criteria){
        $daysNeeded = $criteria->getRequireMonth() / 60 / 60 / 24;
        $userRows = getUser($conn," WHERE date_created < date_sub(now(), interval $daysNeeded day) AND (topup_criteria_id < ".$criteria->getId()." OR topup_criteria_id IS NULL) ");
        if($userRows){
            foreach ($userRows as $user){
                if(!updateDynamicData($conn,"user"," WHERE uid = ? ",array("topup_criteria_id"),array($criteria->getId(),$user->getUid()),"is")){
                    logError("Error in updating user's topup criteria id. ::: uid: " . $user->getUid() . " criteriaId: " . $criteria->getId());
                }
            }
        }
    }
}

function logError($errorMsg){
    $datetimeInGMT = new DateTime("now", new DateTimeZone("UTC"));
    $errorMsg = $datetimeInGMT->format('l, Y/m/d H:i:s') . " (UTC) ::: " . $errorMsg;
    error_log("$errorMsg\n", 3, "cron-errors.log");
}