<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{ 
     if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
          $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
     {
          header('Location:index.php');
          exit();
     }
     else
     {
          $uid = $_SESSION['uid'];
          $conn = connDB();
          $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
     }
}

$editProfile = null;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     if(isset($_POST['checkTrans']))
     {
          $receiptHashKey = "%".$_POST['field_1']."%";
          $sql_plan_A = "SELECT uid,amount,dateCreated FROM temp_invest_table WHERE receiptHashKey LIKE ? ";
          if ($stmt = $conn->prepare($sql_plan_A)) 
          {
          $stmt->bind_param('s',$receiptHashKey);

          $stmt->execute();

          $stmt->store_result();

          $num_of_rows = $stmt->num_rows;

          $stmt->bind_result($uidUser,$amountBtcTheyEntered,$dateUserClicked);

          $planA_Uid = array();
          $planA_AmountEntered = array();
          $planA_DateClicked = array();

          while($stmt->fetch())
          {
                    array_push($planA_Uid,$uidUser);
                    array_push($planA_AmountEntered,$amountBtcTheyEntered);
                    array_push($planA_DateClicked,$dateUserClicked);
          }

          /* free results */
          $stmt->free_result();

          }

          $sql_plan_B = "SELECT uid,amount,dateCreated FROM temp_invest_table_b WHERE receiptHashKey LIKE ? ";
          if ($stmt = $conn->prepare($sql_plan_B)) 
          {
          $stmt->bind_param('s',$receiptHashKey);

          $stmt->execute();

          $stmt->store_result();

          $num_of_rows = $stmt->num_rows;

          $stmt->bind_result($uidUserB,$amountBtcTheyEnteredB,$dateUserClickedB);

          $planB_Uid = array();
          $planB_AmountEntered = array();
          $planB_DateClicked = array();

          while($stmt->fetch())
          {
                    array_push($planB_Uid,$uidUserB);
                    array_push($planB_AmountEntered,$amountBtcTheyEnteredB);
                    array_push($planB_DateClicked,$dateUserClickedB);
          }

          /* free results */
          $stmt->free_result();

          /* close statement */
          $stmt->close();
          }
     }
     if(isset($_POST['insertValuePlanA']))
     {

     }
     if(isset($_POST['insertValuePlanB']))
     {
          
     }
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <title>ZCXC</title>
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();

  ?>
  <div id="firefly" class="firefly-class min-height">  
     <div class="width100 same-padding more-separate-margin-top edit-div">
            <a href="adminR.php"><div class="btn btn-warning btn-lg ml-2 mb-5">CRU</div></a>
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Check Transaction Here</b></h4>
            <form class="register-form"  method="POST" >
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Hash Keys (TX_ID)</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_1" id="field_1" ></h4></td>
                  </tr>

                  <!-- <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">邮箱</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" placeholder="电邮" ></h4></td>
                  </tr> -->

                </table>
                <div class="clear"></div>
               <button class="register-button2 clean orange-hover inputb-button" name="checkTrans" id="checkTrans" >Check Transaction</button>
               
            </form>
     </div> 
      <div class="clear"></div>
      <?php 
          if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST')
          {
               $total_planA = count($planA_Uid);
               $total_planB = count($planB_Uid);
               if($total_planA > 0)
               {
                    ?>
                    <div class="row">
                         <div class="col-md-2"></div>
                         <div class="col-md-8">
                              <h3 class="btcw-h4 edit-h4-title white-text mt-5">Plan A</h3>
                              <table class="table table-bordered">
                                   <tr>
                                        <td class="text-light">No</td>
                                        <!-- <td>User ID</td> -->
                                        <td class="text-light">E-mail</td>
                                        <td class="text-light">Amount  User Entered</td>
                                        <td class="text-light">Date Clicked</td>
                                   </tr>
                                   <?php 
                                   for($cnt = 0;$cnt < $total_planA;$cnt++)
                                   {
                                        $userA = getUser($conn,
                                        " WHERE uid = ? ",
                                        array("uid"),
                                        array($planA_Uid[$cnt]),
                                        "s");
                                   ?>
                                   <tr>
                                        <td class="text-light"><?php echo $cnt+1;?></td></td>
                                        <!-- <td><?php //echo $planA_Uid[$cnt];?></td> -->
                                        <td class="text-light"><?php echo $userA[0]->getEmail();?></td>
                                        <td class="text-light"><?php echo removeUselessZero($planA_AmountEntered[$cnt])." BTC";?></td>
                                        <td class="text-light"><?php echo date("g:i:s a",strtotime($planA_DateClicked[$cnt]))." on ".date("d M y",strtotime($planA_DateClicked[$cnt]));?></td>
                                   </tr>
                                   <?php 
                                   }
                                   ?>
                              </table>
                         </div>
                         <div class="col-md-2"></div>
                    </div>
                    <?php 
               }
               else
               {
                    echo "
                    <div class='row mt-5'>
                         <div class='col-md-2'></div>
                         <div class='col-md-8'>
                              <h3 class='text-light'>No Transaction Found For Plan A</h3>
                         </div>
                         <div class='col-md-2'></div>
                    </div>
                    ";
               }

               if($total_planB > 0)
               {
                    ?>
                    <div class="row">
                         <div class="col-md-2"></div>
                         <div class="col-md-8">
                              <h3 class="btcw-h4 edit-h4-title white-text mt-5">Plan B</h3>
                              <table class="table table-bordered">
                                   <tr>
                                        <td class="text-light">No</td>
                                        <!-- <td>User ID</td> -->
                                        <td class="text-light">E-mail</td>
                                        <td class="text-light">Amount User Entered</td>
                                        <td class="text-light">Date Clicked</td>
                                   </tr>
                                   <?php 
                                   for($cnt = 0;$cnt < $total_planB;$cnt++)
                                   {
                                        $userB = getUser($conn,
                                        " WHERE uid = ? ",
                                        array("uid"),
                                        array($planB_Uid[$cnt]),
                                        "s");
                                   ?>
                                   <tr>
                                        <td class="text-light"><?php echo $cnt+1;?></td></td>
                                        <!-- <td><?php //echo $planA_Uid[$cnt];?></td> -->
                                        <td class="text-light"><?php echo $userB[0]->getEmail();?></td>
                                        <td class="text-light"><?php echo removeUselessZero($planB_AmountEntered[$cnt])." BTC";?></td>
                                        <td class="text-light"><?php echo date("g:i:s a",strtotime($planB_DateClicked[$cnt]))." on ".date("d M y",strtotime($planB_DateClicked[$cnt]));?></td>
                                   </tr>
                                   <?php 
                                   }
                                   ?>
                              </table>
                         </div>
                         <div class="col-md-2"></div>
                    </div>
                    <?php 
               }
               else
               {
                    echo "
                    <div class='row mt-3'>
                         <div class='col-md-2'></div>
                         <div class='col-md-8'>
                              <h3 class='text-light'>No Transaction Found For Plan B</h3>
                         </div>
                         <div class='col-md-2'></div>
                    </div>
                    ";
               }
          }
      ?>
     
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <?php require_once 'mainFooter.php';?>
  </body>
</html>