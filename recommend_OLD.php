<?php
//Start the session
session_start();

//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
    exit();
}else{
    $uid = $_SESSION['uid'];
}

require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';

$conn = connDB();

$referralHistoryRows = array();
$thisUserFirstTimeReferralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ORDER BY id ASC LIMIT 1 ",array("referrer_id"),array($uid),"s");
if($thisUserFirstTimeReferralHistoryRows){
   $topReferrerId = $thisUserFirstTimeReferralHistoryRows[0]->getTopReferrerId();
   if($topReferrerId === $uid){
       $thisUserFirstLevel = 1;
       $thisUserLastLevel = 10;
   }else{
       $thisUserFirstLevel = $thisUserFirstTimeReferralHistoryRows[0]->getCurrentLevel();
       $thisUserLastLevel = $thisUserFirstLevel + 9;
   }

   $downlineReferralIdArray = array();
   $downlineReferralIdArray[$uid]  = true ;
   for($loopCurrentLevel = $thisUserFirstLevel; $loopCurrentLevel <= $thisUserLastLevel; $loopCurrentLevel++){
       $thisLevelReferralHistoryRows = getReferralHistory($conn," WHERE top_referrer_id = ? AND current_level = $loopCurrentLevel ORDER BY id ",array("top_referrer_id"),array($topReferrerId),"s");
       if($thisLevelReferralHistoryRows){
           $newDownlineReferralIdArray = array();
           foreach ($thisLevelReferralHistoryRows as $tempReferral) {
               $canAdd = false;
               foreach ($downlineReferralIdArray as $key => $value) {
                   if($tempReferral->getReferrerId() === $key){
                       $newDownlineReferralIdArray[$tempReferral->getReferralId()]  = true ;
                       $canAdd = true;
                       break;
                   }
               }
               if($canAdd){
                   array_push($referralHistoryRows,$tempReferral);
               }
           }
           unset($downlineReferralIdArray);
           $downlineReferralIdArray = array();
           foreach ($newDownlineReferralIdArray as $newDownlineReferralId => $value) {
               $downlineReferralIdArray[$newDownlineReferralId]  = true ;
           }
           unset($newDownlineReferralIdArray);
       }
   }
}

?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/recommend.php" />
        <meta property="og:title" content="比特基金 - 推荐" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

        <title>比特基金 - 推荐</title>
        <link rel="canonical" href="https://btcworg.com/recommend.php" />
  </head>
  <body>
  <?php require 'mainNavbar.php';?>
      <div id="dialog" class="dialog-css" style="display: none">
        链接已复制。
    </div>
  <div id="firefly" class="firefly-class min-height">    
      <div class="width100 same-padding more-separate-margin-top text-center">
        <h4 class="btcw-h4 white-text"><b class="weight-700">专属推荐链接：</b> </h4>
        <p class="share-link"><a href="<?php echo "https://btcworg.com/indexRegister.php?referralId=$uid";?>" id="invest-now-referral-link" class="share-a yellow-text opacity-hover"><?php echo "https://btcworg.com/indexRegister.php?referralId=$uid";?></a></p>
      </div>
      <div class="width100 same-padding copy-button-div">
        <div class="fill-up-space1 ow-height"></div>
        <button class="yellow-line-box yellow-box-width yellow-box-hover white-text text-center clean copy-yellow-line" id="copy-referral-link">复制链接</button>
        <div class="fill-up-space1 ow-height"></div>
      </div>  
    
    
      <div class="width100 same-padding extra-padding-bottom overflow-hidden">
        <div class="fill-up-space1"></div>
          <!--
        <a href="scanQr.php" class=""><div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center">扫描二维码</div></a>
        -->
        <div class="fill-up-space1"></div>
      </div>
    
      <div class="width100 same-padding">
        <div class="left-recommend-div">
            <h4 class="btcw-h4 separate-title white-text"><b class="weight-700">已推荐用户：</b> </h4>
            <?php
                $levelArrayInChinese = array("第一代","第二代","第三代","第四代","第五代","第六代","第七代","第八代","第九代","第十代");
                if($referralHistoryRows && count($referralHistoryRows) > 0) {
                    $currentLevel = $thisUserFirstLevel - 1;
                    $currentNo = 0;
    
                    foreach ($referralHistoryRows as $thisReferral) {
                        $currentNo++;
                        if($currentLevel < $thisReferral->getCurrentLevel()){
                            $currentLevel++;
                            echo '<h4 class="btcw-h4 separate-title white-text"><b class="weight-700">'.$levelArrayInChinese[$currentLevel - $thisUserFirstLevel].'：</b> </h4>        
                                    <table cellspacing="0" cellpadding="0" class="transparent-table recommend-table white-text-table">';
                        }
    
                        $referralUid = $thisReferral->getReferralId();
                        $tempUserRow = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($referralUid),"s");
                        if($tempUserRow && count($tempUserRow) > 0){
                            $referralUser = $tempUserRow[0];
                            $totalReferredCount = getCount($conn,"referral_history","referrer_id"," WHERE referrer_id = ? ", array("referrer_id"), array($referralUser->getUid()),"s");
    
                            echo '<tr>
                                    <td class="c-td1" valign="top"><p class="weight-700">'.$currentNo.'.</p></td>
                                    <td class="c-td2" valign="top"><p class="weight-700">用户名</p></td>
                                    <td class="c-td3" valign="top"><p>:</p></td>
                                    <td class="c-td4" valign="top"><p>'.$referralUser->getUsername().'</p></td>
                                  </tr>
                                  <tr>
                                    <td class="c-td1" valign="top"><p class="weight-700">&nbsp;</p></td>
                                    <td class="c-td2" valign="top"><p class="weight-700">加入日期</p></td>
                                    <td class="c-td3" valign="top"><p>:</p></td>
                                    <td class="c-td4" valign="top"><p>'. date( 'd/m/Y', strtotime($referralUser->getDateCreated()) ) .'</p></td>
                                  </tr>
                                  <tr>
                                    <td class="c-td1" valign="top"><p class="weight-700">&nbsp;</p></td>
                                    <td class="c-td2" valign="top"><p class="weight-700">已推荐</p></td>
                                    <td class="c-td3" valign="top"><p>:</p></td>
                                    <td class="c-td4" valign="top"><p>'.$totalReferredCount.'个用户</p></td>
                                  </tr>';
                        }
    
                        if($currentNo < count($referralHistoryRows) && $referralHistoryRows[$currentNo] && $referralHistoryRows[$currentNo]->getCurrentLevel() == $currentLevel + 1){
                            echo "</table>";
                        }
                    }
    
                    echo "</table>";
                }else{
                    echo "<p class='white-text'>还没推荐任何一个人。</p>";
                }
    
                $conn->close();
            ?>
            
            
          </div>
          <div class="right-recommend-div">
            <img src="img/indexIntro.png" class="half-div-img width100" alt="BTCW" title="BTCW">
          </div>   
      </div>
	</div>

       
   <div id="myModal10" class="modal2 ow-modal2 ow-modal10">
 
          <div class="modal-content2 modal-content-white ow-modal-width ow-modal10a">
            <h4 class="btcw-h4 text-center ow10-h4">链接已复制</h4>
          </div>
   </div>



  <?php require 'mainFooter.php';?>

  <script>
      $("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#invest-now-referral-link').attr("href");
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
      });
  </script>
         <script>
			// Get the modal
			var modal10 = document.getElementById('myModal10');
			
			// Get the button that opens the modal
			var btn10 = document.getElementById("copy-referral-link");

			// When the user clicks the button, open the modal
			btn10.onclick = function() {
			  modal10.style.display = "block";
                setTimeout(closeCopiedModalWithDelay, 1600);
			};


			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal10) {
				modal10.style.display = "none";
			  }
			};

            function closeCopiedModalWithDelay() {
                modal10.style.display = "none";
            }
		</script>
    
  </body>
</html>