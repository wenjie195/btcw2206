<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
require_once 'utilities/calculationFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{ 
     if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
          $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
     {
          header('Location:index.php');
          exit();
     }
     else
     {
          $uid = $_SESSION['uid'];
          $conn = connDB();
          $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
     }
}

$errorMsg = null;
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $emailORUsername = $_POST['field_1'];
     $error = FALSE;

     $userDetails = getUser($conn,
                    " WHERE email = ? OR username = ? OR uid = ? ",
                    array("email","username","uid"),
                    array($emailORUsername,$emailORUsername,$emailORUsername),
                    "sss");

     if(!updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array("5df44493bc090fc9c801ca9c0c380d2845fdd2c3f98e8c4d8b296fac9ed5f309","a1ea4bc33f8c298210480b04761c1ffbc7cbde7c",$userDetails[0]->getUid()),"sss"))
     {
          $errorMsg = "cannot update password";
     }
     else
     {
          $errorMsg = "Password are reverted";
     }            
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <title>ZCXC</title>
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();
     if($errorMsg != null)
     {
          putNotice("Notice",$errorMsg);
     }
  ?>
  <div id="firefly" class="firefly-class min-height">  
     <div class="width100 same-padding more-separate-margin-top edit-div">
          <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a> 
          <div class="clear"></div>
         <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Reset User Password</b></h4>
            <p class="white-text"><b class="weight-700">This is to change the password</b></p>
            <form class="register-form"  method="POST" >
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Email/Username/User ID</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_1" id="field_1" required></h4></td>
                  </tr>
                </table>
                <div class="clear"></div>
               <button class="register-button2 clean orange-hover inputb-button" name="insertValue" id="insertValue" >Reset Password</button>

          </form>
     </div> 
      <div class="clear"></div>
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <?php require_once 'mainFooter.php';?>
  </body>
</html>