<?php
require_once 'utilities/checkSessionFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require 'generalFunction.php';

    // echo $_COOKIE["username"]."<br>";
    // echo $_COOKIE["email"]."<br>";
    // echo $_COOKIE["country"]."<br>";
    // echo $_COOKIE["phoneNo"]."<br>";
    // echo $_COOKIE["countryCode"]."<br>";
    // echo $_COOKIE["password"]."<br>";
?>

<!doctype html>
<html>
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/indexRegister.php/" />
        <meta property="og:title" content="<?= _indexcr_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency wallet platform for Bitcoin in the world. Join us now!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency wallet platform for Bitcoin in the world. Join us now!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency wallet platform, e-wallet, investment, invest, register, member, 会员注册, 比特币, 比特钱包, 预约, 创富平台, 捐赠, 利益, 收益, etc">
        
        <title><?= _indexcr_title ?></title>
        <link rel="canonical" href="https://btcworg.com/indexConfirmRegister.php/" />
  </head>
  <body>
   <?php 
        generateSimpleModal(); // Must be first in order to put modal boxes
       ?>
  
    <div id="firefly" class="firefly-class min-height"> 
    	<?php require 'mainNavbar.php';?> 
    	<div class="width100 same-padding">
    		<div class="login-width text-center white-text">
         		<!---Hide it now but once Mun Chun done the backend, unhide it-->
            	<!--
                <h1 class="register-title text-center">登入微信完成注册</h1>
                <div class="wrap-code text-center">
                	<img src="img/sherry/btcw-qr.png" class="qr-img2">
                    <h4 class="qr-h4">使用微信扫一扫这二维码来登入。</h4>
                </div> 
              	
                <div class="width100 or-div">
                	<div class="fill-space-1 left-fill-space"></div>
                    <div class="or-only"><h4 class="or-h4 text-center">或</h4></div>
                    <div class="fill-space-1 right-fill-space"></div>
                </div>
                -->
                <h1 class="register-title text-center"><?= _indexcr_title ?></h1>
                <form class="register-form"  method="POST">
                		<p class="numbering"><?= _indexcr_step1 ?></p> 
                 		<input type="button" value="<?= _indexcr_send_code ?>" class="register-button2 clean orange-hover margin-0 button-width" name="sendCodeButton" id="sendCodeButton" onclick="sendVerifyCode()"> 
                    	<p class="numbering second-numbering"><?= _indexcr_step2 ?></p> 
                        <div class="input-container1">
                        	<input type="number" class="verification-input inputa clean2 button-width"  name="field_1" placeholder="<?= _indexcr_step2 ?>" id="verificationcode">
							<img src="img/sherry/verify2.png" class="input-icon">
                        </div>
                        <p class="numbering"><?= _indexcr_step3 ?></p> 
                        <input type="button" value="<?= _indexcr_confirm_btn ?>" class="register-button2 clean orange-hover margin-0 button-width" name="loginButton" id="loginButton" onclick="clickVerify()">
                               	                      
                </form>
            <p class="loginRegister"><?= _forgotPassword_not_yet ?><a class="loginRegisterLink orange-text-hover" href="indexRegister.php"><?= _forgotPassword_register_here ?></a></p>
            <p class="loginRegister edit-mtop"><a class="loginRegisterLink orange-text-hover" href="forgotPassword.php"><?= _indexcr_forgot_pw ?></a></p>            
            </div>
        </div>
    </div>
    
    
    
  
    <?php require 'mainFooter.php';?>  
 
    <script>
        $( document ).ready(function() 
        {
            $('#loginButton').css("display","none");
            $('#sendCodeButton').css("display","block");
        });

        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sendCodeButton', {
        'size': 'invisible',
        'callback': function(response) {
            // reCAPTCHA solved, allow signInWithPhoneNumber.
            onSignInSubmit();
        }
        });

        var appVerifier = window.recaptchaVerifier;
        var confirmRes;

        var name =  "<?php echo $_COOKIE['username'];?>";
        var email = "<?php echo $_COOKIE['email'];?>";
        var country =  "<?php echo $_COOKIE['country'];?>";
        var phoneNo =  "<?php echo $_COOKIE['phoneNo']; ?>";
        var password =  "<?php echo $_COOKIE['password']; ?>";
        var receiveNewsLetter =  "<?php echo $_COOKIE['receiveNewsLetter']; ?>";
        var icno =  "<?php echo $_COOKIE['icNo']; ?>";
        var referralId =  "<?php if(isset($_GET['referralId'])) echo $_GET['referralId']; else echo ''; ?>";

        //from here https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
        // dec2hex :: Integer -> String
        function dec2hex (dec) {
            return ('0' + dec.toString(16)).substr(-2)
        }

        // generateId :: Integer -> String
        function generateId (len) {
            var arr = new Uint8Array((len || 40) / 2)
            window.crypto.getRandomValues(arr)
            return Array.from(arr, dec2hex).join('')
        }

        var id = generateId();
        id = "CUSTOM-ID-" + id;

        $.ajax({
            url: "generalFunction.php",
            type: 'POST',
            async: false,
            data:
                {
                    value: 0,
                    fromPage:"confirmRegister",
                    uid : id,
                    name : name,
                    email : email,
                    country : country,
                    phoneNo : phoneNo,
                    password : password,
                    receiveNewsLetter: receiveNewsLetter,
                    icno:icno,
                    referralId: referralId
                }
        })
            .done(function(message)
            {
                console.log(message);
                alert("<?= _indexcr_register_success ?>");
                window.location.replace("indexLogin.php");
            })
            .fail(function()
            {
                alert( "<?= _general_sorry ?>" );
            });

        function sendVerifyCode()
        {
            
            firebase.auth()
            .signInWithPhoneNumber("+"+countryCode+phoneNo,appVerifier) 
            .then(function(confirmationResult) 
            {
                window.confirmationResult = confirmationResult;
                confirmRes =  window.confirmationResult;
                console.log(confirmationResult);
                $('#loginButton').css("display","block");
            });
            $('#sendCodeButton').css("display","none");
            
        }
        function clickVerify()
        {
            confirmRes.confirm(document.getElementById("verificationcode").value)
            .then(function(result) 
            {
                console.log(result);
                var user = result.user;
                console.log(user.uid);
                //Continue HERE

                $.ajax({
                        url: "generalFunction.php",
                        type: 'POST',
                        async: false,
                        data:
                        {
                            value: 0,
                            fromPage:"confirmRegister",
                            uid : user.uid,
                            name : name,
                            email : email,
                            country : country,
                            phoneNo : phoneNo,
                            password : password,
                            receiveNewsLetter: receiveNewsLetter,
                            icno:icno,
                            referralId: referralId
                        }
                    })
                    .done(function(message) 
                    {
                        console.log(message);
                        alert("<?= _indexcr_register_success ?>");
                        window.location.replace("indexLogin.php");
                    })
                    .fail(function() 
                    {
                        alert( "<?= _general_sorry ?>" );
                    });

            }).catch(function(error) 
            {
                console.log(error);
            });
        }
    </script>

  </body>
</html>