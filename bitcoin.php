<?php
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        
        <meta property="og:url" content="https://btcworg.com/" />
        <meta property="og:title" content="<?= _bitcoin_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
        
        <title><?= _bitcoin_title ?></title>
        <link rel="canonical" href="https://btcworg.com/" />
  </head>
  <body>

    <?php require 'mainNavbar.php';?>   
	<div id="firefly" class="firefly-class min-height text-center">  

        	<div class="clear"></div> 
            <div class="width100 same-padding">
        	<h1 class="btcw-h1 text-center first-h1 white-text"><?= _bitcoin_first_h1 ?></h1>
<!--
            <p class="text-center download-p white-text">使用百度网盘下载安卓比特基金APP （ 提取码： j44w ）</p>
            <div class="clear"></div>            
            <div class="fill-space4"></div>
            <div class="actual-width4"><a href="https://pan.baidu.com/s/1oYO_pisyzvHmnd6KB7T_-w" target="_blank" class="opacity-hover"><img src="img/sherry/baidu-pan.png" class="width100"></a></div>
            <div class="fill-space4"></div>
            <div class="clear"></div>                       
            <p class="text-center download-p white-text little-mtop">或用微信扫描以下二维码下载</p>
            <div class="clear"></div>
            <div class="fill-space4"></div>
            <div class="actual-width4"><img src="img/sherry/qrcode-baidu.jpg" class="width100"></div>
            <div class="fill-space4"></div>
            <div class="clear"></div> 
            <p class="text-center download-p white-text little-mtop">海外用户推荐点击以下图片下载</p>             
            <div class="clear"></div>          
            <div class="fill-space4"></div>
            <div class="actual-width4"><a href="app/btwc_platform.apk" class="opacity-hover" download><img src="img/sherry/download-apk.png" class="width100"></a></div>
            <div class="fill-space4"></div>

            <!--<div class="fill-small-space"></div>
            <a href="#" class="a-hover"><div class="yellow-box yellow-box-width yellow-box-hover white-text text-center">马上加入成为比特基金一族</div></a>
            <div class="fill-small-space fill-small-space-m"></div>
            <div class="fill-small-space fill-small-space-m"></div>
            <a href="#" class="a-hover"><div class="yellow-line-box yellow-box-width yellow-line-box-hover white-text text-center">选择你的基金</div></a>
            <div class="fill-small-space"></div>-->
        </div>
        <!--<div class="social-icon-div">
        	<p class="fb-p">
            	<a href="#">
                    <img src="img/indexFBLink.png" class="fb-icon social-icon opacity-hover" alt="Facebook" title="Facebook">
                    
                </a>
            </p>
            <p class="tw-p">
            	<a href="#">            
                    <img src="img/indexTwitterLink.png" class="tw-icon social-icon opacity-hover" alt="Twitter" title="Twitter">
                   
                </a>
            </p>
        </div>-->
        <div class="clear"></div>
        <div class="width100 same-padding text-center video-div">
        	<button id="myBtn2" class="button-css clean hover-container white-text"><img src="img/indexPlayButton.png" class="play-button hover-1" alt="<?= _bitcoin_what_is_btcw ?>" title="<?= _bitcoin_what_is_btcw ?>"><img src="img/sherry/yellow-play.png" class="play-button hover-2" alt="<?= _bitcoin_what_is_btcw ?>" title="<?= _bitcoin_what_is_btcw ?>"> <?= _bitcoin_what_is_btcw ?></button>
        </div>
        
        
        
        <!--- Modal --->
        <div id="myModal2" class="modal2">
        
          <!-- Modal content -->
          <div class="modal-content2">
            <span class="close2">&times;</span>
            <iframe width="100%" height="699" class="youtube-video" src="https://www.youtube.com/embed/Gc2en3nHxA4" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        
        </div>

		<div class="width100 element-div" style="display:none;">
        	<img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
        
        <div class="clear"></div>
        
        
        
        
        <div class="width100 big-div-three-items same-padding overflow-hidden separate-margin-top">
        	<h1 class="btcw-h1 yellow-text text-center news-h1"><?= _BITCOIN_LATEST_NEWS ?></h1>
            <a href="http://www.bitcoin86.com/news/35726.html" target="_blank" class="opacity-hover">
                <div class="one-item-div news-one-item-div">
                    <img src="img/sherry/news1.jpg" class="width100 opacity-hover" alt="<?= _bitcoin_news1_title ?>" title="<?= _bitcoin_news1_title ?>">
                    <p class="date-p opacity-hover">
                    	2019-02-15
                    </p>
                    <p class="news-p opacity-hover white-text-important">
                        <?= _bitcoin_news1_title ?>
                    </p>
                </div>
            </a>
            <a href="http://www.bitcoin86.com/news/35707.html" target="_blank" class="opacity-hover">            
                <div class="one-item-div middle-one-item news-one-item-div">
                    <img src="img/sherry/news2.jpg" class="width100 opacity-hover" alt="<?= _bitcoin_news2_title ?>" title="<?= _bitcoin_news2_title ?>">
                    <p class="date-p opacity-hover">
                    	2019-02-14
                    </p>                    
                    <p class="news-p opacity-hover white-text-important">
                        <?= _bitcoin_news2_title ?>
                    </p>
                </div> 
            </a>
            <a href="http://www.bitcoin86.com/news/35681.html" target="_blank" class="opacity-hover">                       
                <div class="one-item-div news-one-item-div">
                    <img src="img/sherry/news3.jpg" class="width100 opacity-hover" alt="<?= _bitcoin_news3_title ?>" title="<?= _bitcoin_news3_title ?>">
                    <p class="date-p opacity-hover">
                    	2019-02-14
                    </p>
                    <p class="news-p opacity-hover white-text-important">
                       <?= _bitcoin_news3_title ?>
                    </p>
                </div> 
            </a>           
        </div>
        
        <div class="clear"></div>
        
        <div class="width100 big-div-three-items same-padding overflow-hidden more-separate-margin-top">
        	<h1 class="btcw-h1 yellow-text text-center news-h1 white-text"><?= _bitcoin_commercial_mode ?></h1>
            <h1 class="btcw-h1 text-center weight-700 white-text"><?= _bitcoin_profit ?></h1>
            <p class="text-center des-p white-text"><?= _bitcoin_motive ?></p>
        	<div class="one-item-div">
            	<img src="img/sherry/free-registration.png" class="three-item-icon" alt="<?= _bitcoin_free_reg ?>" title="<?= _bitcoin_free_reg ?>">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	<?= _bitcoin_free_reg ?>
                </p>
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/bitcoin.png" class="three-item-icon" alt="<?= _bitcoin_platform_gift ?>" title="<?= _bitcoin_platform_gift ?>">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	<?= _bitcoin_platform_gift ?>
                </p>
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/five-people.png" class="three-item-icon" alt="<?= _bitcoin_recommend5 ?>" title="<?= _bitcoin_recommend5 ?>">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	<?= _bitcoin_recommend5 ?>
                </p>
            </div>            
        </div>
        
        <div class="clear"></div>
        
        <div class="width100 big-div-three-items same-padding overflow-hidden more-separate-margin-top">
        	
            <h1 class="btcw-h1 text-center weight-700 margin-bottom white-text"><?= _bitcoin_gain ?></h1>
           
        	<div class="one-item-div">
            	<img src="img/sherry/triple.png" class="three-item-icon" alt="<?= _bitcoin_one_turn_three_alt ?>" title="<?= _bitcoin_one_turn_three_alt ?>">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	<?= _bitcoin_one_turn_three ?>
                </p>
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/time.png" class="three-item-icon" alt="<?= _bitcoin_withdraw ?>" title="<?= _bitcoin_withdraw ?>">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	<?= _bitcoin_release ?>
                </p>
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/transaction-fee.png" class="three-item-icon" alt="<?= _bitcoin_transaction_fee ?>" title="<?= _bitcoin_transaction_fee ?>">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	<?= _bitcoin_transaction_fee_18 ?>
                </p>
            </div>            
        </div>     
        
        <div class="clear"></div>
        
        <div class="width100 big-div-three-items same-padding overflow-hidden more-separate-margin-top">
        	
            <h1 class="btcw-h1 text-center weight-700 margin-bottom white-text"><?= _bitcoin_movement_recommend ?></h1>
            <p class="text-center des-p white-text"><?= _bitcoin_referral_program ?></p>
        	<div class="one-item-div">
            	<img src="img/sherry/low-risk.png" class="three-item-icon" alt="<?= _bitcoin_community02 ?>" title="<?= _bitcoin_community02 ?>">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	<?= _bitcoin_community02 ?>
                </p>
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/profit.png" class="three-item-icon" alt="<?= _bitcoin_community01 ?>" title="<?= _bitcoin_community01 ?>">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	<?= _bitcoin_community01 ?>
                </p>
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/pyramid.png" class="three-item-icon" alt="<?= _bitcoin_community10 ?>" title="<?= _bitcoin_community10 ?>">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	<?= _bitcoin_community10 ?>
                </p>
            </div>            
        </div>       
           
        <div class="clear"></div>
        
        <div class="width100 same-padding overflow-hidden more-separate-margin-top">
        	<h1 class="btcw-h1 text-center weight-700 margin-bottom white-text"><?= _bitcoin_program_intro ?></h1>
            <div class="right-img-div float-right"><img src="img/indexIntro.png" class="width100" alt="BTCW" title="BTCW"></div>
        	<div class="left-text-div float-left white-text"><?= _bitcoin_program_intro_desc ?></div>
        	
        </div>
                
        <div class="clear"></div>
        
        <div class="width100 big-div-three-items same-padding overflow-hidden more-separate-margin-top extra-padding-bottom">
        	
            <h1 class="btcw-h1 text-center weight-700 margin-bottom white-text"><?= _bitcoin_what_is_btcw ?></h1>
            
        	<div class="one-item-div">
            	<img src="img/sherry/global.png" class="three-item-icon" alt="<?= _bitcoin_common1 ?>" title="">
                <p class="three-item-p text-center yellow-text-important weight-700">
                	<?= _bitcoin_common1 ?>
                </p>
                <p class="three-item-p text-center black-text justify-three white-text-important">
                	<?= _bitcoin_common1_desc ?>
                </p>                
                
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/value.png" class="three-item-icon" alt="<?= _bitcoin_total_value ?>" title="<?= _bitcoin_total_value ?>">
                <p class="three-item-p text-center yellow-text-important weight-700">
                	<?= _bitcoin_total_value ?>
                </p>
                <p class="three-item-p text-center black-text justify-three white-text-important">
					<?= _bitcoin_total_value2000 ?>
                </p>                
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/influence.png" class="three-item-icon" alt="<?= _bitcoin_influence ?>" title="<?= _bitcoin_influence ?>">
                <p class="three-item-p text-center yellow-text-important weight-700">
                	<?= _bitcoin_influence ?>
                </p>
                <p class="three-item-p text-center black-text justify-three white-text-important">
					<?= _bitcoin_influence_desc ?>
                </p>                
                
            </div>            
        </div> 
	</div>



        <script>
			// Get the modal
			var modal = document.getElementById('myModal2');
			
			// Get the button that opens the modal
			var btn = document.getElementById("myBtn2");
			
			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close2")[0];
			
			// When the user clicks the button, open the modal 
			btn.onclick = function() {
			  modal.style.display = "block";
			}
			
			// When the user clicks on <span> (x), close the modal
			span.onclick = function() {
			  modal.style.display = "none";
			}
			
			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal) {
				modal.style.display = "none";
			  }
			}
		</script>
        
        
        
 		<?php require 'mainFooter.php';?>
  		<style>
			.social-icon-div{
				display:none;}
        </style>      

  </body>
</html>