<?php
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php require 'mainHeader.php';?>
<meta property="og:url" content="https://btcworg.com/prelaunching-en.php/" />
<meta property="og:title" content="BTCW - Pre-launching" />
<meta name="description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
<meta property="og:description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
<meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, etc">

<title>BTCW - Pre-launching</title>
<link rel="canonical" href="https://btcworg.com/prelaunching-en.php/" />

</head>

<body>
       
       

    <div id="firefly" class="firefly-class">
    	<?php require 'mainNavbar.php';?>
    	<div class="width100 same-padding switch-row">
        	<a href="prelaunching.php" class="opacity-hover white-text language-a">
            	<p class="language-p">
            		<img src="img/sherry/cn.png" alt="转换成中文" title="转换成中文" class="language-icon language-icon1">
                    <img src="img/sherry/cn2.png" alt="转换成中文" title="转换成中文" class="language-icon language-icon2">
                      转换成中文
           		</p>
           </a>
        </div>
        <div class="width100 same-padding coin-gif-row">
    		<p class="gif-p">
            	<img src="img/sherry/bitcoin2.gif" class="bitcoin-video" alt="比特基金" title="比特基金">
            </p>
            <p class="launching-p white-text">
            	BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world.
            </p>
        </div>
        <div class="width100 big-div-three-items same-padding overflow-hidden">
        	<div class="one-item-div">
            	<img src="img/sherry/low-risk.png" class="three-item-icon" alt="Guaranteed profits for small and medium investors" title="Guaranteed profits for small and medium investors">
                <p class="three-item-p text-center">
                	Guaranteed profits for small and medium investors
                </p>
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/profit.png" class="three-item-icon" alt="Lucrative gains from static and dynamic investment" title="Lucrative gains from static and dynamic investment">
                <p class="three-item-p text-center">
                	Lucrative gains from static and dynamic investment
                </p>
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/invest.png" class="three-item-icon" alt="Optimal Investment Platform" title="Optimal Investment Platform">
                <p class="three-item-p text-center">
                	Optimal investment platform
                </p>
            </div>            
        </div>
        <div class="width100 same-padding text-center">
        	<div class="fill-up-space"></div>
        	<a href="index.php" class="yellow-button-a" target="_blank">
                <div class="yellow-button yellow-left">
                    Learn More
                </div>
           </a>
           <div class="fill-up-space"></div>
        </div>
        <div class="clear"></div>
        <div class="width100 same-padding coin-gif-row pre-register-div">
            <p class="launching-p white-text">
                Be the first to know when BTCW launches!
            </p>
       </div>
       <div class="width100 same-padding coin-gif-row pre-register-inputdiv text-center">
            
             
        	<div class="fill-up-space"></div>
        	<a href="indexRegister.php" class="yellow-button-a" target="_blank">
                <div class="yellow-button yellow-left">
                    Register Now!
                </div>
           </a>
           <div class="fill-up-space"></div>
     
       </div>       
       
               
    </div>

<?php require 'mainFooter.php';?>

</body>
</html>