<footer class="width100 same-padding indexBlackBackground footer-class">
    <div class="footer1 white-text">
        <?= _footer_copyright ?>
    </div>
    <div class="footer2">
        <a href=""><img src="img/sherry/facebook.png" class="footer-social opacity-hover"></a>
        <a href=""><img src="img/sherry/twitter.png" class="footer-social opacity-hover"></a>
    </div>
    <div class="footer3">
        <?php 
        if(empty($_SESSION['uid']))
        {
        ?>
        
          <a class="indexSignUp opacity-hover" href="indexLogin.php"><?= _footer_login ?></a>
          <a class="indexSignUp opacity-hover" href="indexRegister.php"><?= _footer_register ?></a>
        <?php
        }
        else
        {
        ?>
          <a class="indexSignUp opacity-hover" href="profile.php"><?= _footer_profile ?></a>
          <a class="indexSignUp opacity-hover" href="recommend.php"><?= _footer_refer ?></a>
        <?php
        }
        ?>
    </div>
</footer>
	
        <!--- Modal --->
        <div id="menu-modal" class="modal2 menu-modal-output">
        
          <!-- Modal content -->
          <div class="modal-content2 dark-bg menu-inner-modal">
             <span class="close-menu ow-close2 close-style menu-close-style">&times;</span>                   
                        <p class="span-height-p">&nbsp;</p>
                        <!--<a class="new-menu-a a-hover" href="download-btcw-app.php"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; 下载 APP &nbsp; &nbsp; &nbsp; &nbsp; </div></a>-->
                        <a href="profile.php" class="new-menu-a a-hover"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; <?= _footer_profile ?> &nbsp; &nbsp; &nbsp; &nbsp; </div></a>
                        <!-- Fizo Here Unhide the below specialProfile link and only specialProfile can access it-->
                        <?php 
                        if($uid == "CUSTOM-ID-01dc43a42713ebad5780176ba6d62e8407950605"
                        || $uid == "VXtbpgh5sdSoEXGqhKK54UOZDd92"
                        || $uid == "CUSTOM-ID-691768c4318f014372931f1a8225b6386315a442" 
                        || $uid == "CUSTOM-ID-2d777f2fca4f62a0d4d98f0b98efa6e829878335")
                        {
                        ?>
                        <a href="specialProfile.php" class="new-menu-a a-hover"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; <?= _footer_refer_bonus ?> &nbsp; &nbsp; &nbsp; &nbsp; </div></a>
                        <?php
                        }
                        ?>
                        <a href="market.php" class="new-menu-a a-hover"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; <?= _footer_market ?> &nbsp; &nbsp; &nbsp; &nbsp; </div></a>
                        <!--<a href="specialProfile.php" class="new-menu-a a-hover"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; 推荐收益 &nbsp; &nbsp; &nbsp; &nbsp; </div></a>-->
                        <a href="recommend.php" class="new-menu-a a-hover"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; <?= _footer_refer ?> &nbsp; &nbsp; &nbsp; &nbsp; </div></a>
                       
                        <a href="notice.php" class="new-menu-a a-hover"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; <?= _footer_notice ?> &nbsp; &nbsp; &nbsp; &nbsp; </div></a>
                        <a href="#" class="new-menu-a a-hover display-none"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; <?= _footer_contact ?> &nbsp; &nbsp; &nbsp; &nbsp; </div></a>
                          <?php
                            if(isset($_SESSION['lang']) && $_SESSION['lang'] === "en"){
                                echo '<a  class="new-menu-a a-hover" onclick="changeLang(\'ch\');"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; 转换成中文 &nbsp; &nbsp; &nbsp; &nbsp; </div></a>';
                            }else{
                                echo '<a  class="new-menu-a a-hover" onclick="changeLang(\'en\');"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; Switch to English &nbsp; &nbsp; &nbsp; &nbsp; </div></a>';
                            }
                          ?>
                        <a href="indexLogOut.php" class="new-menu-a a-hover"><div class="new-menu-p">&nbsp; &nbsp; &nbsp; &nbsp; <?= _footer_logout ?> &nbsp; &nbsp; &nbsp; &nbsp; </div></a>
                        <a class="new-menu-a a-hover close-menu"><div class="new-menu-p close-menu">&nbsp; &nbsp; &nbsp; &nbsp; <?= _footer_close ?> &nbsp; &nbsp; &nbsp; &nbsp; </div></a>
          </div>
        </div>

<!--Jquery-->
<link rel="stylesheet" type="text/css" href="css/jquery-confirm.css?version=1.0.17" />
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-confirm.js"></script>

<!--Bootstrap and Popper.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!--Firebase(Must Have)-->
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-app.js"></script>

<!--Firebase(Optional)-->
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-functions.js"></script>
<script>
  var config = {
    apiKey: "AIzaSyC2rLJgT1EN-vqlO2M5kVdIKwprFy-sSxw",
    authDomain: "btwc-platform.firebaseapp.com",
    databaseURL: "https://btwc-platform.firebaseio.com",
    projectId: "btwc-platform",
    storageBucket: "btwc-platform.appspot.com",
    messagingSenderId: "1024400218886"
  };
  firebase.initializeApp(config);
</script>
  
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
            }
    
            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
            
            /*close when you click outside of the sideNav*/
            $('body').on('click touchstart', function(){
              if( parseInt( $('#mySidenav').css('width') ) > 0 ){
                closeNav();
              }
            });
           /*$('body').on('click', function(){
           if( parseInt( $('#mySidenav').css('width') ) > 0 ){
           closeNav();
           }
           });*/
	</script>
<!-- Main Script --> 

<script type="text/javascript" src="js/main.js?version=1.0.0"></script>

<script type="text/javascript" src="js/firefly.js"></script>
	<script>
      $.firefly({
        color: '#f7931a',
        minPixel: 1,
        maxPixel: 4,
        total : 60,
        on: '#firefly'
    });
    </script>
    
    <script type="text/javascript" src="js/firefly2.js"></script>
    <script>
      $.firefly2({
        color: '#595b60',
        minPixel: 1,
        maxPixel: 4,
        total : 20,
        on: '#firefly'
    });
    </script>

<script src='js/TweenLite.min.js'></script>
<script src='js/EasePack.min.js'></script>
<script src='js/stellar.js'></script>


<!-- Menu Modal Box -->
        <script>
			// Get the modal
			var modalmenu = document.getElementById('menu-modal');		
			// Get the button that opens the modal
			var btnmenu = document.getElementsByClassName("open-menu")[0];	
			// Get the <span> element that closes the modal
			var spanmenu = document.getElementsByClassName("close-menu")[0];
			var spanmenu2 = document.getElementsByClassName("close-menu")[1];	
			var spanmenu3 = document.getElementsByClassName("close-menu")[2];						
			// When the user clicks the button, open the modal 
			btnmenu.onclick = function() {
			  modalmenu.style.display = "block";
			}	
			// When the user clicks on <span> (x), close the modal
			spanmenu.onclick = function() {
			  modalmenu.style.display = "none";
			}
			spanmenu2.onclick = function() {
			  modalmenu.style.display = "none";
			}
			spanmenu3.onclick = function() {
			  modalmenu.style.display = "none";
			}																		
		</script>

<script>
    //translation work
    function changeLang(lang){
        var url = window.location.href.split('?')[0];
        // url += "?lang="+lang;
        // window.location.replace(url);
        post(url, {lang: lang},'get');
    }

    function post(path, params, method='post') {

        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        const form = document.createElement('form');
        form.method = method;
        form.action = path;

        for (const key in params) {
            if (params.hasOwnProperty(key)) {
                const hiddenField = document.createElement('input');
                hiddenField.type = 'hidden';
                hiddenField.name = key;
                hiddenField.value = params[key];

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();
    }
</script>