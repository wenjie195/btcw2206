<?php
//Start the session
session_start();

//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
}
else
{
    $uid = $_SESSION['uid'];
}

require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'generalFunction.php';
require_once 'utilities/calculationFunction.php';
require_once 'utilities/databaseFunction.php';
// $conn = connDB();
// $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
//     $isNotice = editProfile($userRows);
// }

$editProfile = null;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $editProfile = editUserProfile($_SESSION['uid']);
}
else
{
    $editProfile = getProfileData($_SESSION['uid']);
   // echo $editProfile->getCanSendNewsletter();
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/editProfile.php" />
        <meta property="og:title" content="<?= _editProfile_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

        <title><?= _editProfile_title ?></title>
        <link rel="canonical" href="https://btcworg.com/editProfile.php" />
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();
      if($editProfile->getEditProfileType() == 1)
      {
          putNotice(_editProfile_attention,$editProfile->getRegisterErrorMessage());
      }
      echo getMainJsErrorMsgArray();
  ?>
  <div id="firefly" class="firefly-class min-height">  
      <div class="width100 same-padding more-separate-margin-top edit-div">
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700"><?= _editProfile_edit_details ?></b></h4>
            <form class="register-form"  method="POST" onsubmit="return validateEditProfileForm(errorMsgArrayTranslationForMainJs);">
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _editProfile_username ?></b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_1" id="field_1" placeholder="<?= _editProfile_username ?>" value="<?php echo $editProfile->getUsername();?>"></h4></td>
                  </tr>
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _editProfile_email ?></b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" placeholder="<?= _editProfile_email ?>" value="<?php echo $editProfile->getEmail();?>"></h4></td>
                  </tr>
                    <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _editProfile_ic ?></b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_3" id="field_3" placeholder="<?= _editProfile_ic ?>" value="<?php if($editProfile->getIcNo() != null ){echo $editProfile->getIcNo();}?>"></h4></td>
                  </tr>
    <!--              <tr>-->
    <!--                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">密码</b></h4></td>-->
    <!--                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>-->
    <!--                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="password" class="inputa clean2 inputb"  name="field_3"  id="field_3" placeholder="密码"></h4></td>-->
    <!--              </tr>-->
    <!--              <tr>-->
    <!--                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">再次输入新密码</b></h4></td>-->
    <!--                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>-->
    <!--                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="password" class="inputa clean2 inputb"  name="field_4" id="field_4" placeholder="确认密码"></h4></td>-->
    <!--              </tr>-->
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _general_country ?></b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">
                        <select class="inputa clean2 inputb" onchange="changePhoneNoEditProfile(this.value);" name="field_5" id="field_5">
                                <option disabled><?= _general_country ?></option>
                                <?php
                                    loginGetCityEditProfile($editProfile->getCountryId());
                                ?>
                        </select></h4></td>
                  </tr>               
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _register_phone_number ?></b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top">
                        <h4 class="btcw-h4 edit2-h4">
                            <input type="text" disabled class="country-code-input clean2 inputb-country" name="field_6_CountryCode" id="field_6_CountryCode" placeholder="+" value="<?php
                             editProfileGetPhoneCode($editProfile->getCountryId())
                            ?>">
                            <input type="text" class="mobile-input clean2 inputb-mobile" name="field_6" placeholder="<?= _register_phone_number ?>" id="field_6" min="0" value="<?php
                              echo $editProfile->getPhone();
                            ?>" style="color: white;">
                       </h4>
                    </td>
                  </tr>                 
            
                </table>
                <div class="clear"></div>
                <div class="checkboxb">
                  <?php 
                    getCheckBoxDefaultCheck($editProfile->getReceiveNotification());
                  ?>
                    <label class="form-check-label white-text" for="defaultCheck2">
                        <?= _editProfile_news ?>
                    </label>
               </div>
                <input type="hidden" name="country" id="country" value="<?php echo $editProfile->getCountryID();?>">
               <button class="register-button2 clean orange-hover inputb-button ow-button-en" name="editProfileButton" id="editProfileButton" ><?= _editProfile_confirm_edit ?></button>
               <a href="editPassword.php"><div class="register-button2 clean orange-hover inputb-button edit-password-div ow-button-en"><?= _editPassword_edit_details ?></div></a>
            </form>
     </div> 
      <div class="clear"></div>
    
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <script type="text/javascript" src="js/validid.js?version=1.0.17"></script>
  <?php require_once 'mainFooter.php';?>

  </body>
</html>