<!-- Footer -->
<footer class="width100 same-padding indexBlackBackground footer-class">

  <!-- Copyright -->
    <div class="footer1 white-text">
        © 2019 BTCW 版权所有
    </div>
    <div class="footer2">
        <a href=""><img src="img/sherry/facebook.png" class="footer-social opacity-hover"></a>
        <a href=""><img src="img/sherry/twitter.png" class="footer-social opacity-hover"></a>
    </div>
    <div class="footer3">
        <a class="indexSignUp opacity-hover" href="indexLogin.php">登录</a>
        <a class="indexSignUp opacity-hover" href="indexRegister.php">注册</a>
    </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
<!-- <script 
  src="https://code.jquery.com/jquery-3.3.1.min.js" 
  integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
  crossorigin="anonymous">
  </script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase.js"></script>

<!-- Firebase App is always required and must be first -->
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-app.js"></script>

<!-- Add additional services that you want to use -->
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.2/firebase-functions.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyC2rLJgT1EN-vqlO2M5kVdIKwprFy-sSxw",
    authDomain: "btwc-platform.firebaseapp.com",
    databaseURL: "https://btwc-platform.firebaseio.com",
    projectId: "btwc-platform",
    storageBucket: "btwc-platform.appspot.com",
    messagingSenderId: "1024400218886"
  };
  firebase.initializeApp(config);
</script>
<!-- Main Script --> 
<script type="text/javascript" src="js/main.js?version=1.0.0"></script>

