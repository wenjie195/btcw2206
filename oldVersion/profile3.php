<?php
//Start the session
session_start();

//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
    exit();
}else{
    $uid = $_SESSION['uid'];
}

require_once 'generalFunction.php';
require_once 'utilities/calculationFunction.php';
require_once 'utilities/databaseFunction.php';

$conn = connDB();

$errorMsg = null;
$successMsg = null;
if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(isset($_POST['invest-form'])){
        if(!isPostVariableEmpty("invest-btc-amount") && !isPostVariableEmpty("receiptHashKey")){
            $amount = $_POST["invest-btc-amount"];
            $receiptHashKey = $_POST["receiptHashKey"];

            $newTempInvestTableId = insertDynamicData($conn,"temp_invest_table",array("uid","amount","receiptHashKey"),array($uid,$amount,$receiptHashKey),"sds");
            if($newTempInvestTableId){
                $successMsg = _profile_donate_success.$amount." btc!";
            }else{
                $errorMsg = "对不起,未知错误发生了！！";
            }
        }else{
            $errorMsg = "请填写全部资料";
        }
    }
}

$userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");

//******************************************EXAMPLE START******************************************

//echo $userRows[0]->getDateCreated();
//echo $userRows[0]->getUsername();

//$newTableId = insertDynamicData($conn,"temp_invest_table",array("uid","amount"),array("whahaha id",123.2222),"sd");
//if($newTableId){
//    echo "newId: ".$newTableId;
//}else{
//    echo "error";
//}

////update based on id
//if(updateDynamicData($conn,"temp_invest_table"," WHERE id = ? ",array("uid","amount"),array("uid changed",555.22,2),"sdi")){
//    echo "updated";
//}else{
//    echo "update error";
//}

////update all
//if(updateDynamicData($conn,"temp_invest_table",null,array("uid","amount"),array("uid changed",555.22),"sd")){
//    echo "updated";
//}else{
//    echo "update error";
//}

////update with or
//if(updateDynamicData($conn,"temp_invest_table"," WHERE uid = ? OR uid = ? OR amount = ?",array("uid","amount"),array("yay",888.8888888,"333","555",1.0),"sdssd")){
//    echo "updated";
//}else{
//    echo "update error";
//}

//******************************************EXAMPLE END********************************************

////todo INVEST BTC EXAMPLE THIS 1 IMPORTANT, DONT DELETE START
//$errorMsg = null;
//$successMsg = null;
//$amount = 0.025;
//$planId = 1;
//$errorMsg = investCapitalToPlan($conn,$amount,$planId,null);
//if(!isset($errorMsg)){
//    $amountInvestedString = removeUselessZero($amount) . " BTC!";
//    $successMsg = _profile_donate_success."$amountInvestedString";
//}
////todo INVEST BTC EXAMPLE THIS 1 IMPORTANT, DONT DELETE END

$totalReferredCount = 0;
if($userRows && count($userRows) > 0){
    $user = $userRows[0];
    $username = $user->getUsername();
    $phoneNo = getPhoneNo($conn,$user);
    $email = $user->getEmail();
    $userPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,1),"si");
    $totalReturnedBtc = 0;
    $totalCapitalBtc = 0;
    $totalFreeBtc = 0;
    $allCombinedTotalBtc = 0;
    $currentUtcTimeInMs =  getCurrentUtcTimeInMs();
    if($userPlanRows){
        $totalReturnedBtc = $userPlanRows[0]->getReturnedBtc();
        $totalCapitalBtc = $userPlanRows[0]->getCapitalBtc();
        $totalFreeBtc = $userPlanRows[0]->getFreeBtc();
        $allCombinedTotalBtc = $totalReturnedBtc + $totalCapitalBtc + $totalFreeBtc;
    }
    $totalReferredCount = getCount($conn,"referral_history","referrer_id"," WHERE referrer_id = ? ", array("referrer_id"), array($user->getUid()),"s");

    $planAResult = displayTransactionHistory($conn,$uid,1,5);
    $conn->close();
}else{
    $conn->close();
    header('Location:index.php');
    exit();
}

function getPhoneNo($conn,$user){
    $countryRows = getCountries($conn," WHERE id = ? ",array("id"),array($user->getCountryId()),"i");
    $countryCode = "";
    if($countryRows && count($countryRows) > 0){
        $countryCode = "+".$countryRows[0]->getPhonecode()." ";
    }
    return $countryCode . $user->getPhoneNo();
}

?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/profile.php" />
        <meta property="og:title" content="比特基金 - 个人中心" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

        <title>比特基金 - 个人中心</title>
        <link rel="canonical" href="https://btcworg.com/profile.php" />
  </head>
  <body>
  <?php require 'mainNavbar.php';
    generateInputModalForInvest("invest");
    generateInputModalForInvest("withdraw");
  ?>

    <div id="firefly" class="firefly-class min-height">
    	<div class="width100 same-padding menu-content-distance">
        	<div class="first-profile-div">
            	<p class="white-text profile-data-p hover-container">
                	<b class="weight-700">你好，<?php echo $username; ?></b> 
<!--                	<a href="editProfile.php" ><img src="img/sherry/edit-white.png" class="edit-icon3 hover-1" alt="修改资料" title="修改资料"></a>-->
<!--                	<a href="editProfile.php" ><img src="img/sherry/edit-yellow.png" class="edit-icon3 hover-2" alt="修改资料" title="修改资料"></a>-->
                </p>
                <p class="white-text profile-data-p"><b class="weight-700">联络号码：</b> <?php echo $phoneNo; ?></p>
                <p class="white-text profile-data-p"><b class="weight-700">邮箱：</b> <?php echo $email; ?></p>
                <p class="white-text profile-data-p"><b class="weight-700">社区等级：</b> 
                	<img src="img/sherry/star1.png" class="star-img" alt="社区等级" title="社区等级">
                    <img src="img/sherry/star0.png" class="star-img" alt="社区等级" title="社区等级">
                    <img src="img/sherry/star0.png" class="star-img" alt="社区等级" title="社区等级">
                    <img src="img/sherry/star0.png" class="star-img" alt="社区等级" title="社区等级">
                    <img src="img/sherry/star0.png" class="star-img" alt="社区等级" title="社区等级">
                </p>
            </div>	
            <div class="second-big-div">
                <div class="second-profile-div">
                    <p class="white-text profile-data-p text-center time-p-text weight-700">华盛顿,美国(GMT-4)</p>
                    <div class="clock neon-text">
                      <div class="hours">
                        <div class="first">
                          <div class="number">0</div>
                        </div>
                        <div class="second">
                          <div class="number">0</div>
                        </div>
                      </div>
                      <div class="tick">:</div>
                      <div class="minutes">
                        <div class="first">
                          <div class="number">0</div>
                        </div>
                        <div class="second">
                          <div class="number">0</div>
                        </div>
                      </div>
                      <div class="tick">:</div>
                      <div class="seconds">
                        <div class="first">
                          <div class="number">0</div>
                        </div>
                        <div class="second infinite">
                          <div class="number">0</div>
                        </div>
                      </div>
                </div>
                </div>  
                <div class="third-profile-div">
                    <div class="green-dot">&nbsp;</div><div class="next-to-green"><p class="white-text online-number profile-data-p"><span id="user-online-count-span"></span><span class="grey-span">/20000</span> 会员在线</p></div>
                </div> 
           </div>                  
        </div> 
        <div class="clear"></div> 
        <div class="width100 same-padding two-item-distance">
            <div class="plan-a-box text-center white-text">
                <h4 class="profile-h4">计划A <a class="learn-more orange-text-hover plan-a">(了解更多)</a></h4>
                <div class="dark-box graph-bg first-dark-box">
                    <img src="img/sherry/bitcoin-white.png" class="box-img" alt="比特币" title="比特币">
                    <p class="box-title">捐赠(btc)</p>
                    <p class="box-big-font"><?php echo removeUselessZero($totalCapitalBtc); ?></p>
                    <p class="box-title separate-distance">定期资产（btc）</p>
<!--                    <p class="box-big-font">3(+0.1)</p>-->
                    <p class="box-big-font"><?php echo removeUselessZero($totalCapitalBtc) * 3; ?></p>
                </div>
                <div class="dark-box second-dark empty-dark first-dark-box">
                    <img src="img/sherry/influence-white.png" class="box-img" alt="比特币收益" title="比特币收益">
                    <p class="box-title">释放收益（btc）</p>
<!--                    <p class="box-big-font">0.02</p>-->
                    <p class="box-big-font">-</p>
                    <p class="box-title separate-distance">总释放收益（btc）</p>
<!--                    <p class="box-big-font">0.12</p>-->
                    <p class="box-big-font">-</p>
                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now">捐赠</button>
                    <!--<button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn">提取收益</button>-->
                </div>
                <div class="dark-box tree-bg">
                    <img src="img/sherry/low-risk-white.png" class="box-img" alt="总推荐收益" title="总推荐收益">
                    <p class="box-title">总推荐收益(btc)</p>
<!--                    <p class="box-big-font">5</p>-->
                    <p class="box-big-font">-</p>
                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now">捐赠</button>
                    <!--<button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn">提取收益</button>-->                
                   
                </div>
                <div class="dark-box star-bg second-dark">
                    <img src="img/sherry/bitcoin-stack1-white.png" class="box-img" alt="社区值" title="社区值">
                    <p class="box-title">社区值(美金)</p>
<!--                    <p class="box-big-font">1,000</p>-->
                    <p class="box-big-font">-</p>
                </div>
            </div>         
            <div class="plan-a-box plan-b-box text-center white-text">
                <h4 class="profile-h4">计划B <a class="learn-more orange-text-hover plan-b">(了解更多)</a></h4>
                <div class="dark-box graph-bg first-dark-box b-box">
                    <img src="img/sherry/bitcoin-white.png" class="box-img" alt="比特币" title="比特币">
                    <p class="box-title">捐赠(btc)</p>
<!--                    <p class="box-big-font">0.5</p>-->
                    <p class="box-big-font">-</p>
                    <p class="box-title separate-distance">定期资产（btc）</p>
<!--                    <p class="box-big-font">10(0.5+1.2)</p>-->
                    <p class="box-big-font">-</p>
                </div>
                <div class="dark-box second-dark empty-dark small-dark">
                    <img src="img/sherry/influence-white.png" class="box-img" alt="比特币收益" title="比特币收益">
                    <p class="box-title">加速释放收益（btc）</p>
<!--                    <p class="box-big-font">1</p>-->
                    <p class="box-big-font">-</p>

                    <!--<button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now">捐赠</button>-->
                    <!--<button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn">提取收益</button>-->
                </div>
                <div class="dark-box second-dark small-dark small-dark2">
                    <img src="img/sherry/bitcoin-stack1-white.png" class="box-img" alt="社区值" title="社区值">
                    <p class="box-title">社区值(美金)</p>
<!--                    <p class="box-big-font margin-btm-0">1,000</p>-->
                    <p class="box-big-font margin-btm-0">-</p>
                    <p class="box-big-font">
                        <img src="img/sherry/star1.png" class="star-img" alt="社区等级奖励" title="社区等级奖励">
                        <img src="img/sherry/star0.png" class="star-img" alt="社区等级奖励" title="社区等级奖励">
                        <img src="img/sherry/star0.png" class="star-img" alt="社区等级奖励" title="社区等级奖励">
                        <img src="img/sherry/star0.png" class="star-img" alt="社区等级奖励" title="社区等级奖励">
                        <img src="img/sherry/star0.png" class="star-img" alt="社区等级奖励" title="社区等级奖励">                    
                    </p>
                </div>               
                <div class="dark-box tree-bg">
                    <img src="img/sherry/low-risk-white.png" class="box-img" alt="总推荐收益" title="总推荐收益">
                    <p class="box-title">总推荐收益(btc)</p>
<!--                    <p class="box-big-font">200</p>-->
                    <p class="box-big-font">-</p>
                    <!--<button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now">捐赠</button>-->
                    <!--<button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn">提取收益</button>-->                
                   
                </div>
                <div class="dark-box second-dark star-bg">
                    <img src="img/sherry/triple-white.png" class="box-img" alt="量化分红" title="量化分红">
                    <p class="box-title">量化分红(btc)</p>
<!--                    <p class="box-big-font">20</p>-->
                    <p class="box-big-font">-</p>
                    <!--<button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now">捐赠</button>--》
                    <!--<button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn">提取收益</button>-->                
                   
                </div>
            </div>              
        </div>
	</div>

  <?php require 'mainFooter.php';?>
     
  <!--- Before Edit-->
  
<!--  old version please refer to profile2.php-->
	
        <!--- Modal --->
        <div id="myModal2" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
            <span class="close2 ow-close2">&times;</span>
            
            
                <form class="register-form width100 same-padding"  method="POST">
                    <h4 class="btcw-h4 form-h4 text-center qr-h4-a"><b class="weight-700">请扫描以下二维码捐赠计划A</b></h4>  
                    <p class="qr-img qr-img2"><img src="img/sherry/qr2.jpg" class="width100" alt="捐赠" title="捐赠"></p>
                    <p class="qr-p5">31neH71k4BHwquGZPznGTa1QV2QpANvoby</p>
                    	<!--<div class="input-container1">
                            <select class="inputa clean2"  name="field_1">
                    			<option disabled selected>选择要捐赠的计划</option>
                    			<option>计划A</option>
                    			<option>计划B</option>                                
                			</select>
							<img src="img/sherry/plan.png" class="input-icon">
                        </div>-->
                        
                        <div class="input-container2">
                        	<input type="number" class="inputa clean2" required  name="invest-btc-amount" placeholder="输入捐赠数量(btc)">
                        	<img src="img/sherry/invest-amount.png" class="input-icon">
						</div>
                        
                        <div class="input-container2">
                        	<input type="text" class="inputa clean2" required  name="receiptHashKey" placeholder="转账收据">
                        	<img src="img/sherry/verify2.png" class="input-icon">
						</div>                        
                        <!--<div class="input-container3">	
                            <select class="inputa clean2"   name="field_3">
                    			<option disabled selected>捐赠备注</option>
                    			<option>想增加收益</option>                                
                    			<option>想加入此计划</option>
                    			<option>想提取收益</option>                                 
                    			<option>其他</option>
                			</select>
                        	<img src="img/sherry/remark.png" class="input-icon">
						</div>
                        <textarea class="textarea-reason" name="" placeholder="如果捐赠备注选其他，需填写"></textarea-->
						<div class="width100 overflow">
                        	<div class="fill-up-space1 modal-fill-space-1"></div>
                        	<button class="register-button2 clean orange-hover invest-btn" type="submit" name="invest-form" id="invest-form" >完成</button>
                            <div class="fill-up-space1 modal-fill-space-1"></div>
                         </div>
                         <div class="clear"></div>                           
               </form>            
          </div>
        </div>
     
        <!--- Withdrawal Modal --->
        <div id="myModal3" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
            <span class="close3 ow-close2 close-style">&times;</span>
           
            
                <form class="register-form width100 same-padding"  method="POST">
                	    <h4 class="btcw-h4 form-h4 ow-form-h4"><b class="weight-700">提取收益</b></h4>  
            <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table withdraw-table">
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">可提取额度</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">300</h4></td>
              </tr>
             
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">输入提取总额</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">
                	<input type="" class="inputa clean2 inputb"  name="field_1" placeholder="输入提取总额">
                    </h4>
                </td>
              </tr>              
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">手续费</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"></h4>
                </td>
              </tr>
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">最终可提取总额(btc)</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"> </h4>
                </td>
              </tr>              
              
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">TAC</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">
                	<input type="" class="inputa clean2 inputb next-to-btn-input"  name="field_2" placeholder="输入TAC">
                    <button class="clean tac-button yellow-box tac-button1">获取TAC</button>
                    </h4>
                </td>
              </tr>                                         
              
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">比特币转账链接</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4">
                	<input type="" class="inputa clean2 inputb"  name="field_3" placeholder="输入比特币转账链接">
                    </h4>
                    <p class="register-notice mtop-0">请输入正确比特币转账链接，若有任何失误，本平台将不会做任何补偿。</p>
                </td>
              </tr>                 
              
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">或上载比特币转账二维码</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><button class="clean tac-button yellow-box">上载二维码</button>
                	
                </td>
              </tr> 
              <tr>
                <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">已上载比特币转账二维码预览</b></h4></td>
                <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                <td class="third-td2" width="58%" valign="top"><img src="img/sherry/btcw-qr.png" class="width100">
                	
                </td>
              </tr>              
              
                      
            </table>

						<div class="width100 overflow">
                        	<div class="fill-up-space1 modal-fill-space-1"></div>
                        	<button class="register-button2 clean orange-hover invest-btn" name="loginButton" id="loginButton" >确认提取</button>
                            <div class="fill-up-space1 modal-fill-space-1"></div>
                         </div>                           
           </form>            
          </div>
        </div>
        
        <!--- Plan A Modal --->
        <div id="myModal4" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width2 dark-bg">
            <span class="close4 ow-close4 close-style">&times;</span>
            <h1 class="btcw-h1 text-center plan-a-h1">计划A</h1>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4">第一次捐赠金额</h4>
            <div class="width100 same-padding">
            	<div class="width-two-column1 float-left">
                	<img src="img/sherry/bitcoin-stack1.png" class="two-column-img" alt="最低金额" title="最低金额">
                    <p class="two-column-p weight-700">最低金额</p>
                    <p class="two-column-p">0.025 btc</p>
                </div>
            	<div class="width-two-column2 float-left">
                	<img src="img/sherry/bitcoin-stack2.png" class="two-column-img" alt="最高金额" title="最高金额">
                    <p class="two-column-p weight-700">最高金额</p>
                    <p class="two-column-p">0.2 btc</p>
                </div>                
            </div>
            <div class="clear"></div>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4">一变三</h4>
            <div class="width100 same-padding">
                	<img src="img/sherry/one-to-three.png" class="width-special" alt="一变三" title="一变三">
              
            </div>
            <div class="clear"></div>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4">福利与条规</h4>
            <div class="width100 same-padding four-column-container">
            	<div class="four-column-div">
                	<img src="img/sherry/bitcoin.png" class="four-column-img" alt="加入平台，马上获得 0.05 BTC" title="加入平台，马上获得 0.05 BTC">
                    <p class="two-column-p weight-700">加入平台，马上获得 0.05 BTC</p>
                </div>
            	<div class="four-column-div middle-four second-four">
                	<img src="img/sherry/five-people.png" class="four-column-img" alt="直推五（5）个人，马上获得 0.05 BTC" title="直推五（5）个人，马上获得 0.05 BTC">
                    <p class="two-column-p weight-700">直推五（5）个人，马上获得 0.05 BTC</p>
                </div>   
            	<div class="four-column-div middle-four">
                	<img src="img/sherry/profit.png" class="four-column-img" alt="捐赠0.2BTC， 平台将会送0.4BTC。" title="捐赠0.2BTC， 平台将会送0.4BTC。">
                    <p class="two-column-p weight-700">捐赠0.2BTC， 平台将会送0.4BTC。</p>
                </div>
            	<div class="four-column-div second-four">
                	<img src="img/sherry/invest.png" class="four-column-img" alt="享用股利" title="享用股利">
                    <p class="two-column-p weight-700">若想享用股利，捐赠者必须捐赠0.15BTC或者推荐他人捐赠0.15BTC</p>
                </div>                            
            </div>
            <div class="clear"></div>            
            <div class="width100 same-padding four-column-container">
            	<div class="four-column-div">
                	<img src="img/sherry/transaction-fee.png" class="four-column-img" alt="每天奖励0.2%的收益" title="每天奖励0.2%的收益">
                    <p class="two-column-p weight-700">平台将会每天奖励0.2%的收益给捐赠者所拥有的BTC</p>
                </div>
            	<div class="four-column-div middle-four second-four">
                	<img src="img/sherry/time.png" class="four-column-img" alt="每十（10）天，捐赠者可以领取这十（10）天的收益。" title="每十（10）天，捐赠者可以领取这十（10）天的收益。">
                    <p class="two-column-p weight-700">每十（10）天，捐赠者可以领取这十（10）天的收益。</p>
                </div>   
            	<div class="four-column-div middle-four">
                	<img src="img/sherry/low-risk.png" class="four-column-img" alt="捐赠0.2BTC， 平台将会送0.4BTC。" title="捐赠0.2BTC， 平台将会送0.4BTC。">
                    <p class="two-column-p weight-700">捐赠者可享有高达十代的佣金。</p>
                </div>
            	<div class="four-column-div second-four">
                	<img src="img/sherry/profit.png" class="four-column-img" alt="可以撤回BTC" title="可以撤回BTC">
                    <p class="two-column-p weight-700">若想拿回本金的条件：在首三（3）个月，捐赠者可以撤回BTC，但是平台会收18%行政收费。如果过了三（3）个月，捐赠者不可以撤回本金。</p>
                </div>                            
            </div>        
            <div class="clear"></div>    
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4">社区比例收益</h4>
            <div class="width100 same-padding two-div-container overflow-hidden">
				<div class="width50-left-div">
                	<table class="simple-table">
                    	<tr>
                        	<td class="simple1">1. </td>
                            <td class="simple2">第一代利润为50％。</td>
                        </tr>
                    	<tr>
                        	<td class="simple1">2. </td>
                            <td class="simple2">第二代利润为30%（1千美金）。</td>
                        </tr> 
                    	<tr>
                        	<td class="simple1">3. </td>
                            <td class="simple2">第三代利润为20%（社区总捐赠 4千美金）。</td>
                        </tr>                        
                    	<tr>
                        	<td class="simple1">4. </td>
                            <td class="simple2">第四代利润为10%（社区总捐赠2万美金）。</td>
                        </tr>
                    	<tr>
                        	<td class="simple1">5. </td>
                            <td class="simple2">第五代利润为10%（社区总捐赠5万美金）。</td>
                        </tr> 
                    	<tr>
                        	<td class="simple1">6. </td>
                            <td class="simple2">第六代利润为10%（社区总捐赠20万美金）。</td>
                        </tr>                         
                    	<tr>
                        	<td class="simple1">7. </td>
                            <td class="simple2">第七代利润为10%（社区总捐赠50万美金）。</td>
                        </tr>                        
                    	<tr>
                        	<td class="simple1">8. </td>
                            <td class="simple2">第八代利润为10%（社区总捐赠1亿美金）。</td>
                        </tr>
                    	<tr>
                        	<td class="simple1">9. </td>
                            <td class="simple2">第九代利润为10%（社区总捐赠1.5亿美金）。</td>
                        </tr> 
                    	<tr>
                        	<td class="simple1">10. </td>
                            <td class="simple2">第十代利润为10%（社区总捐赠3亿美金）。</td>
                        </tr>                                           
                    </table>
                </div>
				<div class="width50-right-div">
                	<img src="img/indexIntro.png" class="width100" alt="BTCW 比特基金" title="BTCW 比特基金">
                </div>                            
            </div>            
  			<div class="clear"></div>
            <div class="width100 same-padding overflow-hidden">
                <div class="fill-up-space1 ow-fill-up"></div>
                <div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center invest-now">马上捐赠</div>
                <div class="fill-up-space1 ow-fill-up"></div>
            </div>            
            <div class="width100 same-padding copy-button-div">
              <div class="fill-up-space1 ow-height"></div>
              <button class="yellow-line-box yellow-box-width yellow-box-hover white-text text-center clean copy-yellow-line plan-b">了解计划B</button>
            <div class="fill-up-space1 ow-height"></div>
           </div>                         
         </div>
       </div>        
 
        
        <!--- Plan B Modal --->
        <div id="myModal5" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width2 dark-bg">
            <span class="close5 ow-close4 close-style">&times;</span>
            <h1 class="btcw-h1 text-center plan-a-h1">计划B</h1>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4">捐赠金额</h4>
            <div class="width100 same-padding">
            	<div class="width-two-column1 float-left">
                	<img src="img/sherry/bitcoin-stack1.png" class="two-column-img" alt="最低金额" title="最低金额">
                    <p class="two-column-p weight-700">最低金额</p>
                    <p class="two-column-p">0.025 btc</p>
                </div>
            	<div class="width-two-column2 float-left">
                	<img src="img/sherry/bitcoin-stack2.png" class="two-column-img" alt="最高金额" title="最高金额">
                    <p class="two-column-p weight-700">最高金额</p>
                    <p class="two-column-p">50 btc</p>
                </div>                
            </div>
            <div class="clear"></div>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4">一变八</h4>
            <div class="width100 same-padding">
                	<img src="img/sherry/one-to-eight.png" class="width-special" alt="一变八" title="一变八">
              
            </div>            
            <div class="clear"></div>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4">例子</h4>
            <div class="width100 same-padding two-div-container overflow-hidden">
				<table class="simple-table">
                    	<tr>
                        	<td class="simple1">1. </td>
                            <td class="simple2">捐赠1,000元，捐赠将会得到八（8）倍的回报率，也就是8,000元。</td>
                        </tr>    
                     	<tr>
                        	<td class="simple1">2. </td>
                            <td class="simple2">但是这8,000元必须等到一（1）年后才可以提取。</td>
                        </tr>                       
                     	<tr>
                        	<td class="simple1">3. </td>
                            <td class="simple2">在这一（1）年，平台有做量化分红给予捐赠者。根据过往的业绩，量化分红平均每月可达到百分之五到五十。</td>
                        </tr>  
                    	<tr>
                        	<td class="simple1">4. </td>
                            <td class="simple2">捐赠者可以选择加速释放自己的回报。比如捐赠者推荐甲先生捐赠10,000元在Plan B，捐赠者马上可以享有甲先生所捐赠的10%（1,000元）。不过这1,000元将会从他10,000元的回报所领取。</td>
                        </tr>    
                     	<tr>
                        	<td class="simple1">5. </td>
                            <td class="simple2">除此以外，捐赠者还有权享有推荐佣金。推荐佣金阶级为：8%，5% ，3%，2% 和 1%。</td>
                        </tr>                       
                     	<tr>
                        	<td class="simple1">6. </td>
                            <td class="simple2">因此，甲先生捐赠 1,000元，捐赠者会领取 1,080元 [加速释放所领取的1,000 和推荐佣金 （1,000元 * 8%)]  </td>
                        </tr>  
                     	<tr>
                        	<td class="simple1">7. </td>
                            <td class="simple2">平台将会以BTC单位来计算。</td>
                        </tr> 
                      	<tr>
                        	<td class="simple1">8. </td>
                            <td class="simple2">推荐社区可以获取额外收益。</td>
                        </tr>                                              
                </table>                                                               	
            </div>
            <div class="clear"></div>   

           
  			<div class="clear"></div>
            <!--<div class="width100 same-padding overflow-hidden">
                <div class="fill-up-space1 ow-fill-up"></div>
                <div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center invest-now">马上捐赠</div>
                <div class="fill-up-space1 ow-fill-up"></div>
            </div>-->          
            <div class="width100 same-padding copy-button-div">
              <div class="fill-up-space1 ow-height"></div>
              <button class="yellow-line-box yellow-box-width yellow-box-hover white-text text-center clean copy-yellow-line plan-a">了解计划A</button>
            <div class="fill-up-space1 ow-height"></div>
           </div>                         
         </div>
       </div>         
 

 <!---  End of Sherry coded part-->
        
  <!-- ****************************************************MODALS***********************************************************-->
<!--  <div class="modal fade" id="withdrawModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--      <div class="modal-dialog" role="document">-->
<!--          <div class="modal-content">-->
<!--              <div class="modal-header">-->
<!--                  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>-->
<!--                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--                      <span aria-hidden="true">&times;</span>-->
<!--                  </button>-->
<!--              </div>-->
<!--              <div class="modal-body">-->
<!--                  ...-->
<!--              </div>-->
<!--              <div class="modal-footer">-->
<!--                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
<!--                  <button type="button" class="btn btn-primary">Save changes</button>-->
<!--              </div>-->
<!--          </div>-->
<!--      </div>-->
<!--  </div>-->
   <div id="myModal10" class="modal2 ow-modal2 ow-modal10">
 
          <div class="modal-content2 modal-content-white ow-modal-width ow-modal10a">
            <h4 class="btcw-h4 text-center ow10-h4">链接已复制</h4>
          </div>
   </div>

<!--	<div class="width100 element-div extra-padding-bottom profile-divider">-->
<!--        	<img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">-->
<!--    </div>    -->
	
        <!--- QR Modal --->
        <div id="myModal16" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
            <span class="close16 ow-close2 close-style">&times;</span>
            <h4 class="btcw-h4 form-h4 text-center qr-h4-a"><b class="weight-700">请扫描以下二维码</b></h4>  
            <p class="qr-img qr-img2"><img src="img/sherry/qr2.jpg" class="width100" alt="捐赠" title="捐赠"></p>
            <p class="qr-p5">31neH71k4BHwquGZPznGTa1QV2QpANvoby</p>
              
            <p class="text-center"><button class="register-button2 clean orange-hover complete-btn button-y-width close16" >完成</button></p>
                           
          </div>
        </div>

  <!-- ****************************************************MODALS***********************************************************-->
  <script>
      function randomFunc(type) {
          // Get the modal
          var modal = document.getElementById(type + '-modal');

          // Get the <span> element that closes the modal
          var span = document.getElementsByClassName('closeNoticeModal')[0];
          var span1 = document.getElementsByClassName('closeNoticeModal')[1];

          modal.style.display = 'block';

          // When the user clicks on <span> (x), close the modal
          span.onclick = function() {
              modal.style.display = 'none';
          };
          span1.onclick = function() {
              modal.style.display = 'none';
          };

          // When the user clicks anywhere outside of the modal, close it
          window.onclick = function(event) {
              if (event.target == modal) {
                  modal.style.display = 'none';
              }
          }
      }

  

      $("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#invest-now-referral-link').attr("href");
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
      });
  </script>
          <script>
			// Get the modal
			var modal2 = document.getElementById('myModal2');
			
			// Get the button that opens the modal
			var btn2 = document.getElementsByClassName("invest-now")[0];
			
			// Get the button that opens the modal
			var btn2a = document.getElementsByClassName("invest-now")[1];		
			
			// Get the button that opens the modal
			var btn2b = document.getElementsByClassName("invest-now")[2];				
			
			// Get the button that opens the modal
			var btn2c = document.getElementsByClassName("invest-now")[3];	
			
			// Get the button that opens the modal
			var btn2d = document.getElementsByClassName("invest-now")[4];				
			
			// Get the button that opens the modal
			var btn2e = document.getElementsByClassName("invest-now")[5];				
							
			// Get the <span> element that closes the modal
			var span2 = document.getElementsByClassName("close2")[0];


			// Get the modal
			var modal3 = document.getElementById('myModal3');
			
			// Get the button that opens the modal
			var btn3 = document.getElementsByClassName("withdrawal-btn")[0];
			
			// Get the button that opens the modal
			var btn3a = document.getElementsByClassName("withdrawal-btn")[1];	
			
			
			// Get the button that opens the modal
			var btn3b = document.getElementsByClassName("withdrawal-btn")[2];
			
			// Get the button that opens the modal
			var btn3d = document.getElementsByClassName("withdrawal-btn")[3];				
			
			// Get the button that opens the modal
			var btn3e = document.getElementsByClassName("withdrawal-btn")[4];						
			// Get the <span> element that closes the modal
			var span3 = document.getElementsByClassName("close3")[0];



			// Get the Plan A modal
			var modal4 = document.getElementById('myModal4');
			
			// Get the button that opens the modal
			var btn4 = document.getElementsByClassName("plan-a")[0];
			
			// Get the button that opens the modal
			var btn4a = document.getElementsByClassName("plan-a")[1];			
			
			// Get the <span> element that closes the modal
			var span4 = document.getElementsByClassName("close4")[0];



			// Get the modal
			var modal5 = document.getElementById('myModal5');
			
			// Get the button that opens the modal
			var btn5 = document.getElementsByClassName("plan-b")[0];
			// Get the button that opens the modal
			var btn5a = document.getElementsByClassName("plan-b")[1];		
			// Get the <span> element that closes the modal
			var span5 = document.getElementsByClassName("close5")[0];



			// Get the modal
			var modal16 = document.getElementById('myModal16');
			
			
			// Get the <span> element that closes the modal
			var span16 = document.getElementsByClassName("close16")[0];
			
			
			// Get the <span> element that closes the modal
			var span16a = document.getElementsByClassName("close16")[1];			
              <?php
				  if(isset($errorMsg) || isset($successMsg)){
                      promptAlertMsg($errorMsg,$successMsg);
				  }
              ?>
			
			// When the user clicks the button, open the modal
            if(btn2){
                btn2.onclick = function() {
                    modal2.style.display = "block";
                };
            }

            if(btn2a){
                btn2a.onclick = function() {
                    modal2.style.display = "block";
                };
            }

            if(btn2b){
                btn2b.onclick = function() {
                    modal4.style.display = "none";
                    modal2.style.display = "block";
                };
            }

            if(btn2c){
                btn2c.onclick = function() {
                    modal5.style.display = "none";
                    modal2.style.display = "block";
                };
            }

            if(btn2d){
                btn2d.onclick = function() {
                    modal4.style.display = "none";
                    modal2.style.display = "block";
                };
            }

            if(btn2e){
                btn2e.onclick = function() {
                    modal5.style.display = "none";
                    modal2.style.display = "block";
                };
            }

			// When the user clicks on <span> (x), close the modal
			if(span2){
                span2.onclick = function() {
                    modal2.style.display = "none";
                };
            }

			// When the user clicks the button, open the modal
			if(btn3){
                btn3.onclick = function() {
                    modal3.style.display = "block";
                };
            }

            if(btn3a){
                btn3a.onclick = function() {
                    modal3.style.display = "block";
                };
            }

			if(btn3b){
                btn3b.onclick = function() {
                    modal3.style.display = "block";
                };
            }

            if(btn3d){
                btn3d.onclick = function() {
                    modal3.style.display = "block";
                };
            }
			

            if(btn3e){
                btn3e.onclick = function() {
                    modal3.style.display = "block";
                };
            }			
			
			// When the user clicks on <span> (x), close the modal
			if(span3){
                span3.onclick = function() {
                    modal3.style.display = "none";
                };
            }

			// When the user clicks the button, open the modal
			if(btn4){
                btn4.onclick = function() {
                    modal4.style.display = "block";
                };
            }

			// When the user clicks the button, open the modal
			if(btn4a){
                btn4a.onclick = function() {
                    modal5.style.display = "none";
                    modal4.style.display = "block";
                };
            }

			// When the user clicks on <span> (x), close the modal
			if(span4){
                span4.onclick = function() {
                    modal4.style.display = "none";
                };
            }

			// When the user clicks the button, open the modal
            if(btn5){
                btn5.onclick = function() {
                    modal5.style.display = "block";
                };
            }

			// When the user clicks the button, open the modal
			if(btn5a){
                btn5a.onclick = function() {
                    modal4.style.display = "none";
                    modal5.style.display = "block";
                };
            }
			// When the user clicks on <span> (x), close the modal
			if(span5){
                span5.onclick = function() {
                    modal5.style.display = "none";
                };
            }
			// When the user clicks on <span> (x), close the modal
			if(span16){
                span16.onclick = function() {
                    modal16.style.display = "none";
                };
            }
			// When the user clicks on <span> (x), close the modal
			if(span16a){
                span16a.onclick = function() {
                    modal16.style.display = "none";
                };
            }			
			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal2) {
				modal2.style.display = "none";
			  }
			  if (event.target == modal3) {
				modal3.style.display = "none";
			  }
			  if (event.target == modal4) {
				modal4.style.display = "none";
			  }
			  if (event.target == modal5) {
				modal5.style.display = "none";
			  }
			  if (event.target == modal5) {
				modal16.style.display = "none";
			  }
			}
		</script>
  <!--      
         <script>
			// Get the modal
			var modal10 = document.getElementById('myModal10');
			
			// Get the button that opens the modal
			var btn10 = document.getElementById("copy-referral-link");
            
			// When the user clicks the button, open the modal
			btn10.onclick = function() {
			  modal10.style.display = "block";
                setTimeout(closeCopiedModalWithDelay, 1600);
			};


			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal10) {
				modal10.style.display = "none";
			  }
			};

            function closeCopiedModalWithDelay() {
                modal10.style.display = "none";
            }
		</script>  -->
  
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
            }
    
            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
            
            /*close when you click outside of the sideNav*/
            $('body').on('click touchstart', function(){
              if( parseInt( $('#mySidenav').css('width') ) > 0 ){
                closeNav();
              }
            });
           /*$('body').on('click', function(){
           if( parseInt( $('#mySidenav').css('width') ) > 0 ){
           closeNav();
           }
           });*/
	</script>
    
    <script>
var hoursContainer = document.querySelector('.hours')
var minutesContainer = document.querySelector('.minutes')
var secondsContainer = document.querySelector('.seconds')
var tickElements = Array.from(document.querySelectorAll('.tick'))

var last = new Date(0)
last.setUTCHours(-1)

var tickState = true

var currentTimeInMs = <?php echo $currentUtcTimeInMs; ?>;
function updateTime () {
  currentTimeInMs += 1000;
  var now = new Date(currentTimeInMs).toLocaleString("en-US", {timeZone: "America/New_York"});
  now = new Date(now);
  
  var lastHours = last.getHours().toString()
  var nowHours = now.getHours().toString()
  if (lastHours !== nowHours) {
    updateContainer(hoursContainer, nowHours)
  }
  
  var lastMinutes = last.getMinutes().toString()
  var nowMinutes = now.getMinutes().toString()
  if (lastMinutes !== nowMinutes) {
    updateContainer(minutesContainer, nowMinutes)
  }
  
  var lastSeconds = last.getSeconds().toString()
  var nowSeconds = now.getSeconds().toString()
  if (lastSeconds !== nowSeconds) {
    //tick()
    updateContainer(secondsContainer, nowSeconds)
  }
  
  last = now
}

function tick () {
  tickElements.forEach(t => t.classList.toggle('tick-hidden'))
}

function updateContainer (container, newTime) {
  var time = newTime.split('')
  
  if (time.length === 1) {
    time.unshift('0')
  }
  
  
  var first = container.firstElementChild
  if (first.lastElementChild.textContent !== time[0]) {
    updateNumber(first, time[0])
  }
  
  var last = container.lastElementChild
  if (last.lastElementChild.textContent !== time[1]) {
    updateNumber(last, time[1])
  }
}

function updateNumber (element, number) {
  //element.lastElementChild.textContent = number
  var second = element.lastElementChild.cloneNode(true)
  second.textContent = number
  
  element.appendChild(second)
  element.classList.add('move')

  setTimeout(function () {
    element.classList.remove('move')
  }, 990)
  setTimeout(function () {
    element.removeChild(element.firstElementChild)
  }, 990)
}

setInterval(updateTime, 1000);

//random number generator
var max = 4598;
var min = 2168;
$("#user-online-count-span").html(Math.floor(Math.random()*(max-min+1)+min));

</script>
  </body>
</html>>>>