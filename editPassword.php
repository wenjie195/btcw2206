<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
}
else
{
    $uid = $_SESSION['uid'];
}
require_once 'generalFunction.php';

$message = null;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $message = updateThisPassword($uid);
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/editPassword.php" />
        <meta property="og:title" content="<?= _editPassword_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

        <title><?= _editPassword_title ?></title>
        <link rel="canonical" href="https://btcworg.com/editPassword.php" />
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();
      if($message != null)
      {
        putNotice(_editPassword_error,$message);
      }

      echo getMainJsErrorMsgArray();
  ?>
  <div id="firefly" class="firefly-class min-height">  
      <div class="width100 same-padding more-separate-margin-top edit-div">
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700"><?= _editPassword_edit_details ?></b></h4>
            <form class="register-form"  method="POST" onsubmit="return validateEditPasswordForm(errorMsgArrayTranslationForMainJs);">
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _editPassword_current_password ?></b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><img src="img/sherry/eye.png" class="input-icon button-icon-eye clean button-icon-eye2" id="seePassword1"> <input type="password" class="inputa clean2 inputb password-input"  name="field_1" id="field_1" placeholder="<?= _editPassword_password ?>" value=""></h4></td>
                  </tr>
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _editPassword_new_password ?></b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><img src="img/sherry/eye.png" class="input-icon button-icon-eye clean button-icon-eye2" id="seePassword2"><input type="password" class="inputa clean2 inputb password-input"  name="field_2" id="field_2" placeholder="<?= _editPassword_new_password ?>" value=""></h4></td>
                  </tr>                  
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _editPassword_retype_password ?></b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><img src="img/sherry/eye.png" class="input-icon button-icon-eye clean button-icon-eye2" id="seePassword3"><input type="password" class="inputa clean2 inputb password-input"  name="field_3" id="field_3" placeholder="<?= _editPassword_retype_password ?>" value=""></h4></td>
                  </tr>            
                </table>
                <div class="clear"></div>

               <p class="middle-button-p"><button class="register-button2 clean orange-hover inputb-button ow-button-en2" name="editPasswordButton" id="editPasswordButton" ><?= _editPassword_confirm_edit ?></button></p>

               <div class="fill-space-200"></div><a href="editProfile.php"><div class="register-button2 clean orange-hover inputb-button edit-password-div float-left"><?= _editPassword_back ?></div></a></div>
               <div class="fill-space-200"></div>
            </form>
     <div class="clear"></div>
    
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
     

      
  <?php require_once 'mainFooter.php';?>
  <script>
        var img1 = document.getElementById('seePassword1');
        var img2 = document.getElementById('seePassword2');
        var img3 = document.getElementById('seePassword3');

        var inputPassword1 = document.getElementById('field_1');
        var inputPassword2 = document.getElementById('field_2');
        var inputPassword3 = document.getElementById('field_3');

        img1.onclick = function () 
        {
            if(inputPassword1.type == "password")
            {
                inputPassword1.type = "text";
            }
            else if(inputPassword1.type == "text")
            {
                inputPassword1.type = "password";
            }
        }
        img2.onclick = function () 
        {
            if(inputPassword2.type == "password")
            {
                inputPassword2.type = "text";
            }
            else if(inputPassword2.type == "text")
            {
                inputPassword2.type = "password";
            }
        }
        img3.onclick = function () 
        {
            if(inputPassword3.type == "password")
            {
                inputPassword3.type = "text";
            }
            else if(inputPassword3.type == "text")
            {
                inputPassword3.type = "password";
            }
        }
</script>
  </body>
</html>