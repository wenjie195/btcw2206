<?php
require_once 'utilities/checkSessionFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require 'generalFunction.php';
    $modalDesc = checkLogin();
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/indexLogin.php/" />
        <meta property="og:title" content="<?= _login_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Join us now!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Join us now!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, register, member, 会员登入,登录, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
       
        <title><?= _login_title ?></title>
        <link rel="canonical" href="https://btcworg.com/indexLogin.php/" />
  </head>
  <body>
     <?php
        generateSimpleModal();

         if($modalDesc)
         {
             putNotice(_editProfile_attention,$modalDesc);
         }
       ?>
    <!--<div class="header-div width100 same-padding absolute">
    	<div class="logo-div">
        	<a href="bitcoin.php" class="a-hover"><img src="img/sherry/logo-white.png" class="logo-img opacity-hover" alt="bitcoin" title="bitcoin"></a>
        </div>
        <div class="menu-right-div">
        	<a class="menu-a login-a a-hover white-text-a yellow-hover" href="indexLogin.php">登录</a>
            <a class="menu-a register-a a-hover white-text-a yellow-hover" href="indexRegister.php">注册</a>
        </div>
    </div> -->
    <div id="firefly" class="firefly-class min-height"> 
    	<?php require 'mainNavbar.php';?> 
    	<div class="width100 same-padding">
    		<div class="login-width text-center white-text">
         		
                <h1 class="register-title text-center"><?= _login_member_login ?></h1>
				<form class="register-form"  method="POST">  
                    	<div class="input-container1 ow-input-container1">
                			<input type="text" class="inputa clean2"  name="field_1" placeholder="<?= _editProfile_email ?>">
							<img src="img/sherry/email.png" class="input-icon ow-human-icon">
                        </div>
                        <div class="input-container5 ow-input-container5">
                            <!--Fizo here-->
                            <img src="img/sherry/eye.png" class="width100 input-icon button-icon-eye"  id="seePassword" onclick="seePassword();"> 
                        	<input type="password" class="inputa clean2 password-input"  name="field_2" placeholder="<?= _login_password ?>" id="passValue">
                        	<img src="img/sherry/password.png" class="input-icon ow-password-icon2" >
                           
						</div>
                        <!--<div class="checkbox-div">
                            <div class="checkbox1">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    记下登录资料
                                </label>    
                                
                            </div>
                       </div> -->
                       <button class="register-button2 clean orange-hover button-width" name="loginButton" id="loginButton" ><?= _login_login ?></button>              	                      
                </form>
            <p class="loginRegister"><?= _forgotPassword_not_yet ?><a class="loginRegisterLink orange-text-hover" href="indexRegister.php"><?= _forgotPassword_register_here ?></a></p>
            <p class="loginRegister edit-mtop"><a class="loginRegisterLink orange-text-hover" href="forgotPassword.php"><?= _indexcr_forgot_pw2 ?></a></p>            
            </div>
        </div>    
	</div>
       
    <!-- <div class="loginDisplayBackground">
        <div class="loginWidth">
            <img src="img/loginBackground.png" class="loginBackground">
            <a class="removeLinksDecorations loginLogo" href="index.php">  
                BTCW
            </a>
        </div>
        <form class="loginWidth loginRightSide" method="POST">
            <h1 class="loginHeader">
                会员登录
            </h1>

            <div class="loginInput loginInputmargin">
                <input type="text" class="loginInputStyle1" name="field_1" placeholder="用户名">
            </div>
            <div class="loginInput">
                <input type="password" class="loginInputStyle2" name="field_2" placeholder="密码">
            </div>

            <div class="loginCheckbox">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        记下登录资料
                    </label>
                </div>
            </div>

                <button class="loginButton">
                    登录
                </button>
            
            <p class="loginRegister">还未有账号？<a class="loginRegisterLink" href="#####">点击这里注册</a></p>
        </form>
    </div> -->
    <?php require 'mainFooter.php';?>
    <script>
        var img = document.getElementById('seePassword');
        var inputPassword = document.getElementById('passValue');

        img.onclick = function () 
        {
            if(inputPassword.type == "password")
            {
                inputPassword.type = "text";
            }
            else if(inputPassword.type == "text")
            {
                inputPassword.type = "password";
            }
        }
    </script>
  </body>
</html>