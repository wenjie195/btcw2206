<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';

//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{
    if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
        $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
    {
        header('Location:index.php');
        exit();
    }
    else
    {
        $uid = $_SESSION['uid'];
    }
}

$withdrawHistoryRows = null;
$conn = connDB();

$planARows = getPlan($conn," WHERE id = 1 ");
if(!$planARows){
    echo "gg plan A";
    exit();
}

$withdrawHistoryRows = handleFormSubmit($conn);
$rate = getUsdRate($conn);

$conn->close();

function getUsdRate($conn){
    $rateRows = getBtcUsdRate($conn," ORDER BY date_created DESC LIMIT 1");
    if(!$rateRows){
        echo "rate gg";
        exit();
    }
    return $rateRows[0]->getRate();
}

function handleFormSubmit($conn){
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        if(isset($_POST['change_usd_rate'])){
            $newRate = rewrite($_POST['usd_rate']);

            if(!updateDynamicData($conn,"btc_usd_rate"," WHERE id = 1 ",array("usd","rate"),array($newRate,$newRate),"dd")){
                echo "update usd rate failed";
                exit();
            }
            header('Location:adminRateWithdraw.php');
            exit();
        }else if(isset($_POST['search_withdrawal'])){
            $startDate = rewrite($_POST['start_date']);

            $thRows = getTransactionHistory($conn," WHERE (btc_type_id = 8 OR btc_type_id = 7) AND date_created >= ? AND withdraw_position_type IS NOT NULL ORDER BY date_created DESC ",array("date_created"),array($startDate),"s");
            if(!$thRows){
                return array();
            }else{
                return $thRows;
            }
        }else{
            echo "weird post";
        }
    }

    return null;
}


?>
<!doctype html>
<html lang="en">
<head>
    <?php require_once 'mainHeader.php';?>
    <title>Change USD rate and get withdrawals</title>
</head>
<body>
<?php
require_once 'mainNavbar.php';
generateSimpleModal();

?>
<div id="firefly" class="firefly-class min-height">
    <div class="width100 same-padding more-separate-margin-top edit-div">
        <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a>
        <div class="clear"></div>
        <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>

        <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Change USD Rate Here</b></h4>
        <form class="register-form"  method="POST" >
            <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">USD Rate</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="usd_rate" value="<?php echo removeUselessZero($rate); ?>"></h4></td>
                </tr>

                <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4"></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><button class="register-button2 clean orange-hover inputb-button" name="change_usd_rate" type="submit">Change Rate</button></td>
                </tr>
            </table>
        </form>

        <h4 class="btcw-h4 edit-h4-title white-text" style="margin-top: 20px;"><b class="weight-700">Insert Plan A Here</b></h4>
        <a class="register-button2 clean orange-hover inputb-button" href="utilities/sqlMakerForReferral.php" target="_blank">Insert Plan A</a>
        <h4 class="btcw-h4 edit-h4-title white-text" style="margin-top: 20px;"><b class="weight-700">Insert Plan B Here</b></h4>
        <a class="register-button2 clean orange-hover inputb-button" href="utilities/sqlMakerForReferralPlanB.php" target="_blank">Insert Plan B</a>

        <h4 class="btcw-h4 edit-h4-title white-text" style="margin-top: 20px;"><b class="weight-700">Search Withdrawal History Here</b></h4>
        <form class="register-form"  method="POST" >
            <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Withdrawal Start Date until today (example: 2019-05-20)</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="start_date" value="<?php if(isset($_POST['start_date'])){echo $_POST['start_date'];}else{echo '2019-05-20';} ?>"></h4></td>
                </tr>

            </table>
            <div class="clear"></div>
            <button class="register-button2 clean orange-hover inputb-button" name="search_withdrawal" type="submit" >Search Withdrawal History</button>

        </form>
    </div>
    <div class="clear"></div>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h6 class="white-text mt-5"> </h6>
            <table cellspacing="0" cellpadding="0" class="dark-table recommend-table2 white-text-table">
                <?php
                if(isset($_POST['search_withdrawal'])){
                    if(!$withdrawHistoryRows){
                        echo '<tr><td class="text-light" colspan="2">Nothing found!</td></tr>';
                    }else{
                        echo '
                                <tr>
                                    <td class="table2-1">ID</td>
                                    <td class="table2-2">Date</td>
                                    <td class="table2-3">Amount(btc)</td>
                                    <td class="table2-4">Plan</td>
                                    <td class="table2-5">Type</td>
                                    <td class="table2-6">UID</td>
                                    <td class="table2-7">BTC Address</td>
                                    <td class="table2-8">Name</td>
                                    <td class="table2-9">Country</td>
                                    <td class="table2-10">Bank Name</td>
                                    <td class="table2-11">Bank Account No</td>
                                </tr>
                            ';

                        $conn = connDB();
                        foreach ($withdrawHistoryRows as $th) {
                            $sql1 = " SELECT withdrawalBTCAddress, withdrawalUserNameFull,withdrawalUserCountry,withdrawalBankName,withdrawalBankAccNo FROM temp_new_withdrawal WHERE id = ". $th->getWithdrawalId() . " ";
                            $result1 = $conn->query($sql1);

                            $btcAddress = "";
                            $name = "";
                            $country = "";
                            $bankName = "";
                            $bankAccNo = "";
                            if ($result1->num_rows > 0) {
                                // output data of each row
                                while($row = $result1->fetch_assoc()) {
                                    $btcAddress = $row["withdrawalBTCAddress"];
                                    $name = $row["withdrawalUserNameFull"];
                                    $country = $row["withdrawalUserCountry"];
                                    $bankName = $row["withdrawalBankName"];
                                    $bankAccNo = $row["withdrawalBankAccNo"];
                                }
                            } else {
                                echo "0 results";
                            }

                            $id = $th->getId();
                            $date = date( 'Y/m/d h:i:s A', strtotime($th->getDateCreated()) );
                            $amount = "nope";
                            if($th->getBtcTypeId() === 7){
                                $beforePenalty = $th->getCapitalBtcAmountOut();
                                $afterPenalty = bcsub($beforePenalty,bcdiv(bcmul($beforePenalty,$planARows[0]->getWithdrawalPercent(),25),100,25),25);
                                $amount = removeUselessZero($afterPenalty) . "(ORI: ".removeUselessZero($beforePenalty).")";
                            }else if($th->getBtcTypeId() === 8){
                                //7 in position type means plan B's capital and free btc
                                if($th->getWithdrawPositionType() === 7){
                                    $amount = removeUselessZero($th->getFreeBtcAmountOut());
                                }else{
                                    $amount = removeUselessZero($th->getReturnedBtcAmountOut());
                                }
                            }

                            $plan = "GG";
                            if($th->getPlanId() === 1){
                                $plan = "A";
                            }else if($th->getPlanId() === 2){
                                $plan = "B";
                            }

                            $type = "gg type";
                            switch ($th->getWithdrawPositionType()){
                                case 1:
                                    $type = "Capital(A)";
                                    break;
                                case 2:
                                    $type = "总释放收益(A)";
                                    break;
                                case 3:
                                    $type = "总推荐收益(A)";
                                    break;
                                case 4:
                                    $type = "加速释放收益(B)";
                                    break;
                                case 5:
                                    $type = "总推荐收益(B)";
                                    break;
                                case 6:
                                    $type = "量化分红(B)";
                                    break;
                                case 7:
                                    $type = "CapitalFree(B)";
                                    break;
                                default:
                                    $type = "gg type";
                                    break;
                            }

                            $thUid = $th->getUid();

                            echo '
                                    <tr>
                                      <td class="table2-1">'.$id.'</td>
                                      <td class="table2-2">'.$date.'</td>
                                      <td class="table2-3">'.$amount.'</td>
                                      <td class="table2-4">'.$plan.'</td>
                                      <td class="table2-5">'.$type.'</td>
                                      <td class="table2-6">'.$thUid.'</td>
                                      <td class="table2-7">'.$btcAddress.'</td>
                                      <td class="table2-8">'.$name.'</td>
                                      <td class="table2-9">'.$country.'</td>
                                      <td class="table2-10">'.$bankName.'</td>
                                      <td class="table2-11">'.$bankAccNo.'</td>
                                    </tr>
                                ';
                        }
                        $conn->close();
                    }
                }

                ?>
            </table>
        </div>
        <div class="col-md-2"></div>
    </div>

    <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
        <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
    </div>
</div>
<?php require_once 'mainFooter.php';?>
</body>
</html>