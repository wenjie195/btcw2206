<div class="width100 same-padding profile-divider4">
    <div class="left-details float-left ow-left-details">
        <table  cellspacing="0" cellpadding="0" class="transparent-table ow-transparent-table">
            <tr>
                <td class="first-td" width="20%" valign="top"><h4 class="btcw-h4"><b class="weight-700">用户名</b></h4></td>
                <td class="second-td" width="2%" valign="top"><h4 class="btcw-h4">:</h4></td>
                <td class="third-td" width="calc(78% - 40px)" valign="top"><h4 class="btcw-h4"><?php echo $username; ?></h4></td>
                <td class="four-td" width="40px" valign="top"><h4 class="btcw-h4"><a href="editProfile.php" class="hover-container"><img src="img/profileEdit.png" class="edit-icon hover-1" alt="修改个人资料" title="修改个人资料"><img src="img/sherry/profileEdit2.png" class="edit-icon hover-2" alt="修改个人资料" title="修改个人资料"></a></h4></td>
            </tr>
            <!--- Unhide it once all the thing done -->

            <tr>
                <td class="first-td" width="20%" valign="top"><h4 class="btcw-h4"><b class="weight-700">联络号码</b></h4></td>
                <td class="second-td" width="2%" valign="top"><h4 class="btcw-h4">:</h4></td>
                <td class="third-td" width="calc(78% - 40px)" valign="top"><h4 class="btcw-h4"><?php echo $phoneNo; ?></h4></td>
                <td class="four-td" width="40px" valign="top"></td>
            </tr>
            <tr>
                <td class="first-td" width="20%" valign="top"><h4 class="btcw-h4"><b class="weight-700">比特币总额</b></h4></td>
                <td class="second-td" width="2%" valign="top"><h4 class="btcw-h4">:</h4></td>
                <td class="third-td" width="calc(78% - 40px)" valign="top"><h4 class="btcw-h4"><?php echo removeUselessZero($allCombinedTotalBtc); ?> btc<!-- | <a class="orange2-text invest-now">马上捐赠</a> --></h4></td>
                <td class="four-td" width="40px" valign="top"></td>
            </tr>
            <tr>
                <td class="first-td" width="20%" valign="top"><h4 class="btcw-h4"><b class="weight-700">捐赠总额</b></h4></td>
                <td class="second-td" width="2%" valign="top"><h4 class="btcw-h4">:</h4></td>
                <td class="third-td" width="calc(78% - 40px)" valign="top"><h4 class="btcw-h4"><?php echo removeUselessZero($totalCapitalBtc); ?> btc</h4></td>
                <td class="four-td" width="40px" valign="top"></td>
            </tr>
            <tr>
                <td class="first-td" width="20%" valign="top"><h4 class="btcw-h4"><b class="weight-700">免费比特币总额</b></h4></td>
                <td class="second-td" width="2%" valign="top"><h4 class="btcw-h4">:</h4></td>
                <td class="third-td" width="calc(78% - 40px)" valign="top"><h4 class="btcw-h4"><?php echo removeUselessZero($totalFreeBtc); ?> btc</h4></td>
                <td class="four-td" width="40px" valign="top"></td>
            </tr>
            <tr>
                <td class="first-td" width="20%" valign="top"><h4 class="btcw-h4"><b class="weight-700">免费比特币收益总额</b></h4></td>
                <td class="second-td" width="2%" valign="top"><h4 class="btcw-h4">:</h4></td>
                <td class="third-td" width="calc(78% - 40px)" valign="top"><h4 class="btcw-h4"><?php echo removeUselessZero($totalReturnedBtc); ?> btc</h4></td>
                <td class="four-td" width="40px" valign="top"></td>
            </tr>
            <tr>
                <td class="first-td" width="20%" valign="top"><h4 class="btcw-h4"><b class="weight-700">社区比例收益总额</b></h4></td>
                <td class="second-td" width="2%" valign="top"><h4 class="btcw-h4">:</h4></td>
                <td class="third-td" width="calc(78% - 40px)" valign="top"><h4 class="btcw-h4"><?php echo removeUselessZero($user->getDownlineAccumulatedBtc()); ?> btc</h4></td>
                <td class="four-td" width="40px" valign="top"></td>
            </tr>
            <tr>
                <td class="first-td" width="20%" valign="top"><h4 class="btcw-h4"><b class="weight-700">已推荐</b></h4></td>
                <td class="second-td" width="2%" valign="top"><h4 class="btcw-h4">:</h4></td>
                <td class="third-td" width="calc(78% - 40px)" valign="top"><h4 class="btcw-h4"><a href="recommend.php" class="orange1-text"><?php echo $totalReferredCount; ?>个用户</a></h4></td>
                <td class="four-td" width="40px" valign="top"></td>
            </tr>
            <tr class="recommend-tr">
                <td class="first-td" width="20%" valign="top"><h4 class="btcw-h4"><b class="weight-700">专属推荐链接</b></h4></td>
                <td class="second-td" width="2%" valign="top"><h4 class="btcw-h4">:</h4></td>
                <td class="third-td" width="calc(78% - 40px)" valign="top"><h4 class="btcw-h4 link-h41"><a href="<?php echo "https://btcworg.com/indexRegister.php?referralId=$uid";?>" id="invest-now-referral-link" class="invest-a orange1-text"><?php echo "https://btcworg.com/indexRegister.php?referralId=$uid";?></a></h4></td>
                <td class="four-td" width="40px" valign="top"><button class="clean hover-container2 copy-button" id="copy-referral-link"><img src="img/sherry/copy.png" class="edit-icon hover-1a" alt="复制链接" title="复制链接"><img src="img/sherry/copy2.png" class="edit-icon hover-2a" alt="复制链接" title="复制链接"></button></td>
            </tr>

        </table>


        <h4 class="btcw-h4 link-h4"><a href="<?php echo "https://btcworg.com/indexRegister.php?referralId=$uid";?>" id="invest-now" class="invest-a orange1-text"><?php echo "https://btcworg.com/indexRegister.php?referralId=$uid";?></a></h4>


    </div>
</div>




<div class="clear"></div>
<div class="width100 same-padding profile-divider1">
    <h4 class="btcw-h4 text-center weight-700">我们的比特基金平台即将发布！<br>一旦正式发行，您将会收到我们的通知，感谢大家的参与！</h4>
</div>
<!-- Unhide it when all the things are done (Mun Chun Fizo)-->
<!--
  <div class="width100 same-padding profile-divider table-width-div">
      <h4 class="btcw-h4 weight-700 record-h4">总收益纪录</h4>
  </div>
  <div class="width100 same-padding table-width-div">
       <table class="table100">
        <thead class="orange-thead">
            <tr class="orange-tr">
                <th class="th1 th-1 white-text">日期</th>
                <th class="th2 white-text">记录</th>
                <th class="th3 white-text">增加(BTC)</th>
                <th class="th4 white-text">减少(BTC)</th>
                <th class="th5 white-text">免费BTC总数</th>
                <th class="th6 white-text">非免费BTC总数</th>
                <th class="th7 th-7 white-text">所有BTC总数</th>
            </tr>
        </thead>
            <tr class="transparent-tr">
                <td class="th1">11/2/2019</td>
                <td class="th2">社区-小芳的收益</td>
                <td class="th3 color-downline">0.05</td>
                <td class="th4">-</td>
                <td class="th5">0.1</td>
                <td class="th6">299.95</td>
                <td class="th7">300.00</td>
            </tr>
            <tr class="transparent-tr">
                <td class="th1">11/1/2019</td>
                <td class="th2">推荐5个会员收益</td>
                <td class="th3 color-freebtc">0.05</td>
                <td class="th4">-</th>
                <td class="th5">0.1</th>
                <td class="th6">299.90</th>
                <td class="th7">299.95</th>
            </tr>
            <tr class="transparent-tr">
                <td class="th1">5/1/2019</td>
                <td class="th2">计划A收益</td>
                <td class="th3 color-reward">199.90</td>
                <td class="th4">-</td>
                <td class="th5">0.05</td>
                <td class="th6">299.85</td>
                <td class="th7">299.90</td>
            </tr>
            <tr class="transparent-tr">
                <td class="th1">5/1/2019</td>
                <td class="th2">捐赠计划A</td>
                <td class="th3 color-invest">99.95</td>
                <td class="th4">-</td>
                <td class="th5">0.05</td>
                <td class="th6">99.95</td>
                <td class="th7">100.0</td>
            </tr>
            <tr class="transparent-tr">
                <td class="th1">5/1/2019</td>
                <td class="th2">注册收益</td>
                <td class="th3 color-freebtc">0.05</td>
                <td class="th4">-</td>
                <td class="th5">0.05</td>
                <td class="th6">0</td>
                <td class="th7">0.05</td>
            </tr>

        </table>
        <div class="color-div width100">
            <div class="circle-span color-invest-div"></div> <span class="color-span">捐赠</span>
            <div class="circle-span color-free-btc-div"></div> <span class="color-span">免费BTC</span>
            <div class="circle-span color-reward-div"></div> <span class="color-span">捐赠收益</span>
            <div class="circle-span color-recommend-div"></div> <span class="color-span">推荐会员收益</span>
            <div class="circle-span color-downline-div"></div> <span class="color-span">社区比例收益</span>
            <div class="circle-span color-other-div"></div> <span class="color-span">其他收益</span>
        </div>
        <div class="width100 same-padding profile-divider2 overflow-hidden">
          <div class="fill-up-space1"></div>
          <a href="fullRecord.php" class=""><div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center table-button">阅读完整记录</div></a>
          <div class="fill-up-space1"></div>
       </div>
   </div>

  <div class="clear"></div> -->

<div class="width100 same-padding profile-divider table-width-div">
    <h4 class="btcw-h4 weight-700 record-h4">计划A <a class="orange2-text"> (了解更多）</a></h4>
</div>
<div class="width100 same-padding table-width-div">
    <table class="table100">
        <thead class="orange-thead">
        <tr class="orange-tr">
            <th class="th1 th-1 white-text">日期</th>
            <th class="th2 white-text">记录</th>
            <th class="th3 white-text">增加(BTC)</th>
            <th class="th4 white-text">减少(BTC)</th>
            <th class="th5 white-text">免费BTC总数</th>
            <th class="th6 white-text">捐赠BTC总数</th>
            <th class="th7 th-7 white-text">所有BTC总数</th>
        </tr>
        </thead>
        <?php echo $planAResult; ?>
        <!--
        <tr class="transparent-tr">
            <td class="th1">11/2/2019</td>
            <td class="th2">社区-小芳的收益</td>
            <td class="th3 color-downline">0.05</td>
            <td class="th4">-</td>
            <td class="th5">0.1</td>
            <td class="th6">299.95</td>
            <td class="th7">300.00</td>
        </tr>
        <tr class="transparent-tr">
            <td class="th1">11/1/2019</td>
            <td class="th2">推荐5个会员收益</td>
            <td class="th3 color-freebtc">0.05</td>
            <td class="th4">-</th>
            <td class="th5">0.1</th>
            <td class="th6">299.90</th>
            <td class="th7">299.95</th>
        </tr>
        <tr class="transparent-tr">
            <td class="th1">5/1/2019</td>
            <td class="th2">计划A收益</td>
            <td class="th3 color-reward">199.90</td>
            <td class="th4">-</td>
            <td class="th5">0.05</td>
            <td class="th6">299.85</td>
            <td class="th7">299.90</td>
        </tr>
        <tr class="transparent-tr">
            <td class="th1">5/1/2019</td>
            <td class="th2">捐赠计划A</td>
            <td class="th3 color-invest">99.95</td>
            <td class="th4">-</td>
            <td class="th5">0.05</td>
            <td class="th6">99.95</td>
            <td class="th7">100.0</td>
        </tr>
        <tr class="transparent-tr">
            <td class="th1">5/1/2019</td>
            <td class="th2">注册收益</td>
            <td class="th3 color-freebtc">0.05</td>
            <td class="th4">-</td>
            <td class="th5">0.05</td>
            <td class="th6">0</td>
            <td class="th7">0.05</td>
        </tr>
        -->
    </table>
    <div class="color-div width100">
        <div class="circle-span color-invest-div"></div> <span class="color-span">捐赠</span>
        <div class="circle-span color-free-btc-div"></div> <span class="color-span">免费BTC</span>
        <div class="circle-span color-reward-div"></div> <span class="color-span">捐赠收益</span>
        <div class="circle-span color-recommend-div"></div> <span class="color-span">推荐会员收益</span>
        <div class="circle-span color-downline-div"></div> <span class="color-span">社区比例收益</span>
        <div class="circle-span color-other-div"></div> <span class="color-span">其他收益</span>
    </div>
    <div class="width100 same-padding profile-divider2 overflow-hidden">
        <div class="fill-up-space1"></div>
        <a href="fullRecordPlanA.php" class=""><div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center table-button">阅读完整记录</div></a>
        <div class="fill-up-space1"></div>
    </div>
</div>

<div class="clear"></div>


<!--
<div class="width100 same-padding profile-divider table-width-div">
    <h4 class="btcw-h4 weight-700 record-h4">计划B <a class="orange2-text plan-b"> (了解更多）</a></h4>
</div>
<div class="width100 same-padding table-width-div">
     <table class="table100">
      <thead class="orange-thead">
          <tr class="orange-tr">
              <th class="th1 th-1 white-text">日期</th>
              <th class="th2 white-text">记录</th>
              <th class="th3 white-text">增加(BTC)</th>
              <th class="th4 white-text">减少(BTC)</th>
              <th class="th5 white-text">免费BTC总数</th>
              <th class="th6 white-text">非免费BTC总数</th>
              <th class="th7 th-7 white-text">所有BTC总数</th>
          </tr>
      </thead>
          <tr class="transparent-tr">
              <td class="th1">11/2/2019</td>
              <td class="th2">社区-小芳的收益</td>
              <td class="th3 color-downline">0.05</td>
              <td class="th4">-</td>
              <td class="th5">0.1</td>
              <td class="th6">299.95</td>
              <td class="th7">300.00</td>
          </tr>
          <tr class="transparent-tr">
              <td class="th1">11/1/2019</td>
              <td class="th2">推荐5个会员收益</td>
              <td class="th3 color-freebtc">0.05</td>
              <td class="th4">-</th>
              <td class="th5">0.1</th>
              <td class="th6">299.90</th>
              <td class="th7">299.95</th>
          </tr>
          <tr class="transparent-tr">
              <td class="th1">5/1/2019</td>
              <td class="th2">计划A收益</td>
              <td class="th3 color-reward">199.90</td>
              <td class="th4">-</td>
              <td class="th5">0.05</td>
              <td class="th6">299.85</td>
              <td class="th7">299.90</td>
          </tr>
          <tr class="transparent-tr">
              <td class="th1">5/1/2019</td>
              <td class="th2">捐赠计划A</td>
              <td class="th3 color-invest">99.95</td>
              <td class="th4">-</td>
              <td class="th5">0.05</td>
              <td class="th6">99.95</td>
              <td class="th7">100.0</td>
          </tr>
          <tr class="transparent-tr">
              <td class="th1">5/1/2019</td>
              <td class="th2">注册收益</td>
              <td class="th3 color-freebtc">0.05</td>
              <td class="th4">-</td>
              <td class="th5">0.05</td>
              <td class="th6">0</td>
              <td class="th7">0.05</td>
          </tr>

      </table>
      <div class="color-div width100">
          <div class="circle-span color-invest-div"></div> <span class="color-span">捐赠</span>
          <div class="circle-span color-free-btc-div"></div> <span class="color-span">免费BTC</span>
          <div class="circle-span color-reward-div"></div> <span class="color-span">捐赠收益</span>
          <div class="circle-span color-recommend-div"></div> <span class="color-span">推荐会员收益</span>
          <div class="circle-span color-downline-div"></div> <span class="color-span">社区比例收益</span>
          <div class="circle-span color-other-div"></div> <span class="color-span">其他收益</span>
      </div>
      <div class="width100 same-padding profile-divider2 overflow-hidden">
        <div class="fill-up-space1"></div>
        <a href="fullRecordPlanB.php" class=""><div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center table-button">阅读完整记录</div></a>
        <div class="fill-up-space1"></div>
     </div>
 </div>

<div class="clear"></div>
-->


<!--
      <div class="width100 same-padding profile-divider">
          <h4 class="btcw-h4 text-center weight-700 width60">用户只允许在30天内的第10天，第20天或第30天提取金额和需要缴付0%手续费。用户也可以选择投入本金随时提取收益但需缴付18%手续费。</h4>
      </div>
    <div class="clear"></div>



  <div class="width100 same-padding profile-divider3 overflow-hidden">
          <div class="fill-small-space"></div>
          <div class="yellow-box yellow-box-width yellow-box-hover white-text text-center invest-now">捐赠</div>
          <div class="fill-small-space fill-small-space-m"></div>
          <div class="fill-small-space fill-small-space-m"></div>
          <a class="a-hover"><div class="yellow-line-box yellow-box-width yellow-line-box-hover white-text text-center withdrawal-btn">提取收益</div></a>
          <div class="fill-small-space"></div>
  </div>
  <div class="clear"></div>
-->
<!-- Unhide it when all the things are done (Mun Chun Fizo)-->

<div class="width100 element-div extra-padding-bottom profile-divider">
    <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
</div>