<?php
require_once 'utilities/checkSessionFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'generalFunction.php';

    registerLogin();
?>
<!doctype html>
<html lang="en">
<head>
    <?php require 'mainHeader.php';?>
    <meta property="og:url" content="https://btcworg.com/indexRegister.php/" />
    <meta property="og:title" content="<?= _register_title ?>" />
    <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Join us now!">
    <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Join us now!" />
    <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, register, member, 会员注册, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

    <title><?= _register_title ?></title>
    <link rel="canonical" href="https://btcworg.com/indexRegister.php/" />
</head>
<body>
<?php

generateSimpleModal();// Must be first in order to put modal boxes

if(isset($_GET['createModalBox']))
{
    errorModal($_GET['createModalBox']);
}
// $link_url = $_SERVER['REQUEST_URI'];
// if (strpos($link_url, 'referralId') !== false) 
// {
//     $conn = connDB();
//     $linkarr = explode("referralId=",$link_url);
//     $uidUser = $linkarr[1];
//     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uidUser),"s");
// }
echo getMainJsErrorMsgArray();
?>
    <!--<div class="header-div width100 same-padding absolute">
    	<div class="logo-div">
        	<a href="bitcoin.php" class="a-hover"><img src="img/sherry/logo-white.png" class="logo-img opacity-hover" alt="bitcoin" title="bitcoin"></a>
        </div>
        <div class="menu-right-div">
        	<a class="menu-a login-a a-hover white-text-a yellow-hover" href="indexLogin.php">登录</a>
            <a class="menu-a register-a a-hover white-text-a yellow-hover" href="indexRegister.php">注册</a>
        </div>
    </div>-->  
    <div id="firefly" class="firefly-class min-height">
    	<?php require 'mainNavbar.php';?>  
    	<div class="width100 same-padding">
    		<div class="login-width text-center white-text">
                <h1 class="register-title white-text"><?= _register_member_register ?></h1>
            <form class="register-form"  method="POST" name="registerForm" onsubmit="return validateMyForm(errorMsgArrayTranslationForMainJs);">
            <?php //if (strpos($link_url, 'referralId') !== false) 
            //{
                ?>
            <!-- <div class="input-container1 edit-margin">
                <p class="register-ref-p">推荐人: <?php //echo $userRows[0]->getUsername();?></p>
                <img src="img/sherry/user.png" class="input-icon ref-img">
            </div>            
            <div class="input-container1 edit-margin">
                <p class="register-ref-p">推荐人邮箱: <?php //echo $userRows[0]->getEmail();?></p>
                <img src="img/sherry/email.png" class="input-icon ref-img">
            </div>              -->
            <?php
            //}
            ?>
            
            <div class="input-container1 edit-margin">

                <input type="text" class="inputa clean2"  name="field_1" id="field_1" placeholder="<?= _login_username ?>" value="<?php if(isset($_GET['username'])){echo $_GET['username'];}?>">
                <img src="img/sherry/user.png" class="input-icon">
            </div>
			
            <div class="input-container2 edit-margin">
                <input type="text" class="inputa clean2"  name="field_2" id="field_2" placeholder="<?= _editProfile_email ?>" value="<?php if(isset($_GET['email'])){echo $_GET['email'];}?>">
                <img src="img/sherry/email.png" class="input-icon">
            </div>
            <p class="register-notice"><?= _register_email_instru ?></p>
            <?php
            if(isset($_GET['country']))
            {
                loginGetCity($_GET['country']);
                $getCC = getCountryCode($_GET['country']);
                $getPhone = $_GET['phone'];
                ?>
                <div class="input-container4 edit-margin">
                    <input type="text" disabled class="country-code-input clean2 white-text" name="field_4_CountryCode" id="field_4_CountryCode" placeholder="+" value="<?php echo "+".$getCC;?>">
                    <input type="text" class="mobile-input clean2 white-text" name="field_4" placeholder="<?= _register_phone_number ?>" id="field_4" min="0" value="<?php echo $getPhone;?>">
                    <img src="img/sherry/phone.png" class="input-icon">
                </div>
                <?php
            }
            else
            {
                loginGetCity();
                ?>
                <div class="input-container4 edit-margin">
                    <input type="text" disabled class="country-code-input clean2 white-text" name="field_4_CountryCode" id="field_4_CountryCode" placeholder="+" >
                    <input type="text" class="mobile-input clean2 white-text" name="field_4" placeholder="<?= _register_phone_number ?>" id="field_4" min="0">
                    <img src="img/sherry/phone.png" class="input-icon">
                </div>
                <?php
            }
            ?>
            <p class="register-notice"><?= _register_phone_instru ?></p>
            <div class="input-container5">
                <img src="img/sherry/eye.png" class="input-icon button-icon-eye" id="seePassword1" onclick="seePassword();">           
                <input type="password" class="inputa clean2 password-input"  name="field_5" id="field_5" placeholder="<?= _login_password ?>">
                <img src="img/sherry/password.png" class="input-icon">

            </div>
			<p class="register-notice ow-mtop3"><?= _login_pw_instru ?></p>

            <div class="input-container6">
                <img src="img/sherry/eye.png" class="input-icon button-icon-eye" id="seePassword2" onclick="seePassword();">           
                <input type="password" class="inputa clean2 password-input"  name="field_6" id="field_6" placeholder="<?= _login_confirm_pw ?>">
                <img src="img/sherry/password.png" class="input-icon">
               
            </div>
            
             <div class="input-container1">
                 <?php if(isset($_GET['icno']))
                 {
                     $getIcNo = $_GET['icno'];
                 ?>
                     <input type="text" class="inputa clean2"  name="field_7" id="field_7" placeholder="<?= _register_keyin_ic ?>" value="<?php echo $getIcNo;?>">
                 <?php
                 }
                 else
                 {
                 ?>
                     <input type="text" class="inputa clean2"  name="field_7" id="field_7" placeholder="<?= _register_keyin_ic ?>" >
                 <?php
                 }
                 ?>

                <img src="img/sherry/ic.png" class="input-icon">
            </div> 
            
            <!--Fizo Here the referral email-->
            <div class="input-container2 edit-margin">
                <input type="email" class="inputa clean2"  name="referrerEmail" id="referrerEmail" placeholder="<?= _register_referral_email ?>" value="">
                <img src="img/sherry/email.png" class="input-icon">
            </div>                      
            <div id="recaptcha-container"></div>
            <div class="checkbox-div">
                <div class="checkbox1">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" name="defaultCheck1[]">
                    <label class="form-check-label" for="defaultCheck1">
                        <?= _register_check_agree ?>
                    </label>
                    <a class="loginRegisterLink orange-text-hover" href="#"><?= _register_terms ?></a>
                    <label class="form-check-label" for="defaultCheck1">
                        <?= _register_and ?>
                    </label>
                    <a class="loginRegisterLink orange-text-hover" href="#"><?= _register_privacy ?></a>
                </div>
                <div class="checkbox1">
                    <?php
                    if(isset($_GET['news']) && $_GET['news'] == 1)
                    {
                        echo "<input checked class='form-check-input' type='checkbox' value='' id='defaultCheck2' name='defaultCheck2[]'>";
                    }
                    else
                    {
                        echo "<input class='form-check-input' type='checkbox' value='' id='defaultCheck2' name='defaultCheck2[]'>";
                    }
                    ?>
                    <label class="form-check-label" for="defaultCheck2">
                        <?= _editProfile_news ?>
                    </label>
                </div>
            </div>
            <button class="register-button2 clean orange-hover button-width" name="registerButton" id="registerButton" ><?= _register_register ?></button>
        </form>
        <p class="loginRegister"><?= _register_already ?><a class="loginRegisterLink orange-text-hover" href="indexLogin.php"><?= _register_click_here_login ?></a></p>        
            </div>
        </div>
    </div>
    
 

<style>
    .register-a{
        color:#f7931a;}

</style>
<!-- Regex For Taiwan , China , South Korea And Hong Kong -->
<!-- Link : https://github.com/Edditoria/validid -->
<script type="text/javascript" src="js/validid.js?version=1.0.17"></script>
<?php require 'mainFooter.php';?>
<script>
        
        console.log(validid.hkid('OX3519647'));/// Got Prob with hong kong

        var img1 = document.getElementById('seePassword1');
        var img2 = document.getElementById('seePassword2');

        var inputPassword1 = document.getElementById('field_5');
        var inputPassword2 = document.getElementById('field_6');

        img1.onclick = function () 
        {
            if(inputPassword1.type == "password")
            {
                inputPassword1.type = "text";
            }
            else if(inputPassword1.type == "text")
            {
                inputPassword1.type = "password";
            }
        }
        img2.onclick = function () 
        {
            if(inputPassword2.type == "password")
            {
                inputPassword2.type = "text";
            }
            else if(inputPassword2.type == "text")
            {
                inputPassword2.type = "password";
            }
        }
</script>
</body>
</html>