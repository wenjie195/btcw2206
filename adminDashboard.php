<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        
        <meta property="og:url" content="https://btcworg.com/adminDashboard.php" />
        <meta property="og:title" content="BTCW - Admin Dashboard" />
        <meta name="description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
        
        <title>BTCW - Admin Dashboard</title>
        <link rel="canonical" href="https://btcworg.com/adminDashboard.php" />
  </head>
  <body>
  		<?php require 'adminNavbar.php';?>
  		<div class="width100 same-padding padding-top-bottom">
        
        	<a href="adminTransferRequest.php">
                <div class="three-column-div hover-container-01">
                    <img src="img/sherry/transfer.png" class="three-column-img hover01" alt="比特币转移申请待处理" title="比特币转移申请待处理">
                    <img src="img/sherry/transfer-white.png" class="three-column-img hover02" alt="比特币转移申请待处理" title="比特币转移申请待处理">
                    <h3 class="three-column-h3">1,000</h3>
                    <p class="three-column-p">比特币转移申请待处理</p>
                </div>
            </a>
        	<a href="adminInvestRequest.php">            
                <div class="three-column-div middle-div second-three hover-container-01">
                    <img src="img/sherry/invest2.png" class="three-column-img hover01" alt="捐赠申请待处理" title="捐赠申请待处理">
                    <img src="img/sherry/invest2-white.png" class="three-column-img hover02" alt="捐赠申请待处理" title="捐赠申请待处理">                    
                    <h3 class="three-column-h3">1,000</h3>
                    <p class="three-column-p">捐赠申请待处理</p>
                </div>     
            </a>  
        	<a href="adminWithdrawRequest.php">                 
                <div class="three-column-div hover-container-01">
                    <img src="img/sherry/triple.png" class="three-column-img hover01" alt="提取收益申请待处理" title="提取收益申请待处理">
                    <img src="img/sherry/triple-white.png" class="three-column-img hover02" alt="提取收益申请待处理" title="提取收益申请待处理">
                    <h3 class="three-column-h3">1,000</h3>
                    <p class="three-column-p">提取收益申请待处理</p>
                </div>            
			</a>
        	      
        	<div class="three-column-div second-three hover-container-01 cursor-normal">
            	<img src="img/sherry/user3.png" class="three-column-img hover01" alt="用户已注册" title="用户已注册">
                <img src="img/sherry/user3-white.png" class="three-column-img hover02" alt="用户已注册" title="用户已注册">
                <h3 class="three-column-h3">20,000</h3>
                <p class="three-column-p">用户已注册</p>
            </div>
            
        	<div class="three-column-div middle-div hover-container-01 cursor-normal">
            	<img src="img/sherry/bitcoin.png" class="three-column-img hover01" alt="比特币已投入" title="比特币已投入">
                <img src="img/sherry/bitcoin-white.png" class="three-column-img hover02" alt="比特币已投入" title="比特币已投入">
                <h3 class="three-column-h3">100,000,000</h3>
                <p class="three-column-p">比特币已投入</p>
            </div>             
			<a href="changeRate.php"> 
                <div class="three-column-div second-three hover-container-01">
                    <img src="img/sherry/settings.png" class="three-column-img hover01" alt="更改汇率、收益率和福利等" title="更改汇率、收益率和福利等">
                    <img src="img/sherry/settings-white.png" class="three-column-img hover02" alt="更改汇率、收益率和福利等" title="更改汇率、收益率和福利等">
                    <h3 class="three-column-h3">&nbsp;</h3>
                    <p class="three-column-p">更改汇率、收益率和福利等</p>
                </div>            
            </a>        
        	<div class="three-column-div hover-container-01 cursor-normal">
            	<img src="img/sherry/plan-a.png" class="three-column-img hover01" alt="比特币已投入计划A" title="比特币已投入计划A">
                <img src="img/sherry/plan-a-white.png" class="three-column-img hover02" alt="比特币已投入计划A" title="比特币已投入计划A">
                <h3 class="three-column-h3">30,000</h3>
                <p class="three-column-p">比特币已投入计划A</p>
            </div>
                    
        	<div class="three-column-div middle-div second-three hover-container-01 cursor-normal">
            	<img src="img/sherry/plan-a-user.png" class="three-column-img hover01" alt="用户已加入计划A" title="用户已加入计划A">
                <img src="img/sherry/plan-a-user-white.png" class="three-column-img hover02" alt="用户已加入计划A" title="用户已加入计划A">
                <h3 class="three-column-h3">12,000</h3>
                <p class="three-column-p">用户已加入计划A</p>
            </div>            
            <a href="changePlanA.php">         
                <div class="three-column-div hover-container-01">
                    <img src="img/sherry/settings2.png" class="three-column-img hover01" alt="更改计划A收益" title="更改计划A收益">
                    <img src="img/sherry/settings2-white.png" class="three-column-img hover02" alt="更改计划A收益" title="更改计划A收益">
                    <h3 class="three-column-h3">&nbsp;</h3>
                    <p class="three-column-p">更改计划A收益</p>
                </div>
            </a>

        	<div class="three-column-div second-three hover-container-01 cursor-normal">
            	<img src="img/sherry/plan-b.png" class="three-column-img hover01" alt="比特币已投入计划B" title="比特币已投入计划B">
                <img src="img/sherry/plan-b-white.png" class="three-column-img hover02" alt="比特币已投入计划B" title="比特币已投入计划B">
                <h3 class="three-column-h3">70,000</h3>
                <p class="three-column-p">比特币已投入计划B</p>
            </div>            
                    
        	<div class="three-column-div middle-div hover-container-01 cursor-normal">
            	<img src="img/sherry/plan-b-user.png" class="three-column-img hover01" alt="用户已加入计划B" title="用户已加入计划B">
                <img src="img/sherry/plan-b-user-white.png" class="three-column-img hover02" alt="用户已加入计划B" title="用户已加入计划B">
                <h3 class="three-column-h3">8,000</h3>
                <p class="three-column-p">用户已加入计划B</p>
            </div>              
            <a href="changePlanB.php">
                <div class="three-column-div second-three hover-container-01">
                    <img src="img/sherry/settings3.png" class="three-column-img hover01" alt="更改计划B收益" title="更改计划B收益">
                    <img src="img/sherry/settings3-white.png" class="three-column-img hover02" alt="更改计划B收益" title="更改计划B收益">
                    <h3 class="three-column-h3">&nbsp;</h3>
                    <p class="three-column-p">更改计划B收益</p>
                </div> 
            </a>                                              
        </div> 
        <div class="width100 element-div extra-padding-bottom profile-divider">
                <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>        
         
 		<?php require 'adminFooter.php';?>

  </body>
</html>