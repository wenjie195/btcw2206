<?php
//Start the session
session_start();
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    
    header('Location:index.php');
    exit();
}
else
{ 
    $uid = $_SESSION['uid'];
}
//todo 8) (NOT REALLY NEEDED LA ACTUALLY FOR NOW) need make 1 more function for withdraw to confirm really withdrew the money already,
//todo    if failed change transaction to fail status,
//todo    if success change to success and then minus returned_btc in user_plan based on plan id

require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'generalFunction.php';
require_once 'utilities/calculationFunction.php';
require_once 'utilities/databaseFunction.php';

$conn = connDB();
$errorMsg = null;
$successMsg = null;

$link_url = $_SERVER['REQUEST_URI'];
if (strpos($link_url, 'address') !== false)
{
    $addressArr = explode("address=",$link_url);
    $address = $addressArr[1];
    if (strpos($addressArr[0], 'msgType') !== false)
    {
        $msgTypeArr = explode("msgType=",$addressArr[0]);

        if (strpos($msgTypeArr[1], '&') !== false) 
        {
            $msgTypeArr = explode("&",$msgTypeArr[1]);
            $msgType = $msgTypeArr[0];
            // echo $msgType." ".$address;

            if($msgType == 1)
            {
                $successMsg = _profile_donate_success.$address;
            }
        }       
    }
}
else if(strpos($link_url, 'msgType') !== false) 
{
    $msgTypeArr = explode("msgType=",$link_url);
    $msgType = $msgTypeArr[1];
    if($msgType == 2 && $_SESSION['msgValid'] == 0)
    {
        $successMsg = _profile_in_progress;
        $_SESSION['msgValid'] = 1;
    }
    else if ($msgType == 3 && $_SESSION['msgValid'] == 0) 
    {
        $errorMsg = _profile_withdraw_0;
        $_SESSION['msgValid'] = 1;
    }
    else if($msgType == 4 && $_SESSION['msgValid'] == 0)
    {
        $errorMsg = _profile_error_try_again;
        $_SESSION['msgValid'] = 1;
    }
    else if($msgType == 5 && $_SESSION['msgValid'] == 0)
    {
        $errorMsg = _profile_error_overload;
        $_SESSION['msgValid'] = 1;
    }
    else if ($msgType == 6 && $_SESSION['msgValid'] == 0)
    {
        $errorMsg = _profile_error_too_low;
        $_SESSION['msgValid'] = 1;
    }
    else if ($msgType == 7 && $_SESSION['msgValid'] == 0)
    {
        $errorMsg = _profile_error_overload;
        $_SESSION['msgValid'] = 7;
    }
}

$userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");

//******************************************EXAMPLE START******************************************

//echo $userRows[0]->getDateCreated();
//echo $userRows[0]->getUsername();

//$newTableId = insertDynamicData($conn,"temp_invest_table",array("uid","amount"),array("whahaha id",123.2222),"sd");
//if($newTableId){
//    echo "newId: ".$newTableId;
//}else{
//    echo "error";
//}

////update based on id
//if(updateDynamicData($conn,"temp_invest_table"," WHERE id = ? ",array("uid","amount"),array("uid changed",555.22,2),"sdi")){
//    echo "updated";
//}else{
//    echo "update error";
//}

////update all
//if(updateDynamicData($conn,"temp_invest_table",null,array("uid","amount"),array("uid changed",555.22),"sd")){
//    echo "updated";
//}else{
//    echo "update error";
//}

////update with or
//if(updateDynamicData($conn,"temp_invest_table"," WHERE uid = ? OR uid = ? OR amount = ?",array("uid","amount"),array("yay",888.8888888,"333","555",1.0),"sdssd")){
//    echo "updated";
//}else{
//    echo "update error";
//}

//******************************************EXAMPLE END********************************************

$totalReferredCount = 0;
if($userRows && count($userRows) > 0){
    $user = $userRows[0];
    $username = $user->getUsername();
    $phoneNo = getPhoneNo($conn,$user);
    $email = $user->getEmail();
    $userPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,1),"si");
    $totalInvestedReturnFreeBtc = getSum($conn,"free_btc","total_amount"," WHERE uid = ? AND btc_type_id = 1 AND plan_id = 1 AND is_withdrawed = 0 ", array("uid"), array($uid),"s");
    $totalFreeBtcReceived = getSum($conn,"free_btc","total_amount"," WHERE uid = ? AND plan_id = 1 AND is_withdrawed = 0 AND current_day != 0", array("uid"), array($uid),"s");
    $totalFreeBtcReceivedExcludingInvestment = getSum($conn,"free_btc","total_amount"," WHERE uid = ? AND (btc_type_id = 4 OR btc_type_id = 5) AND plan_id = 1 ", array("uid"), array($uid),"s");
    $totalReturnedBtc = 0;
    $totalCapitalBtc = 0;
    $totalFreeBtc = 0;
    $allCombinedTotalBtc = 0;
    $planACommunityLevel = getCommunityLevel($conn,$uid,1);
    $currentUtcTimeInMs =  getCurrentUtcTimeInMs();
    if($userPlanRows)
    {
        $totalReturnedBtc = $userPlanRows[0]->getReturnedBtc();
        $totalCapitalBtc = $userPlanRows[0]->getCapitalBtc();
        $totalFreeBtc = $userPlanRows[0]->getFreeBtc();
        $allCombinedTotalBtc = $totalReturnedBtc + $totalCapitalBtc + $totalFreeBtc;
    }
    $totalReferredCount = getCount($conn,"referral_history","referrer_id"," WHERE referrer_id = ? ", array("referrer_id"), array($user->getUid()),"s");

    $planAResult = displayTransactionHistory($conn,$uid,1,5);

    $planARows = getPlan($conn," WHERE id = ? ORDER BY date_updated DESC LIMIT 1",array("id"),array(1),"i");

    $planA = null;
    if($planARows){
        $planA = $planARows[0];
    }
    $totalReleasedBtc = 0;//2 FIZO_WITHDRAW

    $totalCapitalBtcPlanB = 0;
    $totalRewardCapitalBtcPlanB = 0;
    $totalReleasedBtcFasterPlanB = "-";//4 fizo_withdraw
    $totalDownlineAccumulatedUsdPlanB = "-";
    $totalReferralCommissionPlanB = "-";//5 fizo_withdraw
    $totalReleasedBtcNormalPlanB = "-";//6 fizo_withdraw
    $totalInvestmentOfLevelOneDownline = 0;
    $planBCommunityLevel = getCommunityLevel($conn,$uid,2);

    $everydayReleaseAmountOfBtc = 0;
    if($planA && $totalFreeBtcReceived > 0){
        $everydayReleaseAmountOfBtc = bcdiv(bcmul($totalFreeBtcReceived,$planA->getPercentageGiven(),25),100,25);
        //this is for display only. To let user see that his capital also dropped 0.2% but in actual fact, it didn't. When withdraw, need to manually deduct capital amount and returned btc amount
        $everydayReleaseAmountOfBtc = bcadd($everydayReleaseAmountOfBtc,bcdiv(bcmul($totalCapitalBtc,$planA->getPercentageGiven(),25),100,25),25);

        $freeBtcRows = getFreeBtc($conn," WHERE uid = ? AND plan_id = 1 ",array("uid"),array($uid),"s");
        if($freeBtcRows){
            foreach ($freeBtcRows as $freeBtc){
                //$totalReleaseBtc += (totalAmount * percentageGiven / 100 ) * totalDays;
                $totalReleasedBtc = bcadd($totalReleasedBtc,bcmul(bcdiv(bcmul($freeBtc->getTotalAmount(),$planA->getPercentageGiven(),25),100,25),$freeBtc->getCurrentDay(),25),25);

                $investCapitalPlanATHRows = getTransactionHistory($conn," WHERE free_btc_id = ? AND btc_type_id = 2 ",array("free_btc_id"),array($freeBtc->getId()),"i");
                if($investCapitalPlanATHRows){
                    //this is for display only. To let user see that his capital also dropped 0.2% but in actual fact, it didn't. When withdraw, need to manually deduct capital amount and returned btc amount
                    $totalReleasedBtc = bcadd($totalReleasedBtc,bcmul($freeBtc->getCurrentDay(),bcdiv(bcmul($investCapitalPlanATHRows[0]->getCapitalBtcAmountIn(),$planA->getPercentageGiven(),25),100,25),25),25);
                }
            }
        }

        //this is to minus the btc he withdraw previously
        $totalReleasedBtc = bcsub($totalReleasedBtc,getPreviouslyWithdrewBtc($conn,$uid,2,1),25);
    }

    $thisUserLevelOneReferralRows = getReferralHistory($conn," WHERE referrer_id = ? ",array("uid"),array($uid),"s");
    if($thisUserLevelOneReferralRows){
        foreach ($thisUserLevelOneReferralRows as $tempLevelOneReferral){
            $tempLevelOneUserPlanRows = getUserPlan($conn," WHERE uid = ? AND plan_id = 1 ",array("uid"),array($tempLevelOneReferral->getReferralId()),"s");
            if($tempLevelOneUserPlanRows){
                $totalInvestmentOfLevelOneDownline = bcadd($totalInvestmentOfLevelOneDownline,$tempLevelOneUserPlanRows[0]->getCapitalBtc(),25);
            }

            $tempLevelOneUserPlanRowsPlanB = getUserPlan($conn," WHERE uid = ? AND plan_id = 2 ",array("uid"),array($tempLevelOneReferral->getReferralId()),"s");
            if($tempLevelOneUserPlanRowsPlanB){
                $totalInvestmentOfLevelOneDownline = bcadd($totalInvestmentOfLevelOneDownline,$tempLevelOneUserPlanRowsPlanB[0]->getCapitalBtc(),25);
            }
        }
        //add his own plan B capital also
        $thisUserPlanBRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,2),"si");
        if($thisUserPlanBRows){
            $totalInvestmentOfLevelOneDownline = bcadd($totalInvestmentOfLevelOneDownline,$thisUserPlanBRows[0]->getCapitalBtc(),25);
        }
    }

    //3 fizo_withdraw
    $totalReferralCommissionReceived = getSum($conn,"transaction_history","returned_btc_amount_in"," WHERE uid = ? AND btc_type_id = 3 AND plan_id = 1 ",array("uid"),array($uid),"s");
    //this is to minus the btc he withdraw previously
    $totalReferralCommissionReceived = bcsub($totalReferralCommissionReceived,getPreviouslyWithdrewBtc($conn,$uid,3,1),25);

    //for Plan B only
    $userPlanBRows = getUserPlan($conn," WHERE uid = ? AND plan_id = ? ",array("uid","plan_id"),array($uid,2),"si");

    if($userPlanBRows){
        $userPlanB = $userPlanBRows[0];

        $totalCapitalBtcPlanB = getSum($conn,"transaction_history","capital_btc_amount_in",
            " WHERE uid = ? AND btc_type_id = 2 AND plan_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ",
            array("uid"),array($uid),"s");

        $totalRewardCapitalBtcPlanB = getSum($conn,"transaction_history","free_btc_amount_in",
            " WHERE uid = ? AND btc_type_id = 1 AND plan_id = 2 ",
            array("uid"),array($uid),"s");
        $totalRewardCapitalBtcWithdrawPlanB = getSum($conn,"transaction_history","free_btc_amount_out",
            " WHERE uid = ? AND btc_type_id = 8 AND withdraw_position_type = 7 AND plan_id = 2 AND (withdraw_status = 1 OR withdraw_status = 2) ",
            array("uid"),array($uid),"s");
        $totalRewardCapitalBtcPlanB = bcsub($totalRewardCapitalBtcPlanB,$totalRewardCapitalBtcWithdrawPlanB,25);

        $totalReleasedBtcFasterPlanB = getSum($conn,"transaction_history","returned_btc_amount_in",
            " WHERE uid = ? AND btc_type_id = 12 AND plan_id = 2 ",
            array("uid"),array($uid),"s");

        //need to deduct total reward capital btc with total released btc first before the total released btc is being deducted by the withdrawal because need full amount
        if($totalReleasedBtcFasterPlanB !== "-" && $totalReleasedBtcFasterPlanB > 0){
            $totalRewardCapitalBtcPlanB = bcsub($totalRewardCapitalBtcPlanB,$totalReleasedBtcFasterPlanB,25);
        }

        //this is to minus the btc he withdraw previously
        $totalReleasedBtcFasterPlanB = bcsub($totalReleasedBtcFasterPlanB,getPreviouslyWithdrewBtc($conn,$uid,4,2),25);

        $totalDownlineAccumulatedUsdPlanB = $userPlanB->getDownlineAccumulatedUsd();

        $totalReferralCommissionPlanB = getSum($conn,"transaction_history","returned_btc_amount_in",
            " WHERE uid = ? AND (btc_type_id = 3 OR btc_type_id = 11) AND plan_id = 2  ",
            array("uid"),array($uid),"s");
        //this is to minus the btc he withdraw previously
        $totalReferralCommissionPlanB = bcsub($totalReferralCommissionPlanB,getPreviouslyWithdrewBtc($conn,$uid,5,2),25);

        $totalReleasedBtcNormalPlanB = getSum($conn,"transaction_history","returned_btc_amount_in",
            " WHERE uid = ? AND (btc_type_id = 6 OR btc_type_id = 13) AND plan_id = 2  ",
            array("uid"),array($uid),"s");
        //this is to minus the btc he withdraw previously
        $totalReleasedBtcNormalPlanB = bcsub($totalReleasedBtcNormalPlanB,getPreviouslyWithdrewBtc($conn,$uid,6,2),25);

        if($totalCapitalBtcPlanB <= 0 || $totalCapitalBtcPlanB === "-"){
            $totalCapitalBtcPlanB = "0";
        }

        if($totalRewardCapitalBtcPlanB <= 0 || $totalRewardCapitalBtcPlanB === "-"){
            $totalRewardCapitalBtcPlanB = "0";
        }

        if($totalReleasedBtcFasterPlanB <= 0 || $totalReleasedBtcFasterPlanB === "-"){
            $totalReleasedBtcFasterPlanB = "-";
        }
        if($totalDownlineAccumulatedUsdPlanB <= 0 || $totalDownlineAccumulatedUsdPlanB === "-"){
            $totalDownlineAccumulatedUsdPlanB = "-";
        }

        if($totalReferralCommissionPlanB <= 0 || $totalReferralCommissionPlanB === "-"){
            $totalReferralCommissionPlanB = "-";
        }

        if($totalReleasedBtcNormalPlanB <= 0 || $totalReleasedBtcNormalPlanB === "-"){
            $totalReleasedBtcNormalPlanB = "-";
        }


        
    }

    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if(isset($_POST['invest-form']))
        {
            if(!isPostVariableEmpty("field_1")
//            && !isPostVariableEmpty("invest-btc-amount")
//            && !isPostVariableEmpty("receiptHashKey")
            && !isPostVariableEmpty("btc_address_hidden_input")
            && !isPostVariableEmpty("btc_address_id_hidden_input")){
                $planSelected = $_POST["field_1"];//Plans Selected
//                $amount = $_POST["invest-btc-amount"];
                $amount = 0;
                $receiptHashKey = "_EMPTY_";
                if(isset($_POST["receiptHashKey"]) && isPostVariableEmpty("receiptHashKey") && strlen($_POST['receiptHashKey']) > 0){
                    $receiptHashKey = rewrite($_POST["receiptHashKey"]);
                }
    
                if($planSelected == "A")
                {
                    if($amount <= 0.2 || $amount === 0)
                    {
                        if($amount >= 0.025 || $amount === 0){
                            $newTempInvestTableId = insertDynamicData($conn,"temp_invest_table",array("uid","amount","receiptHashKey"),array($uid,$amount,$receiptHashKey),"sds");
                            if($newTempInvestTableId)
                            {
                                $newApplicationHistoryId = insertDynamicData($conn,"application_history",
                                    array("amount","uid","coinbase_address_id","coinbase_receive_address","user_send_address","plan_id"),
                                    array($amount,$uid,$_POST['btc_address_id_hidden_input'],$_POST['btc_address_hidden_input'],$receiptHashKey,1),
                                    "dssssi");

                                if($newApplicationHistoryId){
                                    // $successMsg = _profile_donate_success.$amount." btc!";
                                    header('Location:profile.php?msgType=1&address='.$_POST['btc_address_hidden_input']);
                                }else{
                                    $errorMsg = _general_sorry;
                                }

                            }
                            else
                            {
                                $errorMsg = _general_sorry;
                            }
                        }else{
                            $errorMsg = _profile_min;
                        }
                    }
                    else
                    {
                        $errorMsg = _profile_max;
                    }
                }
                else if($planSelected == "B")
                {
                    if($amount <= 50 || $amount === 0)
                    {
                        if($amount >= 0.025 || $amount === 0){
                            $newTempInvestTableId = insertDynamicData($conn,"temp_invest_table_b",array("uid","amount","receiptHashKey"),array($uid,$amount,$receiptHashKey),"sds");
                            if($newTempInvestTableId)
                            {
                                $newApplicationHistoryId = insertDynamicData($conn,"application_history",
                                    array("amount","uid","coinbase_address_id","coinbase_receive_address","user_send_address","plan_id"),
                                    array($amount,$uid,$_POST['btc_address_id_hidden_input'],$_POST['btc_address_hidden_input'],$receiptHashKey,2),
                                    "dssssi");

                                if($newApplicationHistoryId){
                                    // $successMsg = _profile_donate_success.$amount." btc!";
                                    header('Location:profile.php?msgType=1&address='.$_POST['btc_address_hidden_input']);
                                }else{
                                    $errorMsg = _general_sorry;
                                }

                            }
                            else
                            {
                                $errorMsg = _general_sorry;
                            }
                        }else{
                            $errorMsg = _profile_min;
                        }

                    }
                    else
                    {
                        $errorMsg = _profile_max50;
                    }
                }           
            }
            else
            {
                $errorMsg = _profile_keyin_all;
            }
        }
        
        if(isset($_POST['reinvestYes']))
        {
            $reinvestPosition = $_POST['reinvestYes'];
            $reinvestPlan = null;
            $reinvestStatus = null;
            $reinvestType = null;
            
            // auto reinvest
            if($reinvestPosition == "1")
            {
                $reinvestPlan = 1;//for plan A
                $reinvestStatus = 0;
                $reinvestType = 1;
            }
            else if($reinvestPosition == "3")
            {
                $reinvestPlan = 2;//for plan B
                $reinvestStatus = 0;
                $reinvestType = 1;
            }
            
            else if($reinvestPosition == "5")
            {
                $reinvestPlan = 2;//for plan B
                $reinvestStatus = 0;
                $reinvestType = 1;
            }
    
            // manual reinvest
            else if($reinvestPosition == "2")
            {
                $reinvestPlan = 1;//for plan A
                $reinvestType = 0;
                $reinvestStatus = 2;
            }
            else if($reinvestPosition == "4")
            {
                $reinvestPlan = 2;//for plan B
                $reinvestType = 0;
                $reinvestStatus = 2;
            }
    
            $confirmedReinvest = insertDynamicData($conn,
                        "temp_new_reinvest",
                        array("uid","reinvestType","reinvestStatus","reinvestPlanType","reinvestPosition"),
                        array($uid,$reinvestType,$reinvestStatus,$reinvestPlan,$reinvestPosition),
                        "siiii");
    
            $errorMsg = reinvestFreeBtcToCapital($conn,$uid,0,$reinvestPosition + 1,$reinvestPlan,true);
    
            if($confirmedReinvest && !isset($errorMsg))
            {
                //  echo "entered";
                header('Location:profile.php');
            }
            else
            {
                if($errorMsg === _profile_reinvest_0 && $reinvestType === 1){
                    header('Location:profile.php');
                }
            }
        }
        if(isset($_POST['reinvestCancelYes']))
        {
            $reinvestPosition = $_POST['reinvestCancelYes'];
            $reinvestStatus = 1;
    
            if(!updateDynamicData($conn,"temp_new_reinvest"," WHERE uid LIKE ? AND reinvestPosition LIKE ? ORDER BY dateCreated DESC LIMIT 1",array("reinvestStatus"),array($reinvestStatus,$uid,$reinvestPosition),"isi"))
            {
                // echo "error";
            }
            else
            {
                //  echo "entered";
                header('Location:profile.php');
            }
        }
        if(isset($_POST['opt1_withdrawal_submit']))
        {
            if($_POST['opt1_withdrawal_submit'] != 1){
                $errorMsg = checkIsAllowWithdraw($conn,$_POST['opt1_withdrawal_amount']);
            }
            if(!isset($errorMsg)){
                $withdrawalPercent = $_POST['getPercentage1'];
                $withdrawalAmountBTCAfter = $_POST['getTotal1'];
                $withdrawalBTCAddress = $_POST['opt1_withdrawal_BTCaddress'];
    
                $withdrawalPosition = $_POST['opt1_withdrawal_submit'];

                if($withdrawalPosition == 1)//Plan A Capital
                {
                    $withdrawalAmountBTCBefore = $_POST['getMaximValue1'];
                    // $withdrawalAmountBTCBefore = 2; // for testing only
                    $_SESSION['msgValid'] = 0;
    
                    if($withdrawalAmountBTCBefore > 0)
                    {
                        $confirmedWithdrawal = insertDynamicData($conn,
                            "temp_new_withdrawal",
                            array("uid","withdrawalPlan","withdrawalPosition","withdrawalPaymentOption","withdrawalAmountBTCBefore","withdrawalPercent","withdrawalAmountBTCAfter","withdrawalBTCAddress"),
                            array($uid,1,$withdrawalPosition,1,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalBTCAddress),
                            "siiiddds");
    
                        if(!$confirmedWithdrawal)
                        {
                            header('Location:profile.php?msgType=4');
                            // $errorMsg = "错误。 请稍后再试。";
                        }
                        else
                        {
                            $successfullWithdrawal = withdrawCapitalPlanA($conn,$uid,$confirmedWithdrawal);
                            if($successfullWithdrawal === null)
                            {
                                // $errorMsg = "您的交易正在处理当中，谢谢。";
                                header('Location:profile.php?msgType=2');
                                // header('Location:profile.php?msgType=1&amount='.$amount);
                            }
                            else
                            {
                                $errorMsg = $successfullWithdrawal;
                            }
                        }
                    }
                    else
                    {
                         header('Location:profile.php?msgType=3');
                        //$errorMsg = "您的可提取额度是0";
                    }
                }
                else
                {
                    
                    $withdrawalMaxVal = $_POST['getMaximValue1'];
                    $withdrawalAmountBTCBefore = $_POST['opt1_withdrawal_amount'];
                    $_SESSION['msgValid'] = 0;
    
                    if($withdrawalPosition == 2 && $withdrawalAmountBTCBefore <= $totalReleasedBtc)
                    {
                        preventSameWithdrawal_1($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalBTCAddress);
                    }
                    else if($withdrawalPosition == 3 && $withdrawalAmountBTCBefore <= $totalReferralCommissionReceived)
                    {
                        preventSameWithdrawal_1($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalBTCAddress);
                    }
                    else if($withdrawalPosition == 4 && $withdrawalAmountBTCBefore <= $totalReleasedBtcFasterPlanB)
                    {
                        preventSameWithdrawal_1($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalBTCAddress);
                    }
                    else if($withdrawalPosition == 5 && $withdrawalAmountBTCBefore <= $totalReferralCommissionPlanB)
                    {
                        preventSameWithdrawal_1($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalBTCAddress);
                    }
                    else if($withdrawalPosition == 6 && $withdrawalAmountBTCBefore <= $totalReleasedBtcNormalPlanB)
                    {
                        preventSameWithdrawal_1($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalBTCAddress);
                    }
                    else
                    {
                        header('Location:profile.php?msgType=3');
                    }
                }
            }
        }
        if(isset($_POST['opt2_withdrawal_submit']))
        {
            if($_POST['opt2_withdrawal_submit'] != 1){
                $errorMsg = checkIsAllowWithdraw($conn,$_POST['opt2_withdrawal_amount']);
            }
            if(!isset($errorMsg)){
                $withdrawalFullName = $_POST['opt2_withdrawal_fullName'];
                $withdrawalCountry = $_POST['opt2_withdrawal_country'];
                $withdrawalBankName = $_POST['opt2_withdrawal_bankName'];
                $withdrawalBankAccountNo = $_POST['opt2_withdrawal_bankAccountNo'];
                $_SESSION['msgValid'] = 0;
    
                $withdrawalPosition = $_POST['opt2_withdrawal_submit'];
    
                $withdrawalPercent = $_POST['getPercentage2'];
                $withdrawalAmountBTCAfter = $_POST['getTotal2'];
    
                if($withdrawalPosition == 1)//Plan A Capital
                {
                    $withdrawalAmountBTCBefore = $_POST['getMaximValue2'];
    
                    // $withdrawalAmountBTCBefore = 2; // for testing only
                    if($withdrawalAmountBTCBefore > 0)
                    {
                        $confirmedWithdrawal = insertDynamicData($conn,
                            "temp_new_withdrawal",
                            array("uid","withdrawalPlan","withdrawalPosition","withdrawalPaymentOption","withdrawalAmountBTCBefore","withdrawalPercent","withdrawalAmountBTCAfter","withdrawalUserNameFull","withdrawalUserCountry","withdrawalBankName","withdrawalBankAccNo"),
                            array($uid,1,$withdrawalPosition,2,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalFullName,$withdrawalCountry,$withdrawalBankName,$withdrawalBankAccountNo),
                            "siiidddssss");
    
                        if(!$confirmedWithdrawal)
                        {
                            header('Location:profile.php?msgType=4');
                            // $errorMsg = "错误。 请稍后再试。";
                        }
                        else
                        {
                            $successfullWithdrawal = withdrawCapitalPlanA($conn,$uid,$confirmedWithdrawal);
                            if($successfullWithdrawal === null)
                            {
                                // $errorMsg = "您的交易正在处理当中，谢谢。";
                                // header('Location:profile.php');
                                header('Location:profile.php?msgType=2');
                            }
                            else
                            {
                                $errorMsg = $successfullWithdrawal;
                            }
    
                        }
                    }
                    else
                    {
                        header('Location:profile.php?msgType=3');
                        // $errorMsg = "您的可提取额度是0";
                    }
                }
                else
                {
                    $withdrawalMaxVal = $_POST['getMaximValue2'];
                    $withdrawalAmountBTCBefore = $_POST['opt2_withdrawal_amount'];
                    $_SESSION['msgValid'] = 0;
    
                    
                    if($withdrawalPosition == 2 && $withdrawalAmountBTCBefore <= $totalReleasedBtc)
                    {
                        preventSameWithdrawal_2($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalFullName,$withdrawalCountry,$withdrawalBankName,$withdrawalBankAccountNo);
                    }
                    else if($withdrawalPosition == 3 && $withdrawalAmountBTCBefore <= $totalReferralCommissionReceived)
                    {
                        preventSameWithdrawal_2($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalFullName,$withdrawalCountry,$withdrawalBankName,$withdrawalBankAccountNo);
                    }
                    else if($withdrawalPosition == 4 && $withdrawalAmountBTCBefore <= $totalReleasedBtcFasterPlanB)
                    {
                        preventSameWithdrawal_2($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalFullName,$withdrawalCountry,$withdrawalBankName,$withdrawalBankAccountNo);
                    }
                    else if($withdrawalPosition == 5 && $withdrawalAmountBTCBefore <= $totalReferralCommissionPlanB)
                    {
                        preventSameWithdrawal_2($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalFullName,$withdrawalCountry,$withdrawalBankName,$withdrawalBankAccountNo);
                    }
                    else if($withdrawalPosition == 6 && $withdrawalAmountBTCBefore <= $totalReleasedBtcNormalPlanB)
                    {
                        preventSameWithdrawal_2($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalFullName,$withdrawalCountry,$withdrawalBankName,$withdrawalBankAccountNo);
                    }
                    else
                    {
                        header('Location:profile.php?msgType=3');
                    }
                    
                }
            }
        }
    }


    $conn->close();
}else{
    $conn->close();
    header('Location:index.php');
    exit();
}
function preventSameWithdrawal_2($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalFullName,$withdrawalCountry,$withdrawalBankName,$withdrawalBankAccountNo)
{
    $uid = $_SESSION['uid'];
    $conn = connDB();
    // $withdrawalMaxVal = 2; // for testing only
    // $withdrawalAmountBTCBefore = 2; // for testing only

    if($withdrawalMaxVal > 0 && $withdrawalAmountBTCBefore > 0)
    {
        if($withdrawalAmountBTCBefore <= $withdrawalMaxVal)
        {
            if($withdrawalPosition == 2 || $withdrawalPosition == 3)
            {
                $withdrawalPlan = 1;
            }
            else
            {
                $withdrawalPlan = 2;
            }

            $confirmedWithdrawal = insertDynamicData($conn,
                "temp_new_withdrawal",
                array("uid","withdrawalPlan","withdrawalPosition","withdrawalPaymentOption","withdrawalAmountBTCBefore","withdrawalPercent","withdrawalAmountBTCAfter","withdrawalUserNameFull","withdrawalUserCountry","withdrawalBankName","withdrawalBankAccNo"),
                array($uid,$withdrawalPlan,$withdrawalPosition,2,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalFullName,$withdrawalCountry,$withdrawalBankName,$withdrawalBankAccountNo),
                "siiidddssss");

            if(!$confirmedWithdrawal)
            {
                header('Location:profile.php?msgType=4');
                // $errorMsg = "错误。 请稍后再试。";
            }
            else
            {
                $successfullWithdrawal = withdrawReturnedBtc($conn,$uid,$withdrawalAmountBTCBefore,$withdrawalPlan,$withdrawalPosition,$confirmedWithdrawal);
                if($successfullWithdrawal === null)
                {
                    // $errorMsg = "您的交易正在处理当中，谢谢。";
                    // header('Location:profile.php');
                    header('Location:profile.php?msgType=2');
                }
                else
                {
                    $errorMsg = $successfullWithdrawal;
                }
            }
        }
        else
        {
            header('Location:profile.php?msgType=5');
            // $errorMsg = "您提取的数额已超过可提取额度。请重新输入数额等于或少于可提取额度。";
        }
    }
    else
    {
        header('Location:profile.php?msgType=5');
        // $errorMsg = "您提取的数额已超过可提取额度。请重新输入数额等于或少于可提取额度。";
    }
}
function preventSameWithdrawal_1($withdrawalPosition,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalMaxVal,$withdrawalBTCAddress)
{
    $uid = $_SESSION['uid'];
    $conn = connDB();
    // $withdrawalMaxVal = 2; // for testing only
    // $withdrawalAmountBTCBefore = 2; // for testing only
    if($withdrawalMaxVal > 0 && $withdrawalAmountBTCBefore > 0)
    {
        if($withdrawalAmountBTCBefore <= $withdrawalMaxVal)
        {
            if($withdrawalPosition == 2 || $withdrawalPosition == 3)
            {
                $withdrawalPlan = 1;
            }
            else
            {
                $withdrawalPlan = 2;
            }

            $confirmedWithdrawal = insertDynamicData($conn,
                "temp_new_withdrawal",
                array("uid","withdrawalPlan","withdrawalPosition","withdrawalPaymentOption","withdrawalAmountBTCBefore","withdrawalPercent","withdrawalAmountBTCAfter","withdrawalBTCAddress"),
                array($uid,$withdrawalPlan,$withdrawalPosition,1,$withdrawalAmountBTCBefore,$withdrawalPercent,$withdrawalAmountBTCAfter,$withdrawalBTCAddress),
                "siiiddds");

            if(!$confirmedWithdrawal)
            {
                header('Location:profile.php?msgType=4');
                // $errorMsg = "错误。 请稍后再试。";
            }
            else
            {
                $successfullWithdrawal = withdrawReturnedBtc($conn,$uid,$withdrawalAmountBTCBefore,$withdrawalPlan,$withdrawalPosition,$confirmedWithdrawal);
                if($successfullWithdrawal === null)
                {
                    header('Location:profile.php?msgType=2');
                    // $errorMsg = "您的交易正在处理当中，谢谢。";
                    // header('Location:profile.php');
                }
                else
                {
                    $errorMsg = $successfullWithdrawal;
                }
            }
        }
        else
        {
            header('Location:profile.php?msgType=5');
            // $errorMsg = "您提取的数额已超过可提取额度。请重新输入数额等于或少于可提取额度。";
        }
    }
    else
    {
        header('Location:profile.php?msgType=6');
        // $errorMsg = "您提取的数额必须超过0。请重新输入数额。";
    }
}
function getMultiplier($sessionUID,$planType)
{
    $conn = connDB();
    $totalBTCTheyInvest = 0;
    $sql = "SELECT btcSetToThisUser,promotionMultiplier FROM temporary_plan_b WHERE uid = ? AND planType = ? ";
    if ($stmt = $conn->prepare($sql)) 
    {
        $stmt->bind_param('si',$sessionUID,$planType);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($btcAssigned,$promoMulti);

        while($stmt->fetch())
        {
            // echo $btcAssigned." ".$promoMulti."<br>";
            // echo "<br>";

            $totalBTCTheyInvest += ($btcAssigned * $promoMulti);
            // $totalBTCTheyInvest += ($btcAssigned * $promoMulti);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if ($num_of_rows > 0) 
        {
            return $totalBTCTheyInvest;
        } 
        else
        {
            return null;
        }
    }
}

function temporarilyCheckAndDisplayThatAmountToThisUserOnly($sessionUID,$planType)
{
    $conn = connDB();
    $returnArray = array();
    $totalBTCTheyInvest = 0;
    $sql = "SELECT btcSetToThisUser FROM temporary_plan_b WHERE uid = ? AND planType = ? ";
    if ($stmt = $conn->prepare($sql)) 
    {
        $stmt->bind_param('si',$sessionUID,$planType);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($btcAssigned);

        while($stmt->fetch())
        {
            $totalBTCTheyInvest += $btcAssigned;
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if ($num_of_rows > 0) 
        {
            return $totalBTCTheyInvest;
        } 
        else
        {
            if($planType == 1)
            {
                return "0";
            }
            else
            {
                return "-";
            }
           
        }
    }
}
function getTodayReinvest($userUid,$reinvestPosition,$sqlTypeAuto)
{
    // echo $reinvestPosition;
    $conn = connDB();
    $arrayGetReinvest = array();
    $todayDate =  "".idate("Y")."-".idate("m")."-".idate("d")."";
    // echo $todayDate;
   
    if($sqlTypeAuto == 1)
    {
        $sql = " SELECT reinvestStatus FROM temp_new_reinvest WHERE uid LIKE ? AND reinvestPosition LIKE ? ORDER BY dateCreated DESC LIMIT 1";
    }
    
    //  echo $sql;
    if ($stmt = $conn->prepare($sql)) 
    {
        $stmt->bind_param('si',$userUid,$reinvestPosition);
        $stmt->execute();
        $stmt->store_result();
        $ns = $stmt->num_rows;
        $stmt->bind_result($reinvestStatus);
        while($stmt->fetch())
        {
            array_push($arrayGetReinvest,$reinvestStatus);
        }
        $stmt->free_result();
        $stmt->close();
        // echo "<br>".$ns;
        if ($ns > 0) 
        {
            return $arrayGetReinvest;
        } 
        else
        {
            return null;
        }
    }   
}

function getPhoneNo($conn,$user){
    $countryRows = getCountries($conn," WHERE id = ? ",array("id"),array($user->getCountryId()),"i");
    $countryCode = "";
    if($countryRows && count($countryRows) > 0){
        $countryCode = "+".$countryRows[0]->getPhonecode()." ";
    }
    return $countryCode . $user->getPhoneNo();
}

?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/profile.php" />
        <meta property="og:title" content="<?= _profile_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

        <title><?= _profile_title ?></title>
        <link rel="canonical" href="https://btcworg.com/profile.php" />
  </head>
  <body>
  <?php require 'mainNavbar.php';
   
    generateInputModalForInvest("invest");
    generateInputModalForInvest("withdraw");
  ?>

    <div id="firefly" class="firefly-class min-height">
    	<div class="width100 same-padding menu-content-distance">
        	<div class="first-profile-div">
            	<p class="white-text profile-data-p hover-container">
                	<b class="weight-700"><?= _profile_ni_hao ?> <?php echo $username; ?></b> 
                	<a href="editProfile.php" ><img src="img/sherry/edit-white.png" class="edit-icon3 hover-1" alt="<?= _editPassword_edit_details ?>" title="<?= _editPassword_edit_details ?>"></a>
                	<a href="editProfile.php" ><img src="img/sherry/edit-yellow.png" class="edit-icon3 hover-2" alt="<?= _editPassword_edit_details ?>" title="<?= _editPassword_edit_details ?>"></a>
                </p>
                <p class="white-text profile-data-p"><b class="weight-700"><?= _profile_phone_number ?></b> <?php echo $phoneNo; ?></p>
                <p class="white-text profile-data-p"><b class="weight-700"><?= _profile_email ?> </b> <?php echo $email; ?></p>
            </div>	
            <div class="second-big-div">
                <div class="second-profile-div">
                    <p class="white-text profile-data-p text-center time-p-text weight-700"><?= _profile_newyork ?></p>
                    <div class="clock neon-text">
                      <div class="hours">
                        <div class="first">
                          <div class="number">0</div>
                        </div>
                        <div class="second">
                          <div class="number">0</div>
                        </div>
                      </div>
                      <div class="tick">:</div>
                      <div class="minutes">
                        <div class="first">
                          <div class="number">0</div>
                        </div>
                        <div class="second">
                          <div class="number">0</div>
                        </div>
                      </div>
<!--                      <div class="tick">:</div>-->
                      <div class="seconds">
                        <div class="first">
                          <div class="number">0</div>
                        </div>
                        <div class="second infinite">
                          <div class="number">0</div>
                        </div>
                      </div>
                </div>
                </div>  
                <div class="third-profile-div">
                    <div class="green-dot">&nbsp;</div><div class="next-to-green"><p class="white-text online-number profile-data-p"><span id="user-online-count-span"></span><span class="grey-span">/312655+</span> <?= _profile_online ?></p></div>
                </div> 
           </div>                  
        </div> 
        <div class="clear"></div> 
        <div class="width100 same-padding button-div-distance" id="mainInvestButton">
        	<div class="fill-space3"></div>
            <div class="button-space-div"><button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now invest-button1"><?= _profile_donate ?></button></div>
            <div class="fill-space3"></div>
        </div>
        <div class="clear"></div>
        <div class="width100 same-padding two-item-distance">
            <div class="plan-a-box text-center white-text">
                <h4 class="profile-h4"><?= _profile_plan_a ?><a class="learn-more orange-text-hover plan-a"><?= _profile_learn_more ?></a></h4>
                <div class="dark-box graph-bg first-dark-box a-box1 a-box0">
                	<div class="profile-box-mheight">
                        <img src="img/sherry/bitcoin-white.png" class="box-img" alt="<?= _profile_bitcoin ?>" title="<?= _profile_bitcoin ?>">
                        <p class="box-title"><?= _profile_donate_btc ?></p>
                        <p class="box-big-font">
                        <?php
                        
    //                        $planAInvested = temporarilyCheckAndDisplayThatAmountToThisUserOnly($_SESSION['uid'],1);
    //                        if($planAInvested != "-")
    //                        {
    //                            // echo sprintf("%.3f", $result);
    //                            $rounded = 0.001 * (int)($planAInvested*1000);
    //                            echo $rounded;
    //                        }
    //                        else
    //                        {
    //                            echo $planAInvested;
    //                        }
                            if($totalCapitalBtc > 0){
                                echo removeUselessZero($totalCapitalBtc);
                            }else{
                                echo "0";
                            }
                        ?>
                        </p>
                        <p class="box-title separate-distance"><?= _profile_fix_btc ?></p>
    <!--                    <p class="box-big-font">3(+0.1)</p>-->
                        <p class="box-big-font">
                        <?php 
    //                     if($planAInvested != "-")
    //                     {
    //                        // getMultiplier($_SESSION['uid'],1);
    //                         $multiplyInvestA = getMultiplier($_SESSION['uid'],1);
    //                         if($multiplyInvestA != null)
    //                         {
    //                            echo $multiplyInvestA." + ".removeUselessZero($totalFreeBtc);
    //                         }
    //                         else
    //                         {
    //                            echo "0 + ".removeUselessZero($totalFreeBtc);
    //                         }
    //                        //  $multiplyInvestRounded_A = 0.001 * (int)($multiplyInvestA*1000);
    //
    //                     }
    //                     else
    //                     {
    //                         echo $planAInvested;
    //                     }
                            if($totalInvestedReturnFreeBtc > 0 && $totalCapitalBtc > 0){
                                echo removeUselessZero(bcadd($totalInvestedReturnFreeBtc,$totalCapitalBtc,25));
                            }else{
                                echo "0";
                            }
                            echo "</br>+</br>";
                            if($totalFreeBtcReceivedExcludingInvestment > 0){
                                echo removeUselessZero($totalFreeBtcReceivedExcludingInvestment);
                            }else{
                                echo "0";
                            }
                        ?>
                        </p>
                    </div>
                    <?php 
                            if(isAllowReinvestAndWithdraw()) // later have to change != to ==
                            {
                    ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn" id="withdrawal_1"><?= _profile_withdraw ?></button><!--提取收益-->
                    <?php
                            }
                            else
                            {
                    ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn disabled-btn" id="withdrawal_1" disabled><?= _profile_withdraw ?></button><!--提取收益-->
                    <?php            
                            }
                    ?>
                    </div>
                <div class="dark-box second-dark empty-dark first-dark-box a-box1">
                	<div class="profile-box-mheight">
                        <img src="img/sherry/influence-white.png" class="box-img" alt="<?= _profile_btc_profit ?>" title="<?= _profile_btc_profit ?>">
                        <p class="box-title"><?= _profile_release_profit ?></p>
    <!--                    <p class="box-big-font">0.02</p>-->
                        <p class="box-big-font">
                            <?php
                                //only show this if he got entitled to dropping
                                if($userRows[0] && $userPlanRows[0] && $planA && (bcadd($totalInvestmentOfLevelOneDownline,$userPlanRows[0]->getCapitalBtc(),25) >= $planA->getMinBtcToWithdraw() || $userPlanRows[0]->getCapitalBtc() > 0) && $everydayReleaseAmountOfBtc > 0){
                                    echo removeUselessZero($everydayReleaseAmountOfBtc);
                                }else{
                                    echo "-";
                                }
                            ?>
                        </p>
                        <p class="box-title separate-distance"><?= _profile_total_release_profit ?></p>
    <!--                    <p class="box-big-font">0.12</p>-->
                        <p class="box-big-font">
                            <?php
                                if($userRows[0] && $userPlanRows[0] && isset($totalReleasedBtc) && $totalReleasedBtc > 0){
                                    echo removeUselessZero($totalReleasedBtc);
                                }else{
                                    echo "-";
                                }
                            ?>
                        </p>
                      </div>
                        <?php 
                            if(isAllowReinvestAndWithdraw()) // later have to change != to ==
                            {
                                $todayReinvest = getTodayReinvest($uid,1,1);
                                if($todayReinvest == null)
                                {
                                    ?>
                                        <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="reinvest_1"><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                    <?php 
                                }
                                else
                                {
                                    // echo $todayReinvest[0];
                                    if($todayReinvest[0] == 0)
                                    { 
                                        ?>
                                            <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="cancelReinvest_1"><?= _profile_stop_reinvest ?></button><!--捐赠-->
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                            <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="reinvest_1"><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                        <?php
                                    }
                                }
                                ?>
                                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn" id="withdrawal_2"><?= _profile_withdrawal1 ?></button><!--提取收益-->
                                <?php
                            }
                            else // later have to change != to ==
                            {
                                $todayReinvest = getTodayReinvest($uid,1,1);
                                if($todayReinvest == null)
                                {
                                    ?>
                                        <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="reinvest_1" disabled><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                    <?php 
                                }
                                else
                                {
                                    // echo $todayReinvest[0];
                                    if($todayReinvest[0] == 0)
                                    { 
                                        ?>
                                            <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="cancelReinvest_1" disabled><?= _profile_stop_reinvest ?></button><!--捐赠-->
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                            <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="reinvest_1" disabled><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                        <?php
                                    }
                                }
                                ?>
                                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn disabled-btn" id="withdrawal_2" disabled><?= _profile_withdrawal1 ?></button><!--提取收益-->
                                <?php
                            }
                        ?>
                </div>
                <div class="dark-box tree-bg">
                    <img src="img/sherry/low-risk-white.png" class="box-img" alt="<?= _profile_total_referral_bonus ?>" title="<?= _profile_total_referral_bonus ?>">
                    <p class="box-title"><?= _profile_total_referral_bonus ?>(btc)</p>
<!--                    <p class="box-big-font">5</p>-->
                    <p class="box-big-font">
                        <?php
                            if($totalReferralCommissionReceived > 0){
                                echo removeUselessZero($totalReferralCommissionReceived);
                            }else{
                                echo "-";
                            }
                        ?>
                    </p>
                    <?php 
                            if(isAllowReinvestAndWithdraw()) // later have to change != to ==
                            {
                    ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="reinvest_2"><?= _profile_donate ?></button><!--捐赠-->
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn" id="withdrawal_3"><?= _profile_withdrawal1 ?></button><!--提取收益-->         
                    <?php
                            }
                            else
                            {
                    ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="reinvest_2" disabled><?= _profile_donate ?></button><!--捐赠-->
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn disabled-btn" id="withdrawal_3" disabled><?= _profile_withdrawal1 ?></button><!--提取收益-->         
                    <?php
                            }
                    ?>
                </div>
                <div class="dark-box star-bg second-dark">
                    <img src="img/sherry/bitcoin-stack1-white.png" class="box-img" alt="<?= _profile_community_value ?>" title="<?= _profile_community_value ?>">
                    <p class="box-title"><?= _profile_community_value_usd ?></p>
<!--                    <p class="box-big-font">1,000</p>-->
                    <p class="box-big-font margin-btm-0">
                        <?php
                            if($user && $user->getDownlineAccumulatedUsd() > 0){
                                echo removeUselessZero($user->getDownlineAccumulatedUsd());
                            }else{
                                echo "0";
                            }
                        ?>
                    </p>
                    <p class="box-big-font">
                        <?php
                            echo getCommunityLevelStarsImg($planACommunityLevel);
                        ?>
                    </p>
                </div>
            </div>         
            <div class="plan-a-box plan-b-box text-center white-text">
                <h4 class="profile-h4"><?= _profile_plan_b ?> <a class="learn-more orange-text-hover plan-b"><?= _profile_learn_more ?></a></h4>
                <div class="dark-box graph-bg first-dark-box b-box">
                    <img src="img/sherry/bitcoin-white.png" class="box-img" alt="<?= _profile_bitcoin ?>" title="<?= _profile_bitcoin ?>">
                    <p class="box-title"><?= _profile_donate_btc ?></p>
<!--                    <p class="box-big-font">0.5</p>-->
                    <p class="box-big-font">
                    <?php 

//                        $result = temporarilyCheckAndDisplayThatAmountToThisUserOnly($_SESSION['uid'],2);
//                        if($result != "-")
//                        {
//                            // echo sprintf("%.3f", $result);
//                            $rounded = 0.001 * (int)($result*1000);
//                            echo $rounded;
//                        }
//                        else
//                        {
//                            echo $result;
//                        }
                        echo removeUselessZero($totalCapitalBtcPlanB);
                    ?>
                    </p>
                    <p class="box-title separate-distance"><?= _profile_fix_btc ?></p>
<!--                    <p class="box-big-font">10(0.5+1.2)</p>-->
                    <p class="box-big-font">
                    <?php 
//                        if($result != "-")
//                        {
//                            $planB = getMultiplier($_SESSION['uid'],2);
//                            // echo sprintf("%.3f", $planB);
//                            if($planB != null)
//                            {
//                                echo $planB ;
//                            }
//                            else
//                            {
//                                echo "-";
//                            }
//                        }
//                        else
//                        {
//                            echo $result;
//                        }
                        echo removeUselessZero($totalRewardCapitalBtcPlanB);
                    ?>
                    </p>
                </div>
                <div class="dark-box second-dark empty-dark small-dark">
                    <img src="img/sherry/influence-white.png" class="box-img" alt="<?= _profile_btc_profit ?>" title="<?= _profile_btc_profit ?>">
                    <p class="box-title"><?= _profile_fast_release_profit ?></p>
<!--                    <p class="box-big-font">1</p>-->
                    <p class="box-big-font">
                        <?php
                        echo removeUselessZero($totalReleasedBtcFasterPlanB);
                        ?>
                    </p>
                    <?php 
                        if(isAllowReinvestAndWithdraw()) // later have to change != to ==
                        {
                            $todayReinvest = getTodayReinvest($uid,3,1);
                            if($todayReinvest == null)
                            {
                                ?>
                                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="reinvest_3"><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                <?php 
                            }
                            else
                            {
                                // echo $todayReinvest[0];
                                if($todayReinvest[0] == 0)
                                { 
                                    ?>
                                        <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="cancelReinvest_3"><?= _profile_stop_reinvest ?></button><!--捐赠-->
                                    <?php
                                }
                                else
                                {
                                    ?>
                                        <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="reinvest_3"><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                    <?php
                                }
                            }
                            ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn" id="withdrawal_4"><?= _profile_withdrawal1 ?></button><!--提取收益-->     
                            <?php
                        }
                        else
                        {
                            $todayReinvest = getTodayReinvest($uid,3,1);
                            if($todayReinvest == null)
                            {
                                ?>
                                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="reinvest_3" disabled><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                <?php 
                            }
                            else
                            {
                                // echo $todayReinvest[0];
                                if($todayReinvest[0] == 0)
                                { 
                                    ?>
                                        <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="cancelReinvest_3" disabled><?= _profile_stop_reinvest ?></button><!--捐赠-->
                                    <?php
                                }
                                else
                                {
                                    ?>
                                        <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="reinvest_3" disabled><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                    <?php
                                }
                            }
                            ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn disabled-btn" id="withdrawal_4" disabled><?= _profile_withdrawal1 ?></button><!--提取收益-->     
                            <?php
                        }
                    ?>
                </div>
                <div class="dark-box second-dark small-dark small-dark2">
                    <img src="img/sherry/bitcoin-stack1-white.png" class="box-img" alt="<?= _profile_community_value ?>" title="<?= _profile_community_value ?>">
                    <p class="box-title"><?= _profile_community_value_usd ?></p>
<!--                    <p class="box-big-font margin-btm-0">1,000</p>-->
                    <p class="box-big-font margin-btm-0">
                        <?php
                            echo removeUselessZero($totalDownlineAccumulatedUsdPlanB);
                        ?>
                    </p>
                    <p class="box-big-font">
                        <?php
                            echo getCommunityLevelStarsImg($planBCommunityLevel);
                        ?>
                    </p>
                </div>               
                <div class="dark-box tree-bg">
                  
                    <img src="img/sherry/low-risk-white.png" class="box-img" alt="<?= _profile_total_referral_bonus ?>" title="<?= _profile_total_referral_bonus ?>">
                    <p class="box-title mini-top"><?= _profile_total_referral_bonus ?>(btc)</p>
<!--                    <p class="box-big-font">200</p>-->
                    <p class="box-big-font">
                        <?php
                        echo removeUselessZero($totalReferralCommissionPlanB);
                        ?>
                    </p>
             
                    <?php 
                            if(isAllowReinvestAndWithdraw()) // later have to change != to ==
                            {
                    ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="reinvest_4"><?= _profile_donate ?></button><!--捐赠-->
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn" id="withdrawal_5"><?= _profile_withdrawal1 ?></button><!--提取收益-->              
                    <?php
                            }
                            else // later have to change != to ==
                            {
                    ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="reinvest_4" disabled><?= _profile_donate ?></button><!--捐赠-->
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn disabled-btn" id="withdrawal_5" disabled><?= _profile_withdrawal1 ?></button><!--提取收益-->              
                    <?php
                            }
                    ?>

                </div>
                <div class="dark-box second-dark star-bg">
                  
                    <img src="img/sherry/triple-white.png" class="box-img" alt="<?= _profile_bonus1 ?>" title="<?= _profile_bonus1 ?>">
                    <p class="box-title mini-top"><?= _profile_bonus1 ?>(btc)</p>
<!--                    <p class="box-big-font">20</p>-->
                    <p class="box-big-font">
                        <?php
                        echo removeUselessZero($totalReleasedBtcNormalPlanB);
                        ?>
                    </p>
               
                    <?php
                        if(isAllowReinvestAndWithdraw()) // later have to change != to ==
                        {
                            $todayReinvest = getTodayReinvest($uid,5,1);
                            if($todayReinvest == null)
                            {
                                ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="reinvest_5"><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                <?php
                            }
                            else
                            {
                                // echo $todayReinvest[0];
                                if($todayReinvest[0] == 0)
                                {
                                    ?>
                                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="cancelReinvest_5"><?= _profile_stop_reinvest ?></button><!--捐赠-->
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now" id="reinvest_5"><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                    <?php
                                }
                            }
                            ?>
                            <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn" id="withdrawal_6"><?= _profile_withdrawal1 ?></button><!--提取收益-->
                            <?php
                        }
                        else
                        {
                            $todayReinvest = getTodayReinvest($uid,5,1);
                            if($todayReinvest == null)
                            {
                                ?>
                                <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="reinvest_5" disabled><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                <?php
                            }
                            else
                            {
                                // echo $todayReinvest[0];
                                if($todayReinvest[0] == 0)
                                {
                                    ?>
                                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="cancelReinvest_5" disabled><?= _profile_stop_reinvest ?></button><!--捐赠-->
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width invest-now disabled-btn" id="reinvest_5" disabled><?= _profile_auto_reinvest ?></button><!--捐赠-->
                                    <?php
                                }
                            }
                            ?>
                            <button class="clean2 yellow-box-hover white-text text-center yellow-box y-width withdrawal-btn disabled-btn" id="withdrawal_6" disabled><?= _profile_withdrawal1 ?></button><!--提取收益-->
                            <?php
                        }
                    ?>
                </div>
            </div>              
        </div>
	</div>

  <?php require 'mainFooter.php';?>
     
  <!--- Before Edit-->
  
<!--  old version please refer to profile2.php-->

<!--    information modal before entering invest modal-->
      <div id="investInfoModal" class="modal2 ow-modal2">

          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
              <span class="close2 ow-close2 close-style">&times;</span>

                <div class="register-form width100 same-padding tutorial-div">
                	<p class="tutorial-title sp-p5"><?= _profile_invest_tutorial_1 ?></p>
                    
                    <p class="box-title tutorial-p normal"><?= _profile_invest_tutorial_2 ?></p>
                    <p class="box-title tutorial-p normal"><?= _profile_invest_tutorial_3 ?><br><?= _profile_invest_tutorial_4 ?></p>
                    
                    <p class="box-title tutorial-p normal"><?= _profile_invest_tutorial_5 ?></p>
                    <p class="box-title tutorial-p normal"><?= _profile_invest_tutorial_6 ?></p>
                    <p class="box-title tutorial-p normal"><?= _profile_invest_tutorial_7 ?><br><?= _profile_invest_tutorial_8 ?></p>
                    
                    <p class="box-title tutorial-p normal text-center sp-p5">
                        <button onclick="openInvestModal();" class="clean2 yellow-box-hover white-text text-center yellow-box understand-btn"><?= _profile_invest_tutorial_9 ?></button>
                            <!--<label><input type="checkbox" value="" onclick="openInvestModal();"></label>-->
                    </p>

                </div>

          </div>
      </div>




        <!--- Modal --->
        <div id="myModal2" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
            <span class="close2 ow-close2 close-style">&times;</span>
            
            
                <form class="register-form width100 same-padding"  method="POST" onsubmit="return validatePlans();" autocomplete="off">
                    <h4 class="btcw-h4 form-h4 text-center qr-h4-a normal"><b class="weight-700"><?= _profile_donation_address_and_qr ?></b></h4>  
                    <p class="qr-img qr-img2"><div id="btc_address_qr_code" class="width100" style="text-align: center"></div></p>
                    <p class="qr-p5"><span  id="btc_address_text"></span><span class="copy-span"><span class="copy-btn clean" id="copy-address"><img src="img/sherry/copy3.png?version=1.0.18" class="copy-icon3"></span></span></p>
                    <input type="hidden" id="btc_address_hidden_input" name = "btc_address_hidden_input" value="" />
                    <input type="hidden" id="btc_address_id_hidden_input" name = "btc_address_id_hidden_input" value="" />
                    <div class="clear"></div>
                    <p class="qr-p5 sp-p5 normal"><?= _editProfile_attention ?>: <?= _profile_rule ?>
                         
                    </p>                   
                    	<div class="input-container1">
                            <select class="inputa clean2 disabled-input"  name="field_1" id="field_1" required onchange="planSelected(this.value);"><!--remove class disabled-input if this input is not disabled-->
                    			<option disabled selected ><?= _profile_donate_option ?></option>
                    			<option value="A"><?= _profile_plan_a ?></option>
                    			<option value="B"><?= _profile_plan_b ?></option>                                
                			</select>
							<img src="img/sherry/plan-grey.png" class="input-icon"><!--Use img/sherry/plan.png if the it is not disabled-->
                            <p class="register-notice normal"><?= _profile_reminder ?><br><?= _profile_note_transfer_duration ?><br><?= _profile_blockchain_follow ?><br><a class="pop-out-a" onclick="openInvestInfoModal()"><?= _profile_view_tutorial ?></a></p>
                            
                        </div>
                        
                        <!--<div class="input-container2">
                        	<input type="number" class="inputa clean2" required id="field_2" name="invest-btc-amount" placeholder="<?= _profile_keyin_donation_amount ?> (btc)" step="0.001" min="0.025" >
                        	<img src="img/sherry/invest-amount.png" class="input-icon">
                            <p class="register-notice normal"><?//= _profile_correct_amount ?></p>
						</div>-->
                        
<!--                        <div class="input-container2">-->
<!--                        	<input type="text" class="inputa clean2" required id="field_3" name="receiptHashKey" placeholder="--><?//= _profile_transaction_receipt ?><!--">-->
<!--                        	<img src="img/sherry/verify2.png" class="input-icon">-->
<!--                            <p class="register-notice normal">--><?//= _profile_transaction_receipt_rule ?><!--</p>-->
<!--						</div>-->
                        <!--<p class="qr-p5 sp-p5 normal"><?//= _profile_reminder ?>:<br>
                         	<?//= _profile_topup ?>
                    	</p>                           
                        <!--<div class="input-container3">	
                            <select class="inputa clean2"   name="field_3">
                    			<option disabled selected>捐赠备注</option>
                    			<option>想增加收益</option>                                
                    			<option>想加入此计划</option>
                    			<option>想提取收益</option>                                 
                    			<option>其他</option>
                			</select>
                        	<img src="img/sherry/remark.png" class="input-icon">
						</div>
                        <textarea class="textarea-reason" name="" placeholder="如果捐赠备注选其他，需填写"></textarea-->
						<div class="width100 overflow">
                        	<div class="fill-up-space1 modal-fill-space-1"></div>
                        	<button class="register-button2 clean orange-hover invest-btn close2" type="submit" name="invest-form" id="invest-form" ><?= _profile_complete ?></button>
                            <div class="fill-up-space1 modal-fill-space-1"></div>
                         </div>
                         <div class="clear"></div>                           
               </form>            
          </div>
        </div>
     
        <!--- Withdrawal Modal --->
        <div id="myModal3" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
            <span class="close3 ow-close2 close-style">&times;</span>
           
            
            <form class="register-form width100 same-padding" method="POST" id="withdrawForm" onsubmit="return withdrawalValidation();" autocomplete="off">
                <h4 class="btcw-h4 form-h4 ow-form-h4 withdraw-h4"><b class="weight-700" id="withdrawal_title"><?= _profile_withdraw ?></b></h4>
                <div id="paymentOptionMenu" style="display:block;">
                    <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table withdraw-table mb-2">
                        <tr>
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_choose ?></b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top">
                                <h4 class="btcw-h4 edit2-h4">
                                    <select class="inputa clean2 disabled-input no-padding-left"  name="paymentOption" id="paymentOption" required onchange="selectPaymentOption(this.value);"><!--remove class disabled input if it is not disabled-->
                                        <option disabled selected ><?= _profile_choose1 ?></option>
                                        <option value="1"><?= _profile_bitcoin ?></option>
                                        <option value="2"><?= _profile_c2c ?></option>
                                    </select>
                                </h4>
                            </td>
                        </tr>  
                    </table>
                </div>  
                <div id="paymentOption_1" style="display:none;">
                    <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table withdraw-table mb-2">
                        <tr>
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_total_amount ?> (btc)</b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4" id="getValueMax1"></h4></td>
                        </tr>
                        <tr id="hideForCapitalPlanA_1" style="display:table-row;">
                            <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">输入提取总额</b></h4></td> -->
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_keyin_withdraw_amount ?> (btc)</b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top">
                                <h4 class="btcw-h4 edit2-h4">
                                    <input type="number" class="inputa clean2 inputb"  name="opt1_withdrawal_amount" id="opt1_withdrawal_amount" placeholder="<?= _profile_keyin_withdraw_amount ?> (btc)" step="0.0000000001">
                                </h4>
                            </td>
                        </tr>   
                        <tr>
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _bitcoin_transaction_fee ?></b></h4></td>
                            <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Processing Fee</b></h4></td> -->
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4" id="getProcessingFee1"></h4>
                            </td>
                        </tr>
                        <tr>
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_final_withdrawal_amount ?> (btc)</b></h4></td>
                            <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Total Withdrawal Amount(BTC)</b></h4></td> -->
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4" id="getTotalWith1"> </h4>
                            </td>
                        </tr>  
                        <tr>
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_btc_address ?></b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top">
                                <h4 class="btcw-h4 edit2-h4">
                                    <input type="text" class="inputa clean2 inputb"  name="opt1_withdrawal_BTCaddress" id="opt1_withdrawal_BTCaddress" placeholder="<?= _profile_keyin_btc_address ?>" >
                                </h4>
                                <p class="btcw-h4 edit2-h4"><?= _profile_keyin_btc_address_note ?></p>
                            </td>
                        </tr>    

   
                    </table>
                    <input type="hidden" name="getMaximValue1" id="getMaximValue1">
                    <input type="hidden" name="getPercentage1"  id="getPercentage1">
                    <input type="hidden" name="getTotal1"  id="getTotal1">
                                    <!--<input type="checkbox" name="confirmOpt1" id="confirmOpt1" class="pt-3" value="continue">
                                    <p class="btcw-h4 edit2-h4 ml-2 ">*如您填写错误的比特币地址，您所提现的比特币资产将不可找回。</p>-->
                   <div class="clear"></div>                 
                   <div class="checkbox-div">
                        <div class="checkbox1">
                            <input class="form-check-input position-relative-important" type="checkbox" value="" name="confirmOpt1" id="confirmOpt1">
                            <label class="form-check-label profile-checkbox" for="defaultCheck1">
                                *<?= _profile_keyin_btc_address_note ?>
                            </label>
                         </div>
                    </div>            
                    
                    <div class="width100 overflow">
                        <div class="fill-up-space1 modal-fill-space-1"></div>
                            <button class="register-button2 clean orange-hover invest-btn text-center" name="opt1_withdrawal_submit" id="opt1_withdrawal_submit"><?= _profile_confirm_withdraw ?></button>
                            <!-- <button class="register-button2 clean orange-hover invest-btn" name="opt1_withdrawal_submit" id="opt1_withdrawal_submit" >Confirm Withdrawal</button> -->
                        <div class="fill-up-space1 modal-fill-space-1"></div>
                    </div>
                </div>
                <div id="paymentOption_2" style="display:none;">
                    <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table withdraw-table mb-2">
                        <!-- <tr id="hideForCapitalPlanA_2" style="display:none;"> -->
                        <tr>
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_can_withdraw_amount ?> (btc)</b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4" id="getValueMax2"></h4></td>
                        </tr>
                        <tr id="hideForCapitalPlanA_3" style="display:none;">
                            <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">手续费</b></h4></td> -->
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _bitcoin_transaction_fee ?></b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4" id="getProcessingFee2"></h4>
                            </td>
                        </tr>
                        <tr id="hideForCapitalPlanA_4" style="display:none;">
                            <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">最终可提取总额(btc)</b></h4></td> -->
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_final_withdrawal_amount ?>(btc)</b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4" id="getTotalWith2"> </h4>
                            </td>
                        </tr>  
                        <tr>
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_reg_bank_name ?></b></h4></td>
                            <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Full Name</b></h4></td> -->
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top">
                                <h4 class="btcw-h4 edit2-h4">
                                    <input type="text" class="inputa clean2 inputb"  name="opt2_withdrawal_fullName" id="opt2_withdrawal_fullName" placeholder="<?= _profile_keyin_name ?>" >
                                </h4>
                            </td>
                        </tr>  
                        <tr>
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _general_country ?></b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top">
                                <h4 class="btcw-h4 edit2-h4">
                                    <input type="text" class="inputa clean2 inputb"  name="opt2_withdrawal_country" id="opt2_withdrawal_country" placeholder="<?= _profile_keyin_country ?>" >
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">银行名字</b></h4></td> -->
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_bank_name ?></b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top">
                                <h4 class="btcw-h4 edit2-h4">
                                    <input type="text" class="inputa clean2 inputb"  name="opt2_withdrawal_bankName" id="opt2_withdrawal_bankName" placeholder="<?= _profile_keyin_bank_name ?>" >
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">银行帐户号码</b></h4></td> -->
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_bank_acc_no ?></b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top">
                                <h4 class="btcw-h4 edit2-h4">
                                    <input type="text" class="inputa clean2 inputb"  name="opt2_withdrawal_bankAccountNo" id="opt2_withdrawal_bankAccountNo" placeholder="<?= _profile_keyin_bank_acc_no ?>" >
                                </h4>
                            </td>
                        </tr> 
                        <tr id="hideForCapitalPlanA_5" style="display:table-row;">
                            <!-- <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">输入提取总额</b></h4></td> -->
                            <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700"><?= _profile_withdrawal_amount2 ?>(btc)</b></h4></td>
                            <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                            <td class="third-td2" width="58%" valign="top">
                                <h4 class="btcw-h4 edit2-h4">
                                    <input type="number" class="inputa clean2 inputb"  name="opt2_withdrawal_amount" id="opt2_withdrawal_amount" placeholder="<?= _profile_keyin_amount2 ?>" step="0.0000000001">
                                </h4>
                                <p class="btcw-h4 edit2-h4"><?= _profile_withdrawal_amount2_note ?></p>
                            </td>
                        </tr>    
                    </table>
                    <input type="hidden" name="getMaximValue2" id="getMaximValue2">
                    <input type="hidden" name="getPercentage2"  id="getPercentage2">
                    <input type="hidden" name="getTotal2"  id="getTotal2">
                   <div class="clear"></div>                 
                   <div class="checkbox-div">
                        <div class="checkbox1">
                            <input class="form-check-input position-relative-important" type="checkbox" value=""  name="confirmOpt2" id="confirmOpt2">
                            <label class="form-check-label profile-checkbox" for="confirmOpt2">
                                *<?= _profile_correct_info_note ?>
                            </label>
                         </div>
                    </div> 
                    <div class="width100 overflow">
                        <div class="fill-up-space1 modal-fill-space-1"></div>
                            
                            <button class="register-button2 clean orange-hover invest-btn text-center" name="opt2_withdrawal_submit" id="opt2_withdrawal_submit" ><?= _profile_confirm_withdraw ?></button>
                            <!-- <div class="register-button2 clean orange-hover invest-btn" name="opt2_withdrawal_submit" id="opt2_withdrawal_submit" >Confirm Withdrawal</div> -->
                        <div class="fill-up-space1 modal-fill-space-1"></div>
                    </div>
                </div>
            </form>            
          </div>
        </div>
        
        <!--- Plan A Modal --->
        <div id="myModal4" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width2 dark-bg">
            <span class="close4 ow-close4 close-style">&times;</span>
            <h1 class="btcw-h1 text-center plan-a-h1"><?= _profile_plan_a ?></h1>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4"><?= _profile_first_donation ?></h4>
            <div class="width100 same-padding">
            	<div class="width-two-column1 float-left">
                	<img src="img/sherry/bitcoin-stack1.png" class="two-column-img" alt="<?= _profile_min_donation ?>" title="<?= _profile_min_donation ?>">
                    <p class="two-column-p weight-700"><?= _profile_min_donation ?></p>
                    <p class="two-column-p">0.025 btc</p>
                </div>
            	<div class="width-two-column2 float-left">
                	<img src="img/sherry/bitcoin-stack2.png" class="two-column-img" alt="<?= _profile_max_donation ?>" title="<?= _profile_max_donation ?>">
                    <p class="two-column-p weight-700"><?= _profile_max_donation ?></p>
                    <p class="two-column-p">0.2 btc</p>
                </div>                
            </div>
            <div class="clear"></div>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4"><?= _profile_one_become_three ?></h4>
            <div class="width100 same-padding">
                	<img src="img/sherry/one-to-three.png" class="width-special" alt="<?= _profile_one_become_three ?>" title="<?= _profile_one_become_three ?>">
              
            </div>
            <div class="clear"></div>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4"><?= _profile_terms ?></h4>
            <div class="width100 same-padding four-column-container">
            	<div class="four-column-div">
                	<img src="img/sherry/bitcoin.png" class="four-column-img" alt="<?= _profile_join_and_earn ?>0.05 BTC" title="<?= _profile_join_and_earn ?>0.05 BTC">
                    <p class="two-column-p weight-700"><?= _profile_join_and_earn ?>0.05 BTC</p>
                </div>
            	<div class="four-column-div middle-four second-four">
                	<img src="img/sherry/five-people.png" class="four-column-img" alt="<?= _profile_refer_5_earn ?>0.05 BTC" title="<?= _profile_refer_5_earn ?>0.05 BTC">
                    <p class="two-column-p weight-700"><?= _profile_refer_5_earn ?>0.05 BTC</p>
                </div>   
            	<div class="four-column-div middle-four">
                	<img src="img/sherry/profit.png" class="four-column-img" alt="<?= _profile_donate_and_earn ?>" title="<?= _profile_donate_and_earn ?>">
                    <p class="two-column-p weight-700"><?= _profile_donate_and_earn ?></p>
                </div>
            	<div class="four-column-div second-four">
                	<img src="img/sherry/invest.png" class="four-column-img" alt="<?= _profile_interest ?>" title="<?= _profile_interest ?>">
                    <p class="two-column-p weight-700"><?= _profile_interest_note ?></p>
                </div>                            
            </div>
            <div class="clear"></div>            
            <div class="width100 same-padding four-column-container">
            	<div class="four-column-div en-css">
                	<img src="img/sherry/transaction-fee.png" class="four-column-img" alt="<?= _profile_daily02 ?>" title="<?= _profile_daily02 ?>">
                    <p class="two-column-p weight-700"><?= _profile_daily02_desc ?></p>
                </div>
            	<div class="four-column-div middle-four second-four en-css">
                	<img src="img/sherry/time.png" class="four-column-img" alt="<?= _profile_ten_days ?>" title="<?= _profile_ten_days ?>">
                    <p class="two-column-p weight-700"><?= _profile_ten_days ?></p>
                </div>   
            	<div class="four-column-div middle-four">
                	<img src="img/sherry/low-risk.png" class="four-column-img" alt="<?= _profile_ten_levels ?>" title="<?= _profile_ten_levels ?>">
                    <p class="two-column-p weight-700"><?= _profile_ten_levels ?></p>
                </div>
            	<div class="four-column-div second-four">
                	<img src="img/sherry/profit.png" class="four-column-img" alt="<?= _profile_can_withdraw ?>" title="<?= _profile_can_withdraw ?>">
                    <p class="two-column-p weight-700"><?= _profile_can_withdraw_note ?></p>
                </div>                            
            </div>        
            <div class="clear"></div>    
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4"><?= _profile_community_com_income ?></h4>
            <div class="width100 same-padding two-div-container overflow-hidden">
				<div class="width50-left-div">
                	<table class="simple-table">
                    	<tr>
                        	<td class="simple1">1. </td>
                            <td class="simple2"><?= _profile_1st_level ?></td>
                        </tr>
                    	<tr>
                        	<td class="simple1">2. </td>
                            <td class="simple2"><?= _profile_2nd_level ?></td>
                        </tr> 
                    	<tr>
                        	<td class="simple1">3. </td>
                            <td class="simple2"><?= _profile_3rd_level ?></td>
                        </tr>                        
                    	<tr>
                        	<td class="simple1">4. </td>
                            <td class="simple2"><?= _profile_4th_level ?></td>
                        </tr>
                    	<tr>
                        	<td class="simple1">5. </td>
                            <td class="simple2"><?= _profile_5th_level ?></td>
                        </tr> 
                    	<tr>
                        	<td class="simple1">6. </td>
                            <td class="simple2"><?= _profile_6th_level ?></td>
                        </tr>                         
                    	<tr>
                        	<td class="simple1">7. </td>
                            <td class="simple2"><?= _profile_7th_level ?></td>
                        </tr>                        
                    	<tr>
                        	<td class="simple1">8. </td>
                            <td class="simple2"><?= _profile_8th_level ?></td>
                        </tr>
                    	<tr>
                        	<td class="simple1">9. </td>
                            <td class="simple2"><?= _profile_9th_level ?></td>
                        </tr> 
                    	<tr>
                        	<td class="simple1">10. </td>
                            <td class="simple2"><?= _profile_10th_level ?></td>
                        </tr>                                           
                    </table>
                </div>
				<div class="width50-right-div">
                	<img src="img/indexIntro.png" class="width100" alt="BTCW 比特基金" title="BTCW 比特基金">
                </div>                            
            </div>            
  			<div class="clear"></div>
            <div class="width100 same-padding overflow-hidden">
                <div class="fill-up-space1 ow-fill-up"></div>
                <div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center invest-now" id="clickInvestPlanA"><?= _profile_invest_now ?></div>
                <div class="fill-up-space1 ow-fill-up"></div>
            </div>            
            <div class="width100 same-padding copy-button-div">
              <div class="fill-up-space1 ow-height"></div>
              <button class="yellow-line-box yellow-box-width yellow-box-hover white-text text-center clean copy-yellow-line plan-b"><?= _profile_learn_plan_b ?></button>
            <div class="fill-up-space1 ow-height"></div>
           </div>                         
         </div>
       </div>        
 
        
        <!--- Plan B Modal --->
        <div id="myModal5" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width2 dark-bg">
            <span class="close5 ow-close4 close-style">&times;</span>
            <h1 class="btcw-h1 text-center plan-a-h1"><?= _profile_plan_b ?></h1>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4"><?= _profile_donation1 ?></h4>
            <div class="width100 same-padding">
            	<div class="width-two-column1 float-left">
                	<img src="img/sherry/bitcoin-stack1.png" class="two-column-img" alt="<?= _profile_min_donation ?>" title="<?= _profile_min_donation ?>">
                    <p class="two-column-p weight-700"><?= _profile_min_donation ?></p>
                    <p class="two-column-p">0.025 btc</p>
                </div>
            	<div class="width-two-column2 float-left">
                	<img src="img/sherry/bitcoin-stack2.png" class="two-column-img" alt="<?= _profile_max_donation ?>" title="<?= _profile_max_donation ?>">
                    <p class="two-column-p weight-700"><?= _profile_max_donation ?></p>
                    <p class="two-column-p">50 btc</p>
                </div>                
            </div>
            <div class="clear"></div>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4"><?= _profile_one_become_eight ?></h4>
            <div class="width100 same-padding">
                	<img src="img/sherry/one-to-seven.png" class="width-special" alt="<?= _profile_one_become_eight ?>" title="<?= _profile_one_become_eight ?>">
              
            </div>            
            <div class="clear"></div>
            <h4 class="btcw-h4 text-center weight-700 plan-a-h4"><?= _profile_example ?></h4>
            <div class="width100 same-padding two-div-container overflow-hidden">
				<table class="simple-table">
                    	<tr>
                        	<td class="simple1">1. </td>
                            <td class="simple2"><?= _profile_planb1 ?></td>
                        </tr>    
                     	<tr>
                        	<td class="simple1">2. </td>
                            <td class="simple2"><?= _profile_planb2 ?></td>
                        </tr>                       
                     	<tr>
                        	<td class="simple1">3. </td>
                            <td class="simple2"><?= _profile_planb3 ?></td>
                        </tr>  
                    	<tr>
                        	<td class="simple1">4. </td>
                            <td class="simple2"><?= _profile_planb4 ?></td>
                        </tr>    
                     	<tr>
                        	<td class="simple1">5. </td>
                            <td class="simple2"><?= _profile_planb5 ?></td>
                        </tr>                       
                     	<tr>
                        	<td class="simple1">6. </td>
                            <td class="simple2"><?= _profile_planb6 ?></td>
                        </tr>  
                     	<tr>
                        	<td class="simple1">7. </td>
                            <td class="simple2"><?= _profile_planb7 ?></td>
                        </tr> 
                      	<tr>
                        	<td class="simple1">8. </td>
                            <td class="simple2"><?= _profile_planb8 ?></td>
                        </tr>                                              
                </table>                                                               	
            </div>
            <div class="clear"></div>   

           
  			<div class="clear"></div>
            <!--<div class="width100 same-padding overflow-hidden">
                <div class="fill-up-space1 ow-fill-up"></div>
                <div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center invest-now">马上捐赠</div>
                <div class="fill-up-space1 ow-fill-up"></div>
            </div>--> 
            
            <div class="width100 same-padding overflow-hidden">
                <div class="fill-up-space1 ow-fill-up"></div>
                <div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center invest-now" id="clickInvestPlanB"><?= _profile_invest_now ?></div>
                <div class="fill-up-space1 ow-fill-up"></div>
            </div>                         
            <div class="width100 same-padding copy-button-div">
              <div class="fill-up-space1 ow-height"></div>
              <button class="yellow-line-box yellow-box-width yellow-box-hover white-text text-center clean copy-yellow-line plan-a"><?= _profile_learn_plan_a ?></button>
            <div class="fill-up-space1 ow-height"></div>
           </div>                         
         </div>
       </div>         
 

 <!---  End of Sherry coded part-->
        
  <!-- ****************************************************MODALS***********************************************************-->
<!--  <div class="modal fade" id="withdrawModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--      <div class="modal-dialog" role="document">-->
<!--          <div class="modal-content">-->
<!--              <div class="modal-header">-->
<!--                  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>-->
<!--                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--                      <span aria-hidden="true">&times;</span>-->
<!--                  </button>-->
<!--              </div>-->
<!--              <div class="modal-body">-->
<!--                  ...-->
<!--              </div>-->
<!--              <div class="modal-footer">-->
<!--                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
<!--                  <button type="button" class="btn btn-primary">Save changes</button>-->
<!--              </div>-->
<!--          </div>-->
<!--      </div>-->
<!--  </div>-->
   <div id="myModal10" class="modal2 ow-modal2 ow-modal10">
 
          <div class="modal-content2 modal-content-white ow-modal-width ow-modal10a">
            <h4 class="btcw-h4 text-center ow10-h4"><?= _profile_link_copied ?></h4>
          </div>
   </div>

<!--	<div class="width100 element-div extra-padding-bottom profile-divider">-->
<!--        	<img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">-->
<!--    </div>    -->
	
        <!--- Reconfirm Invest Modal --->
        <div id="myModal17" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
            <span class="close17 ow-close2 close-style">&times;</span>
            <h4 class="reconfirm-h4"><b class="weight-700">捐赠计划 : </b>echo here plan A or B 计划A</h4>
            <h4 class="reconfirm-h4"><b class="weight-700">输入捐赠数量(btc) : </b>echo here the invest amount 1</h4>
            <h4 class="reconfirm-h4"><b class="weight-700">转账收据 : </b>echo here the receipt wqewq2qafqe32edwafqrqqrr3wfqfq2erqqrq2r</h4> 
            
            <p class="text-center"><button class="register-button2 clean orange-hover complete-btn button-y-width close17" >完成</button></p>
            <p class="text-center"><button class="register-button2 clean orange-hover complete-btn button-y-width copy-yellow-line3" >返回</button></p>                
          </div>
        </div>

         <!--- Re-Invest Modal --->
         <div id="myModal18" class="modal2 ow-modal2">
        
        <!-- Modal content -->
        <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
          <span class="close18 ow-close2 close-style">&times;</span>
        
          <h4 class="reconfirm-h4 mb-5 text-center bigger-title"><b class="weight-700"><?= _profile_confirm_donate ?></h4>
          <form method="POST" autocomplete="off">
              <p class="text-center mt-5"><button class="register-button2 clean orange-hover complete-btn button-y-width close18" id="reinvestYes" name="reinvestYes"><?= _profile_confirm_yes ?></button></p>
              <p class="text-center"><button class="register-button2 clean orange-hover complete-btn button-y-width copy-yellow-line3" id="reinvestNo" name="reinvestNo"><?= _profile_confirm_no ?></button></p>                
          </form>
        </div>
      </div>

       <!--- Cancel Re-Invest Modal --->
       <div id="myModal19" class="modal2 ow-modal2">
        
        <!-- Modal content -->
        <div class="modal-content2 modal-content-white ow-modal-width dark-bg">
          <span class="close19 ow-close2 close-style">&times;</span>
         
          <h4 class="reconfirm-h4 mb-5 bigger-title text-center"><b class="weight-700"><?= _profile_confirm_stop_auto ?></h4>
          <form method="POST" autocomplete="off">
              <p class="text-center mt-5"><button class="register-button2 clean orange-hover complete-btn button-y-width close19" id="reinvestCancelYes" name="reinvestCancelYes"><?= _profile_confirm_yes ?></button></p>
              <p class="text-center"><button class="register-button2 clean orange-hover complete-btn button-y-width copy-yellow-line3" id="reinvestCancelNo" name="reinvestCancelNo"><?= _profile_confirm_no ?></button></p>                
          </form>
        </div>
      </div>
    <?php generateSimpleModal();?>
  <!-- ****************************************************MODALS***********************************************************-->

  <script src="js/jquery.qrcode.min.js" type="text/javascript"></script>

  <script>
      function randomFunc(type) {
          // Get the modal
          var modal = document.getElementById(type + '-modal');

          // Get the <span> element that closes the modal
          var span = document.getElementsByClassName('closeNoticeModal')[0];
          var span1 = document.getElementsByClassName('closeNoticeModal')[1];

          modal.style.display = 'block';

          // When the user clicks on <span> (x), close the modal
          span.onclick = function() {
              modal.style.display = 'none';
          };
          span1.onclick = function() {
              modal.style.display = 'none';
          };

          // When the user clicks anywhere outside of the modal, close it
          window.onclick = function(event) {
              if (event.target == modal) {
                  modal.style.display = 'none';
              }
          }
      }

  

      $("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#invest-now-referral-link').attr("href");
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
      });
  </script>
          <script>
			// Get the modal
			var investInfoModal = document.getElementById('investInfoModal');
			var modal2 = document.getElementById('myModal2');
            var modal3 = document.getElementById('myModal3');
            var modal4 = document.getElementById('myModal4');
            var modal5 = document.getElementById('myModal5');
            var modal16 = document.getElementById('myModal16');
            var modal18 = document.getElementById('myModal18');
            var modal19 = document.getElementById('myModal19');
			
			// Get the button that opens the modal
			var btn2 = document.getElementsByClassName("invest-now")[0];
			var btn2a = document.getElementById("reinvest_1");		
			var btn2b = document.getElementById("reinvest_2");	
			var btn2c = document.getElementById("reinvest_3");	
			var btn2d = document.getElementById("reinvest_4");		
			var btn2e = document.getElementById("reinvest_5");

            var clickInvestPlanA = document.getElementById("clickInvestPlanA");
            var clickInvestPlanB = document.getElementById("clickInvestPlanB");

            var cancelReinvest_1 = document.getElementById("cancelReinvest_1");
            var cancelReinvest_3 = document.getElementById("cancelReinvest_3");
            var cancelReinvest_5 = document.getElementById("cancelReinvest_5");	

            var reinvestYes = document.getElementById("reinvestYes");
            var reinvestNo = document.getElementById("reinvestNo");

            var reinvestCancelYes = document.getElementById("reinvestCancelYes");
            var reinvestCancelNo = document.getElementById("reinvestCancelNo");

            var withdrawal_title = document.getElementById("withdrawal_title");
							
			var btn3 = document.getElementById("withdrawal_1");
			var btn3a = document.getElementById("withdrawal_2");	
			var btn3b = document.getElementById("withdrawal_3");
			var btn3d = document.getElementById("withdrawal_4");
			var btn3e = document.getElementById("withdrawal_5");
            var btn3f = document.getElementById("withdrawal_6");

            var opt1_withdrawal_submit = document.getElementById("opt1_withdrawal_submit");
            var opt2_withdrawal_submit = document.getElementById("opt2_withdrawal_submit");

            var btn4 = document.getElementsByClassName("plan-a")[0];
			var btn4a = document.getElementsByClassName("plan-a")[1];	

			var btn5 = document.getElementsByClassName("plan-b")[0];
			var btn5a = document.getElementsByClassName("plan-b")[1];	
			
            var investInfoSpan = document.getElementsByClassName("close2")[0];
            var span2 = document.getElementsByClassName("close2")[1];
			var span3 = document.getElementsByClassName("close3")[0];
			var span4 = document.getElementsByClassName("close4")[0];
            var span5 = document.getElementsByClassName("close5")[0];

			var span16 = document.getElementsByClassName("close16")[0];
			var span16a = document.getElementsByClassName("close16")[1];

            var span18 = document.getElementsByClassName("close18")[0];
            var span18a = document.getElementsByClassName("close19")[0];
            
            //for capital A withdrawal
            var hideForCapitalPlanA_1 = document.getElementById("hideForCapitalPlanA_1");
            // var hideForCapitalPlanA_2 = document.getElementById("hideForCapitalPlanA_2");
            var hideForCapitalPlanA_3 = document.getElementById("hideForCapitalPlanA_3");
            var hideForCapitalPlanA_4 = document.getElementById("hideForCapitalPlanA_4");
            var hideForCapitalPlanA_5 = document.getElementById("hideForCapitalPlanA_5");

            // Maximum Amount
            var getValueMax1 = document.getElementById("getValueMax1");
            var getValueMax2 = document.getElementById("getValueMax2");
            // Get Processing Fee 
            var getProcessingFee1 = document.getElementById("getProcessingFee1");
            var getProcessingFee2 = document.getElementById("getProcessingFee2");

            var getTotalWith1 = document.getElementById("getTotalWith1");
            var getTotalWith2 = document.getElementById("getTotalWith2");

            // Get Processing Fee 
            var getMaximValue1 = document.getElementById("getMaximValue1");
            var getPercentage1 = document.getElementById("getPercentage1");
            var getTotal1 = document.getElementById("getTotal1");

            var getMaximValue2 = document.getElementById("getMaximValue2");
            var getPercentage2 = document.getElementById("getPercentage2");
            var getTotal2 = document.getElementById("getTotal2");


              <?php
				  if(isset($errorMsg) || isset($successMsg)){
                      promptAlertMsg($errorMsg,$successMsg);
				  }
              ?>
			
            if(clickInvestPlanA){
                clickInvestPlanA.onclick = function() {
                    // modal2.style.display = "block";
                    investInfoModal.style.display = "block";
                    modal4.style.display = "none";
                };
            }
            if(clickInvestPlanB){
                clickInvestPlanB.onclick = function() {
                    // modal2.style.display = "block";
                    investInfoModal.style.display = "block";
                    modal5.style.display = "none";
                };
            }

            if(btn2){
                btn2.onclick = function() {
                    // modal2.style.display = "block";
                    investInfoModal.style.display = "block";
                };
            }
            if(btn2a){
                btn2a.onclick = function() {
                    modal4.style.display = "none";
                    modal18.style.display = "block";
                    reinvestYes.value = "1";
                };
            }

            if(btn2b){
                btn2b.onclick = function() {
                    modal5.style.display = "none";
                    modal18.style.display = "block";
                    reinvestYes.value = "2";
                };
            }

            if(btn2c){
                btn2c.onclick = function() {
                    modal5.style.display = "none";
                    modal18.style.display = "block";
                    reinvestYes.value = "3";
                };
            }

            if(btn2d){
                btn2d.onclick = function() {
                    modal4.style.display = "none";
                    modal18.style.display = "block";
                    reinvestYes.value = "4";
                };
            }

            if(btn2e){
                btn2e.onclick = function() {
                    modal5.style.display = "none";
                    modal18.style.display = "block";
                    reinvestYes.value = "5";
                };
            }
            
            if(cancelReinvest_1){
                cancelReinvest_1.onclick = function() {
                    modal5.style.display = "none";
                    modal19.style.display = "block";
                    reinvestCancelYes.value = "1";
                };
            }
            if(cancelReinvest_3){
                cancelReinvest_3.onclick = function() {
                    modal5.style.display = "none";
                    modal19.style.display = "block";
                    reinvestCancelYes.value = "3";
                };
            }
            if(cancelReinvest_5){
                cancelReinvest_5.onclick = function() {
                    modal5.style.display = "none";
                    modal19.style.display = "block";
                    reinvestCancelYes.value = "5";
                };
            }
            
            if(btn3){
                btn3.onclick = function() {
                    modal3.style.display = "block";

                    let totalCapitalBtc = parseFloat('<?php echo removeUselessZero($totalCapitalBtc);?>');
                    let percent = parseFloat('<?php echo removeUselessZero($planA->getWithdrawalPercent());?>');
                    let total_withdrawable = totalCapitalBtc - (totalCapitalBtc * (percent / 100));

                    withdrawal_title.innerHTML = "<?= _profile_withdraw ?>";

                    hideForCapitalPlanA_1.style.display = "none";
                    // hideForCapitalPlanA_2.style.display = "table-row";
                    hideForCapitalPlanA_3.style.display = "table-row";
                    hideForCapitalPlanA_4.style.display = "table-row";
                    hideForCapitalPlanA_5.style.display = "none";

                    opt1_withdrawal_submit.value = "1";
                    opt2_withdrawal_submit.value = "1";

                    getValueMax1.innerHTML = totalCapitalBtc+" BTC";
                    getValueMax2.innerHTML = totalCapitalBtc+" BTC";

                    getProcessingFee1.innerHTML= percent +" %";
                    getProcessingFee2.innerHTML= percent +" %";

                    getTotalWith1.innerHTML= total_withdrawable +" BTC";
                    getTotalWith2.innerHTML= total_withdrawable +" BTC";


                    getMaximValue1.value = totalCapitalBtc;
                    getMaximValue2.value = totalCapitalBtc;

                    getPercentage1.value = percent;
                    getPercentage2.value = percent;

                    getTotal1.value = total_withdrawable;
                    getTotal2.value = total_withdrawable;
                };
            }

            if(btn3a){
                btn3a.onclick = function() {
                    putVal('<?php echo removeUselessZero($totalReleasedBtc);?>','<?php echo 0;?>',"2");
                };
            }

			if(btn3b){
                btn3b.onclick = function() {
                    putVal('<?php echo removeUselessZero($totalReferralCommissionReceived);?>','<?php echo 0;?>',"3");
                };
            }

            if(btn3d){
                btn3d.onclick = function() {
                    putVal('<?php echo $totalReleasedBtcFasterPlanB;?>','<?php echo 0;?>',"4");
                };
            }

            if(btn3e){
                btn3e.onclick = function() {
                    putVal('<?php echo $totalReferralCommissionPlanB;?>','<?php echo 0;?>',"5");
                };
            }
			
            if(btn3f){
                btn3f.onclick = function() {
                    putVal('<?php echo $totalReleasedBtcNormalPlanB;?>','<?php echo 0;?>',"6");
                };
            }
			
			if(btn4){
                btn4.onclick = function() {
                    modal4.style.display = "block";
                };
            }

			if(btn4a){
                btn4a.onclick = function() {
                    modal5.style.display = "none";
                    modal4.style.display = "block";
                };
            }
            	
            if(btn5){
                btn5.onclick = function() {
                    modal5.style.display = "block";
                };
            }

			if(btn5a){
                btn5a.onclick = function() {
                    modal4.style.display = "none";
                    modal5.style.display = "block";
                };
            }		

			if(span2){
                span2.onclick = function() {
                    modal2.style.display = "none";
                };
            }
            if(investInfoSpan){
                investInfoSpan.onclick = function() {
                    investInfoModal.style.display = "none";
                };
            }
			if(span3){
                span3.onclick = function() {
                    document.getElementById('paymentOptionMenu').style.display = 'block';
                    document.getElementById('paymentOption_1').style.display = 'none';
                    document.getElementById('paymentOption_2').style.display = 'none';
                    document.getElementById("paymentOption").selectedIndex = "0";
                    hideForCapitalPlanA_1.style.display = "none";
                    // hideForCapitalPlanA_2.style.display = "table-row";
                    hideForCapitalPlanA_3.style.display = "table-row";
                    hideForCapitalPlanA_4.style.display = "table-row";
                    hideForCapitalPlanA_5.style.display = "none";
                    modal3.style.display = "none";
                    opt1_withdrawal_submit.value = "";
                    opt2_withdrawal_submit.value = "";
                    document.getElementById("withdrawForm").reset();
                };
            }

			if(span4){
                span4.onclick = function() {
                    modal4.style.display = "none";
                };
            }

			if(span5){
                span5.onclick = function() {
                    modal5.style.display = "none";
                };
            }

			if(span16){
                span16.onclick = function() {
                    modal16.style.display = "none";
                };
            }
            
			if(span16a){
                span16a.onclick = function() {
                    modal16.style.display = "none";
                };
            }	

            if(span18){
                span18.onclick = function() {
                    modal18.style.display = "none";
                };
            }
            if(span18a){
                span18a.onclick = function() {
                    modal19.style.display = "none";
                };
            }			
            
			window.onclick = function(event) {
			  if (event.target == modal2) {
				modal2.style.display = "none";
			  }
                if (event.target == investInfoModal) {
                    investInfoModal.style.display = "none";
                }
			  if (event.target == modal3) {
                document.getElementById('paymentOptionMenu').style.display = 'block';
                document.getElementById('paymentOption_1').style.display = 'none';
                document.getElementById('paymentOption_2').style.display = 'none';
                document.getElementById("paymentOption").selectedIndex = "0";
                hideForCapitalPlanA_1.style.display = "none";
                // hideForCapitalPlanA_2.style.display = "table-row";
                hideForCapitalPlanA_3.style.display = "table-row";
                hideForCapitalPlanA_4.style.display = "table-row";
                hideForCapitalPlanA_5.style.display = "none";
                opt1_withdrawal_submit.value = "";
                opt2_withdrawal_submit.value = "";
                document.getElementById("withdrawForm").reset();
                modal3.style.display = "none";
			  }
			  if (event.target == modal4) {
				modal4.style.display = "none";
			  }
			  if (event.target == modal5) {
				modal5.style.display = "none";
			  }
			  if (event.target == modal5) {
				modal16.style.display = "none";
			  }
              if (event.target == modal18) {
				modal18.style.display = "none";
			  }
              if (event.target == modal19) {
				modal19.style.display = "none";
              }
			}

            function putVal(maxVal,perCent,buttonValue)
            {
                modal3.style.display = "block";

                if(maxVal == "-")
                {
                    maxVal = 0;
                }

                let totalReleasedBtc = parseFloat(maxVal);
                let percent = parseFloat(perCent);
                let total_withdrawable = totalReleasedBtc - (totalReleasedBtc * (percent / 100));

                withdrawal_title.innerHTML = "<?= _profile_withdrawal1 ?>";
                getValueMax1.innerHTML = totalReleasedBtc+" BTC";
                getMaximValue1.value = totalReleasedBtc;

                getValueMax2.innerHTML = totalReleasedBtc+" BTC";
                getMaximValue2.value = totalReleasedBtc;

                getProcessingFee1.innerHTML= percent +" %";
                getPercentage1.value = percent;
                getPercentage2.value = percent;

                getTotalWith1.innerHTML= total_withdrawable +" BTC";
                getTotal1.value = total_withdrawable;
                getTotal2.value = total_withdrawable;

                opt1_withdrawal_submit.value = buttonValue;
                opt2_withdrawal_submit.value = buttonValue;

                hideForCapitalPlanA_1.style.display = "table-row";
                // hideForCapitalPlanA_2.style.display = "none";
                hideForCapitalPlanA_3.style.display = "none";
                hideForCapitalPlanA_4.style.display = "none";
                hideForCapitalPlanA_5.style.display = "table-row";
            }
		</script>
  <!--      
         <script>
			// Get the modal
			var modal10 = document.getElementById('myModal10');
			
			// Get the button that opens the modal
			var btn10 = document.getElementById("copy-referral-link");
            
			// When the user clicks the button, open the modal
			btn10.onclick = function() {
			  modal10.style.display = "block";
                setTimeout(closeCopiedModalWithDelay, 1600);
			};


			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal10) {
				modal10.style.display = "none";
			  }
			};

            function closeCopiedModalWithDelay() {
                modal10.style.display = "none";
            }
		</script>  -->
  
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
            }
    
            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
            
            /*close when you click outside of the sideNav*/
            $('body').on('click touchstart', function(){
              if( parseInt( $('#mySidenav').css('width') ) > 0 ){
                closeNav();
              }
            });
           /*$('body').on('click', function(){
           if( parseInt( $('#mySidenav').css('width') ) > 0 ){
           closeNav();
           }
           });*/
	</script>
    
    <script>
var hoursContainer = document.querySelector('.hours');
var minutesContainer = document.querySelector('.minutes');
var secondsContainer = document.querySelector('.seconds');
var tickElements = Array.from(document.querySelectorAll('.tick'));

var last = new Date(0);
last.setUTCHours(-1);

var currentTimeInMs = "<?php echo $currentUtcTimeInMs; ?>";
function updateTime () {
  currentTimeInMs = parseInt(currentTimeInMs,10) + 1000;
  var now = new Date(parseInt(currentTimeInMs,10)).toLocaleString("en-US", {timeZone: "America/New_York"});

  if(!now || now === null || now.length <= 0 || now === "Na" || now === "NaN" || now.length <= 10){
      var d = new Date();
      var offset = d.getTimezoneOffset() / 60;
      offset = 60 * 60 * offset * 1000;
      now = new Date(parseInt(currentTimeInMs,10) - (60 * 60 * 4 * 1000) + offset);
  }else{
      now = new Date(now);
  }

  var lastHours = last.getHours().toString();
  var nowHours = now.getHours().toString();
  if (lastHours !== nowHours) {
      if(now.getHours() > 12){
          nowHours = (now.getHours() - 12).toString();
      }else if(now.getHours() === 0){
          nowHours = "12";
      }
    updateContainer(hoursContainer, nowHours)
  }
  
  var lastMinutes = last.getMinutes().toString();
  var nowMinutes = now.getMinutes().toString();
  if (lastMinutes !== nowMinutes) {
    updateContainer(minutesContainer, nowMinutes)
  }
  
  // var lastSeconds = last.getSeconds().toString();
  var lastSeconds = last.getHours() < 12? 'am' : 'pm';
  // var nowSeconds = now.getSeconds().toString();
  var nowSeconds = now.getHours() < 12? 'am' : 'pm';
  // if (lastSeconds !== nowSeconds) {
  //   //tick()
  //   updateContainer(secondsContainer, nowSeconds);
  // }
  updateContainer(secondsContainer, nowSeconds);

  last = now
}

function tick () {
  tickElements.forEach(t => t.classList.toggle('tick-hidden'));
}

function updateContainer (container, newTime) {
  var time = newTime.split('');
  
  if (time.length === 1) {
    time.unshift('0')
  }
  
  
  var first = container.firstElementChild;
  if (first.lastElementChild.textContent !== time[0]) {
    updateNumber(first, time[0])
  }
  
  var last = container.lastElementChild;
  if (last.lastElementChild.textContent !== time[1]) {
    updateNumber(last, time[1])
  }
}

function updateNumber (element, number) {
  //element.lastElementChild.textContent = number
  var second = element.lastElementChild.cloneNode(true);
  second.textContent = number;
  
  element.appendChild(second);
  element.classList.add('move');

  setTimeout(function () {
    element.classList.remove('move')
  }, 990)
  setTimeout(function () {
    element.removeChild(element.firstElementChild)
  }, 990)
}

setInterval(updateTime, 1000);

//random number generator
var max = 61195;
var min = 29894;
$("#user-online-count-span").html(Math.floor(Math.random()*(max-min+1)+min));

</script>
<script>
function selectPaymentOption(paymentOption)
{
    if(paymentOption == 1)
    {
        document.getElementById('paymentOption_1').style.display = 'block';
        document.getElementById('paymentOption_2').style.display = 'none';
        // document.getElementById("withdrawForm").reset();
    }
    else if(paymentOption == 2)
    {
        document.getElementById('paymentOption_1').style.display = 'none';
        document.getElementById('paymentOption_2').style.display = 'block';
        // document.getElementById("withdrawForm").reset();
    }
}
function planSelected(value)
{
    if(value == "A")
    {
        document.getElementById("field_2").max = 0.2;
    }
    if(value == "B")
    {
        document.getElementById("field_2").max = 50;
    }
}
function withdrawalValidation()
{
    let errorArray = [];
    let canValidateLevel_2 = true;
    let combinationArray =[errorArray,canValidateLevel_2];

    let paymentOp = $('#paymentOption').val();

    if(paymentOp != null)
    {
        if(paymentOp == 1)
        {
            let opt1_withdrawal_submit = $('#opt1_withdrawal_submit').val();
            if(opt1_withdrawal_submit == 1)//for plan A Capital
            {
                let btcAddress = $('#opt1_withdrawal_BTCaddress').val();
                let confirmOpt1 = $('#confirmOpt1').val();

                if(btcAddress == null || btcAddress == "" || btcAddress.length == 0 )
                {
                    putNoticeJavascript("<?= _editProfile_attention ?>","<?= _profile_keyin_btc_address ?>");
                    event.preventDefault();
                }
                else
                {
                    if($('#confirmOpt1').is(":checked"))
                    {
                        
                    }
                    else
                    {
                        putNoticeJavascript("<?= _editProfile_attention ?>","<?= _profile_checked1 ?>");
                        event.preventDefault();
                    }
                }
            }
            else //for others
            {
                let opt1_withdrawal_amount = $('#opt1_withdrawal_amount').val();
                let opt1_withdrawal_BTCaddress = $('#opt1_withdrawal_BTCaddress').val();
                let confirmOpt1 = $('#confirmOpt1').val();

                combinationArray = checkNullOrEmpty(combinationArray,opt1_withdrawal_amount,"<?= _profile_keyin_amount2 ?>");
                combinationArray = checkNullOrEmpty(combinationArray,opt1_withdrawal_BTCaddress,"<?= _profile_btc_address ?>");

                if(!combinationArray[1])
                {
                    let errorText = "<?= _profile_keyin_below_details ?> ：<br>";
                    for(let counter = 0;counter < combinationArray[0].length ;counter++)
                    {
                        errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
                    }

                    putNoticeJavascript("<?= _editProfile_attention ?>",errorText);
                    event.preventDefault();
                }
                else
                {
                    if($('#confirmOpt1').is(":checked"))
                    {
                        
                    }
                    else
                    {
                        putNoticeJavascript("<?= _editProfile_attention ?>","<?= _profile_checked1 ?>");
                        event.preventDefault();
                    }
                }
            }
        }
        else if(paymentOp == 2)
        {
            let opt2_withdrawal_submit = $('#opt2_withdrawal_submit').val();
            if(opt2_withdrawal_submit == 1)//for plan A Capital
            {
                let confirmOpt2 = $('#confirmOpt2').val();

                let opt2_withdrawal_fullName = $('#opt2_withdrawal_fullName').val();
                let opt2_withdrawal_country = $('#opt2_withdrawal_country').val();
                let opt2_withdrawal_bankName = $('#opt2_withdrawal_bankName').val();
                let opt2_withdrawal_bankAccountNo = $('#opt2_withdrawal_bankAccountNo').val();
                
                combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_fullName,"<?= _profile_keyin_name ?>");
                combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_country,"<?= _profile_keyin_country ?>");
                combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_bankName,"<?= _profile_keyin_bank_name ?>");
                combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_bankAccountNo,"<?= _profile_keyin_bank_acc_no ?>");
            
                if(!combinationArray[1])
                {
                    let errorText = "<?= _profile_keyin_below_details ?> ：<br>";
                    for(let counter = 0;counter < combinationArray[0].length ;counter++)
                    {
                        errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
                    }

                    putNoticeJavascript("<?= _editProfile_attention ?>",errorText);
                    event.preventDefault();
                }
                else
                {
                    if($('#confirmOpt2').is(":checked"))
                    {
                        
                    }
                    else
                    {
                        putNoticeJavascript("<?= _editProfile_attention ?>","<?= _profile_checked1 ?>");
                        event.preventDefault();
                    }
                }
            }
            else //for others
            {
                let confirmOpt2 = $('#confirmOpt2').val();

                let opt2_withdrawal_fullName = $('#opt2_withdrawal_fullName').val();
                let opt2_withdrawal_country = $('#opt2_withdrawal_country').val();
                let opt2_withdrawal_bankName = $('#opt2_withdrawal_bankName').val();
                let opt2_withdrawal_bankAccountNo = $('#opt2_withdrawal_bankAccountNo').val();
                let opt2_withdrawal_amount = $('#opt2_withdrawal_amount').val();

                combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_fullName,"<?= _profile_keyin_name ?>");
                combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_country,"<?= _profile_keyin_country ?>");
                combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_bankName,"<?= _profile_keyin_bank_name ?>");
                combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_bankAccountNo,"<?= _profile_keyin_bank_acc_no ?>");
                combinationArray = checkNullOrEmpty(combinationArray,opt2_withdrawal_amount,"<?= _profile_keyin_amount2 ?>");

                if(!combinationArray[1])
                {
                    let errorText = "<?= _profile_keyin_below_details ?> ：<br>";
                    for(let counter = 0;counter < combinationArray[0].length ;counter++)
                    {
                        errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
                    }

                    putNoticeJavascript("<?= _editProfile_attention ?>",errorText);
                    event.preventDefault();
                }
                else
                {
                    if($('#confirmOpt2').is(":checked"))
                    {
                        
                    }
                    else
                    {
                        putNoticeJavascript("<?= _editProfile_attention ?>","<?= _profile_checked1 ?>");
                        event.preventDefault();
                    }
                }
            }
        }
    }
    else
    {
        alert('fail');
        event.preventDefault();
    }
}
function validatePlans()
{
    let errorArray = [];
    let canValidateLevel_2 = true;
    let combinationArray =[errorArray,canValidateLevel_2];

    let plans = $('#field_1').val();
    let investedAmount = $('#field_2').val();
    let hashkey = $('#field_3').val();

    combinationArray = checkNullOrEmpty(combinationArray,plans,"<?= _profile_choose_invest_plan ?>");
    //combinationArray = checkNullOrEmpty(combinationArray,investedAmount,"<?//= _profile_keyin_donation_amount ?>//");
    //combinationArray = checkNullOrEmpty(combinationArray,hashkey,"<?//= _profile_keyin_hashkey ?>//");

    if(!combinationArray[1])
    {
        let errorText = "<?= _profile_keyin_below_details ?> ：<br>";
        for(let counter = 0;counter < combinationArray[0].length ;counter++)
        {
            errorText += "("+(counter+1)+")" + combinationArray[0][counter]+"<br>";
        }

        putNoticeJavascript("<?= _editProfile_attention ?>",errorText);
        event.preventDefault();
    }
}

    $("#clickInvestPlanA").click(function(){
        generateBtcAddress();
    });

    $("#clickInvestPlanB").click(function(){
        generateBtcAddress();
    });

    $("#mainInvestButton").click(function(){
        generateBtcAddress();
    });

    function generateBtcAddress(){
        if($('#btc_address_hidden_input').val().length > 0 && $('#btc_address_id_hidden_input').val().length > 0){
            return;
        }

        $.ajax({
            crossOrigin: false,
            url: "gateway/getPaymentAddress.php",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                if(!data){
                    console.error("error calling btc address ajax");
                    return;
                }

                if(data['isSuccess'] === "1" && data['address']){
                    $('#btc_address_hidden_input').val(data['address']);
                    $('#btc_address_id_hidden_input').val(data['addressId']);

                    $('#btc_address_text').html(data['address']);
                    //qr code from here https://github.com/jeromeetienne/jquery-qrcode
                    $('#btc_address_qr_code').qrcode({
                        text	: data['address']
                    });
                }else{
                    console.error("error getting btc address");
                }
            }
        });
    }

    function openInvestModal(){
        var investModal = document.getElementById('myModal2');
        var investInfoModal = document.getElementById('investInfoModal');
        investModal.style.display = "block";
        investInfoModal.style.display = "none";
    }

    function openInvestInfoModal(){
        var investModal = document.getElementById('myModal2');
        var investInfoModal = document.getElementById('investInfoModal');
        investModal.style.display = "none";
        investInfoModal.style.display = "block";
    }

    $("#copy-address").click(function(){
        copyAddress();
    });

    $("#btc_address_text").click(function(){
        copyAddress();
    });

    function copyAddress(){
        var textArea = document.createElement("textarea");
        textArea.value = $('#btc_address_text').html();
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();
        <?php
        promptAlertMsg( null,_profile_copied);
        ?>
    }
</script>

  </body>
</html>