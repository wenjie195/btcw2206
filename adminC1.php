<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
require_once 'utilities/calculationFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{ 
     if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
          $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
     {
          header('Location:index.php');
          exit();
     }
     else
     {
          $uid = $_SESSION['uid'];
          $conn = connDB();
          $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
     }
}

$errorMsg = null;
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     if (!isPostVariableEmpty("field_1") && !isPostVariableEmpty("field_2"))
     {
          $referrerEmail = $_POST["field_1"];
          $referralEmail = $_POST["field_2"];

          $referrerUserDetails = getUser($conn," WHERE email = ? ORDER BY date_created DESC LIMIT 1",array("email"), array($referrerEmail),"s");
          $referralUserDetails = getUser($conn," WHERE email = ? ORDER BY date_created DESC LIMIT 1",array("email"), array($referralEmail),"s");

          if($referrerUserDetails)
          {
               if($referralUserDetails)
               {
                    $getUplineHistory = getReferralHistory($conn," WHERE referrer_id = ? ",array("uid"),array($referrerUserDetails[0]->getUid()),"s");
                    if($getUplineHistory)
                    {
                         $currentLevel = $getUplineHistory[0]->getCurrentLevel();
                         $topReferrerID = $getUplineHistory[0]->getTopReferrerId();

                         $getDownlineReferral = getReferralHistory($conn," WHERE referral_id = ? ",array("uid"),array($referralUserDetails[0]->getUid()),"s");
                         if(!$getDownlineReferral)
                         {
                              $getDownlineReferrer = getReferralHistory($conn," WHERE referrer_id = ? ",array("uid"),array($referralUserDetails[0]->getUid()),"s");
                              if(!$getDownlineReferral)
                              {
                                   $addReferralToThisReferrer = insertDynamicData($conn,
                                   "referral_history",
                                   array("referrer_id","referral_id","current_level","top_referrer_id"),
                                   array($referrerUserDetails[0]->getUid(),$referralUserDetails[0]->getUid(),$currentLevel,$topReferrerID),
                                   "ssis");

                                   if($addReferralToThisReferrer)
                                   {
                                        $errorMsg = "Successfully Adding :<br>Downline -> ".$referralUserDetails[0]->getEmail()."<br>Upline -> ".$referrerUserDetails[0]->getEmail();
                                   }
                                   else
                                   {
                                        $errorMsg = "Error Adding Database";
                                   }
                              }
                              else
                              {
                                   $errorMsg = "Put it as pending first (downline referrer contains records)";
                              }
                         }
                         else
                         {
                              $errorMsg = "Put it as pending first (downline referral contains records)";
                         }
                    }
                    else
                    {
                         $getUplineReferrals = getReferralHistory($conn," WHERE referral_id = ? ",array("uid"),array($referrerUserDetails[0]->getUid()),"s");
                         if($getUplineReferrals)
                         {
                              //Plus 1 if hes already a referral
                              $currentLevel = $getUplineReferrals[0]->getCurrentLevel() + 1 ;
                              $topReferrerID =  $getUplineReferrals[0]->getTopReferrerId();

                              $getDownlineReferral = getReferralHistory($conn," WHERE referral_id = ? ",array("uid"),array($referralUserDetails[0]->getUid()),"s");
                              if(!$getDownlineReferral)
                              {
                                   $getDownlineReferrer = getReferralHistory($conn," WHERE referrer_id = ? ",array("uid"),array($referralUserDetails[0]->getUid()),"s");
                                   if(!$getDownlineReferral)
                                   {
                                        $addReferralToThisReferrer = insertDynamicData($conn,
                                        "referral_history",
                                        array("referrer_id","referral_id","current_level","top_referrer_id"),
                                        array($referrerUserDetails[0]->getUid(),$referralUserDetails[0]->getUid(),$currentLevel,$topReferrerID),
                                        "ssis");

                                        if($addReferralToThisReferrer)
                                        {
                                             $errorMsg = "Successfully Adding :<br>Downline -> ".$referralUserDetails[0]->getEmail()."<br>Upline -> ".$referrerUserDetails[0]->getEmail();
                                        }
                                        else
                                        {
                                             $errorMsg = "Error Adding Database";
                                        }
                                   }
                                   else
                                   {
                                        $errorMsg = "Put it as pending first (downline referrer contains records)";
                                   }
                              }
                              else
                              {
                                   $errorMsg = "Put it as pending first (downline referral contains records)";
                              }
                         }
                         else
                         {
                              // top guy
                              $currentLevel = 1 ;
                              $topReferrerID = $referrerUserDetails[0]->getUid();

                              $getDownlineReferral = getReferralHistory($conn," WHERE referral_id = ? ",array("uid"),array($referralUserDetails[0]->getUid()),"s");
                              if(!$getDownlineReferral)
                              {
                                   $getDownlineReferrer = getReferralHistory($conn," WHERE referrer_id = ? ",array("uid"),array($referralUserDetails[0]->getUid()),"s");
                                   if(!$getDownlineReferral)
                                   {
                                        $addReferralToThisReferrer = insertDynamicData($conn,
                                        "referral_history",
                                        array("referrer_id","referral_id","current_level","top_referrer_id"),
                                        array($referrerUserDetails[0]->getUid(),$referralUserDetails[0]->getUid(),$currentLevel,$topReferrerID),
                                        "ssis");

                                        if($addReferralToThisReferrer)
                                        {
                                             $errorMsg = "Successfully Adding :<br>Downline -> ".$referralUserDetails[0]->getEmail()."<br>Upline -> ".$referrerUserDetails[0]->getEmail();
                                        }
                                        else
                                        {
                                             $errorMsg = "Error Adding Database";
                                        }
                                   }
                                   else
                                   {
                                        $errorMsg = "Put it as pending first (downline referrer contains records)";
                                   }
                              }
                              else
                              {
                                   $errorMsg = "Put it as pending first (downline referral contains records)";
                              }
                         }
                    }
               }
               else
               {
                    $errorMsg = "referral (downline) does not exist (Put it as Error first)";
               }
          }
          else
          {
               $errorMsg = "referrer (upline) does not exist (Put it as Error first)";
          }
     }
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <title>ZCXC</title>
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();
     if($errorMsg != null)
     {
          putNotice("Notice",$errorMsg);
     }
  ?>
  <div id="firefly" class="firefly-class min-height">  
     <div class="width100 same-padding more-separate-margin-top edit-div">
          <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a> 
          <div class="clear"></div>
            <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Add Referral To This Referrer</b></h4>
            <p class="white-text"><b class="weight-700">Put referrer email and referral email only</b></p>
            <form class="register-form"  method="POST" >
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Referrer Email</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_1" id="field_1" required></h4></td>
                  </tr>
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Referral Email</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" required></h4></td>
                  </tr>

                </table>
                <div class="clear"></div>
               <button class="register-button2 clean orange-hover inputb-button" name="insertValue" id="insertValue" >Add This referrer</button>

          </form>
     </div> 
      <div class="clear"></div>
      <?php 
          if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST')
          {
              
          }
      ?>
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <?php require_once 'mainFooter.php';?>
  </body>
</html>