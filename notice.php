<?php
//Start the session
session_start();

//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
    exit();
}else{
    $uid = $_SESSION['uid'];
}

require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';

$conn = connDB();

$referralHistoryRows = array();
$thisUserFirstTimeReferralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ORDER BY id ASC LIMIT 1 ",array("referrer_id"),array($uid),"s");
if($thisUserFirstTimeReferralHistoryRows){
   $topReferrerId = $thisUserFirstTimeReferralHistoryRows[0]->getTopReferrerId();
   if($topReferrerId === $uid){
       $thisUserFirstLevel = 1;
       $thisUserLastLevel = 10;
   }else{
       $thisUserFirstLevel = $thisUserFirstTimeReferralHistoryRows[0]->getCurrentLevel();
       $thisUserLastLevel = $thisUserFirstLevel + 9;
   }

   $downlineReferralIdArray = array();
   $downlineReferralIdArray[$uid]  = true ;
   for($loopCurrentLevel = $thisUserFirstLevel; $loopCurrentLevel <= $thisUserLastLevel; $loopCurrentLevel++){
       $thisLevelReferralHistoryRows = getReferralHistory($conn," WHERE top_referrer_id = ? AND current_level = $loopCurrentLevel ORDER BY id ",array("top_referrer_id"),array($topReferrerId),"s");
       if($thisLevelReferralHistoryRows){
           $newDownlineReferralIdArray = array();
           foreach ($thisLevelReferralHistoryRows as $tempReferral) {
               $canAdd = false;
               foreach ($downlineReferralIdArray as $key => $value) {
                   if($tempReferral->getReferrerId() === $key){
                       $newDownlineReferralIdArray[$tempReferral->getReferralId()]  = true ;
                       $canAdd = true;
                       break;
                   }
               }
               if($canAdd){
                   array_push($referralHistoryRows,$tempReferral);
               }
           }
           unset($downlineReferralIdArray);
           $downlineReferralIdArray = array();
           foreach ($newDownlineReferralIdArray as $newDownlineReferralId => $value) {
               $downlineReferralIdArray[$newDownlineReferralId]  = true ;
           }
           unset($newDownlineReferralIdArray);
       }
   }
}

?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/notice.php" />
        <meta property="og:title" content="<?= _notice_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

        <title><?= _notice_title ?></title>
        <link rel="canonical" href="https://btcworg.com/notice.php" />
  </head>

    <body>
    	<?php require 'mainNavbar.php';?>
  			<div id="firefly" class="firefly-class min-height">
            	<div class="width100 same-padding menu-content-distance">
            		<!--<p class="notice-img-p"><img src="img/sherry/notice.png" class="notice-img" alt="<?//= _notice_title ?>" title="<?//= _notice_title ?>"></p>
                	<p class="white-text notice1-p"><?//= _notice_no_notice ?></p>-->
                    <h2 class="notice-title"><?= _notice_h2 ?></h2>
                    <div class="dark-box graph-bg notice-box">
                    	<p class="notice-box-p"><?= _notice_one_to_five ?></p>
                        <p class="notice-box-p notice-date-p"><?= _notice_first_july ?></p>
                    </div>                    
                    <div class="dark-box graph-bg notice-box">
                    	<p class="notice-box-p"><?= _notice_one_to_six ?></p>
                        <p class="notice-box-p notice-date-p"><?= _notice_third_june ?></p>
                    </div>
                    
                    
                </div>
            </div>    		
    	<?php require 'mainFooter.php';?>
    </body>
</html>