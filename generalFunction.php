<?php
require_once 'dbCon/dbCon.php';
require_once 'utilities/databaseFunction.php';
require_once 'classes/Register.php';

if(isset($_POST["fromPage"]) && $_POST['fromPage'] == "login")
{
    getCountryCode($_POST['value']);
    // echo $_POST['value'];
}
if(isset($_POST["fromPage"]) && $_POST['fromPage'] == "confirmRegister") // save into db
{
        $uid = $_POST['uid'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $country = $_POST['country'];
        $phoneNo = $_POST['phoneNo'];
        //$countryCode = $_POST['countryCode'];
        $password = $_POST['password'];
        $receiveNewsLetter = $_POST['receiveNewsLetter'];
        $ic_no = $_POST['icno'];

        // $_SESSION["uid"] = $uid;
        // $_SESSION["name"] = $_POST['name'];
        // $_SESSION["email"] = $_POST['email'];
        // $_SESSION["country"] = $_POST['country'];
        // $_SESSION["phoneNo"] = $_POST['phoneNo'];
        // $_SESSION["countryCode"] = $_POST['countryCode'];
        // $_SESSION["password"] = $_POST['password'];
        // $_SESSION["receiveNewsLetter"] = $_POST['receiveNewsLetter'];

    $password = hash('sha256',$password);
    $salt = substr(sha1(mt_rand()), 0, 100);
    $finalPassword = hash('sha256', $salt.$password);

    $conn = connDB();

    //insert new user into database
    insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","country_id","is_phone_verified","login_type","user_type","can_send_newsletter","ic_no"),
                      array($uid,$name,$email,$finalPassword,$salt,$phoneNo,$country,1,1,1,$receiveNewsLetter,$ic_no),"ssssssiiiiis");

    //get entry reward free btc
    $entryBtcTypeRows = getBtcType($conn," WHERE id = ? ORDER BY date_updated DESC LIMIT 1",array("id"),array(4),"i");
    $entryBtc = 0;
    if($entryBtcTypeRows){
        $entryBtc = $entryBtcTypeRows[0]->getAmount();

        if($entryBtc > 0){
            $descriptionTH = _general_register_profit;
            $freeBtcInTHId = insertIntoTransactionHistory($conn,$entryBtc,0,0,0,
                0,0,0,$entryBtc,
                0,0,$uid,0,null,
                0,null,4,$descriptionTH,1);

            if(!$freeBtcInTHId){
//                echo "free btc in error";
                echo _general_sorry;
            }

            $newFreeBtcTableId = insertDynamicData($conn,"free_btc",array("total_amount","uid","btc_type_id","transaction_id"),
                array($entryBtc,$uid,4,$freeBtcInTHId),"dsii");
            if(!$newFreeBtcTableId){
//                echo "insert free btc table error";
                echo _general_sorry;
            }else{
                if(!updateTransactionHistoryWithNewFreeBtcId($conn,$newFreeBtcTableId,$freeBtcInTHId)){
                    echo _general_sorry;
                }
            }

        }
    }

    //insert new user's user_plan table data
    $newUserPlanTableId = insertDynamicData($conn,"user_plan",array("uid","plan_id","free_btc"),
        array($uid,1,$entryBtc),"sid");
    if($newUserPlanTableId){

    }else{
        echo _general_sorry;
    }

    $newUserPlanBTableId = insertDynamicData($conn,"user_plan",array("uid","plan_id"),
        array($uid,2),"si");
    if($newUserPlanBTableId){

    }else{
        echo _general_sorry;
    }

    //referral TEMP QUICK WORKOUT
    if(isset($_POST['referralId']) && $_POST['referralId'] !== '' && $_POST['referralId'] !== $uid){
        $referrerId = $_POST['referralId'];
        $sql = "SELECT current_level,top_referrer_id FROM referral_history WHERE referral_id = ? ORDER BY date_created DESC LIMIT 1 ";

        if ($stmt = $conn->prepare($sql)) {
            $stmt->bind_param('s', $referrerId);

            /* execute query */
            $stmt->execute();

            /* Store the result (to get properties) */
            $stmt->store_result();

            /* Get the number of rows */
            $num_of_rows = $stmt->num_rows;

            /* Bind the result to variables */
            $stmt->bind_result($currentLevel,$topReferrerId);

            $stmt->fetch();

            /* free results */
            $stmt->free_result();

            /* close statement */
            $stmt->close();

            if ($num_of_rows <= 0) {
                $currentLevel = 1;
                $topReferrerId = $referrerId;
            } else {
                if($referrerId === $topReferrerId){

                }else{
                    $currentLevel++;
                }
            }

            $newReferralHistoryTableId = insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","current_level","top_referrer_id"),
                array($referrerId,$uid,$currentLevel,$topReferrerId),"ssis");
            if(!$newReferralHistoryTableId){
                echo _general_sorry;
            }

            $minIntroPpl = 0;
            $introBtc = 0;
            $introBtcTypeRows = getBtcType($conn," WHERE id = ? ORDER BY date_updated DESC LIMIT 1",array("id"),array(5),"i");
            if($introBtcTypeRows) {
                $minIntroPpl = $introBtcTypeRows[0]->getMinPplIntro();
                $introBtc = $introBtcTypeRows[0]->getAmount();
            }

            //**********************FROM HERE IS FOR REWARDING THE REFERRER IF REQUIREMENT IS SET START********************************/
            $isThisReferrerAlreadyTakenReferralReward = getFreeBtc($conn," WHERE uid = ? AND btc_type_id = ? ",array("uid","btc_type_id"),array($referrerId,5),"si"); //MAKE IT ONE TIME ONLY (ACTUALLY IS NOT ONE TIME ONLY AND NOT EVERY 5 PERSON LOL)
            $referralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($referrerId),"s");
            if($referralHistoryRows && !$isThisReferrerAlreadyTakenReferralReward){ //MAKE IT ONE TIME ONLY (ACTUALLY IS NOT ONE TIME ONLY AND NOT EVERY 5 PERSON LOL)
                $totalTimesNeedToAwardBtc = floor(count($referralHistoryRows) / $minIntroPpl);

                $transactionHistoryRows = getTransactionHistory($conn," WHERE uid = ? AND btc_type_id = ?",array("uid","btc_type_id"),array($referrerId,5),"si");
                $totalTimesAwardedBtc = 0;
                if($transactionHistoryRows){
                    $totalTimesAwardedBtc = count($transactionHistoryRows);
                }

                $needLoopHowManyTimes = $totalTimesNeedToAwardBtc - $totalTimesAwardedBtc;
                if($needLoopHowManyTimes > 1){//MAKE IT ONE TIME ONLY (ACTUALLY IS NOT ONE TIME ONLY AND NOT EVERY 5 PERSON LOL)
                    $needLoopHowManyTimes = 1;
                }
                if($needLoopHowManyTimes > 0){
                    $totalIntroBtcEarned = 0.0;
                    $userPlanRows = getUserPlan($conn," WHERE uid = ? && plan_id = ? ORDER BY date_created DESC LIMIT 1",array("uid","plan_id"),array($referrerId,1),"si");

                    for($i = 0; $i < $needLoopHowManyTimes; $i++){
                        $totalIntroBtcEarned += $introBtc;

                        $descriptionTH = _general_recommend5;
                        $freeBtcInTHId = insertIntoTransactionHistory($conn,$introBtc,0,0,0,
                            0,0,$userPlanRows[0]->getReturnedBtc(),$userPlanRows[0]->getFreeBtc() + $totalIntroBtcEarned,
                            $userPlanRows[0]->getCapitalBtc(),0,$referrerId,0,null,
                            0,null,5,$descriptionTH,1);

                        if(!$freeBtcInTHId){
//                echo "free btc in error";
                            echo _general_sorry;
                        }else{
                            //now insert the free btc into free_btc table for everyday 0.2% drop. the above insert is just for transaction_history
                            $newFreeBtcTableId = insertDynamicData($conn,"free_btc",array("total_amount","uid","btc_type_id","transaction_id"),
                                array($introBtc,$referrerId,5,$freeBtcInTHId),"dsii");
                            if(!$newFreeBtcTableId){
//                echo "insert free btc table error";
                                echo _general_sorry;
                            }else{
                                if(!updateTransactionHistoryWithNewFreeBtcId($conn,$newFreeBtcTableId,$freeBtcInTHId)){
                                    echo _general_sorry;
                                }
                            }
                        }
                    }
                    if($totalIntroBtcEarned > 0){
                        //update user's current total free btc and capital btc into user_plan table
                        if(!updateDynamicData($conn,"user_plan"," WHERE uid = ? AND plan_id = ? ",array("free_btc"),
                            array($userPlanRows[0]->getFreeBtc() + $totalIntroBtcEarned,
                                $referrerId,1),
                            "dsi")){
//                echo "update free and capital btc error";
                            echo _general_sorry;
                        }
                    }
                }
            }
            //**********************FROM HERE IS FOR REWARDING THE REFERRER IF REQUIREMENT IS SET END********************************/

        } else {
//                echo "Prepare Error: ($conn->errno) $conn->error";
            echo _general_sorry;
        }
    }

    $conn->close();
}
function loginGetCityEditProfile($countryID)
{
    $conn = connDB();
    $countryRows = getCountries($conn);
    if($countryRows && count($countryRows) > 0)
    {
        foreach ($countryRows as $country)
        {
            if($country->getId() == $countryID)
            {
                echo "<option selected value='".$country->getId()."'>".$country->getName()."</option>";
            }
            else
            {
                echo "<option value='".$country->getId()."'>".$country->getName()."</option>";
            }
        }
    }
}
function getCheckBoxDefaultCheck($sendNewsletter)
{
    if($sendNewsletter == 1)
    {
        echo "<input class='form-check-input  inputb-checkbox' type='checkbox' id='defaultCheck2' name='defaultCheck2[]' checked>";
    }
    else
    {
        echo "<input class='form-check-input  inputb-checkbox' type='checkbox' id='defaultCheck2' name='defaultCheck2[]'>";
    }
}

function editProfileGetPhoneCode($countryId)
{
    $conn = connDB();
    $countryRows = getCountries($conn," WHERE id = ? ",array("id"),array($countryId),"i");
    $countryCode = "";
    if($countryRows && count($countryRows) > 0){
        $countryCode = "+".$countryRows[0]->getPhonecode()." ";
    }
    echo $countryCode;
}
function loginGetCity($value = null)
{
    $conn = connDB();
    $sqlGetCountry = " SELECT * FROM countries ";
    $querylisting = mysqli_query($conn,$sqlGetCountry);
    // echo "Prepare Error: ($conn->errno) $conn->error";
    if (mysqli_num_rows($querylisting) > 0)
    {
        if($value == null)
        {
            echo "<div class='input-container3'>";
            echo " <select class='inputa clean2' onchange='changePhoneNo(this.value);' name='field_3' id='field_3'>";
            echo "      <option disabled selected>". _general_country."</option>";
    
            while($row = mysqli_fetch_array($querylisting))
            {
                echo "  <option value='".$row['id']."'>".$row['name']."</option>";
            }
            echo "      </select>";
            echo "      <img src='img/sherry/country.png' class='input-icon'>";
            echo "</div>";
            
        }
        else
        {   
            echo "<div class='input-container3'>";
            echo "<select class='inputa clean2' onchange='changePhoneNo(this.value);' name='field_3' id='field_3'>";
            echo "      <option disabled>". _general_country."</option>";
            while($row = mysqli_fetch_array($querylisting))
            {
                if($value == $row['id'])
                {
                    echo "<option selected value='".$row['id']."'>".$row['name']."</option>";
                }
                else
                {
                    echo "<option value='".$row['id']."'>".$row['name']."</option>";
                }
            }
            echo "</select>";
            echo "<img src='img/sherry/country.png' class='input-icon'>";
            echo "</div>";
        }
    }
}
function getCountryCode($value)
{
    $conn = connDB();
    $sqlGetCountry = " SELECT * FROM countries WHERE id = ".$value;
    $querylisting = mysqli_query($conn,$sqlGetCountry);
    if (mysqli_num_rows($querylisting) > 0)
    {
        while($row = mysqli_fetch_array($querylisting))
        {
            $phonecode = $row['phonecode'];
            if(isset($_POST["fromPage"]) && $_POST['fromPage'] == "login")
            {
                echo "+". $phonecode;
            }
        }
    }
    return $phonecode;
}
function rewrite($info)
{
    $info = stripslashes($info);
    $info = trim($info);
    $info = htmlspecialchars($info,ENT_QUOTES,'ISO-8859-1', true);
    return $info;
}

function registerLogin()
{
    require_once 'utilities/databaseFunction.php';
    $conn = connDB();
    $canRegister = TRUE;
    $errTXT = "";

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {//Second Level Validation
        $registerClass = new Register();
        $registerClass->setUsername(rewrite($_POST['field_1']));
        $registerClass->setEmail(rewrite($_POST['field_2']));
        $registerClass->setCountryID(rewrite($_POST['field_3']));
        $registerClass->setPhone(rewrite($_POST['field_4']));
        $registerClass->setPassword($_POST['field_5']);
        $registerClass->setConfirmPassword($_POST['field_6']);
        $registerClass->setIcNo($_POST['field_7']);
        
       

        if(isset($_POST['defaultCheck1']) || $_POST['defaultCheck1'] == "")
        {
            $registerClass->setTermNCondition(1);
        }

        if(!isset($_POST['defaultCheck2']) || $_POST['defaultCheck2'] == "")
        {
            $receiveNewsLetter = 0;
            $registerClass->setReceiveNotification($receiveNewsLetter);
        }
        else
        {
            $receiveNewsLetter = 1;
            $registerClass->setReceiveNotification($receiveNewsLetter);
        }

        $registerClass->setEmail(filter_var($registerClass->getEmail(),FILTER_SANITIZE_EMAIL));

        if(!filter_var($registerClass->getEmail(), FILTER_VALIDATE_EMAIL))
        {
            $headerErrorText = "Location: indexRegister.php"."?"
                ."createModalBox=1"
                ."&username=".$registerClass->getUsername()
                ."&email=".$registerClass->getEmail()
                ."&country=".$registerClass->getCountryID()
                ."&phone=".$registerClass->getPhone()
                ."&term=".$registerClass->getTermNCondition()
                ."&news=".$registerClass->getReceiveNotification()
                ."&icno=".$registerClass->getIcNo();
            if(isset($_GET['referralId']))
            {
                $headerErrorText.="&referralId=".$_GET['referralId'];
            }
            header($headerErrorText);
        }
        else
        {
            if (!preg_match("/^\d+$/",$registerClass->getPhone()))
            {
                $headerErrorText = "Location: indexRegister.php"."?"
                    ."createModalBox=2"
                    ."&username=".$registerClass->getUsername()
                    ."&email=".$registerClass->getEmail()
                    ."&country=".$registerClass->getCountryID()
                    ."&phone=".$registerClass->getPhone()
                    ."&term=".$registerClass->getTermNCondition()
                    ."&news=".$registerClass->getReceiveNotification()
                    ."&icno=".$registerClass->getIcNo();
                if(isset($_GET['referralId']))
                {
                    $headerErrorText.="&referralId=".$_GET['referralId'];
                }
                header($headerErrorText);
            }
            else
            {
                if(strlen($registerClass->getPassword()) < 6)
                {
                    $headerErrorText = "Location: indexRegister.php"."?"
                        ."createModalBox=7"
                        ."&username=".$registerClass->getUsername()
                        ."&email=".$registerClass->getEmail()
                        ."&country=".$registerClass->getCountryID()
                        ."&phone=".$registerClass->getPhone()
                        ."&term=".$registerClass->getTermNCondition()
                        ."&news=".$registerClass->getReceiveNotification()
                        ."&icno=".$registerClass->getIcNo();
                    if(isset($_GET['referralId']))
                    {
                        $headerErrorText.="&referralId=".$_GET['referralId'];
                    }
                    header($headerErrorText);
                }
                else
                {
                    if (!preg_match("/^[a-zA-Z0-9]+$/",$registerClass->getPassword()))
                    {
                        $headerErrorText = "Location: indexRegister.php"."?"
                            ."createModalBox=3"
                            ."&username=".$registerClass->getUsername()
                            ."&email=".$registerClass->getEmail()
                            ."&country=".$registerClass->getCountryID()
                            ."&phone=".$registerClass->getPhone()
                            ."&term=".$registerClass->getTermNCondition()
                            ."&news=".$registerClass->getReceiveNotification()
                            ."&icno=".$registerClass->getIcNo();
                        if(isset($_GET['referralId']))
                        {
                            $headerErrorText.="&referralId=".$_GET['referralId'];
                        }
                        header($headerErrorText);
                    }
                    else
                    {
                        if($registerClass->getConfirmPassword() != $registerClass->getPassword())
                        {
                            $headerErrorText = "Location: indexRegister.php"."?"
                                ."createModalBox=4"
                                ."&username=".$registerClass->getUsername()
                                ."&email=".$registerClass->getEmail()
                                ."&country=".$registerClass->getCountryID()
                                ."&phone=".$registerClass->getPhone()
                                ."&term=".$registerClass->getTermNCondition()
                                ."&news=".$registerClass->getReceiveNotification()
                                ."&icno=".$registerClass->getIcNo();
                            if(isset($_GET['referralId']))
                            {
                                $headerErrorText.="&referralId=".$_GET['referralId'];
                            }
                            header($headerErrorText);
                        }
                        else
                        {
                            $userRows = getUser($conn,
                                " WHERE email = ? ",
                                array("email"),
                                array($registerClass->getEmail()),
                                "s");

                            if(!is_null($userRows) && count($userRows) > 0)// Not First User
                            {
                                $headerErrorText = "Location: indexRegister.php"."?"
                                    ."createModalBox=5"
                                    ."&username=".$registerClass->getUsername()
                                    ."&email=".$registerClass->getEmail()
                                    ."&country=".$registerClass->getCountryID()
                                    ."&phone=".$registerClass->getPhone()
                                    ."&term=".$registerClass->getTermNCondition()
                                    ."&news=".$registerClass->getReceiveNotification()
                                    ."&icno=".$registerClass->getIcNo();
                                if(isset($_GET['referralId']))
                                {
                                    $headerErrorText.="&referralId=".$_GET['referralId'];
                                }
                                header($headerErrorText);
                            }
                            else
                            {
                                $userPhoneRows = getUser($conn,
                                    " WHERE phone_no = ? AND country_id = ? ",
                                    array("phone_no","country_id"),
                                    array($registerClass->getPhone(),$registerClass->getCountryID()),
                                    "si");

                                if(!is_null($userPhoneRows) && count($userPhoneRows) > 0)// Not First User
                                {
                                    $headerErrorText = "Location: indexRegister.php"."?"
                                        ."createModalBox=6"
                                        ."&username=".$registerClass->getUsername()
                                        ."&email=".$registerClass->getEmail()
                                        ."&country=".$registerClass->getCountryID()
                                        ."&phone=".$registerClass->getPhone()
                                        ."&term=".$registerClass->getTermNCondition()
                                        ."&news=".$registerClass->getReceiveNotification()
                                        ."&icno=".$registerClass->getIcNo();
                                    if(isset($_GET['referralId']))
                                    {
                                        $headerErrorText.="&referralId=".$_GET['referralId'];
                                    }
                                    header($headerErrorText);
                                }
                                else
                                {
                                    $userICRows = getUser($conn,
                                        " WHERE ic_no = ? ",
                                        array("ic_no"),
                                        array($registerClass->getIcNo()),
                                        "s");
                                    if(!is_null($userICRows) && count($userICRows) > 0)// Not First User
                                    {
                                        $headerErrorText = "Location: indexRegister.php"."?"
                                            ."createModalBox=8"
                                            ."&username=".$registerClass->getUsername()
                                            ."&email=".$registerClass->getEmail()
                                            ."&country=".$registerClass->getCountryID()
                                            ."&phone=".$registerClass->getPhone()
                                            ."&term=".$registerClass->getTermNCondition()
                                            ."&news=".$registerClass->getReceiveNotification()
                                            ."&icno=".$registerClass->getIcNo();
                                        if(isset($_GET['referralId']))
                                        {
                                            $headerErrorText.="&referralId=".$_GET['referralId'];
                                        }
                                        header($headerErrorText);
                                    }
                                    else
                                    {
                                        $referrerEmail = $_POST['referrerEmail'];
                                        if($referrerEmail !="")
                                        {
                                            $referrerUser = getUser($conn,
                                                            " WHERE email = ? ",
                                                            array("email"),
                                                            array($referrerEmail),
                                                            "s");

                                            if(!$referrerUser)
                                            {
                                                $headerErrorText = "Location: indexRegister.php"."?"
                                                    ."createModalBox=9"
                                                    ."&username=".$registerClass->getUsername()
                                                    ."&email=".$registerClass->getEmail()
                                                    ."&country=".$registerClass->getCountryID()
                                                    ."&phone=".$registerClass->getPhone()
                                                    ."&term=".$registerClass->getTermNCondition()
                                                    ."&news=".$registerClass->getReceiveNotification()
                                                    ."&icno=".$registerClass->getIcNo();
                                                if(isset($_GET['referralId']))
                                                {
                                                    $headerErrorText.="&referralId=".$_GET['referralId'];
                                                }
                                                header($headerErrorText);
                                            }
                                            else
                                            {
                                                setcookie("username", $registerClass->getUsername());
                                                setcookie("email", $registerClass->getEmail());
                                                setcookie("country", $registerClass->getCountryID());
                                                setcookie("phoneNo", $registerClass->getPhone());
                                                setcookie("password", $registerClass->getPassword());
                                                setcookie("receiveNewsLetter",$registerClass->getReceiveNotification());
                                                setcookie("icNo",$registerClass->getIcNo());

                                                $referralId = $referrerUser[0]->getUid();
                                                $registerClass->setReferralID($referralId);
                                                
                                                header("Location: indexConfirmRegister.php?referralId=$referralId");

                                            }
                                        }
                                        else
                                        {
                                            setcookie("username", $registerClass->getUsername());
                                            setcookie("email", $registerClass->getEmail());
                                            setcookie("country", $registerClass->getCountryID());
                                            setcookie("phoneNo", $registerClass->getPhone());
                                            setcookie("password", $registerClass->getPassword());
                                            setcookie("receiveNewsLetter",$registerClass->getReceiveNotification());
                                            setcookie("icNo",$registerClass->getIcNo());

                                            header("Location: indexConfirmRegister.php");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function errorModal($createModalBox)
{
    if($createModalBox == 1)
    {
        putNotice(_general_notice, _general_email_format);
    }
    if($createModalBox == 2)
    {
        putNotice(_general_notice, _general_only_numbers);
    }
    if($createModalBox == 3)
    {
        putNotice(_general_notice,_general_only_numbers_abc);
    }
    if($createModalBox == 4)
    {
        putNotice(_general_notice,_general_match_password);
    }
    if($createModalBox == 5)
    {
        putNotice(_general_notice, _general_ald_reg);
    }
    if($createModalBox == 6)
    {
        putNotice(_general_notice, _general_ald_phone_reg);
    }
   if($createModalBox == 7)
   {
       putNotice(_general_notice,_general_pw_min);
   }
    if($createModalBox == 8)
    {
        putNotice(_general_notice, _general_ald_reg_ic);
    }
   if($createModalBox == 9)
   {
       putNotice(_general_notice, _general_ref_no_found);
   }
//    if($createModalBox == 10)
//    {
//        putNotice(_general_notice, _general_keyin_correct);
//    }
}
function checkLogin()
{
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        if(!isset($_POST['field_1']))
        {
            return _general_keyin_id_email;
        }
        if(!isset($_POST['field_2']))
        {
            return _general_keyin_password;
        }

        if(isset($_POST['field_1']) && isset($_POST['field_2']))
        {
            $conn = connDB();
    
            $emailORusername = rewrite($_POST['field_1']);
            $password = $_POST['field_2'];

            //later on Email Only
            $userRows = getUser($conn," WHERE email  = ? ",array("email"),array($emailORusername),"s");
            
            if(is_null($userRows) || count($userRows) <= 0)
            {
                $conn->close();
                return _general_acc_not_exist;
            }
            else
            {
                $dbPassword = $userRows[0]->getPassword();
                $dbSalt = $userRows[0]->getSalt();

                //echo $dbPassword."<br>";
            
                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $dbSalt.$tempPass);

                //echo $finalPassword."<br>";

                if($finalPassword == $dbPassword)
                {
                    // session_start();
                    $_SESSION['uid'] = $userRows[0]->getUid();
                    $_SESSION['username'] = $userRows[0]->getUsername();
                    $_SESSION['email'] = $userRows[0]->getEmail();
                    $_SESSION['phone_no'] = $userRows[0]->getPhoneNo();
                    $_SESSION['country_id'] = $userRows[0]->getCountryId();
                    $_SESSION['user_type'] = $userRows[0]->getUserType();
                    $_SESSION['startSessionTime'] = time();
                    $conn->close();
                    echo '<script>window.location.replace("profile.php");</script>';
                }
                else
                {
                    $conn->close();
                    return _general_keyin_correct_id_pw;
                }
            }
        }
    }
}
function generateRandomString($length = 10) 
{
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}
function forgotPassword()
{
    if(isset($_POST["field_1"]))
    {
        $email = rewrite($_POST["field_1"]);
        //cleans the email
        $email = filter_var($email,FILTER_SANITIZE_EMAIL);
        //check whether the email is correct or not
        if (!filter_var($email,FILTER_VALIDATE_EMAIL)) 
        {
            $emailError = _general_incorrect_email;
            putNotice(_general_notice,$emailError);
        }
        else
        {
            $conn = connDB();
    
            $uids ="";
            $tempPass = generateRandomString();
            // $tempPass = hash('sha256',$tempPass);
            $salt = substr(sha1(mt_rand()), 0, 100);
            $finalTempPassword = hash('sha256', $salt.$tempPass);
        
            $stmt = $conn->prepare("UPDATE user SET password = ?,salt = ? WHERE email = ?");
            $stmt->bind_param("sss",$finalTempPassword, $salt ,$_POST['field_1']);
            $stmt->execute();

            $stmt = $conn->prepare("SELECT uid FROM user WHERE email = ?");
            $stmt->bind_param("s", $_POST['field_1']);
            $stmt->execute();
            $result = $stmt->get_result();
            if($result->num_rows === 0) 
            {
                putNotice(_editPassword_error,_general_email_not_exist);
            }
            else
            {
                while($row = $result->fetch_assoc()) 
                {
                    $uids = $row['uid'];
                }
                // echo $tempPass."<br>";
                // echo $salt."<br>";
                // echo $salt.$tempPass."<br>";
                // echo $finalTempPassword;

                // iov7MaCjSk
                // 624a02410d8f9f7102e32c9d2eab91d67e4d40b1
                // 624a02410d8f9f7102e32c9d2eab91d67e4d40b1iov7MaCjSk
                // cfc13b59fb2cbbd09d9582b63f85a2166617e986fcb2d9345a70b36e5da79963

                require("PHPMailer/src/PHPMailer.php");
                require("PHPMailer/src/SMTP.php");
                require("PHPMailer/src/Exception.php");
        
        
                $mail = new PHPMailer\PHPMailer\PHPMailer();
                //Enable SMTP debugging. 
                // $mail->SMTPDebug = 3;                               
                //Set PHPMailer to use SMTP.
                $mail->isSMTP();

                //USING GMAIL VERSION
//                //Set SMTP host name
//                $mail->Host = "smtp.gmail.com";
//                //Set this to true if SMTP host requires authentication to send email
//                $mail->SMTPAuth = true;
//                //Provide username and password
//                $mail->Username = "btwc.vidatech@gmail.com";
//                $mail->Password = "btwc.vidatech.168";
//                //If SMTP requires TLS encryption then set it
//                $mail->SMTPSecure = "tls";
//                //Set TCP port to connect to
//                $mail->Port = 587;
//
//                $mail->From = "btwc.vidatech@gmail.com";
//                $mail->FromName = "btcw vidatech";

                //Set SMTP host name USING BTCW OWN EMAIL
                $mail->Host = "mail.btcworg.com";
                //Set this to true if SMTP host requires authentication to send email
                $mail->SMTPAuth = true;
                //Provide username and password
                $mail->Username = "no-reply@btcworg.com";
                $mail->Password = "NuT1h!CAd9XQ";
                //If SMTP requires TLS encryption then set it
                $mail->SMTPSecure = "ssl";
                //Set TCP port to connect to
                $mail->Port = 465;
                $mail->CharSet = 'UTF-8';

                $mail->From = "no-reply@btcworg.com";
                $mail->FromName = "no-reply@btcworg.com";
        
                $mail->addAddress($email);
        
                $mail->isHTML(true);
        
                $mail->Subject = _general_reset_pw1;
                // <a href='https://btcworg.com/indexResetPassword.php?uid=".$uids."'>link</a> 
                $mail->Body = " <p>'._general_please_at.'
                                <a href='https://btcworg.com/indexResetPassword.php?uid=".$uids."'>'._general_this_link.'</a> 
                               '._general_use_code.'</p>
                                <p>'._general_reset_pw_link.'  =  <a href='https://btcworg.com/indexResetPassword.php?uid=".$uids."'>https://btcworg.com/indexResetPassword.php?uid=".$uids."</a></p>
                                <p>'._general_code.' = ".$tempPass."</p>";

                // $mail->Body = " <p>Please reset your password in this 
                //                 <a href='http://localhost/btcw_web/indexResetPassword.php?uid=".$uids."'>link</a> 
                //                 using this key code below</p>
                //                 <p>Link to Reset Password  =  <a href='http://localhost/btcw_web/indexResetPassword.php?uid=".$uids."'>http://localhost/btcw_web/indexResetPassword.php?uid=".$uids."</a></p>
                //                 <p>Key Code = ".$tempPass."</p>";



                $mail->AltBody = "";
        
                if(!$mail->send()) 
                {
                    putNotice("Mailer Error: ",$mail->ErrorInfo);
                } 
                else 
                {
                    putNoticeFromThisPageToThatPage(_general_notice,_general_msg_success,"index.php");
                }
            }
            $stmt->close();
        }
    }
    // else 
    // {
    //     putNotice("Notice","Please Enter Your Email Address");
    // } 
    // old       : 
    // generated :
    // new       :
}

function putNotice($title,$paragraph)
{
    echo "<script>
    var header = '".$title."';
    var text = '".$paragraph."';

    document.getElementById('noticeHeader').innerHTML = header;
    document.getElementById('noticeText').innerHTML = text;
    
    // Get the modal
    var modal = document.getElementById('noticeModal');

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName('closeNoticeModal')[0];

    modal.style.display = 'block';
    

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
    modal.style.display = 'none';
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = 'none';
    }
    }
    </script>";
}
function putNoticeFromThisPageToThatPage($title,$paragraph,$location)
{
    // echo "window.location.replace('".$location."');";
    echo "<script>
    var header = '".$title."';
    var text = '".$paragraph."';

    document.getElementById('noticeHeader').innerHTML = header;
    document.getElementById('noticeText').innerHTML = text;
    
    // Get the modal
    var modal = document.getElementById('noticeModal');

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName('closeNoticeModal')[0];

    modal.style.display = 'block';
    

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
    modal.style.display = 'none';
    window.location.replace('".$location."');
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = 'none';
        window.location.replace('".$location."');
    }
    }
    </script>";
}
function generateSimpleModal()
{
    echo "
        <div id='noticeModal' class='noticeModal'>
            <!-- Modal content -->
            <div class='modal-content'>
                <span class='closeNoticeModal' id='closeNoticeModal'>&times;</span>
                <div class='row'>
                    <div class='col'></div>
                    <div class='col-xl-10'>
                        <h4 class='modalHeader' id='noticeHeader'></h5>
                        <p id='noticeText' style='color: black;'></p>
                    </div>
                    <div class='col'></div>
                </div>
            </div>
        </div>
    ";
}
function resetPasswordInDB()
{
    if(isset($_POST["field_1"]) && isset($_POST["field_2"]) && isset($_POST["field_3"]))
    {
        if($_POST["field_3"] == $_POST["field_2"])
        {
            $conn = connDB();
            $tempPassword = $_POST["field_1"];
            $tempNewPassword = $_POST["field_2"];
            $tempConfirmNewPassword = $_POST["field_3"];
            $tempPassInDB = "";
            $tempSaltInDB = "";

                // $password = hash('sha256',$password);
                // $password = hash('sha256', $salt.$password);

                // echo $tempPassword."<br>";
                // echo $tempNewPassword."<br>";
                // echo $tempConfirmNewPassword."<br>";

            $stmt = $conn->prepare("SELECT password,salt FROM user WHERE uid = ?");
            $stmt->bind_param("s", $_GET['uid']);
            $stmt->execute();
            $result = $stmt->get_result();
            if($result->num_rows === 0) exit('No rows');
            while($row = $result->fetch_assoc()) 
            {
                $tempPassInDB = $row['password'];
                $tempSaltInDB = $row['salt'];
            }
            // echo $tempPassInDB."<br>";
            // echo $tempSaltInDB."<br>";

            // $tempPassword = hash('sha256',$tempPassword);
            $tempPassword = hash('sha256',$tempSaltInDB.$tempPassword);

            // echo $tempPassword."<br>";


            if($tempPassword == $tempPassInDB)
            {
                $tempNewPassword = hash('sha256',$tempNewPassword);
                $newSalt = substr(sha1(mt_rand()), 0, 100);
                $finalNewPassword = hash('sha256', $newSalt.$tempNewPassword);

                $stmt = $conn->prepare("UPDATE user SET password = ?,salt = ? WHERE uid = ?");
                $stmt->bind_param("sss",$finalNewPassword, $newSalt ,$_GET['uid']);
                $stmt->execute();

                return _general_reset_pw_success;
//                putNoticeFromThisPageToThatPage(_general_notice,_general_reset_pw_success,"indexLogin.php");
               
            }
            else
            {
                return _general_email_code;
//                putNotice(_general_notice,_general_email_code);
            }

            $stmt->close();
        }
        else
        {
            return _general_email_code;
//            putNotice(_general_notice,_general_new_retype_pw);
        }
        
    }
}
function getProfileData($uidSession)
{
    $conn = connDB();
    $arrProfile = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($uidSession),"s");

    $registerClass = new Register();

    $registerClass->setUsername($arrProfile[0]->getUsername());
    $registerClass->setEmail($arrProfile[0]->getEmail());
    $registerClass->setCountryID($arrProfile[0]->getCountryId());
    $registerClass->setPhone($arrProfile[0]->getPhoneNo());
    $registerClass->setEditProfileType(0);
    $registerClass->setIcNo($arrProfile[0]->getIcNo());

    //echo $arrProfile[0]->getCanSendNewsletter();
    $registerClass->setReceiveNotification($arrProfile[0]->getCanSendNewsletter());
    // echo $arrProfile[0]->getCanSendNewsletter();
    // echo $registerClass->getReceiveNotification();
    return $registerClass;
}
function editUserProfile($uidSession)
{
    $conn = connDB();
    //Temporarily using the register class to edit profile back
    $registerClass = new Register();
    
    $registerClass->setUsername(rewrite($_POST['field_1']));
    $registerClass->setEmail(rewrite($_POST['field_2']));
    $registerClass->setIcNo($_POST['field_3']);
    // $registerClass->setCountryID($_POST['country']);
    $registerClass->setCountryID(rewrite($_POST['field_5']));
    $registerClass->setPhone(rewrite($_POST['field_6']));
    $registerClass->setEditProfileType(1);

    if(!isset($_POST['defaultCheck2']) || $_POST['defaultCheck2'] == "")
    {
        $registerClass->setReceiveNotification(0);
    }
    else
    {
        $registerClass->setReceiveNotification(1);
    }

    //Filter Email
    $registerClass->setEmail(filter_var($registerClass->getEmail(),FILTER_SANITIZE_EMAIL));

    if(!filter_var($registerClass->getEmail(), FILTER_VALIDATE_EMAIL))
    {
        $registerClass->setRegisterErrorMessage(_general_email_format);
        return $registerClass;
    }
    else
    {
        $isUsernameDifferent = TRUE;
        $isEmailDifferent = FALSE;
        $isIcNoDifferent = FALSE;
        $boolValue = array($isUsernameDifferent,$isEmailDifferent);

        $allUserRows = getUser($conn,
            " WHERE email = ? ",
            array("email"),
            array($registerClass->getEmail()),
            "s");
        if($allUserRows == null)
        {
            $boolValue[0] = TRUE;
            $boolValue[1] = TRUE;

            $registerClass = updateProfile($registerClass,$boolValue,$uidSession);
            return $registerClass;
        }
        else
        {
            for($cnt = 0;$cnt < count($allUserRows);$cnt++)
            {
                if($allUserRows[$cnt]->getUid() == $uidSession)//can continue but dont save if same
                {
                    // if($allUserRows[$cnt]->getUsername() != $registerClass->getUsername()
                    // && $allUserRows[$cnt]->getEmail() == $registerClass->getEmail())
                    // {
                    //     $boolValue[0] = TRUE;
                    //     $registerClass = updateProfile($registerClass,$boolValue,$uidSession);
                    //     return $registerClass;
                    // }
                    // else if($allUserRows[$cnt]->getEmail() != $registerClass->getEmail()
                    // && $allUserRows[$cnt]->getUsername() == $registerClass->getUsername())
                    // {
                    //     $boolValue[1] = TRUE;
                    //     $registerClass = updateProfile($registerClass,$boolValue,$uidSession);
                    //     return $registerClass;
                    // }
                    // else if($allUserRows[$cnt]->getEmail() == $registerClass->getEmail()
                    //     && $allUserRows[$cnt]->getUsername() == $registerClass->getUsername())
                    // {
                    //     $registerClass = updateProfile($registerClass,$boolValue,$uidSession);
                    //     return $registerClass;
                    // }
                    // else
                    // {
                    //     $registerClass->setRegisterErrorMessage("server Error : Code (Username / Email)");
                    //     return $registerClass;
                    // }
                    if($allUserRows[$cnt]->getEmail() == $registerClass->getEmail())
                    {
                        $registerClass = updateProfile($registerClass,$boolValue,$uidSession);
                        return $registerClass;
                    }
                    else
                    {
                        $registerClass->setRegisterErrorMessage("server Error : Code (Username / Email)");
                        return $registerClass;
                    }
                }
                else
                {
                    $registerClass->setRegisterErrorMessage(_general_ald_reg);
                    return $registerClass;
                }
            }
        }
    }
}
function getPhoneNoFromDBToCompare($registerClass,$uidSession)
{
    $conn = connDB();
    $phoneNoExist = FALSE;

    $getPhoneNoFromThisUser = getUser($conn,
            " WHERE phone_no = ? AND country_id = ? ",
            array("phone_no","country_id"),
            array($registerClass->getPhone(),$registerClass->getCountryID()),
            "ss");

    if($getPhoneNoFromThisUser)
    {
        if ($getPhoneNoFromThisUser[0]->getUid() != $uidSession)
        {
            $phoneNoExist = TRUE;
        }
    }

    return $phoneNoExist;
}
function updateProfile($registerClass,$boolValue,$userDataUid)
{

    if(!getPhoneNoFromDBToCompare($registerClass,$userDataUid))
    {
        $conn = connDB();
        $columnField = array();
        $columnValues = array();
        $valueType = "";

        if($boolValue[0])
        {
            array_push($columnField,"username");
            array_push($columnValues,$registerClass->getUsername());
            $valueType .= "s";
        }
        if($boolValue[1])
        {
            array_push($columnField,"email");
            array_push($columnValues,$registerClass->getEmail());
            $valueType .= "s";
        }
        // if($boolValue[2])
        // {
        //     array_push($columnField,"phone_no");
        //     array_push($columnValues,$registerClass->getPhone());
        //     $valueType .= "s";
        // }
        // if($boolValue[3])
        // {
        //     array_push($columnField,"country_id");
        //     array_push($columnValues,$registerClass->getCountryID());
        //     $valueType .= "i";
        // }

        array_push($columnField,"ic_no");
        array_push($columnValues,$registerClass->getIcNo());
        $valueType .= "s";

        array_push($columnField,"can_send_newsletter");
        array_push($columnValues,$registerClass->getReceiveNotification());
        $valueType .= "i";

        array_push($columnField,"phone_no");
        array_push($columnValues,$registerClass->getPhone());
        $valueType .= "s";

        array_push($columnField,"country_id");
        array_push($columnValues,$registerClass->getCountryID());
        $valueType .= "i";

    //    array_push($columnField,"date_updated");
    //    array_push($columnValues,"CURRENT_TIMESTAMP");
    //    $valueType .= "s";

    //    array_push($columnField,"uid");
        array_push($columnValues,$userDataUid);
        $valueType .= "s";

    //    echo count($columnField);
    //    echo count($columnValues);
    //    echo strlen($valueType);
    //
    //    foreach ($columnField as $key)
    //    {echo $key;}
    //    foreach ($columnValues as $key1)
    //    {echo $key1;}


        if(updateDynamicData($conn,"user"," WHERE uid = ? ",$columnField,$columnValues,$valueType))
        {
            $registerClass->setRegisterErrorMessage(_general_edit_success);
            return $registerClass;
        }
        else
        {
            $registerClass->setRegisterErrorMessage(_general_sorry);
            return $registerClass;
        }
    }
    else
    {
        $registerClass->setRegisterErrorMessage("No Phone Existed! Please Try using other Phone No");//Here Sherry
        return $registerClass;
    }


    
}
function updateThisPassword($uid)
{
    $conn = connDB();
    $currentPassword = $_POST['field_1'];
    $newPassword = $_POST['field_2'];
    $confirmPassword = $_POST['field_3'];

    $registerClass = new Register();
    $registerClass->setPassword($newPassword);
    $registerClass->setConfirmPassword($confirmPassword);

    $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

    $dbPassWord = $userRows[0]->getPassword();
    $dbSalt = $userRows[0]->getSalt();

    // echo $dbPassWord."<br>";
    // echo $finalPassword."<br>";
    if(strlen($registerClass->getPassword()) < 6)
    {
        return _general_pw_min;
    }
    else
    {
        if (!preg_match("/^[a-zA-Z0-9]+$/",$registerClass->getPassword()))
        {
            return _general_only_numbers_abc;
        }
        else
        {
            if($registerClass->getConfirmPassword() != $registerClass->getPassword())
            {
                return _general_pw_error;
            }
            else
            {
                $tempCurrentPass = hash('sha256',$currentPassword);
                $finalCurrentPassword = hash('sha256', $dbSalt.$tempCurrentPass);
    
                if($finalCurrentPassword != $dbPassWord)
                {
                    return _general_pw_error;
                }
                else
                {
                    $tempNewPass = hash('sha256',$registerClass->getPassword());
                    $finalNewPassword = hash('sha256', $dbSalt.$tempNewPass);
    
                    if($finalNewPassword == $dbPassWord)
                    {
                        return _general_new_pw_not_same;
                    }
                    else
                    {   
                        //
                        $resettingNewPassword= hash('sha256',$registerClass->getPassword());
                        $newSalt = substr(sha1(mt_rand()), 0, 100);
                        $finalNewPassword = hash('sha256', $newSalt.$resettingNewPassword);


                        $stmt = $conn->prepare("UPDATE user SET password = ?,salt = ? WHERE uid = ?");
                        $stmt->bind_param("sss",$finalNewPassword, $newSalt ,$uid);
                        $stmt->execute();
                        $stmt->close();
                        return _general_pw_changed;
                    }    
                    //continue
                }
            }
        }
    }
}
?>