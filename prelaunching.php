<?php
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php require 'mainHeader.php';?>

<meta property="og:url" content="https://btcworg.com/prelaunching.php/" />
<meta property="og:title" content="BTCW - 预约" />
<meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。马上预约并在比特基金正式启动时获取通知！">
<meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。马上预约并在比特基金正式启动时获取通知！" />
<meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

<title>BTCW - 预约</title>
<link rel="canonical" href="https://btcworg.com/prelaunching.php/" />

</head>

<body>
       
  
    <div id="firefly" class="firefly-class">
    	<?php require 'mainNavbar.php';?>
    	<div class="width100 same-padding switch-row">
        	<a href="prelaunching-en.php" class="opacity-hover white-text language-a">
            	<p class="language-p">
            		<img src="img/sherry/en.png" alt="Switch to English" title="Switch to English" class="language-icon language-icon1">
                    <img src="img/sherry/en2.png" alt="Switch to English" title="Switch to English" class="language-icon language-icon2">
                      Switch to English
           		</p>
           </a>
        </div>
        <div class="width100 same-padding coin-gif-row">
    		<p class="gif-p">
            	<img src="img/sherry/bitcoin2.gif" class="bitcoin-video" alt="比特基金" title="比特基金">
            </p>
            <p class="launching-p white-text">
            	比特基金是全球唯一由官方打造的比特币创富平台。
            </p>
        </div>
        <div class="width100 big-div-three-items same-padding overflow-hidden">
        	<div class="one-item-div">
            	<img src="img/sherry/low-risk.png" class="three-item-icon" alt="保障中小型比特币捐赠者的利益" title="保障中小型比特币捐赠者的利益">
                <p class="three-item-p text-center">
                	保障中小型比特币捐赠者的利益
                </p>
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/profit.png" class="three-item-icon" alt="丰厚的静态捐赠和动态推广收益" title="丰厚的静态捐赠和动态推广收益">
                <p class="three-item-p text-center">
                	丰厚的静态捐赠和动态推广收益
                </p>
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/invest.png" class="three-item-icon" alt="最佳比特币创富平台" title="最佳比特币创富平台">
                <p class="three-item-p text-center">
                	最佳比特币创富平台
                </p>
            </div>            
        </div>
        <div class="width100 same-padding text-center">
        	<div class="fill-up-space"></div>
        	<a href="index.php" class="yellow-button-a" target="_blank">
                <div class="yellow-button yellow-left">
                    了解更多
                </div>
           </a>
           <div class="fill-up-space"></div>
        </div>
        <div class="clear"></div>
        <div class="width100 same-padding coin-gif-row pre-register-div">
            <p class="launching-p white-text">
                马上预约并在比特基金正式启动时获取通知！
            </p>
       </div>
       <div class="width100 same-padding coin-gif-row pre-register-inputdiv text-center">
            
             
        	<div class="fill-up-space"></div>
        	<a href="indexRegister.php" class="yellow-button-a" target="_blank">
                <div class="yellow-button yellow-left">
                    注册您的资料
                </div>
           </a>
           <div class="fill-up-space"></div>
     
       </div>      
       
               
    </div>

<?php require 'mainFooter.php';?>

</body>
</html>