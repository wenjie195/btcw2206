<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{
    if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
        $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
    {
        header('Location:index.php');
        exit();
    }
    else
    {
        $uid = $_SESSION['uid'];
        $conn = connDB();
        $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
    }
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['upline_email']) && isset($_POST['downline_email'])){
    $uplineEmail = rewrite($_POST['upline_email']);
    $downlineEmail = rewrite($_POST['downline_email']);

    //LEFT IS REFERRER/UPLINE RIGHT IS REFERRAL/DOWNLINE
    $emailArray = array(
        array($uplineEmail,$downlineEmail)
    );

    $no = 1;
    foreach($emailArray as $bothEmail){
        $uplineUserRows = getUser($conn," WHERE email = ? ",array("email"),array($bothEmail[0]),"s");
        $downlineUserRows = getUser($conn," WHERE email = ? ",array("email"),array($bothEmail[1]),"s");

        if(!$uplineUserRows){
            echoError("$no) No such upline user found with this email " . $bothEmail[0]);
            continue;
        }

        if(!$downlineUserRows){
            echoError("$no) No such downline user found with this email " . $bothEmail[1]);
            continue;
        }

        changeUplineForAnUpline($conn,$uplineUserRows[0]->getUid(),$downlineUserRows[0]->getUid(),$no);

        $no++;
    }
}else{
//    echoError("Didnt specify upline or downline email");
}

$conn->close();

function changeUplineForAnUpline($conn,$uplineUid,$downlineUid,$no){
    //$downlineType has 4 scenario:
    // 1 - can insert directly,
    // 2 - downline has referred someone,
    // 3 - downline already referred by someone else,
    // 4 - both
    $downlineType = 0; // 0 = error

    $downlineUplineRows = getUplineDetails($conn,$downlineUid);//got referred by anyone?
    $downlineDownlineRows = getDownlineDetails($conn,$downlineUid);//has this user referred anyone before?

    if(!$downlineUplineRows && !$downlineDownlineRows){
        $downlineType = 1;
    }else if($downlineUplineRows && $downlineDownlineRows){
        $downlineType = 4;
    }else if($downlineUplineRows){
        $downlineType = 3;
    }else if($downlineDownlineRows){
        $downlineType = 2;
    }

    $newReferrerHistoryRef = null;

    $uplineUplineRows = getUplineDetails($conn,$uplineUid);
    $uplineDownlineRows = getDownlineDetails($conn,$uplineUid);

    if($uplineDownlineRows){
        $newReferrerHistoryRef = new ReferralHistory();
        $newReferrerHistoryRef->setTopReferrerId($uplineDownlineRows[0]->getTopReferrerId());
        $newReferrerHistoryRef->setCurrentLevel($uplineDownlineRows[0]->getCurrentLevel());
    }else if($uplineUplineRows){
        $newReferrerHistoryRef = new ReferralHistory();
        $newReferrerHistoryRef->setTopReferrerId($uplineUplineRows[0]->getTopReferrerId());
        $currentLevel = $uplineUplineRows[0]->getCurrentLevel() + 1;
        $newReferrerHistoryRef->setCurrentLevel($currentLevel);
    }else if(!$uplineDownlineRows && !$uplineUplineRows){
        $newReferrerHistoryRef = new ReferralHistory();
        $newReferrerHistoryRef->setTopReferrerId($uplineUid);
        $newReferrerHistoryRef->setCurrentLevel(1);
    }

    if(!$newReferrerHistoryRef){
        echoError("$no) EEEEEERROR dont have newReferrerHistoryRef : upline - $uplineUid --- downline - $downlineUid");
        return;
    }

    switch ($downlineType){
        case 0:
            echoError("$no) EEEEEERROR 0 : upline - $uplineUid --- downline - $downlineUid");
            break;
        case 1:
            $newTableId = insertIntoReferralHistory($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef->getCurrentLevel(),$newReferrerHistoryRef->getTopReferrerId());
            if($newTableId){
                echoError("$no) success 1 : upline - $uplineUid --- downline - $downlineUid");
            }else{
                echoError("$no) EEEEEERROR 1 can insert directly: upline - $uplineUid --- downline - $downlineUid");
            }
            break;
        case 2:
            updateForDownlineThatHasReferredSomeone($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef,$no,$downlineType);
            break;
        case 3:
            updateForDownlineThatHasReferredSomeone($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef,$no,$downlineType);
            break;
        case 4:
            updateForDownlineThatHasReferredSomeone($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef,$no,$downlineType);
            break;
        default:
            echoError("$no) EEEEEERROR default: upline - $uplineUid --- downline - $downlineUid");
            break;
    }
}

function insertIntoReferralHistory($conn,$referrerId,$referralId,$currentLevel,$topReferrerId){
    return insertDynamicData($conn,"referral_history",
        array("referrer_id","referral_id","current_level","top_referrer_id"),
        array($referrerId,$referralId,$currentLevel,$topReferrerId),
        "ssis");
}

function updateForDownlineThatHasReferredSomeone($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef,$no,$downlineType){
    $hasError = false;

    if($downlineType === 4){
        if(!updateIntoReferralHistoryFIRST_TARGET($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef->getCurrentLevel(),$newReferrerHistoryRef->getTopReferrerId())){
            $hasError = true;
            echoError("$no) EEEEEERROR1 $downlineType downline has referred someone: upline - $uplineUid --- downline - $downlineUid " . $newReferrerHistoryRef->getCurrentLevel() . " " . $newReferrerHistoryRef->getTopReferrerId());
            return;
        }
    }else if($downlineType === 3){//3 means this referral/downline guy has not refer anyone
        if(!updateIntoReferralHistoryFIRST_TARGET($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef->getCurrentLevel(),$newReferrerHistoryRef->getTopReferrerId())){
            $hasError = true;
            echoError("$no) EEEEEERROR2 $downlineType downline has referred someone: upline - $uplineUid --- downline - $downlineUid");
            return;
        }
    }else if($downlineType === 2){//2 means this referral/downline guy is not referred by anyone
        if(!insertIntoReferralHistory($conn,$uplineUid,$downlineUid,$newReferrerHistoryRef->getCurrentLevel(),$newReferrerHistoryRef->getTopReferrerId())){
            $hasError = true;
            echoError("$no) EEEEEERROR3 $downlineType downline has referred someone: upline - $uplineUid --- downline - $downlineUid");
            return;
        }
    }else{
        echoError(" NOTHING SELECTED");
        return;
    }

    $wholeDownlineTree = getWholeDownlineTree($conn,$downlineUid,false);
    if($wholeDownlineTree && count($wholeDownlineTree) > 0){
        $firstLevel = $wholeDownlineTree[0]->getCurrentLevel();
        $count = 1;
        foreach ($wholeDownlineTree as $thisReferral) {
            if($firstLevel !== $thisReferral->getCurrentLevel()){
                $firstLevel++;
                $count++;
            }

            $thisReferralCurrentLevel = $newReferrerHistoryRef->getCurrentLevel() + $count;
            if(!updateIntoReferralHistoryALL_FOLLOWING_DOWNLINES($conn,$thisReferral->getReferralId(),$thisReferralCurrentLevel,$newReferrerHistoryRef->getTopReferrerId())){
                echoError("$no) EEEEEERROR4 $downlineType downline has referred someone FOLLOWING: upline - $downlineUid --- downline - ".$thisReferral->getReferralId());
                $hasError = true;
            }
        }
    }

    if(!$hasError){
        echoError("$no) success $downlineType : upline - $uplineUid --- downline - $downlineUid");
    }
}

//this is only for the USER THAT WANTS TO CHANGE UPLINE (NOT FOR UPDATING ALL HIS FOLLOWING DOWNLINES)
function updateIntoReferralHistoryFIRST_TARGET($conn,$referrerId,$referralId,$currentLevel,$topReferrerId){
    return updateDynamicData($conn,"referral_history"," WHERE referral_id = ? ",
        array("referrer_id","current_level","top_referrer_id"),
        array($referrerId,$currentLevel,$topReferrerId,$referralId),
        "siss");
}

//this is only for updating all the user's (user = USER THAT WANTS TO CHANGE UPLINE) following downlines
//current level need++ depend on how many loop
function updateIntoReferralHistoryALL_FOLLOWING_DOWNLINES($conn,$referralId,$currentLevel,$topReferrerId){
    return updateDynamicData($conn,"referral_history"," WHERE referral_id = ? ",
        array("current_level","top_referrer_id"),
        array($currentLevel,$topReferrerId,$referralId),
        "iss");
}

function getUplineDetails($conn,$uid){
    //check if this suer has been referred by anyone
    return getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
}

function getDownlineDetails($conn,$uid){
    //check if this user has referred anyone or not

    return getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uid),"s");
}

function echoError($message){
    echo "<p> $message </p>";
}

?>
<!doctype html>
<html lang="en">
<head>
    <?php require_once 'mainHeader.php';?>
    <title>Level 1 Invest</title>
</head>
<body>
<?php
require_once 'mainNavbar.php';
generateSimpleModal();

?>
<div id="firefly" class="firefly-class min-height">
    <div class="width100 same-padding more-separate-margin-top edit-div">
        <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a>
        <div class="clear"></div>

        <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>

        <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Change Referral Tree:</b></h4>
        <form class="register-form"  method="POST" >
            <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">UPLINE email</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="upline_email" id="upline_email" value="<?php if(isset($_POST['upline_email'])){echo $_POST['upline_email'];} ?>"></h4></td>
                </tr>

                <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">DOWNLINE email</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="downline_email" id="downline_email" value="<?php if(isset($_POST['downline_email'])){echo $_POST['downline_email'];} ?>"></h4></td>
                </tr>

                <!-- <tr>
                  <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">邮箱</b></h4></td>
                  <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                  <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" placeholder="电邮" ></h4></td>
                </tr> -->

            </table>
            <div class="clear"></div>
            <button class="register-button2 clean orange-hover inputb-button" name="insertValue" id="insertValue" >Change Referral Tree</button>

        </form>
    </div>
    <div class="clear"></div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h4 class="btcw-h4 separate-title white-text mt-5"><b class="weight-700">Total User : <?php //echo count($userInvestedData);?></b></h4>
            <h4 class="btcw-h4 separate-title white-text"><b class="weight-700">All user's total investment data：</b> </h4>
            <table cellspacing="0" cellpadding="0" class="dark-table recommend-table2 white-text-table mt-5">

                <?php
                if(false && $userInvestedData && count($userInvestedData) > 0) {
                    echo '
                             <tr>
                                 <th class="table2-1">序</th>
                                 <th class="table2-2">UID</th>
                                 <th class="table2-3">用户名</th>
                                 <th class="table2-4">Email</th>
                                 <th>Plan A</th>
                                 <th>Plan B</th>
                             </tr>
                         ';

                    $currentNo = 0;
                    foreach ($userInvestedData as $data) {
                        $currentNo++;

                        echo '
                                         <tr class="tr2">
                                             <td class="table2-1">'.$currentNo.'.</td>
                                             <td class="table2-2">'.$data['user']->getUid().'</td>
                                             <td class="table2-3">'.$data['user']->getUsername().'</td>
                                             <td class="table2-4">'.$data['user']->getEmail().'</td>';
                        if((isset($_POST['more_field']) && strlen($_POST['more_field']) > 0) || isset($_POST['less_field']) && strlen($_POST['less_field']) > 0 ){
                            if(strlen($_POST['more_field']) > 0 && strlen($_POST['less_field']) > 0){
                                if($data['planA'] >= $_POST['more_field'] && $data['planA'] <= $_POST['less_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planA'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planA'].'</td>';
                                }
                                if($data['planB'] >= $_POST['more_field'] && $data['planB'] <= $_POST['less_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planB'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planB'].'</td>';
                                }
                            }
                            else if(strlen($_POST['more_field']) > 0){
                                if($data['planA'] >= $_POST['more_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planA'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planA'].'</td>';
                                }
                                if($data['planB'] >= $_POST['more_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planB'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planB'].'</td>';
                                }
                            }else if(strlen($_POST['less_field']) > 0){
                                if($data['planA'] <= $_POST['less_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planA'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planA'].'</td>';
                                }
                                if($data['planB'] <= $_POST['less_field']){
                                    echo '<td style="background-color: yellow; color: black;">'.$data['planB'].' (HERE)</td>';
                                }else{
                                    echo '<td>'.$data['planB'].'</td>';
                                }
                            }
                        }else{
                            echo   '<td>'.$data['planA'].'</td>';
                            echo   '<td>'.$data['planB'].'</td>';
                        }

                        echo       '</tr>';
                    }

                    echo "</table>";
                }else{
//                    echo "<p class='white-text'>还没推荐任何一个人。</p>";
                }

                ?>

            </table>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
        <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
    </div>
</div>
<?php require_once 'mainFooter.php';?>
</body>
</html>