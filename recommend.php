<?php
//Start the session
session_start();

//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
    exit();
}else{
    $uid = $_SESSION['uid'];
}

require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';

$conn = connDB();

$referralHistoryRows = array();
$thisUserFirstTimeReferralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ORDER BY id ASC LIMIT 1 ",array("referrer_id"),array($uid),"s");
if($thisUserFirstTimeReferralHistoryRows){
   $topReferrerId = $thisUserFirstTimeReferralHistoryRows[0]->getTopReferrerId();
   if($topReferrerId === $uid){
       $thisUserFirstLevel = 1;
       $thisUserLastLevel = 10;
   }else{
       $thisUserFirstLevel = $thisUserFirstTimeReferralHistoryRows[0]->getCurrentLevel();
       $thisUserLastLevel = $thisUserFirstLevel + 9;
   }

   $downlineReferralIdArray = array();
   $downlineReferralIdArray[$uid]  = true ;
   for($loopCurrentLevel = $thisUserFirstLevel; $loopCurrentLevel <= $thisUserLastLevel; $loopCurrentLevel++){
       $thisLevelReferralHistoryRows = getReferralHistory($conn," WHERE top_referrer_id = ? AND current_level = $loopCurrentLevel ORDER BY id ",array("top_referrer_id"),array($topReferrerId),"s");
       if($thisLevelReferralHistoryRows){
           $newDownlineReferralIdArray = array();
           foreach ($thisLevelReferralHistoryRows as $tempReferral) {
               $canAdd = false;
               foreach ($downlineReferralIdArray as $key => $value) {
                   if($tempReferral->getReferrerId() === $key){
                       $newDownlineReferralIdArray[$tempReferral->getReferralId()]  = true ;
                       $canAdd = true;
                       break;
                   }
               }
               if($canAdd){
                   array_push($referralHistoryRows,$tempReferral);
               }
           }
           unset($downlineReferralIdArray);
           $downlineReferralIdArray = array();
           foreach ($newDownlineReferralIdArray as $newDownlineReferralId => $value) {
               $downlineReferralIdArray[$newDownlineReferralId]  = true ;
           }
           unset($newDownlineReferralIdArray);
       }
   }
}

?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        <meta property="og:url" content="https://btcworg.com/recommend.php" />
        <meta property="og:title" content="<?= _recommend_title ?>" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

        <title><?= _recommend_title ?></title>
        <link rel="canonical" href="https://btcworg.com/recommend.php" />
  </head>
  <body>
  <?php require 'mainNavbar.php';?>
      <div id="dialog" class="dialog-css" style="display: none">
       <?= _profile_link_copied ?>
    </div>
  <div id="firefly" class="firefly-class min-height">    
      <!-- <div class="width100 same-padding more-separate-margin-top text-center">
        <h4 class="btcw-h4 white-text"><b class="weight-700">专属推荐链接：</b> </h4>
        <p class="share-link"><a href="<?php //echo "https://btcworg.com/indexRegister.php?referralId=$uid";?>" id="invest-now-referral-link" class="share-a yellow-text opacity-hover"><?php //echo "https://btcworg.com/indexRegister.php?referralId=$uid";?></a></p>
      </div>
      <div class="width100 same-padding copy-button-div">
        <div class="fill-up-space1 ow-height"></div>
        <button class="yellow-line-box yellow-box-width yellow-box-hover white-text text-center clean copy-yellow-line" id="copy-referral-link">复制链接</button>
        <div class="fill-up-space1 ow-height"></div>
      </div>   -->
    
    
      <div class="width100 same-padding overflow-hidden">
        <div class="fill-up-space1"></div>
          <!--
        <a href="scanQr.php" class=""><div class="yellow-box no-margin yellow-box-width yellow-box-hover white-text text-center">扫描二维码</div></a>
        -->
        <div class="fill-up-space1"></div>
      </div>
    
      <div class="width100 same-padding recommend-padding">
     
            <h4 class="btcw-h4 separate-title white-text text-center"><b class="weight-700"><?= _recommend_user ?>：</b> </h4>
            <!-- Unhide it when the function is ready-->
            <!--<div class="search-input-div input-container2 edit-margin">
                <input type="text" class="search-input inputa clean2" placeholder="输入用户名">
                <img src="img/sherry/search2.png" class="search-icon input-icon" alt="搜索" title="搜索">
            </div>
            
             <h4 class="btcw-h4 separate-title white-text text-center"><b class="weight-700">代数：</b> </h4>
             
             <div class="tab">
              <button class="tablinks" onclick="swicthTab(event, 'Gen1')">1</button>
              <button class="tablinks" onclick="swicthTab(event, 'Gen2')">2</button>
              <button class="tablinks" onclick="swicthTab(event, 'Gen3')">3</button>
              <button class="tablinks" onclick="swicthTab(event, 'Gen4')">4</button>
              <button class="tablinks" onclick="swicthTab(event, 'Gen5')">5</button>
              <button class="tablinks second-last-button" onclick="swicthTab(event, 'Gen6')">6</button>  
              <button class="tablinks" onclick="swicthTab(event, 'Gen7')">7</button>
              <button class="tablinks" onclick="swicthTab(event, 'Gen8')">8</button>
              <button class="tablinks" onclick="swicthTab(event, 'Gen9')">9</button>
              <button class="tablinks" onclick="swicthTab(event, 'Gen10')">10</button>
              <button class="tablinks last-button" onclick="swicthTab(event, 'Gen0')">1-10</button>     
            </div>--> 
 

	 <div id="Gen0" class="tabcontent display-it">             
            <table cellspacing="0" cellpadding="0" class="dark-table recommend-table2 white-text-table">

                <?php
                    if($referralHistoryRows && count($referralHistoryRows) > 0) {
                        echo '
                            <tr>
                                <th class="table2-1">'._recommend_number.'</th>
                                <th class="table2-2">'._recommend_username.'</th>
                                <th class="table2-3">'._recommend_join_date.'</th>
                                <th>'._recommend_number_of_referee.'</th>
                                <th>'._recommend_level.'</th>
                            </tr>
                        ';
                        $currentLevel = $thisUserFirstLevel - 1;
                        $currentNo = 0;

                        foreach ($referralHistoryRows as $thisReferral) {
                            $currentNo++;
                            if($currentLevel < $thisReferral->getCurrentLevel()){
                                $currentLevel++;
                            }

                            $referralUid = $thisReferral->getReferralId();
                            $tempUserRow = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($referralUid),"s");

                            //for tracking this user's downline total invested capital btc
//                            $transactionHistoryRow = getTransactionHistory($conn," WHERE uid = ? ",array("uid"),array($referralUid),"s");
//                            $thisUserInvestedBtc = 0;
//                            if($transactionHistoryRow){
//                                foreach($transactionHistoryRow as $transactionHistory){
////                                    $thisUserInvestedBtc = bcadd($thisUserInvestedBtc,$transactionHistory->getCapitalBtcAmountIn(),25);
//                                    $thisUserInvestedBtc += $transactionHistory->getCapitalBtcAmountIn();
//                                }
//                            }
//                            if($thisUserInvestedBtc <= 0){
//                                $thisUserInvestedBtc = "-";
//                            }

                            if($tempUserRow && count($tempUserRow) > 0){
                                $referralUser = $tempUserRow[0];
                                $totalReferredCount = getCount($conn,"referral_history","referrer_id"," WHERE referrer_id = ? ", array("referrer_id"), array($referralUser->getUid()),"s");

                                echo '
                                        <tr class="tr2">
                                            <td class="table2-1">'.$currentNo.'.</td>
                                            <td class="table2-2">'.$referralUser->getUsername().'</td>
                                            <td class="table2-3">'. date( 'd/m/Y', strtotime($referralUser->getDateCreated()) ) .'</td>
                                            <td class="table2-4">'.$totalReferredCount.'</td>
                                            <td>'.($currentLevel - $thisUserFirstLevel + 1).'</td>
                                        </tr>';
                            }
                        }

                        echo "</table>";
                    }else{
                        echo "<p class='white-text'>"._recommend_no_referee."</p>";
                    }

                    $conn->close();
                ?>
            
            </table>
           </div>

        
        <div id="Gen1" class="tabcontent">
 		<!-- insert the table here but remove the column number of generation 代数-->
       
        </div>
        
        <div id="Gen2" class="tabcontent">
         <!-- insert the table here but remove the column number of generation 代数-->

        </div>
        
        <div id="Gen3" class="tabcontent">
         <!-- insert the table here but remove the column number of generation 代数-->

        </div>
        <div id="Gen4" class="tabcontent">
 		<!-- insert the table here but remove the column number of generation 代数-->

        </div>
        
        <div id="Gen5" class="tabcontent">
         <!-- insert the table here but remove the column number of generation 代数-->

        </div>
        
        <div id="Gen6" class="tabcontent">
         <!-- insert the table here but remove the column number of generation 代数-->
        </div>
        <div id="Gen7" class="tabcontent">
         <!-- insert the table here but remove the column number of generation 代数-->
        </div>
        <div id="Gen8" class="tabcontent">
 		<!-- insert the table here but remove the column number of generation 代数-->
        </div>
        
        <div id="Gen9" class="tabcontent">
         <!-- insert the table here but remove the column number of generation 代数-->
        </div>
        
        <div id="Gen10" class="tabcontent">
         <!-- insert the table here but remove the column number of generation 代数-->
        </div>           
           
           
          </div>
 
      </div>
	</div>

 

<script>
function swicthTab(evt, genNumber) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(genNumber).style.display = "block";
  evt.currentTarget.genNumber += " active";
}
</script>



       
   <div id="myModal10" class="modal2 ow-modal2 ow-modal10">
 
          <div class="modal-content2 modal-content-white ow-modal-width ow-modal10a">
            <h4 class="btcw-h4 text-center ow10-h4"><?= _profile_link_copied ?></h4>
          </div>
   </div>



  <?php require 'mainFooter.php';?>

  <script>
      $("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#invest-now-referral-link').attr("href");
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
      });
  </script>
         <script>
			// Get the modal
			var modal10 = document.getElementById('myModal10');
			
			// Get the button that opens the modal
			var btn10 = document.getElementById("copy-referral-link");

			// When the user clicks the button, open the modal
			btn10.onclick = function() {
			  modal10.style.display = "block";
                setTimeout(closeCopiedModalWithDelay, 1600);
			};


			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal10) {
				modal10.style.display = "none";
			  }
			};

            function closeCopiedModalWithDelay() {
                modal10.style.display = "none";
            }
		</script>
    
  </body>
</html>