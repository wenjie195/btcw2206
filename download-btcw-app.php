<?php
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        
        <meta property="og:url" content="https://btcworg.com/" />
        <meta property="og:title" content="比特基金" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
        
        <title>比特基金</title>
        <link rel="canonical" href="https://btcworg.com/" />
  </head>
  <body>

    <?php require 'mainNavbar.php';?>         
	<div id="firefly" class="firefly-class min-height text-center">

            <div class="width100 same-padding">
                <h1 class="btcw-h1 text-center first-h1 white-text">比特基金是全球唯一由官方打造的比特币创富平台。</h1>
    
                <p class="text-center download-p white-text">使用百度网盘下载安卓比特基金APP （ 提取码： j44w ）</p>
                <div class="clear"></div>            
                <div class="fill-space4"></div>
                <div class="actual-width4"><a href="https://pan.baidu.com/s/1oYO_pisyzvHmnd6KB7T_-w" target="_blank" class="opacity-hover"><img src="img/sherry/baidu-pan.png" class="width100"></a></div>
                <div class="fill-space4"></div>
                <div class="clear"></div>                       
                <p class="text-center download-p white-text little-mtop">或用微信扫描以下二维码下载</p>
                <div class="clear"></div>
                <div class="fill-space4"></div>
                <div class="actual-width4"><img src="img/sherry/qrcode-baidu.jpg" class="width100"></div>
                <div class="fill-space4"></div>
                <div class="clear"></div> 
                <p class="text-center download-p white-text little-mtop">海外用户推荐点击以下图片下载</p>             
                <div class="clear"></div>          
                <div class="fill-space4"></div>
                <div class="actual-width4"><a href="app/btwc_platform.apk" class="opacity-hover" download><img src="img/sherry/download-apk.png" class="width100"></a></div>
                <div class="fill-space4"></div>
                <div class="clear"></div>
                <p class="text-center download-p white-text little-mtop">&nbsp;</p>    
                         
                <!--<div class="fill-small-space"></div>
                <a href="#" class="a-hover"><div class="yellow-box yellow-box-width yellow-box-hover white-text text-center">马上加入成为比特基金一族</div></a>
                <div class="fill-small-space fill-small-space-m"></div>
                <div class="fill-small-space fill-small-space-m"></div>
                <a href="#" class="a-hover"><div class="yellow-line-box yellow-box-width yellow-line-box-hover white-text text-center">选择你的基金</div></a>
                <div class="fill-small-space"></div>-->
            </div>

 
     </div>
            <div class="clear"></div>     
             <?php require 'mainFooter.php';?> 

  		<style>

			.social-icon-div{
				display:none;}
        </style>      

  </body>
</html>