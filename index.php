<?php
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        
        
        <meta property="og:url" content="https://btcworg.com/" />
        <meta property="og:title" content="比特基金" />
        <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。 BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
        
        <title>比特基金</title>
        <link rel="canonical" href="https://btcworg.com/" />
  </head>
  <body>
  <div class="index-div"></div>
  <style>
  	.index-div{
		min-height:100vh;
		background-color:#13161f;}
  </style>
     <!--   <?php //require 'mainNavbar.php';?>
        <div class="clear"></div> 
        
    <div id="firefly" class="firefly-class min-height">
            <div class="width100 same-padding">
        	<h1 class="btcw-h1 text-center first-h1 white-text">比特基金是全球唯一由官方打造的比特币创富平台。</h1>
            <div class="fill-space4"></div>
            <div class="actual-width4"><a href="app/btwc_platform.apk" class="opacity-hover" download><img src="img/sherry/download-apk.png" class="width100"></a></div>
            <div class="fill-space4"></div>
            
            <!--<div class="fill-small-space"></div>
            <a href="#" class="a-hover"><div class="yellow-box yellow-box-width yellow-box-hover white-text text-center">马上加入成为比特基金一族</div></a>
            <div class="fill-small-space fill-small-space-m"></div>
            <div class="fill-small-space fill-small-space-m"></div>
            <a href="#" class="a-hover"><div class="yellow-line-box yellow-box-width yellow-line-box-hover white-text text-center">选择你的基金</div></a>
            <div class="fill-small-space"></div>-->
        </div>
        <!--<div class="social-icon-div">
        	<p class="fb-p">
            	<a href="#">
                    <img src="img/indexFBLink.png" class="fb-icon social-icon opacity-hover" alt="Facebook" title="Facebook">
                    
                </a>
            </p>
            <p class="tw-p">
            	<a href="#">            
                    <img src="img/indexTwitterLink.png" class="tw-icon social-icon opacity-hover" alt="Twitter" title="Twitter">
                   
                </a>
            </p>
        </div>-->
        <div class="clear"></div>
        <div class="width100 same-padding text-center video-div">
        	<button id="myBtn2" class="button-css clean hover-container white-text"><img src="img/indexPlayButton.png" class="play-button hover-1" alt="什么是比特基金?" title="什么是比特基金?"><img src="img/sherry/yellow-play.png" class="play-button hover-2" alt="什么是比特基金?" title="什么是比特基金?"> 什么是比特基金?</button>
        </div>
        
        
        
        <!--- Modal --->
        <div id="myModal2" class="modal2">
        
          <!-- Modal content -->
          <div class="modal-content2">
            <span class="close2">&times;</span>
            <iframe width="100%" height="699" class="youtube-video" src="https://www.youtube.com/embed/Gc2en3nHxA4" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        
        </div>

		<div class="width100 element-div">
        	<img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
        
        <div class="clear"></div>
        
        
        
        
        <div class="width100 big-div-three-items same-padding overflow-hidden separate-margin-top">
        	<h1 class="btcw-h1 yellow-text text-center news-h1">最新消息</h1>
            <a href="http://www.bitcoin86.com/news/35726.html" target="_blank" class="opacity-hover">
                <div class="one-item-div news-one-item-div">
                    <img src="img/sherry/news1.jpg" class="width100 opacity-hover" alt="2019年，虚拟货币的走势如何呢？" title="2019年，虚拟货币的走势如何呢？">
                    <p class="date-p opacity-hover">
                    	2019-02-15
                    </p>
                    <p class="news-p opacity-hover white-text-important">
                        2019年，虚拟货币的走势如何呢？
                    </p>
                </div>
            </a>
            <a href="http://www.bitcoin86.com/news/35707.html" target="_blank" class="opacity-hover">            
                <div class="one-item-div middle-one-item news-one-item-div">
                    <img src="img/sherry/news2.jpg" class="width100 opacity-hover" alt="你的币去哪了？资产追踪成了币圈2019年第一个风口" title="你的币去哪了？资产追踪成了币圈2019年第一个风口">
                    <p class="date-p opacity-hover">
                    	2019-02-14
                    </p>                    
                    <p class="news-p opacity-hover white-text-important">
                        你的币去哪了？资产追踪成了币圈2019年第一个风口
                    </p>
                </div> 
            </a>
            <a href="http://www.bitcoin86.com/news/35681.html" target="_blank" class="opacity-hover">                       
                <div class="one-item-div news-one-item-div">
                    <img src="img/sherry/news3.jpg" class="width100 opacity-hover" alt="一个新事物诞生了：向金钱控制体系发起冲锋" title="一个新事物诞生了：向金钱控制体系发起冲锋">
                    <p class="date-p opacity-hover">
                    	2019-02-14
                    </p>
                    <p class="news-p opacity-hover white-text-important">
                        一个新事物诞生了：向金钱控制体系发起冲锋
                    </p>
                </div> 
            </a>           
        </div>
        
        <div class="clear"></div>
        
        <div class="width100 big-div-three-items same-padding overflow-hidden more-separate-margin-top">
        	<h1 class="btcw-h1 yellow-text text-center news-h1 white-text">比特基金商业模式</h1>
            <h1 class="btcw-h1 text-center weight-700 white-text">1. 大手笔空投奖励</h1>
            <p class="text-center des-p white-text">比特基金创立的目的就是为了保障中小型比特币捐赠者的利益，增加比特币流通量，助推比特币升值。 </p>
        	<div class="one-item-div">
            	<img src="img/sherry/free-registration.png" class="three-item-icon" alt="免费注册" title="免费注册">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	免费注册。
                </p>
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/bitcoin.png" class="three-item-icon" alt="平台即送0.05个 BTC" title="平台即送0.05个 BTC">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	平台即送0.05个BTC。
                </p>
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/five-people.png" class="three-item-icon" alt="直推5人再送0.05个" title="直推5人再送0.05个">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	直推5人再送0.05个。
                </p>
            </div>            
        </div>
        
        <div class="clear"></div>
        
        <div class="width100 big-div-three-items same-padding overflow-hidden more-separate-margin-top">
        	
            <h1 class="btcw-h1 text-center weight-700 margin-bottom white-text">2. 丰厚的静态捐赠收益</h1>
           
        	<div class="one-item-div">
            	<img src="img/sherry/triple.png" class="three-item-icon" alt="资产立即放大3倍" title="资产立即放大3倍">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	只需投入1个 BTC，平台将赠送用户2个 BTC，资产立即放大3倍。
                </p>
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/time.png" class="three-item-icon" alt="提取金额" title="提取金额">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	以每日0.2%的比例释放到你的账户。这0.2%的比例释放将会在30天后才生效。用户只允许在30天内的第10天，第20天或第30天提取金额和需要缴付0%手续费。
                </p>
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/transaction-fee.png" class="three-item-icon" alt="手续费" title="手续费">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	用户也可以选择投入本金随时提取收益但需缴付18%手续费。
                </p>
            </div>            
        </div>     
        
        <div class="clear"></div>
        
        <div class="width100 big-div-three-items same-padding overflow-hidden more-separate-margin-top">
        	
            <h1 class="btcw-h1 text-center weight-700 margin-bottom white-text">3. 丰厚的动态推广收益</h1>
            <p class="text-center des-p white-text">比特基金除了可以通过复投让收益最大化，还有躺赚模式——推广收益。</p>
        	<div class="one-item-div">
            	<img src="img/sherry/low-risk.png" class="three-item-icon" alt="社区获取0.2%的比例时" title="社区获取0.2%的比例时">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	社区获取0.2%的比例时
                </p>
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/profit.png" class="three-item-icon" alt="上线将会获得0.1%的比例收益" title="上线将会获得0.1%的比例收益">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	上线将会获得0.1%的比例收益
                </p>
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/pyramid.png" class="three-item-icon" alt="用户最多只可拥有十代的社区" title="用户最多只可拥有十代的社区">
                <p class="three-item-p text-center yellow-text-important weight-700 white-text-important">
                	用户最多只可拥有十代的社区
                </p>
            </div>            
        </div>       
           
        <div class="clear"></div>
        
        <div class="width100 same-padding overflow-hidden more-separate-margin-top">
        	<h1 class="btcw-h1 text-center weight-700 margin-bottom white-text">项目介绍</h1>
            <div class="right-img-div float-right"><img src="img/indexIntro.png" class="width100" alt="BTCW" title="BTCW"></div>
        	<div class="left-text-div float-left white-text">比特基金，简称BTCW。由比特币开发团队和全球比特币社区联合打造。致力于比特币的捐赠管理，通过先进的金融技术手法，各种捐赠组合彻底保障中小型比特币捐赠者的利益；致力于在全球范围推广比特币，扩大比特币粉丝群体从而增加比特币流通量，拉升比特币价值。比特基金愿景：与广大捐赠者一起，共创市场奇迹, 再造财富传奇! </div>
        	
        </div>
                
        <div class="clear"></div>
        
        <div class="width100 big-div-three-items same-padding overflow-hidden more-separate-margin-top extra-padding-bottom">
        	
            <h1 class="btcw-h1 text-center weight-700 margin-bottom white-text">什么是比特基金</h1>
            
        	<div class="one-item-div">
            	<img src="img/sherry/global.png" class="three-item-icon" alt="普及度第一" title="普及度第一">
                <p class="three-item-p text-center yellow-text-important weight-700">
                	普及度第一
                </p>
                <p class="three-item-p text-center black-text justify-three white-text-important">
                	到目前为止， 比特币的普及以及不再是小范围的。从委内瑞拉到津巴布韦，从韩国到瑞士，全球社区都开始接纳比特币。自比特币诞生以来，全球交易量就一直在稳步增长。相关的数据十分惊人，但更令人感到兴奋的是，比特币市场的发展并不仅限于小范围内，而是全球性的，并且是几乎统一的。当然，技术越先进的国家呈现出的普及度越高。
                </p>                
                
            </div>
        	<div class="one-item-div middle-one-item">
            	<img src="img/sherry/value.png" class="three-item-icon" alt="总价值第一" title="总价值第一">
                <p class="three-item-p text-center yellow-text-important weight-700">
                	总价值第一
                </p>
                <p class="three-item-p text-center black-text justify-three white-text-important">
					据统计截止到目前为止，全球数字货币市场总市值达到2000亿美元，而比特币总市值就超过了700亿美元，牢牢占据全球第一的宝座。
                </p>                
            </div>            
        	<div class="one-item-div">
            	<img src="img/sherry/influence.png" class="three-item-icon" alt="影响力第一" title="影响力第一">
                <p class="three-item-p text-center yellow-text-important weight-700">
                	影响力第一
                </p>
                <p class="three-item-p text-center black-text justify-three white-text-important">
					比特币是伴随着区块链技术诞生的全球第一种数字货币，收到了全球最多的关注。比特币的市值也是整个数字货币市场的晴雨表。
                </p>                
                
            </div>            
        </div> 
      </div> --> 


        <script>
			// Get the modal
			var modal = document.getElementById('myModal2');
			
			// Get the button that opens the modal
			var btn = document.getElementById("myBtn2");
			
			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close2")[0];
			
			// When the user clicks the button, open the modal 
			btn.onclick = function() {
			  modal.style.display = "block";
			}
			
			// When the user clicks on <span> (x), close the modal
			span.onclick = function() {
			  modal.style.display = "none";
			}
			
			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal) {
				modal.style.display = "none";
			  }
			}

            var width = screen.width;
			if(width > 1000){
                window.location.replace("bitcoin.php");
            }
			if(width < 1000){
                window.location.replace("splashscreen.php");
            }
		</script>
        
        
        
 		<?php require 'mainFooter.php';?>

  </body>
</html>