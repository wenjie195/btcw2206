<?php
//Start the session
session_start();
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'dbCon/dbCon.php';
require_once 'generalFunction.php';
//Check if the session uid is empty/exist or not
if(empty($_SESSION['uid'])) //Michael Acc
{
    header('Location:index.php');
    exit();
}
else
{ 
     if(  $_SESSION['uid'] != "VXtbpgh5sdSoEXGqhKK54UOZDd92" && //Test Acc
          $_SESSION['uid'] != "CUSTOM-ID-66466961aed2c8a6add27b7e1ee675933efddf85")
     {
          header('Location:index.php');
          exit();
     }
     else
     {
          $uid = $_SESSION['uid'];
          $conn = connDB();
          $userRows = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($_SESSION['uid']),"s");
     }
}

$additionalInfo = array();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     if(isset($_POST['field_1']))
     {
         $startDate = "2018-05-20";
         if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['field_0'])){
             $startDate = rewrite($_POST['field_0']);
         }

          $emailORUsername = $_POST['field_1'];
          $error = FALSE;

          $userDetails = getUser($conn,
                         " WHERE email = ? OR username = ? OR uid = ? ",
                         array("email","username","uid"),
                         array($emailORUsername,$emailORUsername,$emailORUsername),
                         "sss");

          if($userDetails != null) 
          {
               $referralHistoryRows = array();
               $userDetails[0]->getUid()."<br>";
               $thisUserFirstTimeReferralHistoryRows = getReferralHistory($conn," WHERE referrer_id = ? ORDER BY id ASC LIMIT 1 ",array("referrer_id"),array($userDetails[0]->getUid()),"s");
               if($thisUserFirstTimeReferralHistoryRows){
                    $topReferrerId = $thisUserFirstTimeReferralHistoryRows[0]->getTopReferrerId();
               if($topReferrerId === $userDetails[0]->getUid()){
                    $thisUserFirstLevel = 1;
                    $thisUserLastLevel = 10;
               }else{
                    $thisUserFirstLevel = $thisUserFirstTimeReferralHistoryRows[0]->getCurrentLevel();
                    $thisUserLastLevel = $thisUserFirstLevel + 9;
               }
               
               $downlineReferralIdArray = array();
               $downlineReferralIdArray[$userDetails[0]->getUid()]  = true ;
               for($loopCurrentLevel = $thisUserFirstLevel; $loopCurrentLevel <= $thisUserLastLevel; $loopCurrentLevel++){
                    $thisLevelReferralHistoryRows = getReferralHistory($conn," WHERE top_referrer_id = ? AND current_level = $loopCurrentLevel ORDER BY id ",array("top_referrer_id"),array($topReferrerId),"s");
                    if($thisLevelReferralHistoryRows){
                         $newDownlineReferralIdArray = array();
                         foreach ($thisLevelReferralHistoryRows as $tempReferral) {
                              $canAdd = false;
                              foreach ($downlineReferralIdArray as $key => $value) {
                              if($tempReferral->getReferrerId() === $key){
                                   $newDownlineReferralIdArray[$tempReferral->getReferralId()]  = true ;
                                   $canAdd = true;
                                   break;
                              }
                              }
                              if($canAdd){
                              array_push($referralHistoryRows,$tempReferral);
                              }
                         }
                         unset($downlineReferralIdArray);
                         $downlineReferralIdArray = array();
                         foreach ($newDownlineReferralIdArray as $newDownlineReferralId => $value) {
                              $downlineReferralIdArray[$newDownlineReferralId]  = true ;
                         }
                         unset($newDownlineReferralIdArray);
                    }
               }

                   $additionalInfo = getTotalInvestedAmountOfBothPlans($conn,$referralHistoryRows,$startDate);

               }
          }
     }
}

function getTotalInvestedAmountOfBothPlans($conn, $referralHistoryRows,$startDate){
    $additionalInfo = array();
    for($i = 0; $i < count($referralHistoryRows); $i++){
        $tempUid = $referralHistoryRows[$i]->getReferralId();

        $totalInvestedPlanA = getSum($conn,"transaction_history","capital_btc_amount_in"," WHERE uid = ? AND date_created >= ? AND plan_id = 1 AND btc_type_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ",array("uid","date_created"),array($tempUid,$startDate),"ss");
        $totalInvestedPlanB = getSum($conn,"transaction_history","capital_btc_amount_in"," WHERE uid = ? AND date_created >= ? AND plan_id = 2 AND btc_type_id = 2 AND (withdraw_status = 0 OR withdraw_status = 3) ",array("uid","date_created"),array($tempUid,$startDate),"ss");

        $tempInfo = array();
        $tempInfo['planA'] = removeUselessZero($totalInvestedPlanA);
        $tempInfo['planB'] = removeUselessZero($totalInvestedPlanB);

        array_push($additionalInfo,$tempInfo);
    }
    return $additionalInfo;
}
?>
<!doctype html>
<html lang="en">
  <head>
        <?php require_once 'mainHeader.php';?>
        <title>ZCXC</title>
  </head>
  <body>
  <?php
      require_once 'mainNavbar.php';
      generateSimpleModal();

  ?>
  <div id="firefly" class="firefly-class min-height">  
     <div class="width100 same-padding more-separate-margin-top edit-div">
          <a href="check.php"><div class="btn btn-outline-warning btn-lg mb-2">Back to Transaction Check</div></a> 
          <div class="clear"></div>
         <? require_once dirname(__FILE__) . '/adminNavMenu.php'; ?>
            <h4 class="btcw-h4 edit-h4-title white-text"><b class="weight-700">Check Referral (downlines)</b></h4>
            <form class="register-form"  method="POST" >
                <table  cellspacing="0" cellpadding="0" class="transparent-table edit-table white-text-table">
                    <tr>
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Investment Start Date until today (example: 2019-05-20)</b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_0" id="field_0" value="<?php if(isset($_POST['field_0'])){echo $_POST['field_0'];} ?>"></h4></td>
                    </tr>
                  <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Email/Username/User ID</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_1" id="field_1" value="<?php if(isset($_POST['field_1'])){echo $_POST['field_1'];} ?>"></h4></td>
                  </tr>
                    <tr>
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">More than or equal to</b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="more_field" id="more_field" value="<?php if(isset($_POST['more_field'])){echo $_POST['more_field'];} ?>"></h4></td>
                    </tr>

                    <tr>
                        <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">Less than or equal to</b></h4></td>
                        <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                        <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="less_field" id="less_field" value="<?php if(isset($_POST['less_field'])){echo $_POST['less_field'];} ?>"></h4></td>
                    </tr>

                  <!-- <tr>
                    <td class="first-td2" width="40%" valign="top"><h4 class="btcw-h4 edit2-h4"><b class="weight-700">邮箱</b></h4></td>
                    <td class="second-td2" width="2%" valign="top"><h4 class="btcw-h4 edit2-h4">:</h4></td>
                    <td class="third-td2" width="58%" valign="top"><h4 class="btcw-h4 edit2-h4"><input type="text" class="inputa clean2 inputb"  name="field_2" id="field_2" placeholder="电邮" ></h4></td>
                  </tr> -->

                </table>
                <div class="clear"></div>
               <button class="register-button2 clean orange-hover inputb-button" name="insertValue" id="insertValue" >Check Insertion Value</button>

          </form>
          </div> 
          <div class="clear"></div>
          <div class="row">
               <div class="col-md-1"></div>
               <div class="col-md-10">
                         <h4 class="btcw-h4 separate-title white-text mt-5"><b class="weight-700">Total Downlines : <?php if($_SERVER['REQUEST_METHOD'] == 'POST')
{ echo count($referralHistoryRows);}?></b></h4>
                    <h4 class="btcw-h4 separate-title white-text"><b class="weight-700">已推荐用户：</b> </h4>
                    <table cellspacing="0" cellpadding="0" class="dark-table recommend-table2 white-text-table mt-5">

                <?php
                if($_SERVER['REQUEST_METHOD'] == 'POST')
                {
                    if($referralHistoryRows && count($referralHistoryRows) > 0) {
                         echo '
                             <tr>
                                 <th class="table2-1">序</th>
                                 <th class="table2-2">UID</th>
                                 <th class="table2-3">用户名</th>
                                 <th class="table2-4">Email</th>
                                 <th>Plan A</th>
                                 <th>Plan B</th>
                                 <th>代数</th>
                             </tr>
                         ';
                         $currentLevel = $thisUserFirstLevel - 1;
                         $currentNo = 0;
 
                         foreach ($referralHistoryRows as $thisReferral) {
                             $currentNo++;
                             if($currentLevel < $thisReferral->getCurrentLevel()){
                                 $currentLevel++;
                             }
 
                             $referralUid = $thisReferral->getReferralId();
                             $tempUserRow = getUser($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1",array("uid"),array($referralUid),"s");
                             if($tempUserRow && count($tempUserRow) > 0){
                                 $referralUser = $tempUserRow[0];
                                 $totalReferredCount = getCount($conn,"referral_history","referrer_id"," WHERE referrer_id = ? ", array("referrer_id"), array($referralUser->getUid()),"s");
 
                                 echo '
                                         <tr class="tr2">
                                             <td class="table2-1">'.$currentNo.'.</td>
                                             <td class="table2-2">'.$referralUser->getUid().'</td>
                                             <td class="table2-3">'.$referralUser->getUsername().'</td>
                                             <td class="table2-4">'.$referralUser->getEmail().'</td>';
                                 if((isset($_POST['more_field']) && strlen($_POST['more_field']) > 0) || isset($_POST['less_field']) && strlen($_POST['less_field']) > 0 ){
                                     if(strlen($_POST['more_field']) > 0 && strlen($_POST['less_field']) > 0){
                                         if($additionalInfo[$currentNo - 1]['planA'] >= $_POST['more_field'] && $additionalInfo[$currentNo - 1]['planA'] <= $_POST['less_field']){
                                             echo '<td style="background-color: yellow; color: black;">'.$additionalInfo[$currentNo - 1]['planA'].' (HERE)</td>';
                                         }else{
                                             echo '<td>'.$additionalInfo[$currentNo - 1]['planA'].'</td>';
                                         }
                                         if($additionalInfo[$currentNo - 1]['planB'] >= $_POST['more_field'] && $additionalInfo[$currentNo - 1]['planB'] <= $_POST['less_field']){
                                             echo '<td style="background-color: yellow; color: black;">'.$additionalInfo[$currentNo - 1]['planB'].' (HERE)</td>';
                                         }else{
                                             echo '<td>'.$additionalInfo[$currentNo - 1]['planB'].'</td>';
                                         }
                                     }
                                     else if(strlen($_POST['more_field']) > 0){
                                         if($additionalInfo[$currentNo - 1]['planA'] >= $_POST['more_field']){
                                             echo '<td style="background-color: yellow; color: black;">'.$additionalInfo[$currentNo - 1]['planA'].' (HERE)</td>';
                                         }else{
                                             echo '<td>'.$additionalInfo[$currentNo - 1]['planA'].'</td>';
                                         }
                                         if($additionalInfo[$currentNo - 1]['planB'] >= $_POST['more_field']){
                                             echo '<td style="background-color: yellow; color: black;">'.$additionalInfo[$currentNo - 1]['planB'].' (HERE)</td>';
                                         }else{
                                             echo '<td>'.$additionalInfo[$currentNo - 1]['planB'].'</td>';
                                         }
                                     }else if(strlen($_POST['less_field']) > 0){
                                         if($additionalInfo[$currentNo - 1]['planA'] <= $_POST['less_field']){
                                             echo '<td style="background-color: yellow; color: black;">'.$additionalInfo[$currentNo - 1]['planA'].' (HERE)</td>';
                                         }else{
                                             echo '<td>'.$additionalInfo[$currentNo - 1]['planA'].'</td>';
                                         }
                                         if($additionalInfo[$currentNo - 1]['planB'] <= $_POST['less_field']){
                                             echo '<td style="background-color: yellow; color: black;">'.$additionalInfo[$currentNo - 1]['planB'].' (HERE)</td>';
                                         }else{
                                             echo '<td>'.$additionalInfo[$currentNo - 1]['planB'].'</td>';
                                         }
                                     }
                                 }else{
                                     echo   '<td>'.$additionalInfo[$currentNo - 1]['planA'].'</td>
                                             <td>'.$additionalInfo[$currentNo - 1]['planB'].'</td>';
                                 }

                                 echo       '<td>'.($currentLevel - $thisUserFirstLevel + 1).'</td>
                                         </tr>';
                             }
                         }
 
                         echo "</table>";
                     }else{
                         echo "<p class='white-text'>还没推荐任何一个人。</p>";
                     }
 
                     $conn->close();
                }
                ?>
            
            </table>
               </div>
               <div class="col-md-1"></div>
          </div>
        <div class="width100 element-div extra-padding-bottom more-separate-margin-top">
            <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>
    </div>
  <?php require_once 'mainFooter.php';?>
  </body>
</html>