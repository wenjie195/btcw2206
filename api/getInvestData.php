<?php
header("Access-Control-Allow-Origin: *");

//error code
//100 = no such user found
//101 = Please specify the type of plan (1 = A, 2 = B)
//102 = This user did not invested any bitcoin
//1 = success

//plan,email,ic,username

require_once dirname(__FILE__) . '/../generalFunction.php';
require_once dirname(__FILE__) . '/../utilities/calculationFunction.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';

$conn = connDB();

echo json_encode(getInvestedAmount($conn));

$conn->close();

function getInvestedAmount($conn){
    $query = "null";
//    $planId = 0;

    $resultArray = array();
    $resultArray['status'] = "success";
    $resultArray['description'] = "success";
    $resultArray['code'] = "1";

//    if(isset($_GET["plan"])){
//        $planId = $_GET["plan"];
//    }

//    if($planId <= 0){
//        $resultArray['status'] = "failed";
//        $resultArray['code'] = "101";
//        $resultArray['description'] = "Please specify the type of plan (1 = A, 2 = B)";
//        return $resultArray;
//    }else{
//        $resultArray['plan'] = $planId;
//    }

    if(isset($_GET["query"])){
        $query = $_GET["query"];
    }

//    $userRows = getUser($conn," WHERE email = ? OR ic_no = ? OR username = ? ",array("email","ic_no","username"),array($query,$query,$query),"sss");
    $userRows = getUser($conn," WHERE ic_no = ? ",array("ic_no"),array($query),"s");

    if($userRows){
        $user = $userRows[0];
        $username = $user->getUsername();
        $ic = $user->getIcNo();

        if(!$username){
            $username = "";
        }

        if(!$ic){
            $ic = "";
        }

        $allInvestedCapitalTHRows = getTransactionHistory($conn,
            " WHERE uid = ? AND ( withdraw_status = 0 OR withdraw_status = 3 ) AND btc_type_id = 2 ORDER BY date_created DESC ",
            array("uid"),
            array($user->getUid()),
            "s");

        if($allInvestedCapitalTHRows){
            $totalAmountInvested = 0;
            $dateInvested = null;

            foreach ($allInvestedCapitalTHRows as $transactionHistory){
                $totalAmountInvested = bcadd($totalAmountInvested,$transactionHistory->getCapitalBtcAmountIn(),25);
                $dateInvested = $transactionHistory->getDateCreated();
            }

            $resultArray['name'] = $username;
            $resultArray['ic'] = $ic;
            $resultArray['amountInvested'] = removeUselessZero($totalAmountInvested);
            $resultArray['dateInvested'] = date('Y/m/d',strtotime($dateInvested));
            return $resultArray;
        }else{
            $resultArray['status'] = "failed";
            $resultArray['code'] = "102";
            $resultArray['description'] = "This user did not invested any bitcoin for plan $planId";
            return $resultArray;
        }
    }else{
        $resultArray['status'] = "failed";
        $resultArray['code'] = "100";
        $resultArray['description'] = "No such user found";
        return $resultArray;
    }
}



