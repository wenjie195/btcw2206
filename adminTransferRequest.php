<!doctype html>
<html lang="en">
  <head>
        <?php require 'mainHeader.php';?>
        
        <meta property="og:url" content="https://btcworg.com/adminTransferRequest.php" />
        <meta property="og:title" content="BTCW - Transfer Bitcoin Request" />
        <meta name="description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
        <meta property="og:description" content="BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
        <meta name="keywords" content="BTCW, bitcoin, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">
        
        <title>BTCW - Transfer Bitcoin Request</title>
        <link rel="canonical" href="https://btcworg.com/adminTransferRequest.php" />
  </head>
  <body>
  		<?php require 'adminNavbar.php';?>

  		<div class="width100 same-padding padding-top-bottom">
            <div class="width100 edit-spacing">
                <div class="float-left left-title">
                    <h4 class="btcw-h4 weight-700 record-h4">比特币转移申请 <a class="orange2-text plan-a" href="adminTransferHistory.php"> (查看记录）</a></h4>
                </div>
                <div class="right-filter float-right">
                    <select class="inputa clean2 filter-select"  name="">
                                    <option selected>最旧申请开始</option>
                                    <option>最新申请开始</option>
                                    <option>最大转移总额开始</option>  
                                    <option>最小转移总额开始</option>                                                                                         
                   </select>
                </div>
            </div>		
         <div class="clear"></div>
         <table class="table100">
            <thead class="orange-thead">
                <tr class="orange-tr">
                    <th class="th1 th-1 white-text">序</th>
                    <th class="th2 white-text">日期</th>
                    <th class="th3 white-text">用户名</th>
                    <th class="th4 white-text">转移总额</th>
                    <th class="th5 white-text">备注</th>
                    <th class="th6 white-text">文件</th>
                    <th class="th7 th-7 white-text">决策</th>
                </tr>
            </thead> 
        	<tr class="transparent-tr">
            	<td class="th1">1.</td>
                <td class="th2">11/2/2019</td>
                <td class="th3"><a href="profile.php" target="_blank" class="orange1-text">小明</a></td>
                <td class="th4">500</td>
                <td class="th5">我想加入这平台</td>
                <td class="th6"><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                <td class="th7"><button class="opacity-hover clean accept-btn btn-css"><img src="img/sherry/approve.png" class="opacity-hover action-btn-img"></button>
                				<button class="opacity-hover clean reject-btn extra-space btn-css"><img src="img/sherry/reject.png" class="opacity-hover action-btn-img"></button>
                </td>
            </tr>   
         	<tr class="transparent-tr">
            	<td class="th1">2.</td>
                <td class="th2">12/2/2019</td>
                <td class="th3"><a href="profile.php" target="_blank" class="orange1-text">小芳</a></td>
                <td class="th4">100</td>
                <td class="th5">我想加入这平台</td>
                <td class="th6"><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                <td class="th7"><button class="opacity-hover clean accept-btn btn-css"><img src="img/sherry/approve.png" class="opacity-hover action-btn-img"></button>
                				<button class="opacity-hover clean reject-btn extra-space btn-css"><img src="img/sherry/reject.png" class="opacity-hover action-btn-img"></button>
                </td>
            </tr>           
         	<tr class="transparent-tr">
            	<td class="th1">3.</td>
                <td class="th2">13/2/2019</td>
                <td class="th3"><a href="profile.php" target="_blank" class="orange1-text">小李</a></td>
                <td class="th4">300</td>
                <td class="th5">我想加入这平台</td>
                <td class="th6"><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                <td class="th7"><button class="opacity-hover clean accept-btn btn-css"><img src="img/sherry/approve.png" class="opacity-hover action-btn-img"></button>
                				<button class="opacity-hover clean reject-btn extra-space btn-css"><img src="img/sherry/reject.png" class="opacity-hover action-btn-img"></button>
                </td>
            </tr>            
         	<tr class="transparent-tr">
            	<td class="th1">4.</td>
                <td class="th2">13/2/2019</td>
                <td class="th3"><a href="profile.php" target="_blank" class="orange1-text">小紫</a></td>
                <td class="th4">400</td>
                <td class="th5">我想加入这平台</td>
                <td class="th6"><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                <td class="th7"><button class="opacity-hover clean accept-btn btn-css"><img src="img/sherry/approve.png" class="opacity-hover action-btn-img"></button>
                				<button class="opacity-hover clean reject-btn extra-space btn-css"><img src="img/sherry/reject.png" class="opacity-hover action-btn-img"></button>
                </td>
            </tr>  
         	<tr class="transparent-tr">
            	<td class="th1">5.</td>
                <td class="th2">13/2/2019</td>
                <td class="th3"><a href="profile.php" target="_blank" class="orange1-text">小芯</a></td>
                <td class="th4">500</td>
                <td class="th5">我想加入这平台</td>
                <td class="th6"><button class="opacity-hover clean document-btn btn-css"><img src="img/sherry/document.png" class="opacity-hover action-btn-img"></button></td>
                <td class="th7"><button class="opacity-hover clean accept-btn btn-css"><img src="img/sherry/approve.png" class="opacity-hover action-btn-img"></button>
                				<button class="opacity-hover clean reject-btn extra-space btn-css"><img src="img/sherry/reject.png" class="opacity-hover action-btn-img"></button>
                </td>
            </tr>             
        </table>    
                                                                   
        </div> 
        <div class="width100 element-div extra-padding-bottom profile-divider">
                <img src="img/indexLinkage.png" class="bitcoin-element width100" alt="bitcoin" title="bitcoin">
        </div>        

        <!--- Modal --->
        <div id="myModal12" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width">
            <span class="close12 ow-close2 close-style">&times;</span>
            
            <div class="width100 small-padding">
            	<h4 class="btcw-h4 form-h4"><b class="weight-700">上载资料</b></h4>
            	<img src="img/sherry/document2.png" class="width100" alt="uploaded-document" title="uploaded-document">
            </div>
          </div>
        </div>
	
        <!--- Modal --->
        <div id="myModal11" class="modal2 ow-modal2">
        
          <!-- Modal content -->
          <div class="modal-content2 modal-content-white ow-modal-width">
            <span class="close11 ow-close2 close-style">&times;</span>
            <p class="qr-img"><img src="img/sherry/transfer.png" class="qr" alt="拒绝转移原因" title="拒绝转移原因"></p>
            
                <form class="register-form width100 same-padding"  method="POST">
                	    <h4 class="btcw-h4 form-h4"><b class="weight-700">拒绝转移原因</b></h4>  
                    	<div class="input-container1">
                            <select class="inputa clean2"  name="field_1">
                    			<option disabled selected>选择拒绝原因</option>
                    			<option>资料有误</option>
                    			<option>不符合条件</option>  
                    			<option>少于最低捐赠金额</option>
                    			<option>超出最高捐赠金额</option> 
                    			<option>其他</option>                                                                                              
                			</select>
							<img src="img/sherry/remark.png" class="input-icon">
                        </div>
                        
                        <textarea class="textarea-reason" name="" placeholder="如果原因选其他，需填写"></textarea>
						<div class="width100 overflow">
                        	<div class="fill-up-space1 modal-fill-space-1"></div>
                        	<button class="register-button2 clean orange-hover invest-btn" name="loginButton" id="loginButton" >发送</button>
                            <div class="fill-up-space1 modal-fill-space-1"></div>
                         </div>
                         <div class="clear"></div>
 						<div class="width100 overflow">
                        	<div class="fill-up-space1 modal-fill-space-1"></div>                        
                        	<button class="register-button2 yellow-line-box yellow-box-width copy-yellow-line clean orange-line-hover close11 box-box-space ow-orange-line" name="loginButton" id="loginButton" >取消</button>
                            <div class="fill-up-space1 modal-fill-space-1"></div>
                        </div>                            
               </form>            
          </div>
        </div>


         
 		<?php require 'adminFooter.php';?>
          <script>
			// Get the modal
			var modal11 = document.getElementById('myModal11');
			
			// Get the button that opens the modal
			var btn11 = document.getElementsByClassName("reject-btn")[0];
			
			// Get the button that opens the modal
			var btn11a = document.getElementsByClassName("reject-btn")[1];			
			
			// Get the button that opens the modal
			var btn11b = document.getElementsByClassName("reject-btn")[2];	
			
			
			// Get the button that opens the modal
			var btn11c = document.getElementsByClassName("reject-btn")[3];			

			
			// Get the button that opens the modal
			var btn11d = document.getElementsByClassName("reject-btn")[4];	
			

						
			// Get the <span> element that closes the modal
			var span11 = document.getElementsByClassName("close11")[0];

			// Get the <span> element that closes the modal
			var span11a = document.getElementsByClassName("close11")[1];



			// Get the modal
			var modal12 = document.getElementById('myModal12');
			
			// Get the button that opens the modal
			var btn12 = document.getElementsByClassName("document-btn")[0];
			
			// Get the button that opens the modal
			var btn12a = document.getElementsByClassName("document-btn")[1];			
			
			// Get the button that opens the modal
			var btn12b = document.getElementsByClassName("document-btn")[2];	
			
			
			// Get the button that opens the modal
			var btn12c = document.getElementsByClassName("document-btn")[3];			

			
			// Get the button that opens the modal
			var btn12d = document.getElementsByClassName("document-btn")[4];	
			

						
			// Get the <span> element that closes the modal
			var span12 = document.getElementsByClassName("close12")[0];

		






			
			// When the user clicks the button, open the modal
			btn11.onclick = function() {
			  modal11.style.display = "block";
			};
			
			// When the user clicks the button, open the modal
			btn11a.onclick = function() {
			  modal11.style.display = "block";
			};
			
			// When the user clicks the button, open the modal
			btn11b.onclick = function() {
			  modal11.style.display = "block";
			};			
			
			// When the user clicks the button, open the modal
			btn11c.onclick = function() {
			  modal11.style.display = "block";
			};
			
			// When the user clicks the button, open the modal
			btn11d.onclick = function() {
			  modal11.style.display = "block";
			};			
			




			
			// When the user clicks the button, open the modal
			btn12.onclick = function() {
			  modal12.style.display = "block";
			};
			
			// When the user clicks the button, open the modal
			btn12a.onclick = function() {
			  modal12.style.display = "block";
			};
			
			// When the user clicks the button, open the modal
			btn12b.onclick = function() {
			  modal12.style.display = "block";
			};			
			
			// When the user clicks the button, open the modal
			btn12c.onclick = function() {
			  modal12.style.display = "block";
			};
			
			// When the user clicks the button, open the modal
			btn12d.onclick = function() {
			  modal12.style.display = "block";
			};	



						
			// When the user clicks on <span> (x), close the modal
			span11.onclick = function() {
			  modal11.style.display = "none";
			};


			// When the user clicks on <span> (x), close the modal
			span11a.onclick = function() {
			  modal11.style.display = "none";
			};

						
			// When the user clicks on <span> (x), close the modal
			span12.onclick = function() {
			  modal12.style.display = "none";
			};


			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal11) {
				modal11.style.display = "none";
			  }
			  if (event.target == modal12) {
				modal12.style.display = "none";
			  }

			}
		</script>

  </body>
</html>