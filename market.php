<?php
//Start the session
session_start();

if(empty($_SESSION['uid'])) {
    // Go back to index page
    // NOTE : MUST PROMPT ERROR

    header('Location:index.php');
    exit();
}
else
{
    $uid = $_SESSION['uid'];
}

require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once 'generalFunction.php';
require_once 'utilities/calculationFunction.php';
require_once 'utilities/databaseFunction.php';

$conn = connDB();

$graphDataRows = getBtcUsdPairGraph($conn," ORDER BY date_created DESC LIMIT 14 ");

$dateString = "";
$btcString = "";
$usdString = "";

for($i = (count($graphDataRows) - 1); $i >= 0; $i--){
    $graphData = $graphDataRows[$i];

    $thisGraphDate = date( 'i:s', strtotime($graphData->getDateCreated()) );

    $dateString .= '"' . $thisGraphDate . '",';
    $btcString .= $graphData->getBtcTransaction() . ",";
    $usdString .= $graphData->getUsdTransaction() . ",";
}

//remove last character of ,
$dateString = substr_replace($dateString ,"",-1);
$btcString = substr_replace($btcString ,"",-1);
$usdString = substr_replace($usdString ,"",-1);

$conn->close();

?>

<!doctype html>
<html lang="en">
<head>
    <?php require 'mainHeader.php';?>
    <meta property="og:url" content="https://btcworg.com/market.php" />
    <meta property="og:title" content="<?= _market_title ?>" />
    <meta name="description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!">
    <meta property="og:description" content="比特基金是全球唯一由官方打造的比特币创富平台。BTCW is the only official cryptocurrency foundation platform for Bitcoin in the world. Be the first to know when BTCW launches!" />
    <meta name="keywords" content="BTCW, bitcoin, profile, user, bitcoin.org, bitcoinorg, cryptocurrency foundation platform, e-foundation, investment, invest, 用户, 个人页面, 比特币, 比特基金, 预约, 创富平台, 捐赠, 利益, 收益, etc">

    <title><?= _market_title ?></title>
    <link rel="canonical" href="https://btcworg.com/market.php" />

    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
    <style>
        #lineGraph{
            background-color: #001033;
            border-radius: 20px;
			padding-top:20px;
			padding-bottom:20px;
			padding-left:35px;
			padding-right:35px;
        }
    </style>
</head>
<body>
<?php require 'mainNavbar.php';?>

<div class="livetrade-bg">

    <div class="width100 same-padding recommend-padding live-mtop">
            <h1 class="live-blue-text live-bold"><b class="live-bold"><?= _market_live_trades ?></b></h1>

            <table cellspacing="0" cellpadding="0" class="dark-table recommend-table2 white-text-table live-first-table" id="live_trade_table">

            </table>

    </div>

    <div class="width100 same-padding recommend-padding live-mtop market-normal-padding">
            <h1 class="live-blue-text live-bold gain-h1"><b class="live-bold"><?= _market_gain_loss ?></b></h1>
			<div class="live-three-div" id="okex-gl">
            	<div class="live-half-top">
                    <h1 class="green-buy live-big-h1">2.04%</h1>
                    <img class="platform-logo" src="img/sherry/okex.png" alt="OKEX" title="OKEX">
                </div>
                <div class="live-half-bottom">
                	<p class="live-half-p"><span class="green-buy p-span">Buy</span> USD 8881</p>
                    <p class="live-half-p"><span class="red-sell p-span">Sell</span> USD 8899</p>
                </div>
            </div>
			<div class="live-three-div live-three-middle second-three-div" id="huobi-gl">
            	<div class="live-half-top">
                    <h1 class="green-buy live-big-h1">3.50%</h1>
                    <img class="platform-logo" src="img/sherry/huobi.png" alt="<?= _market_huobi ?>" title="<?= _market_huobi ?>">
                </div>
                <div class="live-half-bottom">
                	<p class="live-half-p"><span class="green-buy p-span">Buy</span> USD 8881</p>
                    <p class="live-half-p"><span class="red-sell p-span">Sell</span> USD 8899</p>
                </div>
            </div>
			<div class="live-three-div" id="bitfinex-gl">
            	<div class="live-half-top">
                    <h1 class="green-buy live-big-h1">2.75%</h1>
                    <img class="platform-logo" src="img/sherry/bitfinex.png" alt="BITFINEX" title="BITFINEX">
                </div>
                <div class="live-half-bottom">
                	<p class="live-half-p"><span class="green-buy p-span">Buy</span> USD 8881</p>
                    <p class="live-half-p"><span class="red-sell p-span">Sell</span> USD 8899</p>
                </div>
            </div>
 			<div class="live-three-div second-three-div" id="binance-gl">
            	<div class="live-half-top">
                    <h1 class="green-buy live-big-h1">4.09%</h1>
                    <img class="platform-logo" src="img/sherry/binance.png" alt="<?= _market_binance ?>" title="<?= _market_binance ?>">
                </div>
                <div class="live-half-bottom">
                	<p class="live-half-p"><span class="green-buy p-span">Buy</span> USD 8881</p>
                    <p class="live-half-p"><span class="red-sell p-span">Sell</span> USD 8899</p>
                </div>
            </div>
			<div class="live-three-div live-three-middle" id="coinbene-gl">
            	<div class="live-half-top">
                    <h1 class="green-buy live-big-h1">5.60%</h1>
                    <img class="platform-logo" src="img/sherry/coinbene.png" alt="COINBENE" title="COINBENE">
                </div>
                <div class="live-half-bottom">
                	<p class="live-half-p"><span class="green-buy p-span">Buy</span> USD 8881</p>
                    <p class="live-half-p"><span class="red-sell p-span">Sell</span> USD 8899</p>
                </div>
            </div>
 			<div class="live-three-div second-three-div" id="bitforex-gl">
            	<div class="live-half-top">
                    <h1 class="green-buy live-big-h1">6.04%</h1>
                    <img class="platform-logo" src="img/sherry/bitforex.png" alt="BITFOREX" title="BITFOREX">
                </div>
                <div class="live-half-bottom">
                	<p class="live-half-p"><span class="green-buy p-span">Buy</span> USD 8881</p>
                    <p class="live-half-p"><span class="red-sell p-span">Sell</span> USD 8899</p>
                </div>
            </div>
    </div>
    <div class="clear"></div>

    <div class="width100 same-padding recommend-padding live-mtop live-pbottom market-normal-padding">
            <h1 class="live-blue-text live-bold gain-h1"><b class="live-bold"><?= _market_bitcoin_usd ?></b></h1>
            <canvas id="lineGraph"></canvas>

    </div>




</div>

<?php require 'mainFooter.php';?>

<script>
    //trade data

    getBitcoinPlatformData();
    window.setInterval(function(){
        getBitcoinPlatformData();
    }, 5000);

    function getBitcoinPlatformData(){
        $.ajax({
            crossOrigin: false,
            url: "apiCron/getAjaxLiveTrades.php",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                if(!data){
                    console.error("live btc data is null");
                    return;
                }

                let liveTradeTableContent = '';
                if(!data['live_trades'] || data['live_trades'].length <= 0){
                    console.error("live trade is empty");
                    liveTradeTableContent = '<tr><td class="table2-1"><?= _market_no_trades ?></td></tr>';
                }else{
                    liveTradeTableContent = '<tr>\n' +
                        '                       <th class="table2-1 first-column normal"><?= _market_time ?></th>\n' +
                        '                       <th class="table2-2 normal"><?= _market_platform ?></th>\n' +
                        '                       <th class="table2-3 normal"><?= _market_type ?></th>\n' +
                        '                       <th class="table2-4 normal"><?= _market_price ?></th>\n' +
                        '                       <th class="last-column normal"><?= _market_amount ?></th>\n' +
                        '                   </tr>';
                    $.each(data['live_trades'], function(key, value) {
                        var date = new Date(Date.parse(value['timestamp'].replace(/-/g, '/')));
                        let platfrom = "null";
                        let type = "null";

                        switch (value['apiType']){
                            case 1:
                                platfrom = "<?= _market_huobi ?>";
                                break;
                            case 2:
                                platfrom = "<?= _market_binance ?>";
                                break;
                            case 3:
                                platfrom = "<?= _market_okex ?>";
                                break;
                            case 4:
                                platfrom = "<?= _market_bitstamp ?>";
                                break;
                            case 5:
                                platfrom = "POLONIEX";
                                break;
                            case 6:
                                platfrom = "BITFINEX";
                                break;
                            case 7:
                                platfrom = "COINBENE";
                                break;
                            case 8:
                                platfrom = "BITFOREX";
                                break;
                            default:
                                platfrom = "null";
                                break;
                        }

                        switch (value['direction']){
                            case 1:
                                type = "<span class='green-buy'><?= _market_buy ?></span>";
                                break;
                            case 2:
                                type = "<span class='red-sell'><?= _market_sell ?></span>";
                                break;
                            default:
                                type = "null";
                                break;
                        }

                        //key is the count since i didnt specify anything
                        liveTradeTableContent += "<tr>";
                        liveTradeTableContent += '<td class="table2-1 first-column normal">'+date.toLocaleTimeString() +'</td>';
                        liveTradeTableContent += '<td class="table2-2 normal">'+platfrom+'</td>';
                        liveTradeTableContent += '<td class="table2-3 normal">'+type+'</td>';
                        liveTradeTableContent += '<td class="table2-4 normal">'+value['price']+'</td>';
                        liveTradeTableContent += '<td class="last-column normal">'+value['amount']+'</td>';
                        liveTradeTableContent += "</tr>";
                    });
                }

                $( '#live_trade_table' ).html(liveTradeTableContent);
            }
        });
    }
</script>

<script>
    //gain/loss data

    getGainLoss();
    window.setInterval(function(){
        getGainLoss();
    }, 5000);

    function getGainLoss(){
        $.ajax({
            crossOrigin: false,
            url: "apiCron/getAjaxGainLoss.php",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                if(!data){
                    console.error("live btc data is null");
                    return;
                }

                if(!data || data.length <= 0){
                    console.error("huobi gain loss is empty");
                }else{
                    $.each(data, function(key, valueRows) {
                        if(!valueRows || valueRows.length <= 0){

                        }else{
                            let value = valueRows[0];
                            let buyGl = value['latestBuy'];
                            let sellGl = value['latestSell'];
                            let percentGl = value['percent'];
                            let isGain = value['isGain'];

                            let colour = "";
                            if(isGain){
                                colour = "green";
                            }else{
                                colour = "red";
                            }

                            let glPlatformId = "null";
                            let newHtmlData =       '<div class="live-half-top">';
                            if(isGain){
                                newHtmlData +=          '<h1 class="green-buy live-big-h1">'+percentGl+'%</h1>';
                            }else{
                                newHtmlData +=          '<h1 class="red-sell live-big-h1">'+percentGl+'%</h1>';
                            }

                            switch (key){
                                case "huobi":
                                    glPlatformId = "huobi-gl";
                                    newHtmlData +=      '<img class="platform-logo" src="img/sherry/huobi.png" alt="OKEX" title="OKEX">';
                                    break;
                                case "binance":
                                    glPlatformId = "binance-gl";
                                    newHtmlData +=      '<img class="platform-logo" src="img/sherry/binance.png" alt="OKEX" title="OKEX">';
                                    break;
                                case "okex":
                                    glPlatformId = "okex-gl";
                                    newHtmlData +=      '<img class="platform-logo" src="img/sherry/okex.png" alt="OKEX" title="OKEX">';
                                    break;
                                case "bitstamp":

                                    break;
                                case "poloniex":

                                    break;
                                case "bitfinex":
                                    glPlatformId = "bitfinex-gl";
                                    newHtmlData +=      '<img class="platform-logo" src="img/sherry/bitfinex.png" alt="OKEX" title="OKEX">';
                                    break;
                                case "coinbene":
                                    glPlatformId = "coinbene-gl";
                                    newHtmlData +=      '<img class="platform-logo" src="img/sherry/coinbene.png" alt="OKEX" title="OKEX">';
                                    break;
                                case "bitforex":
                                    glPlatformId = "bitforex-gl";
                                    newHtmlData +=      '<img class="platform-logo" src="img/sherry/bitforex.png" alt="OKEX" title="OKEX">';
                                    break;
                            }

                            newHtmlData +=          '</div>';
                            newHtmlData +=          '<div class="live-half-bottom">';
                            newHtmlData +=              '<p class="live-half-p"><span class="green-buy p-span">Buy</span> USD '+buyGl+'</p>';
                            newHtmlData +=              '<p class="live-half-p"><span class="red-sell p-span">Sell</span> USD '+sellGl+'</p>';
                            newHtmlData +=          '</div>';

                            $( '#' + glPlatformId ).html(newHtmlData);
                        }

                    });
                }

            }
        });
    }
</script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.min.js'></script>
<script>
    //graph data

    $(document).ready(function(){
        var data = {
            labels: [<?php echo $dateString ?>],
            datasets: [{
                label: "BTC",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#f9bd00",
                borderColor: "#f9bd00", // The main line color
                borderCapStyle: 'circle',
                borderDash: [], // try [5, 15] for instance
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "#f9bd00",
                pointBackgroundColor: "#f9bd00",
                pointBorderWidth: 1,
                pointHoverRadius: 8,
                pointHoverBackgroundColor: "#f9bd00",
                pointHoverBorderColor: "#f9bd00",
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                // notice the gap in the data and the spanGaps: true
                data: [<?php echo $btcString ?>],
                spanGaps: true
            }, {
                label: "USD",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#00bbee",
                borderColor: "#00bbee",
                borderCapStyle: 'circle',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "#00bbee",
                pointBackgroundColor: "#00bbee",
                pointBorderWidth: 1,
                pointHoverRadius: 8,
                pointHoverBackgroundColor: "#00bbee",
                pointHoverBorderColor: "#00bbee",
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                // notice the gap in the data and the spanGaps: false
                data: [<?php echo $usdString ?>],
                spanGaps: true
            }

            ]
        };

        // this is ugly, don't judge me
        var updateData = function(oldData,newLabel,newDataBtc,newDataUsd){
            var labels = oldData["labels"];
            var dataSetA = oldData["datasets"][0]["data"];
            var dataSetB = oldData["datasets"][1]["data"];

            labels.shift();
            labels.push(newLabel);
            dataSetA.push(newDataBtc);
            dataSetB.push(newDataUsd);
            dataSetA.shift();
            dataSetB.shift();
        };

        /****************SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS******************/

        var canvas = document.getElementById("lineGraph");
        var ctx = canvas.getContext('2d');

        // Global Options:
        Chart.defaults.global.defaultFontColor = 'white';
        Chart.defaults.global.defaultFontSize = 16;

        // Notice the scaleLabel at the same level as Ticks
        var options = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Transaction',
                        fontSize: 20
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Date (Exposure)',
                        fontSize: 20
                    }
                }]
            },
            animation : false
        };

        // Chart declaration:
        var btcUsdPairLineGraph = new Chart(ctx, {
            type: 'line',
            data: data,
            options: options
        });

        // https://embed.plnkr.co/z3Qy9C/
        window.setInterval(function(){
            getAjaxBtcUsdGraphData();
        }, 1000);

        function getAjaxBtcUsdGraphData(){
            $.ajax({
                crossOrigin: false,
                url: "apiCron/getAjaxBtcUsdPairGraphData.php",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(newData) {
                    if(!newData){
                        console.error("live graph data is null");
                        return;
                    }

                    if (btcUsdPairLineGraph) {
                        btcUsdPairLineGraph.destroy();
                    }
                    updateData(data,newData['dateString'],newData['btcString'],newData['usdString']);
                    btcUsdPairLineGraph = new Chart(ctx, {
                        type: 'line',
                        data: data,
                        options: options
                    });
                }
            });
        }
    });

</script>


</body>
</html>
